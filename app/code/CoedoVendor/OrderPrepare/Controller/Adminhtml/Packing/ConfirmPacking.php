<?php

namespace CoedoVendor\OrderPrepare\Controller\Adminhtml\Packing;

class ConfirmPacking extends \BoostMyShop\OrderPreparation\Controller\Adminhtml\Packing\ConfirmPacking
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $orderId = $this->getRequest()->getParam('order_id');
        try {

            $quantities  = $this->getRequest()->getPost('products');
            $totalWeight = $this->getRequest()->getPost('total_weight');
            $parcelCount = $this->getRequest()->getPost('parcel_count');

            $parcelHeight = $this->getRequest()->getPost('parcel_height');
            $parcelWidth  = $this->getRequest()->getPost('parcel_width');
            $parcelLength = $this->getRequest()->getPost('parcel_length');

            $createInvoice  = $this->_configFactory->create()->getCreateInvoice();
            $createShipment = $this->_configFactory->create()->getCreateShipment();

            $boxDetails = $this->getRequest()->getPost('boxes');
            if ($boxDetails) {
                $boxDetails[1]  = [
                    'total_weight'  => $totalWeight,
                    'parcel_count'  => $parcelCount,
                    'parcel_length' => $parcelLength,
                    'parcel_width'  => $parcelWidth,
                    'parcel_height' => $parcelHeight,
                ];
                $boxDetailsJson = json_encode($boxDetails);
                $this->getCurrentOrderInProgress($orderId)->addParcelBoxes($boxDetailsJson);
            }

            $this->getCurrentOrderInProgress($orderId)->pack($createShipment, $createInvoice, $quantities, $totalWeight, $parcelCount, $parcelHeight, $parcelWidth, $parcelLength);

            $this->_eventManager->dispatch('bms_orderpreparation_order_after_pack', ['order_in_progress' => $this->getCurrentOrderInProgress($orderId), 'products' => $quantities, 'request' => $this->getRequest()]);

            $this->messageManager->addSuccess(__('Order packed.'));
        } catch (\Exception $ex) {
            $this->messageManager->addError($ex->getMessage());
            $this->_logger->logException($ex);
        }

        $this->_redirect('*/*/Index', ['order_id' => $this->getCurrentOrderInProgress($orderId)->getId(), 'download' => 1]);
    }

    public function getCurrentOrderInProgress($orderId)
    {
        return $this->_inProgressFactory->create()->load($orderId);
    }
}
