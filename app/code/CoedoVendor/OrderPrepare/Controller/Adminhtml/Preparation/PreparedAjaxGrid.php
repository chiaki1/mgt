<?php

namespace CoedoVendor\OrderPrepare\Controller\Adminhtml\Preparation;

use Magento\Framework\Controller\ResultFactory;

class PreparedAjaxGrid extends \BoostMyShop\OrderPreparation\Controller\Adminhtml\Preparation
{

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Layout
     */
    public function execute()
    {
        $this->_initAction();

        $resultLayout = $this->_resultLayoutFactory->create();

        $block = $resultLayout->getLayout()->getBlock('preparation.grid.prepared');
        $block->setUseAjax(true);
        return $resultLayout;
    }
}
