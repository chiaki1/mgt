<?php
declare(strict_types=1);

namespace CoedoVendor\OrderPrepare\Helper;

use BoostMyShop\OrderPreparation\Model\InProgress\ItemFactory;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    protected $_extendedSalesFlatOrderItemFactory;

    protected $_warehouseFactory;

    protected $_inProgressItemFactory;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \BoostMyShop\AdvancedStock\Model\ExtendedSalesFlatOrderItemFactory $extendedSalesFlatOrderItemFactory,
        \BoostMyShop\AdvancedStock\Model\WarehouseFactory $warehouseFactory,
        ItemFactory $inProgressItemFactory
    ) {
        $this->_extendedSalesFlatOrderItemFactory = $extendedSalesFlatOrderItemFactory;
        $this->_warehouseFactory = $warehouseFactory;
        $this->_inProgressItemFactory = $inProgressItemFactory;
        parent::__construct($context);
    }

    public function isModuleOutputEnabled($moduleName = null)
    {
        return parent::isModuleOutputEnabled($moduleName);
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment\Item $item
     *
     * @return mixed
     */
    public function getWarehouse($item) {
        $flatItem = $this->_extendedSalesFlatOrderItemFactory->create();
        $flatItem->loadByItemId($item->getOrderItemId());
        $warehouseId = $flatItem->getesfoi_warehouse_id();
        return $this->getWarehouseName($warehouseId);
    }

    public function getWarehouseName($warehouseId)
    {
        return $this->_warehouseFactory->create()->load($warehouseId)->getw_name();
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment\Item $item
     *
     * @return mixed
     */
    public function getPackedQty($item)
    {

        if ($item->getOrderItem()->getProductType() == "configurable") {
            $child_items = $item->getOrderItem()->getChildrenItems();
            $packed = 0;
            foreach ($child_items as $child_item) {
                $_item = $this->_inProgressItemFactory->create()->load($child_item->getId(), 'ipi_order_item_id');
                $packed += $_item->getpacked_qty();
            }
            return $packed;
        }
        $item = $this->_inProgressItemFactory->create()->load($item->getOrderItemId(), 'ipi_order_item_id');
        return $item->getpacked_qty() ?? 0;
    }

    public function getConfig($path)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
