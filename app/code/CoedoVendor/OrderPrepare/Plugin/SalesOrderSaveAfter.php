<?php

namespace CoedoVendor\OrderPrepare\Plugin;

use BoostMyShop\AdvancedStock\Helper\Logger;
use BoostMyShop\AdvancedStock\Model\ExtendedSalesFlatOrderItemFactory;
use BoostMyShop\AdvancedStock\Model\Router;
use BoostMyShop\AdvancedStock\Model\Warehouse\Item\ReservationFixer;
use BoostMyShop\AdvancedStock\Model\Warehouse\ItemFactory;
use Magento\Framework\Event\Observer as EventObserver;

class SalesOrderSaveAfter
{
    private $_logger;
    private $_router;
    private $_reservationFixer;
    private $_extendedOrderItemFactory;
    private $_warehouseItemFactory;

    /**
     * @param Logger                            $logger
     * @param Router                            $router
     * @param ExtendedSalesFlatOrderItemFactory $extendedOrderItemFactory
     * @param ItemFactory                       $warehouseItemFactory
     * @param ReservationFixer                  $reservationFixer
     */
    public function __construct(
        Logger                            $logger,
        Router                            $router,
        ExtendedSalesFlatOrderItemFactory $extendedOrderItemFactory,
        ItemFactory                       $warehouseItemFactory,
        ReservationFixer                  $reservationFixer

    ) {
        $this->_logger                   = $logger;
        $this->_router                   = $router;
        $this->_reservationFixer         = $reservationFixer;
        $this->_warehouseItemFactory     = $warehouseItemFactory;
        $this->_extendedOrderItemFactory = $extendedOrderItemFactory;
    }

    /**
     * @param SalesOrderSaveAfter $subject
     * @param callable            $proceed
     * @param EventObserver       $observer
     *
     * @return void
     */
    public function aroundExecute(
        \BoostMyShop\AdvancedStock\Observer\SalesOrderSaveAfter $subject, callable $proceed, EventObserver $observer
    ): void {
        $order = $observer->getEvent()->getOrder();

        if ($order->getStatus() != $order->getOrigData('status') && !in_array($order->getStatus(), ['waiting_to_pickup', 'delivery'])) {
            $this->_logger->log("Order #" . $order->getIncrementId() . " status changes from " . $order->getOrigData('status') . " to " . $order->getStatus(), Logger::kLogReservation);

            $reservationAllowedBefore = $this->_reservationFixer->reservationIsAllowed($order->getOrigData('status'));
            $reservationAllowedAfter  = $this->_reservationFixer->reservationIsAllowed($order->getStatus());

            if ($reservationAllowedAfter != $reservationAllowedBefore) {
                $this->_logger->log("Order #" . $order->getIncrementId() . " reservation status changes, update reservation for items ", Logger::kLogReservation);
                foreach ($order->getAllItems() as $orderItem) {
                    $this->processOrderItem($orderItem, $reservationAllowedAfter);
                }
            }
        }
    }

    protected function processOrderItem($orderItem, $reservationAllowed)
    {
        $extendedOrderItem = $this->_extendedOrderItemFactory->create()->loadByItemId($orderItem->getId());
        $warehouseId       = $extendedOrderItem->getesfoi_warehouse_id();
        if (!$warehouseId)
            return;

        if (!$reservationAllowed) {
            $qtyToRelease = $extendedOrderItem->getesfoi_qty_reserved();
            if ($qtyToRelease > 0)
                $this->_reservationFixer->releaseQuantity($orderItem->getId(), $qtyToRelease);
        } else {
            $warehouseItem      = $this->_warehouseItemFactory->create()->loadByProductWarehouse($orderItem->getProductId(), $warehouseId);
            $reservableQuantity = $warehouseItem->getReservableQuantity();
            $reservableQuantity = min($reservableQuantity, $extendedOrderItem->getQuantityToShip());

            $extendedOrderItem->setesfoi_qty_reserved($reservableQuantity)->save();
        }

        $this->updateWarehouseItem($warehouseId, $orderItem->getProductId());
    }

    protected function updateWarehouseItem($warehouseId, $productId)
    {
        $this->_router->updateQuantityToShip($productId, $warehouseId);
        $this->_router->updateReservedQuantity($productId, $warehouseId);
    }
}
