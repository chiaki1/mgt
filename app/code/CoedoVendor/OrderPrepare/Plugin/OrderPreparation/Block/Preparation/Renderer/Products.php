<?php

namespace CoedoVendor\OrderPrepare\Plugin\OrderPreparation\Block\Preparation\Renderer;

class Products extends \BoostMyShop\AdvancedStock\Plugin\OrderPreparation\Block\Preparation\Renderer\Products
{
    protected $adminSession;
    public function __construct(
        \BoostMyShop\AdvancedStock\Model\ResourceModel\ExtendedSalesFlatOrderItem\CollectionFactory $extendedOrderItemCollectionFactory,
        \Magento\Backend\Model\Auth\SessionFactory $adminSession
    ) {
        $this->adminSession = $adminSession;
        parent::__construct($extendedOrderItemCollectionFactory);
    }

    public function aroundRenderItem(\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\Products $subject, $proceed, $order, $item, $warehouseId)
    {
        $class = '';
        $suffix = '';
        $qty = $item->getesfoi_qty_to_ship();

        if ($warehouseId == $item->getesfoi_warehouse_id())
        {
            if ($item->getesfoi_qty_to_ship() == 0)
                $class = 'shipped';
            else
            {
                if ($item->getesfoi_qty_reserved() == 0)
                    $class = 'backorder';
                else
                {
                    if ($item->getesfoi_qty_reserved() < $item->getesfoi_qty_to_ship()) {
                        $class = 'partial';
                        $suffix = '('.$item->getesfoi_qty_reserved().'/'.$item->getesfoi_qty_to_ship().')';
                    }
                    else
                        $class = 'full';
                }
            }
        }
        else
        {
            if ($this->adminSession->create()->getUser() && $this->adminSession->create()->getUser()->getWarehouseId() != 0) {
                return '';
            }
            $class = 'external';
        }

        return '<div class="preparation-item-'.$class.'">'.$qty.'x '.$item->getSku().' - '.$item->getName().' '.$suffix.'</div>';
    }
}
