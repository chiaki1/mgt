<?php

namespace CoedoVendor\OrderPrepare\Plugin;

use BoostMyShop\OrderPreparation\Block\Preparation\Renderer\InProgressActions;
use BoostMyShop\OrderPreparation\Model\Registry;
use Magento\Framework\DataObject;

class InProgressActionPlugin
{

    protected $preparationRegistry;

    public function __construct(
        Registry $preparationRegistry
    ) {
        $this->preparationRegistry = $preparationRegistry;
    }

    /**
     * @param InProgressActions $subject
     * @param callable          $proceed
     * @param DataObject        $orderInProgress
     */
    public function aroundRender(
        InProgressActions $subject, callable $proceed,
        DataObject        $orderInProgress
    ) {
        $html = [];

        $actions = [];
        if (!$this->preparationRegistry->getCurrentWarehouseId()) {
            $actions[] = ['label' => __('View'), 'url' => $subject->getUrl('sales/order/view', ['order_id' => $orderInProgress->getip_order_id()]), 'target' => ''];
        }
        $actions[] = ['label' => __('Remove'), 'url' => $subject->getUrl('*/*/remove', ['in_progress_id' => $orderInProgress->getId()]), 'target' => ''];
        $actions[] = ['label' => __('Pack'), 'url' => $subject->getUrl('*/packing/index', ['order_id' => $orderInProgress->getId()]), 'target' => ''];

        foreach ($actions as $action) {
            $html[] = '<a href="' . $action['url'] . '" target="' . $action['target'] . '">' . $action['label'] . '</a>';
        }

        return implode('<br>', $html);
    }
}
