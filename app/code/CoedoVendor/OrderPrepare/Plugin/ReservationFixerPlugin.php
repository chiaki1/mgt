<?php

namespace CoedoVendor\OrderPrepare\Plugin;

use BoostMyShop\AdvancedStock\Model\ExtendedSalesFlatOrderItemFactory;
use BoostMyShop\OrderPreparation\Model\Registry;

class ReservationFixerPlugin
{

    protected $_preparationRegistry;
    protected $_extendedOrderItemFactory;

    /**
     * @param Registry                          $preparationRegistry
     * @param ExtendedSalesFlatOrderItemFactory $extendedOrderItemFactory
     */
    public function __construct(
        Registry                          $preparationRegistry,
        ExtendedSalesFlatOrderItemFactory $extendedOrderItemFactory
    ) {
        $this->_preparationRegistry      = $preparationRegistry;
        $this->_extendedOrderItemFactory = $extendedOrderItemFactory;
    }

    /**
     * @param \BoostMyShop\AdvancedStock\Model\Warehouse\Item\ReservationFixer $subject
     * @param callable                                                         $proceed
     * @param                                                                  $orderItemId
     * @param                                                                  $qtyToRelease
     *
     * @return void
     */
    public function aroundReleaseQuantity(
        \BoostMyShop\AdvancedStock\Model\Warehouse\Item\ReservationFixer $subject, callable $proceed, $orderItemId,
                                                                         $qtyToRelease
    ) {
        if ($this->_preparationRegistry->getCurrentWarehouseId()) {
            $extendedOrderItem = $this->_extendedOrderItemFactory->create()->loadByItemId($orderItemId);
            $warehouseId       = $extendedOrderItem->getesfoi_warehouse_id();
            if ($warehouseId == $this->_preparationRegistry->getCurrentWarehouseId()) {
                return $proceed($orderItemId, $qtyToRelease);
            }
        } else {
            return $proceed($orderItemId, $qtyToRelease);
        }
    }
}
