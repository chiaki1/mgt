<?php

namespace CoedoVendor\OrderPrepare\Plugin;

use BoostMyShop\OrderPreparation\Block\Preparation\Renderer\Actions;
use BoostMyShop\OrderPreparation\Model\Registry;
use Magento\Framework\DataObject;

class ActionPlugin
{

    protected $preparationRegistry;

    public function __construct(
        Registry $preparationRegistry
    ) {
        $this->preparationRegistry = $preparationRegistry;
    }

    /**
     * @param Actions    $subject
     * @param callable   $proceed
     * @param DataObject $order
     */
    public function aroundRender(
        Actions $subject, callable $proceed, DataObject $order
    ) {
        $html = [];

        $actions = [];
        if (!$this->preparationRegistry->getCurrentWarehouseId()) {
            $actions[] = ['label' => __('View'), 'url' => $subject->getUrl('sales/order/view', ['order_id' => $order->getId()]), 'target' => ''];
        }
        $actions[] = ['label' => __('Prepare'), 'url' => $subject->getUrl('*/*/addOrder', ['order_id' => $order->getId()]), 'target' => ''];

        foreach($actions as $action)
        {
            $html[] = '<a href="'.$action['url'].'" target="'.$action['target'].'">'.$action['label'].'</a>';
        }

        return implode('<br>', $html);
    }
}
