<?php

namespace CoedoVendor\OrderPrepare\Plugin;

use BoostMyShop\OrderPreparation\Model\Config\Source\CompleteState;

class CompleteStatePlugin
{

    /**
     * @param CompleteState $subject
     * @param               $result
     *
     * @return mixed
     */
    public function afterToOptionArray(CompleteState $subject, $result
    ) {
        $result[] = [
            'value' => 'delivery',
            'label' => __('Delivery')
        ];
        return $result;
    }
}
