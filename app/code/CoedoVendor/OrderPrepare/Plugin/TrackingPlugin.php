<?php

namespace CoedoVendor\OrderPrepare\Plugin;

use BoostMyShop\OrderPreparation\Block\Packing\Tracking;
use Magento\Framework\App\Config\ScopeConfigInterface;

class TrackingPlugin
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param Tracking $subject
     * @param          $result
     */
    public function afterCanDisplay(Tracking $subject, $result)
    {
        return $result && $this->showTracking();
    }

    /**
     * @return bool
     */
    private function showTracking()
    {
        return (bool)$this->scopeConfig->getValue('orderpreparation/packing/show_tracking', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
