<?php

namespace CoedoVendor\OrderPrepare\Block\Packing;

class Order extends \BoostMyShop\OrderPreparation\Block\Packing\Order
{
    protected $_template = 'CoedoVendor_OrderPrepare::OrderPreparation/Packing/Order.phtml';

    public function isAllWarehouse()
    {
        return $this->_preparationRegistry->isAllWarehouse();
    }

    public function getOrderViewUrl()
    {
        if ($this->isAllWarehouse()) {
            return $this->getUrl('sales/order/view', ['order_id' => $this->currentOrderInProgress()->getip_order_id()]);
        }
        return "#";
    }
}
