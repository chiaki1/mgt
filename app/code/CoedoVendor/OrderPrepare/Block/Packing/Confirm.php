<?php

namespace CoedoVendor\OrderPrepare\Block\Packing;

class Confirm extends \BoostMyShop\OrderPreparation\Block\Packing\Confirm
{
    protected $_template = 'CoedoVendor_OrderPrepare::OrderPreparation/Packing/Confirm.phtml';
}
