<?php

namespace CoedoVendor\OrderPrepare\Block\Sales\Order\View\Tab;

use BoostMyShop\OrderPreparation\Model\ResourceModel\InProgress\CollectionFactory;
use BoostMyShop\OrderPreparation\Model\ResourceModel\InProgress\Item\CollectionFactory as ItemCollectionFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Registry;
use Magento\Sales\Block\Adminhtml\Order\AbstractOrder;
use Magento\Sales\Helper\Admin;
use Magento\Sales\Model\Order;
use BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory as WarehouseCollectionFactory;

class Prepare extends AbstractOrder implements TabInterface
{
    protected $_template = 'sales/order/edit/tab/prepare.phtml';

    protected $_inProgressFactory;

    protected $_itemCollectionFactory;

    protected $_warehouseCollectionFactory;

    public function __construct(
        Context           $context,
        Registry          $registry,
        Admin             $adminHelper,
        CollectionFactory $inProgressFactory,
        ItemCollectionFactory $itemCollectionFactory,
        WarehouseCollectionFactory $warehouseCollectionFactory,
        array             $data = []
    ) {
        $this->_inProgressFactory = $inProgressFactory;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_warehouseCollectionFactory = $warehouseCollectionFactory;
        parent::__construct($context, $registry, $adminHelper, $data);
    }

    /**
     * Retrieve order model instance
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    public function getItems()
    {
        $collection = $this->_itemCollectionFactory->create();
        $collection->joinOrderItem();
        $collection->addOrderFilter($this->getOrder()->getId());
        return $collection;
    }

    public function getStatusWarehouse($parent_id)
    {
        $collection = $this->_inProgressFactory->create();
        foreach ($collection as $item) {
            if ($item->getip_id() == $parent_id) {
                return [$item->getip_status(), $this->getWarehouse($item->getip_warehouse_id())];
            }
        }
        return ['',''];
    }

    public function getWarehouse($warehouseId)
    {
        $collection = $this->_warehouseCollectionFactory->create();
        foreach ($collection as $item) {
            if ($item->getId() == $warehouseId) {
                return $item->getw_name();
            }
        }
        return '';
    }

    /**
     * ######################## TAB settings #################################
     */

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Prepare Order');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Prepare Order');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}
