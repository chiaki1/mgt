<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation;

class Tabs extends \BoostMyShop\OrderPreparation\Block\Preparation\Tabs
{
    /**
     * @return \BoostMyShop\OrderPreparation\Block\Preparation\Tabs
     */
    protected function _beforeToHtml()
    {

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\Tab\InStock');
        $html = $block->toHtml();
        $this->addTab(
            'tab_instock',
            [
                'label' => __('In Stock')." (".$block->getCollection()->getSize().")",
                'title' => __('In Stock')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\Tab\BackOrder');
        $html = $block->toHtml();
        $this->addTab(
            'tab_backorder',
            [
                'label' => __('Backorder')." (".$block->getCollection()->getSize().")",
                'title' => __('Backorder')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\Tab\Holded');
        $html = $block->toHtml();
        $this->addTab(
            'tab_holded',
            [
                'label' => __('On Hold')." (".$block->getCollection()->getSize().")",
                'title' => __('On Hold')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\InProgress');
        $html = $block->toHtml();
        $this->addTab(
            'tab_progress',
            [
                'label' => __('In progress')." (".$block->getCollection()->getSize().")",
                'title' => __('In progress')." (".$block->getCollection()->getSize().")",
                'content' => $html,
                'active' => true
            ]
        );

        $block =  $this->getLayout()->createBlock('CoedoVendor\OrderPrepare\Block\Preparation\Tab\Prepared');
        $html = $block->toHtml();
        $this->addTab(
            'tab_prepared',
            [
                'label' => __('Prepared')." (".$block->getCollection()->getSize().")",
                'title' => __('Prepared')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        return \Magento\Backend\Block\Widget\Tabs::_beforeToHtml();
    }
}
