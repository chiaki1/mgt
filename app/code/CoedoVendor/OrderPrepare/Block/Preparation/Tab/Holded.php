<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation\Tab;

class Holded extends \BoostMyShop\OrderPreparation\Block\Preparation\Tab\Holded
{
    protected function _prepareColumns()
    {
        $this->_eventManager->dispatch('bms_order_preparation_holded_grid', ['grid' => $this]);

        $this->addColumn('increment_id', ['header' => __('#'), 'index' => 'increment_id', 'filter_index' => 'main_table.increment_id']);
        $this->addColumn('created_at', ['header' => __('Date'), 'index' => 'created_at', 'filter_index' => 'main_table.created_at', 'type' => 'date', 'renderer' => '\Magento\Backend\Block\Widget\Grid\Column\Renderer\Datetime', 'format' => \IntlDateFormatter::FULL]);
        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status',  'filter_index' => 'main_table.status']);
        $this->addColumn('store_id', ['header' => __('Store'), 'index' => 'store_id', 'filter_index' => 'main_table.store_id', 'renderer' => '\Magento\Backend\Block\Widget\Grid\Column\Renderer\Store']);
        $this->addColumn('shipping_name', ['header' => __('Customer'), 'index' => 'shipping_name', 'filter_index' => 'main_table.shipping_name', 'renderer' => '\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\CustomerName']);
        $this->addColumn('shipping_information', ['header' => __('Shipping method'), 'index' => 'shipping_information', 'filter_index' => 'main_table.shipping_information']);
        if (!$this->_preparationRegistry->getCurrentWarehouseId()) {
            $this->addColumn('weight', ['header' => __('Weight'), 'index' => 'weight', 'type' => 'number']);
        } else {
            $this->addColumn('weight', ['header' => __('Weight'), 'index' => 'weight', 'type' => 'number', 'renderer' => '\CoedoVendor\OrderPrepare\Block\Preparation\Renderer\Weight']);
        }
        $this->addColumn('order_from', ['header' => __('Order From'), 'index' => 'order_from', 'sortable' => false, 'filter_index' => 'main_table.order_from', 'renderer' => '\CoedoVendor\OrderPrepare\Block\Preparation\Renderer\OrderFrom']);
        $this->addColumn('products', ['header' => __('Products'), 'index' => 'entity_id', 'sortable' => false, 'filter_index' => 'main_table.entity_id', 'renderer' => '\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\Products', 'filter' => '\BoostMyShop\OrderPreparation\Block\Preparation\Filter\Products']);
        $this->addColumn('action', ['header' => __('Action'), 'index' => 'index_id', 'filter_index' => 'main_table.entity_id', 'align' => 'center', 'filter' => false, 'renderer' => '\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\Actions']);

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareColumns();
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {

        $collection = $this->_ordersFactory->create();
        $collection->addAdditionalFieldsCustom();

        $collection->addFieldToFilter('main_table.status', ['in' => $this->getAllowedOrderStatuses()]);

        //exclude orders being prepared
        $selectedOrderIds = $this->_inProgressCollectionFactory->create()->getOrderIds($this->_preparationRegistry->getCurrentWarehouseId());
        if (count($selectedOrderIds) > 0)
            $collection->addFieldToFilter('main_table.entity_id', array('nin' => $selectedOrderIds));

        //add filter on warehouse
        $warehouseId = $this->_preparationRegistry->getCurrentWarehouseId();
        $this->addWarehouseFilter($collection, $warehouseId);

        $this->addAdditionnalFilters($collection);

        $this->setCollection($collection);
        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
    }
}
