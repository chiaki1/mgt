<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation\Tab;

class Prepared extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_coreRegistry = null;
    protected $_inProgressFactory = null;
    protected $_preparationRegistry;
    protected $_config;
    protected $_carrierHelper;
    protected $_carrierTemplateCollectionFactory;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Authorization\Model\ResourceModel\Role\CollectionFactory $userRolesFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \BoostMyShop\OrderPreparation\Model\ResourceModel\InProgress\CollectionFactory $inProgressFactory,
        \BoostMyShop\OrderPreparation\Model\Config $config,
        \Magento\Framework\Registry $coreRegistry,
        \BoostMyShop\OrderPreparation\Model\Registry $preparationRegistry,
        \BoostMyShop\OrderPreparation\Helper\Carrier $carrierHelper,
        \BoostMyShop\OrderPreparation\Model\ResourceModel\CarrierTemplate\CollectionFactory $carrierTemplateCollectionFactory,
        array $data = []
    ) {
        $this->_jsonEncoder = $jsonEncoder;
        $this->_inProgressFactory = $inProgressFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_preparationRegistry = $preparationRegistry;
        $this->_config = $config;
        $this->_carrierHelper = $carrierHelper;
        $this->_carrierTemplateCollectionFactory = $carrierTemplateCollectionFactory;

        parent::__construct($context, $backendHelper, $data);

        $this->setMessageBlockVisibility(false);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('tab_prepared');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_inProgressFactory->create();
        $collection->addOrderDetails();
        $collection->addOrderFrom();

        $userId = $this->_preparationRegistry->getCurrentOperatorId();
        $warehouseId = $this->_preparationRegistry->getCurrentWarehouseId();

        $collection->addUserFilter($userId);
        $collection->addWarehouseFilter($warehouseId);
        $collection->addFieldToFilter('ip_status', ['in' => ['packed', 'partial_packed']]);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn('increment_id', ['header' => __('#'), 'index' => 'increment_id']);
        $this->addColumn('created_at', ['header' => __('Date'), 'index' => 'created_at', 'renderer' => '\Magento\Backend\Block\Widget\Grid\Column\Renderer\Datetime', 'format' => \IntlDateFormatter::FULL]);
        $this->addColumn('status', ['header' => __('Order Status'), 'index' => 'status']);
        $this->addColumn('store_id', ['header' => __('Store'), 'index' => 'store_id', 'renderer' => '\Magento\Backend\Block\Widget\Grid\Column\Renderer\Store']);
        $this->addColumn('shipping_name', ['header' => __('Customer'), 'index' => 'shipping_name']);
        $this->addColumn('shipping_information', ['header' => __('Shipping method'), 'index' => 'shipping_information']);
        $this->addColumn('order_from', ['header' => __('Order From'), 'index' => 'order_from', 'sortable' => false, 'renderer' => '\CoedoVendor\OrderPrepare\Block\Preparation\Renderer\OrderFrom']);
        $this->addColumn('products', ['header' => __('Products'), 'index' => 'ip_order_id', 'renderer' => '\CoedoVendor\OrderPrepare\Block\Preparation\Renderer\PreparedProducts', 'filter' => '\BoostMyShop\OrderPreparation\Block\Preparation\Filter\InProgressProducts']);
        $this->addColumn('total_weight', ['header' => __('Weight'), 'index' => 'ip_total_weight', 'type' => 'number']);

        if ($this->_config->getVolumeAttribute())
            $this->addColumn('total_volume', ['header' => __('Volume'), 'index' => 'ip_total_volume', 'type' => 'number']);

        $this->addColumn('ip_status', ['header' => __('Progress'), 'index' => 'ip_status', 'type' => 'options', 'options' => $this->getStatusOptions()]);

        $this->_eventManager->dispatch('bms_order_preparation_prepared_grid', ['grid' => $this]);

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/preparedAjaxGrid', ['_current' => true, 'grid' => 'selected']);
    }

    protected function getStatusOptions()
    {
        $options = [];

        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_NEW] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_NEW);
        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PICKED] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PICKED);
        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PACKED] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PACKED);
        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_SHIPPED] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_SHIPPED);
        $options['partial_packed'] = __('partial_packed');

        return $options;
    }

    protected function getShippingMethods()
    {
        $methods = [];

        foreach($this->_carrierHelper->getCarriers() as $carrier)
        {
            foreach($this->_carrierHelper->getMethods($carrier) as $methodCode => $method)
            {
                $methods[$methodCode] = $carrier->getName().' - '.$method;
            }
        }

        asort($methods);

        return $methods;
    }

    protected function getCarrierTemplates()
    {
        $templates = [];

        foreach($this->_carrierTemplateCollectionFactory->create() as $carrier)
        {
            $templates[$carrier->getId()] = $carrier->getct_name();
        }

        asort($templates);

        return $templates;
    }

}
