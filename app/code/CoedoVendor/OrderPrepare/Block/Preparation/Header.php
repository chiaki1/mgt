<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation;

class Header extends \BoostMyShop\OrderPreparation\Block\Preparation\Header
{
    protected $_template = 'CoedoVendor_OrderPrepare::OrderPreparation/Preparation/Header.phtml';

    public function getCurrentOperatorLabel()
    {
        $currentOperatorId = $this->getCurrentOperatorId();
        $label = '';
        foreach ($this->getOperators() as $operator) {
            if ($operator->getId() == $currentOperatorId) {
                $label = $operator->getUsername();
            }
        }
        return $label;
    }

    public function getCurrentWarehouseLabel()
    {
        $currentWarehouseId = $this->getCurrentWarehouseId();
        $label = '';
        foreach ($this->getWarehouses() as $id => $name) {
            if ($id == $currentWarehouseId) {
                $label = $name;
            }
        }
        return $label;
    }

    public function isAllWarehouse()
    {
        return $this->_preparationRegistry->isAllWarehouse();
    }

    public function getSteps()
    {
        $steps = [];

        $steps[] = ['id' => 'order_selection', 'label' => 'Orders Selection', 'action' => "setLocation('".$this->getUrl('*/preparation/index')."')"];

        if ($this->_config->getSetting('steps/picking'))
            $steps[] = ['id' => 'picking', 'label' => 'Picking', 'action' => "setLocation('".$this->getUrl('*/preparation/pickingList')."')"];
        if ($this->_config->getSetting('steps/packing'))
            $steps[] = ['id' => 'packing', 'label' => 'Packing', 'action' => "setLocation('".$this->getUrl('*/packing/index')."')"];
        if ($this->_config->getSetting('steps/create'))
            $steps[] = ['id' => 'mass_create', 'label' => 'Mass create shipments & invoices', 'action' => "document.getElementById('btn_step_mass_create').disabled = true; setLocation('".$this->getUrl('*/preparation/massCreate')."')"];
        if ($this->_config->getSetting('steps/download'))
            $steps[] = ['id' => 'download_pdf', 'label' => 'Download PDFs', 'action' => "setLocation('".$this->getUrl('*/preparation/downloadDocuments')."')"];
        if ($this->_config->getSetting('steps/shipping'))
            $steps[] = ['id' => 'shipping', 'label' => 'Shipping', 'action' => "setLocation('".$this->getUrl('*/shipping/index')."')"];
//        $steps[] = ['id' => 'flush', 'label' => 'Flush shipped orders', 'action' => "document.getElementById('btn_step_flush').disabled = true; setLocation('".$this->getUrl('*/preparation/flush')."')"];

        $obj = new \Magento\Framework\DataObject();
        $obj->setSteps($steps);
        $this->_eventManager->dispatch('bms_order_preparation_preparation_steps', ['block' => $this, 'obj' => $obj]);
        $steps = $obj->getSteps();

        return $steps;
    }
}
