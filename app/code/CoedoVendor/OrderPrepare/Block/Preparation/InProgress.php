<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation;

class InProgress extends \BoostMyShop\OrderPreparation\Block\Preparation\InProgress
{
    protected function getStatusOptions()
    {
        $options = [];

        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_NEW] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_NEW);
        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PICKED] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PICKED);
        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PACKED] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_PACKED);
        $options[\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_SHIPPED] = __(\BoostMyShop\OrderPreparation\Model\InProgress::STATUS_SHIPPED);
        $options['partial_packed'] = __('partial_packed');

        return $options;
    }

    /**
     * @return InProgress|\Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->_inProgressFactory->create();
        $collection->addOrderDetails();
        $collection->addOrderFrom();

        $userId = $this->_preparationRegistry->getCurrentOperatorId();
        $warehouseId = $this->_preparationRegistry->getCurrentWarehouseId();

        $collection->addUserFilter($userId);
        $collection->addWarehouseFilter($warehouseId);
        $collection->addFieldToFilter('ip_status', ['nin' => ['packed']]);
        //$collection->addStoreFilter($storeId);

        $this->setCollection($collection);
        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
    }

    /**
     * @return InProgress|\Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {

        $this->addColumn('increment_id', ['header' => __('#'), 'index' => 'increment_id']);
        $this->addColumn('created_at', ['header' => __('Date'), 'index' => 'created_at', 'renderer' => '\Magento\Backend\Block\Widget\Grid\Column\Renderer\Datetime', 'format' => \IntlDateFormatter::FULL]);
        $this->addColumn('status', ['header' => __('Order Status'), 'index' => 'status']);
        $this->addColumn('store_id', ['header' => __('Store'), 'index' => 'store_id', 'renderer' => '\Magento\Backend\Block\Widget\Grid\Column\Renderer\Store']);
        $this->addColumn('shipping_name', ['header' => __('Customer'), 'index' => 'shipping_name']);
        $this->addColumn('shipping_information', ['header' => __('Shipping method'), 'index' => 'shipping_information']);
        $this->addColumn('order_from', ['header' => __('Order From'), 'index' => 'order_from', 'sortable' => false, 'renderer' => '\CoedoVendor\OrderPrepare\Block\Preparation\Renderer\OrderFrom']);
        $this->addColumn('products', ['header' => __('Products'), 'index' => 'ip_order_id', 'renderer' => '\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\InProgressProducts', 'filter' => '\BoostMyShop\OrderPreparation\Block\Preparation\Filter\InProgressProducts']);
        $this->addColumn('total_weight', ['header' => __('Weight'), 'index' => 'ip_total_weight', 'type' => 'number']);

        if ($this->_config->getVolumeAttribute())
            $this->addColumn('total_volume', ['header' => __('Volume'), 'index' => 'ip_total_volume', 'type' => 'number']);

        $this->addColumn('ip_status', ['header' => __('Progress'), 'index' => 'ip_status', 'type' => 'options', 'options' => $this->getStatusOptions()]);
        $this->addColumn('action', ['header' => __('Action'), 'index' => 'index_id', 'align' => 'center', 'filter' => false, 'renderer' => '\BoostMyShop\OrderPreparation\Block\Preparation\Renderer\InProgressActions']);

        $this->_eventManager->dispatch('bms_order_preparation_inprogress_grid', ['grid' => $this]);

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareColumns();
    }
}
