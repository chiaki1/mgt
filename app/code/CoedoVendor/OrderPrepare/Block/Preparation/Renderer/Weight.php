<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation\Renderer;

use BoostMyShop\AdvancedStock\Model\ResourceModel\ExtendedSalesFlatOrderItem\CollectionFactory;
use BoostMyShop\OrderPreparation\Model\Registry;
use Magento\Backend\Block\Context;
use Magento\Framework\DataObject;

class Weight extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_extendedOrderItemCollectionFactory;
    protected $_preparationRegistry;

    /**
     * @param Context           $context
     * @param CollectionFactory $extendedOrderItemCollectionFactory
     * @param Registry          $preparationRegistry
     * @param array             $data
     */
    public function __construct(
        Context           $context,
        CollectionFactory $extendedOrderItemCollectionFactory,
        Registry          $preparationRegistry,
        array             $data = []
    ) {
        parent::__construct($context, $data);

        $this->_extendedOrderItemCollectionFactory = $extendedOrderItemCollectionFactory;
        $this->_preparationRegistry                = $preparationRegistry;
    }

    public function render(DataObject $order)
    {
        $warehouseId = $this->_preparationRegistry->getCurrentWarehouseId();

        $collection = $this->getCollection($order);
        $weight     = 0;
        foreach ($collection as $item) {
            if ($warehouseId == $item->getesfoi_warehouse_id()) {
                $weight += $item->getWeight();
            }
        }

        return $weight ?: '0';
    }

    public function getCollection($order)
    {
        return $this->_extendedOrderItemCollectionFactory->create()->joinOrderItem()->addOrderFilter($order->getId())->addProductTypeFilter();
    }
}
