<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation\Renderer;

use Magento\Backend\Block\Context;
use Magento\Framework\DataObject;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory;

class PreparedProducts extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_orderItemCollectionFactory;

    /**
     * @param Context           $context
     * @param CollectionFactory $orderItemCollectionFactory
     * @param array             $data
     */
    public function __construct(
        Context           $context,
        CollectionFactory $orderItemCollectionFactory,
        array             $data = []
    ) {
        parent::__construct($context, $data);

        $this->_orderItemCollectionFactory = $orderItemCollectionFactory;
    }

    public function render(DataObject $inProgress)
    {
        $html = [];

        foreach ($inProgress->getAllItems() as $item) {
            $html[] .= $this->renderItem($item);
        }

        return implode('<br>', $html);
    }

    protected function renderItem($item)
    {
        return $item->getPackedQty() . 'x ' . $item->getSku() . ' - ' . $item->getName();
    }
}
