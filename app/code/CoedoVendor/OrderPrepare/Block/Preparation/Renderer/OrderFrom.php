<?php

namespace CoedoVendor\OrderPrepare\Block\Preparation\Renderer;

use Magento\Framework\DataObject;

class OrderFrom extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{

    public function render(DataObject $order)
    {
        $orderFrom = $this->_getValue($order);
        if ($orderFrom == \CoedoVendor\OrderPrepare\Observer\AddOrderFrom::FROM_BE) {
            return __("BE");
        } else {
            return __("Web/App");
        }
    }
}
