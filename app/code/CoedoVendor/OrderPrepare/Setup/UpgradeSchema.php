<?php

namespace CoedoVendor\OrderPrepare\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    private $moduleManager;

    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_OrderPreparation')) {

            if (version_compare($context->getVersion(), '1.0.0', '<')) {
                if (!$setup->getConnection()->tableColumnExists($setup->getTable('bms_orderpreparation_inprogress_item'),'packed_qty')) {
                    $setup->getConnection()->addColumn(
                        $setup->getTable('bms_orderpreparation_inprogress_item'),
                        'packed_qty',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'nullable' => false,
                            'default' => 0,
                            'comment' => 'Packed Qty'
                        ]
                    );
                }
            }
        }

        $setup->endSetup();
    }
}
