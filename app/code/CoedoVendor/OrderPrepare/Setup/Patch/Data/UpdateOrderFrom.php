<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace CoedoVendor\OrderPrepare\Setup\Patch\Data;

use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class UpdateOrderFrom implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var State
     */
    private $state;

    private $moduleManager;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CollectionFactory        $collectionFactory
     * @param State                    $state
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CollectionFactory        $collectionFactory,
        State                    $state,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleDataSetup   = $moduleDataSetup;
        $this->collectionFactory = $collectionFactory;
        $this->state             = $state;
        $this->moduleManager     = $moduleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_OrderPreparation')) {
            try {
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
            } catch (LocalizedException $e) {
            }
            $collection = $this->collectionFactory->create();
            foreach ($collection as $order) {
                if (!$order->getRemoteIp()) {
                    $order->setOrderFrom(\CoedoVendor\OrderPrepare\Observer\AddOrderFrom::FROM_BE)->save();
                } else {
                    $order->setOrderFrom(\CoedoVendor\OrderPrepare\Observer\AddOrderFrom::FROM_WEB_APP)->save();
                }
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }
}

