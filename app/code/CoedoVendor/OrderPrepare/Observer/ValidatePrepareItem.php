<?php

namespace CoedoVendor\OrderPrepare\Observer;

use BoostMyShop\OrderPreparation\Model\InProgress\ItemFactory;
use CoedoVendor\OrderPrepare\Helper\Data;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Module\Manager;

class ValidatePrepareItem implements ObserverInterface
{

    /**
     * @var ItemFactory
     */
    protected $inProgressItemFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * @param ItemFactory $inProgressItemFactory
     * @param Data        $helper
     * @param Manager     $moduleManager
     */
    public function __construct(
        ItemFactory $inProgressItemFactory,
        Data        $helper,
        Manager     $moduleManager
    ) {
        $this->inProgressItemFactory = $inProgressItemFactory;
        $this->helper                = $helper;
        $this->moduleManager         = $moduleManager;
    }

    /**
     * @param EventObserver $observer
     *
     * @throws LocalizedException
     */
    public function execute(EventObserver $observer)
    {
        if ($this->helper->getConfig('orderpreparation/packing/validate_prepared_qty') && $this->moduleManager->isOutputEnabled('BoostMyShop_OrderPreparation')) {
            /** @var \Magento\Sales\Model\Order\Shipment $shipment */
            $shipment = $observer->getEvent()->getShipment();
            $items    = $shipment->getItems();
            /** @var  \Magento\Sales\Model\Order\Shipment\Item $item */
            foreach ($items as $item) {
                if ($item->getOrderItem()->getProductType() == "configurable") {
                    $child_items = $item->getOrderItem()->getChildrenItems();
                    $orderItemId = [];
                    foreach ($child_items as $child_item) {
                        $orderItemId[] = $child_item->getId();
                    }
                    $qtyToShip   = $item->getQty();
                    $this->validateQty($item->getSku(), $orderItemId, $qtyToShip);
                } else {
                    $orderItemId = $item->getOrderItemId();
                    $qtyToShip   = $item->getQty();
                    $this->validateQty($item->getSku(), $orderItemId, $qtyToShip);
                }
            }
        }
    }

    /**
     * @param $sku
     * @param string|array $orderItemId
     * @param $qtyToShip
     *
     * @throws LocalizedException
     */
    public function validateQty($sku, $orderItemId, $qtyToShip)
    {
        if (is_array($orderItemId)) {
            foreach ($orderItemId as $id) {
                $item = $this->inProgressItemFactory->create()->load($id, 'ipi_order_item_id');
                if ($item->getpacked_qty() < $qtyToShip) {
                    throw new LocalizedException(__("%1: Item is not prepared. The number of items prepared is %2", $sku, $item->getpacked_qty()));
                }
            }
        } else {
            $item = $this->inProgressItemFactory->create()->load($orderItemId, 'ipi_order_item_id');
            if ($item->getpacked_qty() < $qtyToShip) {
                throw new LocalizedException(__("%1: Item is not prepared. The number of items prepared is %2", $sku, $item->getpacked_qty()));
            }
        }
    }

}
