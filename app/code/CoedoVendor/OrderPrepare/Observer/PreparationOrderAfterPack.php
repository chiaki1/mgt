<?php

namespace CoedoVendor\OrderPrepare\Observer;

use BoostMyShop\OrderPreparation\Model\InProgress\ItemFactory;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Module\Manager;

class PreparationOrderAfterPack implements ObserverInterface
{

    /**
     * @var ItemFactory
     */
    protected $inProgressItemFactory;

    protected $moduleManager;

    /**
     * @param ItemFactory $inProgressItemFactory
     * @param Manager     $moduleManager
     */
    public function __construct(
        ItemFactory $inProgressItemFactory,
        Manager $moduleManager
    ) {
        $this->inProgressItemFactory = $inProgressItemFactory;
        $this->moduleManager = $moduleManager;
    }

    public function execute(EventObserver $observer)
    {
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_OrderPreparation')) {
            /** @var \BoostMyShop\OrderPreparation\Model\InProgress $order_in_progress */
            $order_in_progress = $observer->getEvent()->getOrderInProgress();
            $products          = $observer->getEvent()->getProducts();
            foreach ($products as $product => $qty) {
                if ($qty < (int)$this->getQtyOrdered($order_in_progress->getOrder(), $product)) {
                    $order_in_progress->setip_status('partial_packed')->save();
                }
                $obj = $this->inProgressItemFactory->create()->load($product, 'ipi_order_item_id');
                $obj->setipi_qty($obj->getipi_qty() - $qty);
                $obj->setPackedQty($obj->getPackedQty() + $qty);
                $obj->save();
            }
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param                            $itemId
     *
     * @return float|int|null
     */
    public function getQtyOrdered($order, $itemId)
    {
        foreach ($order->getAllItems() as $item) {
            if ($item->getId() == $itemId) {
                return $item->getQtyOrdered();
            }
        }
        return 0;
    }

}
