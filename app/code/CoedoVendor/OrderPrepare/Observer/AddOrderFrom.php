<?php

namespace CoedoVendor\OrderPrepare\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddOrderFrom implements ObserverInterface
{
    const FROM_BE = 3;
    const FROM_WEB_APP = 2;

    /**
     * @param EventObserver $observer
     *
     * @return $this|void
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getOrder();
        if (!$order->getOrderFrom()) {
            if (!$order->getRemoteIp()) {
                $order->setOrderFrom(self::FROM_BE);
            } else {
                $order->setOrderFrom(self::FROM_WEB_APP);
            }
        }

        return $this;
    }

}
