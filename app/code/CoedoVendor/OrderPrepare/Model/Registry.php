<?php

namespace CoedoVendor\OrderPrepare\Model;

class Registry extends \BoostMyShop\OrderPreparation\Model\Registry
{
    public function getRegistry($key)
    {
        $extra = $this->getUserExtra();
        $value = '';
        if (isset($extra['orderpreparation'][$key]))
            $value = $extra['orderpreparation'][$key];
        else {
            switch ($key) {
                case 'current_operator_id':
                    $value = $this->_adminSession->getUser() ? $this->_adminSession->getUser()->getId() : -1;
                    break;
                case 'current_warehouse_id':
                    $value = $this->_adminSession->getUser() ? ($this->_adminSession->getUser()->getWarehouseId() ? $this->_adminSession->getUser()->getWarehouseId() : 1) : 1;
                    break;
            }
        }

        $this->_logger->log('Get ' . $key . ' : ' . $value, 'registry');

        return $value;
    }

    public function isAllWarehouse()
    {
        if ($this->_adminSession->getUser() && $this->_adminSession->getUser()->getWarehouseId() == 0) {
            return true;
        }
        return false;
    }
}
