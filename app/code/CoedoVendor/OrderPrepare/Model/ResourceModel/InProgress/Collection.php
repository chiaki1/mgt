<?php

namespace CoedoVendor\OrderPrepare\Model\ResourceModel\InProgress;

class Collection extends \BoostMyShop\OrderPreparation\Model\ResourceModel\InProgress\Collection
{
    public function addOrderDetails()
    {
        $this->getSelect()->join(['sog' => $this->getTable('sales_order_grid')], 'main_table.ip_order_id = sog.entity_id');
        return $this;
    }

    public function addOrderFrom()
    {
        $this->getSelect()->joinLeft(['so' => $this->getTable('sales_order')], 'main_table.ip_order_id = so.entity_id', ['order_from']);
        return $this;
    }
}
