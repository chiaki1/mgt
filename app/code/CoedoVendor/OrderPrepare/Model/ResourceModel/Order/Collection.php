<?php

namespace CoedoVendor\OrderPrepare\Model\ResourceModel\Order;

class Collection extends \BoostMyShop\OrderPreparation\Model\ResourceModel\Order\Collection
{
    public function addAdditionalFieldsCustom()
    {
        $this->getSelect()->join(
            ['so' => $this->getTable('sales_order')],
            "so.entity_id = main_table.entity_id",
            ['weight', 'order_from']);

        return $this;
    }
}
