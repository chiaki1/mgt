<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface BrandSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Brand list.
     *
     * @return BrandInterface[]
     */
    public function getItems();

    /**
     * Set status list.
     *
     * @param BrandInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

