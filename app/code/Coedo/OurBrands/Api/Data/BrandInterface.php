<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Api\Data;

interface BrandInterface
{

    const NAME         = 'name';
    const CODE         = 'code';
    const LOGO         = 'logo';
    const CATEGORY_IDS = 'category_ids';
    const BRAND_ID     = 'brand_id';
    const CREATED_AT   = 'created_at';

    /**
     * Get brand_id
     *
     * @return string|null
     */
    public function getBrandId();

    /**
     * Set brand_id
     *
     * @param string $brandId
     *
     * @return BrandInterface
     */
    public function setBrandId($brandId);

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BrandInterface
     */
    public function setName($name);

    /**
     * Get code
     *
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return BrandInterface
     */
    public function setCode($code);

    /**
     * Get logo
     *
     * @return string|null
     */
    public function getLogo();

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return BrandInterface
     */
    public function setLogo($logo);

    /**
     * Get category_ids
     *
     * @return string|null
     */
    public function getCategoryIds();

    /**
     * Set category_ids
     *
     * @param string $category_ids
     *
     * @return BrandInterface
     */
    public function setCategoryIds($category_ids);

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     *
     * @param string $createdAt
     *
     * @return BrandInterface
     */
    public function setCreatedAt($createdAt);
}

