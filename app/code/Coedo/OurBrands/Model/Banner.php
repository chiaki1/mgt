<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model;

use Coedo\OurBrands\Model\ResourceModel\Banner\Collection;
use Exception;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Uploader;
use Magento\Framework\Filesystem;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Framework\UrlInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

class Banner extends AbstractModel implements IdentityInterface
{

    const CACHE_TAG = 'current_brand_banners';
    protected $_eventPrefix = 'brand_banners';
    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * @param Context                    $context
     * @param Registry                   $registry
     * @param ResourceModel\Banner       $resource
     * @param Collection                 $resourceCollection
     * @param Filesystem                 $filesystem
     * @param SerializerJson             $serializer
     * @param ImageUploader|null         $imageUploader
     * @param StoreManagerInterface|null $storeManager
     * @param array                      $data
     */
    public function __construct(
        Context               $context,
        Registry              $registry,
        ResourceModel\Banner  $resource,
        Collection            $resourceCollection,
        Filesystem            $filesystem,
        SerializerJson        $serializer,
        ImageUploader         $imageUploader = null,
        StoreManagerInterface $storeManager = null,
        array                 $data = []
    ) {
        $this->_filesystem   = $filesystem;
        $this->serializer    = $serializer;
        $this->imageUploader = $imageUploader ?: ObjectManager::getInstance()->get(ImageUploader::class);
        $this->storeManager  = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        $identities = [
            self::CACHE_TAG . '_' . $this->getId()
        ];

        return $identities;
    }

    /**
     * Gets image name from $value array.
     *
     * Will return empty string in a case when $value is not an array.
     *
     * @param array $value Attribute value
     *
     * @return string
     */
    private function getUploadedImageName($value)
    {
        if (is_array($value) && isset($value[0]['name'])) {
            return $value[0]['name'];
        }

        return '';
    }

    /**
     * Check that image name exists in catalog/category directory and return new image name if it already exists.
     *
     * @param string $imageName
     *
     * @return string
     */
    private function checkUniqueImageName(string $imageName): string // @codingStandardsIgnoreLine
    {
        $mediaDirectory    = $this->_filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $imageAbsolutePath = $mediaDirectory->getAbsolutePath(
            $this->imageUploader->getBasePath() . DIRECTORY_SEPARATOR . $imageName
        );

        // phpcs:ignore Magento2.Functions.DiscouragedFunction
        $imageName = call_user_func([Uploader::class, 'getNewFilename'], $imageAbsolutePath);

        return $imageName;
    }

    /**
     * Avoiding saving potential upload data to DB.
     *
     * Will set empty image attribute value if image was not uploaded.
     *
     * @return $this
     * @since 101.0.8
     */
    public function beforeSave()
    {
        /** @var StoreInterface $store */
        $store        = $this->storeManager->getStore();
        $baseMediaDir = $store->getBaseMediaDir();
        $bannerImg    = $this->getData('banner_image');

        if ($this->isTmpFileAvailable($bannerImg) && $imageName = $this->getUploadedImageName($bannerImg)) {
            try {
                $newImgRelativePath   = $this->imageUploader->moveFileFromTmp($imageName, true);
                $bannerImg[0]['url']  = '/' . $baseMediaDir . '/' . $newImgRelativePath;
                $bannerImg[0]['name'] = $bannerImg[0]['url'];
            } catch (Exception $e) {
                $this->_logger->critical($e);
            }
        } elseif ($this->fileResidesOutsideCategoryDir($bannerImg)) {
            // use relative path for image attribute so we know it's outside of category dir when we fetch it
            // phpcs:ignore Magento2.Functions.DiscouragedFunction
            $bannerImg[0]['url']  = parse_url($bannerImg[0]['url'], PHP_URL_PATH);
            $bannerImg[0]['name'] = $bannerImg[0]['url'];
        }

        if ($imageName = $this->getUploadedImageName($bannerImg)) {
            $imageName = $this->checkUniqueImageName($imageName);
            $this->setData('banner_image', $imageName);
        } elseif (!is_string($bannerImg)) {
            $this->setData('banner_image', '');
        }

        $bannerImgListing    = $this->getData('banner_image_listing');

        if ($this->isTmpFileAvailable($bannerImgListing) && $imageNameListing = $this->getUploadedImageName($bannerImgListing)) {
            try {
                $newImgRelativePath   = $this->imageUploader->moveFileFromTmp($imageNameListing, true);
                $bannerImgListing[0]['url']  = '/' . $baseMediaDir . '/' . $newImgRelativePath;
                $bannerImgListing[0]['name'] = $bannerImgListing[0]['url'];
            } catch (Exception $e) {
                $this->_logger->critical($e);
            }
        } elseif ($this->fileResidesOutsideCategoryDir($bannerImgListing)) {
            // use relative path for image attribute so we know it's outside of category dir when we fetch it
            // phpcs:ignore Magento2.Functions.DiscouragedFunction
            $bannerImgListing[0]['url']  = parse_url($bannerImgListing[0]['url'], PHP_URL_PATH);
            $bannerImgListing[0]['name'] = $bannerImgListing[0]['url'];
        }

        if ($imageNameListing = $this->getUploadedImageName($bannerImgListing)) {
            $imageNameListing = $this->checkUniqueImageName($imageNameListing);
            $this->setData('banner_image_listing', $imageNameListing);
        } elseif (!is_string($bannerImg)) {
            $this->setData('banner_image_listing', '');
        }

        $parameters = $this->getData('parameters');
        if ($parameters && isset($parameters['data_filters']) && count($parameters['data_filters']) > 1) {
            $dataFilters = $this->serializer->serialize($parameters['data_filters']);
            $this->setDataFilters($dataFilters);
        }

        return parent::beforeSave();
    }

    /**
     * Check if temporary file is available for new image upload.
     *
     * @param array $value
     *
     * @return bool
     */
    private function isTmpFileAvailable($value)
    {
        return is_array($value) && isset($value[0]['tmp_name']);
    }

    /**
     * Check for file path resides outside of category media dir. The URL will be a path including pub/media if true
     *
     * @param array|null $value
     *
     * @return bool
     */
    private function fileResidesOutsideCategoryDir($value)
    {
        if (!is_array($value) || !isset($value[0]['url'])) {
            return false;
        }

        $fileUrl      = ltrim($value[0]['url'], '/');
        $baseMediaDir = $this->_filesystem->getUri(DirectoryList::MEDIA);

        if (!$baseMediaDir) {
            return false;
        }

        return strpos($fileUrl, $baseMediaDir) !== false;
    }

    /**
     * Returns image url
     *
     * @param string $attributeCode
     *
     * @return bool|string
     * @throws LocalizedException
     */
    public function getImageUrl($attributeCode = 'banner_image')
    {
        $url   = false;
        $image = $this->getData($attributeCode);
        if ($image) {
            if (is_string($image)) {
                $store = $this->storeManager->getStore();

                $isRelativeUrl = substr($image, 0, 1) === '/';

                $mediaBaseUrl = $store->getBaseUrl(
                    UrlInterface::URL_TYPE_MEDIA
                );

                if ($isRelativeUrl) {
                    $url = $image;
                } elseif (!$isRelativeUrl) {
                    $url = $mediaBaseUrl
                           . ltrim('/chiaki/brand/banners', '/')
                           . '/'
                           . $image;
                }
            } elseif (!is_string($image)) {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
}

