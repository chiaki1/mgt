<?php

namespace Coedo\OurBrands\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class
 */
class Type implements ArrayInterface
{
    const TYPE_BANNER = 0;

    const TYPE_CAMPAIGN = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::TYPE_BANNER, 'label' => __('Banner')],
            ['value' => self::TYPE_CAMPAIGN, 'label' => __('Campaign')],
        ];
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     * phpcs:disable Magento2.Functions.StaticFunction
     */
    public static function getOptionArray()
    {
        return [self::TYPE_BANNER => __('Banner'), self::TYPE_CAMPAIGN => __('Campaign')];
    }
}
