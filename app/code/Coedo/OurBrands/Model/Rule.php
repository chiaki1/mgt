<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coedo\OurBrands\Model;

use Coedo\OurBrands\Model\Rule\Condition\CombineFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Rule\Model\AbstractModel;

/**
 * Rule for catalog widget
 *
 * @api
 * @since 100.0.2
 */
class Rule extends AbstractModel
{
    /**
     * @var Rule\Condition\CombineFactory
     */
    protected $conditionsFactory;

    /**
     * Rule constructor
     *
     * @param Context                         $context
     * @param Registry                        $registry
     * @param FormFactory                     $formFactory
     * @param TimezoneInterface               $localeDate
     * @param Rule\Condition\CombineFactory   $conditionsFactory
     * @param AbstractResource|null           $resource
     * @param AbstractDb|null                 $resourceCollection
     * @param array                           $data
     * @param ExtensionAttributesFactory|null $extensionFactory
     * @param AttributeValueFactory|null      $customAttributeFactory
     *
     * @param Json|null                       $serializer
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context                    $context,
        Registry                   $registry,
        FormFactory                $formFactory,
        TimezoneInterface          $localeDate,
        CombineFactory             $conditionsFactory,
        AbstractResource           $resource = null,
        AbstractDb                 $resourceCollection = null,
        array                      $data = [],
        ExtensionAttributesFactory $extensionFactory = null,
        AttributeValueFactory      $customAttributeFactory = null,
        Json                       $serializer = null
    ) {
        $this->conditionsFactory = $conditionsFactory;
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $localeDate,
            $resource,
            $resourceCollection,
            $data,
            $extensionFactory,
            $customAttributeFactory,
            $serializer
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getConditionsInstance()
    {
        return $this->conditionsFactory->create();
    }

    /**
     * {@inheritdoc}
     */
    public function getActionsInstance()
    {
        return null;
    }
}
