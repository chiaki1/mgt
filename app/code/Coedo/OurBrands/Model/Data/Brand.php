<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model\Data;

use Coedo\OurBrands\Api\Data\BrandInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Brand extends AbstractSimpleObject implements BrandInterface
{

    /**
     * Get brand_id
     *
     * @return string|null
     */
    public function getBrandId()
    {
        return $this->_get(self::BRAND_ID);
    }

    /**
     * Set brand_id
     *
     * @param string $brandId
     *
     * @return BrandInterface
     */
    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BrandInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get code
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return BrandInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get logo
     *
     * @return string|null
     */
    public function getLogo()
    {
        return $this->_get(self::LOGO);
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return BrandInterface
     */
    public function setLogo($logo)
    {
        return $this->setData(self::LOGO, $logo);
    }

    /**
     * Get category_ids
     *
     * @return string|null
     */
    public function getCategoryIds()
    {
        return $this->_get(self::CATEGORY_IDS);
    }

    /**
     * Set category_ids
     *
     * @param string $category_ids
     *
     * @return BrandInterface
     */
    public function setCategoryIds($category_ids)
    {
        return $this->setData(self::CATEGORY_IDS, $category_ids);
    }

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     *
     * @param string $createdAt
     *
     * @return BrandInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}

