<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model\ResourceModel;

use Magento\Framework\DataObject;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Banner extends AbstractDb
{

    protected $_relationshipTable;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('brands_banner', 'banner_id');
    }

    /**
     * Category product table name getter
     *
     * @return string
     */
    public function getRelationshipTable()
    {
        if (!$this->_relationshipTable) {
            $this->_relationshipTable = $this->getTable('brands_banner_relationship');
        }
        return $this->_relationshipTable;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {

        $this->_saveBrandBanners($object);
        return parent::_afterSave($object);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $banner
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveBrandBanners($banner)
    {
        $id = $banner->getId();

        $brandId = $banner->getBrandId();

        if (!$brandId) {
            return $this;
        }

        $data[] = [
            'brand_id'  => (int)$brandId,
            'banner_id' => (int)$id,
            'position'  => 0,
        ];

        $connection = $this->getConnection();
        $connection->insertMultiple($this->getRelationshipTable(), $data);
        return $this;
    }
}

