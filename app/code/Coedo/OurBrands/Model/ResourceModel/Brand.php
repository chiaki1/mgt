<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model\ResourceModel;

use Magento\Framework\DataObject;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Brand extends AbstractDb
{

    protected $_relationshipTable;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('brands', 'brand_id');
    }

    /**
     *
     * @param \Coedo\OurBrands\Model\Brand $brand
     *
     * @return array
     */
    public function getBannersPosition($brand)
    {
        $select = $this->getConnection()->select()->from(
            $this->getRelationshipTable(),
            ['banner_id', 'position']
        )->where(
            "{$this->getTable('brands_banner_relationship')}.brand_id = ?",
            $brand->getId()
        );
        $bind   = ['brand_id' => (int)$brand->getId()];

        return $this->getConnection()->fetchPairs($select, $bind);
    }

    /**
     * Category product table name getter
     *
     * @return string
     */
    public function getRelationshipTable()
    {
        if (!$this->_relationshipTable) {
            $this->_relationshipTable = $this->getTable('brands_banner_relationship');
        }
        return $this->_relationshipTable;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return $this
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {

        $this->_saveBrandBanners($object);
        return parent::_afterSave($object);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $brand
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveBrandBanners($brand)
    {
        $brand->setIsChangedBannerList(false);
        $id = $brand->getId();

        $banners = $brand->getBrandBanners();
        if ($banners && !is_array($banners)) {
            $banners = json_decode($banners, true);
        }

        /**
         * Example re-save category
         */
        if ($banners === null) {
            return $this;
        }

        /**
         * old category-product relationships
         */
        $oldBanners = $brand->getBannersPosition();

        $insert = array_diff_key($banners, $oldBanners);
        $delete = array_diff_key($oldBanners, $banners);

        $update = array_intersect_key($banners, $oldBanners);
        $update = array_diff_assoc($update, $oldBanners);

        $connection = $this->getConnection();

        if (!empty($delete)) {
            $cond = ['banner_id IN(?)' => array_keys($delete), 'brand_id=?' => $id];
            $connection->delete($this->getRelationshipTable(), $cond);
        }

        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $bannerId => $position) {
                $data[] = [
                    'brand_id'  => (int)$id,
                    'banner_id' => (int)$bannerId,
                    'position'  => (int)$position,
                ];
            }
            $connection->insertMultiple($this->getRelationshipTable(), $data);
        }

        if (!empty($update)) {
            $newPositions = [];
            foreach ($update as $bannerId => $position) {
                $delta = $position - $oldBanners[$bannerId];
                if (!isset($newPositions[$delta])) {
                    $newPositions[$delta] = [];
                }
                $newPositions[$delta][] = $bannerId;
            }

            foreach ($newPositions as $delta => $bannerIds) {
                $bind  = ['position' => new \Zend_Db_Expr("position + ({$delta})")];
                $where = ['brand_id = ?' => (int)$id, 'banner_id IN (?)' => $bannerIds];
                $connection->update($this->getRelationshipTable(), $bind, $where);
            }
        }

        if (!empty($insert) || !empty($delete)) {
            $bannerIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));

            $brand->setChangedBannerIds($bannerIds);
        }

        if (!empty($insert) || !empty($update) || !empty($delete)) {
            $brand->setIsChangedBannerList(true);

            $bannerIds = array_keys($insert + $delete + $update);
            $brand->setAffectedBannerIds($bannerIds);
        }
        return $this;
    }
}

