<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model\ResourceModel\Banner;

use Coedo\OurBrands\Model\ResourceModel\Banner;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'banner_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Coedo\OurBrands\Model\Banner::class,
            Banner::class
        );
    }
}

