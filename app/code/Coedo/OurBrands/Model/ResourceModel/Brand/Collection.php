<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model\ResourceModel\Brand;

use Coedo\OurBrands\Model\ResourceModel\Brand;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'brand_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Coedo\OurBrands\Model\Brand::class,
            Brand::class
        );
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this as $item) {
            $options[] = [
                'value'     => $item->getBrandId(),
                'label'     => $item->getName(),
            ];
        }

        if (count($options) > 0) {
            array_unshift(
                $options,
                ['value' => '', 'label' => __('Please select a brand.')]
            );
        }
        return $options;
    }
}

