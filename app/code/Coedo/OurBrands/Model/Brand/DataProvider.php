<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Model\Brand;

use Coedo\OurBrands\Model\ResourceModel\Brand\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{

    protected $loadedData;
    protected $dataPersistor;

    protected $collection;

    /**
     * @var FileInfo
     */
    protected $fileInfo;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor
     *
     * @param string                 $name
     * @param string                 $primaryFieldName
     * @param string                 $requestFieldName
     * @param CollectionFactory      $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface  $storeManager
     * @param FileInfo               $fileInfo
     * @param array                  $meta
     * @param array                  $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        FileInfo $fileInfo,
        array $meta = [],
        array $data = []
    ) {
        $this->collection    = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->fileInfo      = $fileInfo;
        $this->storeManager  = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $result                                       = $model->getData();
            $result                                       = $this->convertValues('logo', $result);
            $result                                       = $this->convertValues('small_logo', $result);
            $result                                       = $this->convertValues('small_banner', $result);
            $result['category_ids']                       = explode(',', $result['category_ids']);
            $this->loadedData[$model->getId()]['content'] = $result;
        }
        $data = $this->dataPersistor->get('brand');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $result                                       = $model->getData();
            $result                                       = $this->convertValues('logo', $result);
            $result                                       = $this->convertValues('small_logo', $result);
            $result                                       = $this->convertValues('small_banner', $result);
            $result['category_ids']                       = explode(',', $result['category_ids']);
            $this->loadedData[$model->getId()]['content'] = $result;
            $this->dataPersistor->clear('brand');
        }

        return $this->loadedData;
    }

    private function convertValues($attributeCode, $bannerData): array // @codingStandardsIgnoreLine
    {
        if (isset($bannerData[$attributeCode]) && $bannerData[$attributeCode] && is_string($bannerData[$attributeCode])) {
            $fileName = $bannerData[$attributeCode];
            unset($bannerData[$attributeCode]);
            if ($this->fileInfo->isExist($fileName)) {
                $stat = $this->fileInfo->getStat($fileName);
                $mime = $this->fileInfo->getMimeType($fileName);

                // phpcs:ignore Magento2.Functions.DiscouragedFunction
                $bannerData[$attributeCode][0]['name'] = basename($fileName);

                $bannerData[$attributeCode][0]['url'] = $this->getUrl($fileName);

                $bannerData[$attributeCode][0]['size'] = isset($stat) ? $stat['size'] : 0;
                $bannerData[$attributeCode][0]['type'] = $mime;
            }
        }

        return $bannerData;
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    public function getUrl($image): string // @codingStandardsIgnoreLine
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $store        = $this->storeManager->getStore();
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->fileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->fileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(FileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

}

