<?php

namespace Coedo\OurBrands\Model\Brand;

use Coedo\OurBrands\Model\ResourceModel\Brand\CollectionFactory as BrandCollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;

class SourceProvider extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    protected $_options;

    /**
     * @var BrandCollectionFactory
     */
    protected $_brandsFactory;

    /**
     * @param CollectionFactory      $attrOptionCollectionFactory
     * @param OptionFactory          $attrOptionFactory
     * @param BrandCollectionFactory $brandsFactory
     */
    public function __construct(
        CollectionFactory      $attrOptionCollectionFactory,
        OptionFactory          $attrOptionFactory,
        BrandCollectionFactory $brandsFactory
    ) {
        $this->_brandsFactory = $brandsFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve all cities options
     *
     * @param bool $withEmpty
     * @param bool $defaultValues
     *
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            $this->_options = $this->_createBrandCollection()->load()->toOptionArray();
        }
        return $this->_options;
    }

    public function _createBrandCollection()
    {
        return $this->_brandsFactory->create();
    }
}
