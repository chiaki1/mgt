<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Product in category grid
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */

namespace Coedo\OurBrands\Block\Adminhtml\Brand\Tab;

use Coedo\OurBrands\Model\BannerFactory;
use Coedo\OurBrands\Model\BrandFactory;
use Coedo\OurBrands\Model\ResourceModel\Banner\Collection;
use Coedo\OurBrands\Model\Source\Type;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;

class Banner extends Extended
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var BannerFactory
     */
    protected $_bannerFactory;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var BrandFactory
     */
    protected $_brandFactory;

    /**
     * @param Context       $context
     * @param Data          $backendHelper
     * @param BannerFactory $bannerFactory
     * @param Registry      $coreRegistry
     * @param BrandFactory  $brandFactory
     * @param array         $data
     * @param Type|null     $type
     */
    public function __construct(
        Context       $context,
        Data          $backendHelper,
        BannerFactory $bannerFactory,
        Registry      $coreRegistry,
        BrandFactory  $brandFactory,
        array         $data = [],
        Type          $type = null
    ) {
        $this->_bannerFactory = $bannerFactory;
        $this->_coreRegistry  = $coreRegistry;
        $this->type           = $type ?: ObjectManager::getInstance()->get(Type::class);
        $this->_brandFactory  = $brandFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('brand_banners');
        $this->setDefaultSort('banner_id');
        $this->setUseAjax(true);
    }

    public function getBrand()
    {
        return $this->_coreRegistry->registry('brand');
    }

    /**
     * @param Column $column
     *
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in brand flag
        if ($column->getId() == 'in_brand') {
            $bannerIds = $this->_getSelectedBanners();
            if (empty($bannerIds)) {
                $bannerIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('main_table.banner_id', ['in' => $bannerIds]);
            } elseif (!empty($bannerIds)) {
                $this->getCollection()->addFieldToFilter('main_table.banner_id', ['nin' => $bannerIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        if ($this->getBrand()->getId()) {
            $this->setDefaultFilter(['in_brand' => 1]);
            $constraint = 'relationship.brand_id=' . $this->getBrand()->getId();
        } else {
            $constraint = 'relationship.brand_id=0';
        }
        /** @var Collection $collection */
        $collection = $this->_bannerFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
            ['relationship' => $collection->getTable("brands_banner_relationship")],
            'relationship.banner_id = main_table.banner_id AND ' . $constraint,
            ['position', 'is_campaign']
        );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_brand',
            [
                'type'             => 'checkbox',
                'name'             => 'in_brand',
                'values'           => $this->_getSelectedBanners(),
                'index'            => 'banner_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'banner_id',
            [
                'header'           => __('ID'),
                'sortable'         => true,
                'index'            => 'banner_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('name', ['header' => __('Name'), 'index' => 'name']);

        $this->addColumn(
            'type',
            [
                'header'  => __('Type'),
                'index'   => 'type',
                'type'    => 'options',
                'options' => $this->type->getOptionArray()
            ]
        );

        $this->addColumn(
            'show_home',
            [
                'header'  => __('Show Home'),
                'index'   => 'show_home',
                'type'    => 'options',
                'options' => ['1' => __('Yes'), '0' => __('No')]
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => 'Action',
                'filter' => false,
                'sortable' => false,
                'width' => '100px',
                'renderer' => \Coedo\OurBrands\Block\Adminhtml\Brand\Renderer\Action::class
            ]
        );

        $this->addColumn(
            'position',
            [
                'header'   => __('Position'),
                'type'     => 'number',
                'index'    => 'position',
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('brands/brand/grid', ['_current' => true]);
    }

    /**
     * @return array
     */
    protected function _getSelectedBanners()
    {
        $banners = $this->getRequest()->getPost('selected_banners');
        if ($banners === null) {
            $banners = $this->getBrand()->getBannersPosition();
            if ($banners) {
                return array_keys($banners);
            }
        }
        return $banners;
    }

}
