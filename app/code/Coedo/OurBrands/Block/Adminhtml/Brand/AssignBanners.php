<?php

namespace Coedo\OurBrands\Block\Adminhtml\Brand;

use Coedo\OurBrands\Block\Adminhtml\Brand\Tab\Banner;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\BlockInterface;

class AssignBanners extends Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'Coedo_OurBrands::brand/banner/edit/assign_banners.phtml';

    /**
     * @var Banner
     */
    protected $blockGrid;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * AssignProducts constructor.
     *
     * @param Context  $context
     * @param Registry              $registry
     * @param EncoderInterface $jsonEncoder
     * @param array                                    $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        EncoderInterface $jsonEncoder,
        array $data = []
    ) {
        $this->registry    = $registry;
        $this->jsonEncoder = $jsonEncoder;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve instance of grid block
     *
     * @return BlockInterface
     * @throws LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                Banner::class,
                'brand.banners.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }

    /**
     * @return string
     */
    public function getBannersJson()
    {
        $banners = $this->getBrand()->getBannersPosition();
        if (!empty($banners)) {
            return $this->jsonEncoder->encode($banners);
        }
        return '{}';
    }

    /**
     * Retrieve current category instance
     *
     * @return array|null
     */
    public function getBrand()
    {
        return $this->registry->registry('brand');
    }
}
