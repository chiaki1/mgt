<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Block\Adminhtml\Banner\Edit;

use Magento\Backend\Block\Widget\Context;

abstract class GenericButton
{

    protected $context;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->context->getRequest()->getParam('banner_id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array  $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getBrandId()
    {
        return $this->context->getRequest()->getParam('brand_id');
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function isFromBrand()
    {
        return $this->context->getRequest()->getParam('from_brand');
    }
}

