<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Block\Adminhtml\Banner\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class SaveAndBackToBrandButton extends GenericButton implements ButtonProviderInterface
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        if ($this->isFromBrand()) {
            $data = [
                'label'      => __('Save and Back to Brand'),
                'class'      => 'save primary',
                'data_attribute' => [
                    'mage-init' => [
                        'buttonAdapter' => [
                            'actions' => [
                                [
                                    'targetName' => 'brands_banner_form.brands_banner_form',
                                    'actionName' => 'save',
                                    'params' => [
                                        true,
                                        [
                                            'from_brand' => true,
                                            'brand_id' => $this->getBrandId()
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                ],
                'sort_order' => 80,
            ];
        }
        return $data;
    }
}

