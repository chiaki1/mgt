<?php

/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coedo\OurBrands\Controller\Adminhtml\Brand;

use Coedo\OurBrands\Block\Adminhtml\Brand\Tab\Banner;
use Coedo\OurBrands\Controller\Adminhtml\Brand;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;

class Grid extends Brand
{
    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @param Context             $context
     * @param Registry                                        $coreRegistry
     * @param RawFactory $resultRawFactory
     * @param LayoutFactory           $layoutFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        RawFactory $resultRawFactory,
        LayoutFactory $layoutFactory
    ) {
        parent::__construct($context, $coreRegistry);
        $this->resultRawFactory = $resultRawFactory;
        $this->layoutFactory    = $layoutFactory;
    }

    /**
     * Grid Action
     * Display list of products related to current category
     *
     * @return Redirect|Raw
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id    = $this->getRequest()->getParam('brand_id');
        $model = $this->initBrand();

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Brand no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/', ['_current' => true, 'id' => null]);
            }
        }
        /** @var Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents(
            $this->layoutFactory->create()->createBlock(
                Banner::class,
                'brand.banners.grid'
            )->toHtml()
        );
    }
}
