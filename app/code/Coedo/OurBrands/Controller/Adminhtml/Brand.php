<?php

declare(strict_types=1);

namespace Coedo\OurBrands\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class Brand extends Action
{

    const ADMIN_RESOURCE = 'Coedo_OurBrands::manage_brand';
    protected $_coreRegistry;

    /**
     * @param Context  $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context  $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     *
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
                   ->addBreadcrumb(__('Product'), __('Product'))
                   ->addBreadcrumb(__('Brand'), __('Brand'));
        return $resultPage;
    }

    /**
     * Init Slider
     *
     * @return \Coedo\OurBrands\Model\Brand
     */
    protected function initBrand()
    {
        $brand_id = (int)$this->getRequest()->getParam('brand_id');
        $model           = $this->_objectManager->create(\Coedo\OurBrands\Model\Brand::class);
        if ($brand_id) {
            $model->load($brand_id);
        }
        $this->_coreRegistry->register('brand', $model);

        return $model;
    }
}

