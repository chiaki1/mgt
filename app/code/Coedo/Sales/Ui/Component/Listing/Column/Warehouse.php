<?php

namespace Coedo\Sales\Ui\Component\Listing\Column;

use Magento\Framework\Module\Manager;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class Warehouse extends Column
{
    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * Warehouse constructor.
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Manager            $moduleManager
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Manager $moduleManager,
        array $components = [],
        array $data = []
    ) {
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepare()
    {
        if (!$this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            $this->_data['config']['componentDisabled'] = true;
        }
        parent::prepare();
    }
}
