<?php

namespace Coedo\CustomAmastyOgrid\Plugin;

use Amasty\Ogrid\Ui\Component\ExportButton;

class CustomExportButton
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * CustomExportButton constructor.
     *
     * @param \Magento\Framework\Module\Manager $moduleManager
     */
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
    }

    /**
     * @param ExportButton $subject
     */
    public function beforePrepare(\Amasty\Ogrid\Ui\Component\ExportButton $subject)
    {
        if ($this->moduleManager->isOutputEnabled('Amasty_Ogrid')) {
            $config = $subject->getData('config');
            if (isset($config['options'])) {
                $options = [];
                foreach ($config['options'] as $option) {
                    switch ($option['value']) {
                        case 'csv':
                            $option['url'] = 'amasty_ogrid/export/gridToCsv';
                            break;

                        case 'xml':
                            $option['url'] = 'amasty_ogrid/export/gridToXml';
                            break;
                        case 'xlsx':
                            $option['url'] = 'amasty_ogrid/export/gridToXlsx';
                            break;
                    }
                    $options[] = $option;
                }
                $config['options'] = $options;
            }
            $subject->setData('config', $config);
        }
    }
}
