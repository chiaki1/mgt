<?php

declare(strict_types=1);

namespace Coedo\CustomAmastyOgrid\Model\Export;

use Chiaki\Sales\Model\Export\ConvertToXlsx as ExportConvertToXlsx;
use Magento\Framework\Filesystem;
use Magento\Framework\Math\Random;
use Magento\Ui\Api\BookmarkManagementInterface;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\MetadataProvider;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ConvertToXlsx extends ExportConvertToXlsx
{
    use \Amasty\Ogrid\Model\Export\ExportTrait;

    /**
     * @var BookmarkManagementInterface
     */
    private $bookmarkManagement;

    /**
     * @var Random
     */
    private $random;

    public function __construct(
        Filter $filter,
        MetadataProvider $metadataProvider,
        BookmarkManagementInterface $bookmarkManagement,
        Filesystem $filesystem,
        Random $random
    ) {
        parent::__construct($filesystem, $filter, $metadataProvider);
        $this->bookmarkManagement = $bookmarkManagement;
        $this->random             = $random;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getXlsxFile()
    {
        $component               = $this->filter->getComponent();
        $bookmark                = $this->bookmarkManagement->getByIdentifierNamespace(
            'current',
            'sales_order_grid'
        );
        $config                  = $bookmark ? $bookmark->getConfig() : null;
        $bookmarksCols           = [];
        $availableProductDetails = [];

        if (is_array($config) && isset($config['current']['columns'])) {
            $bookmarksCols = $config['current']['columns'];
        }

        foreach ($bookmarksCols as $key => $colItem) {
            if (!empty($colItem['visible'])
                && $colItem['visible'] == true
                && stripos($key, 'amasty_ogrid_product') !== false
            ) {
                $availableProductDetails[$key] = $colItem['amogrid_label'] ?? $key;
            }
        }

        $this->filter->prepareComponent($component);
        $this->filter->applySelectionOnTargetProvider();
        $dataProvider = $component->getContext()->getDataProvider();
        $fields       = $this->metadataProvider->getFields($component);
        $options      = $this->metadataProvider->getOptions();

        $this->directory->create('export');

        $headers        = $this->metadataProvider->getHeaders($component);
        $i              = 1;
        $searchCriteria = $dataProvider->getSearchCriteria()
                                       ->setCurrentPage($i)
                                       ->setPageSize($this->pageSize);
        $totalCount     = (int)$dataProvider->getSearchResult()->getTotalCount();

        $itemsData = [];
        $itemsData[] = $this->metadataProvider->getHeaders($component);
        while ($totalCount > 0) {
            $items = $this->getDataProviderItems($dataProvider, $availableProductDetails);

            foreach ($items as $idx => $item) {
                $this->metadataProvider->convertDate($item, $component->getName());
                $itemsData[] = $this->metadataProvider->getRowData($item, $fields, $options);
            }

            $searchCriteria->setCurrentPage(++$i);
            $totalCount = $totalCount - $this->pageSize;
        }

        $sheetTitle     = 'export';

        $spreadsheet = new Spreadsheet();
        $sheet       = $spreadsheet->getActiveSheet();

        $sheet->fromArray($itemsData, null, 'A1');

        $sheet->setTitle($sheetTitle);
        $spreadsheet->setActiveSheetIndex(0);

        $name = md5(microtime());
        $file = '/export/' . $component->getName() . $name . '.xlsx';
        $this->directory->create('export');
        $filepath = $this->directory->getAbsolutePath($file);
        $writer   = new Xlsx($spreadsheet);
        $writer->save($filepath);

        return [
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true
        ];
    }
}
