<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\CatalogGraphql\Model\Resolver\Product;

use Coedo\OurBrands\Model\Brand\FileInfo as BrandFileInfo;
use Coedo\OurBrands\Model\BrandFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\UrlInterface;

/**
 * Returns media url
 */
class Brand implements ResolverInterface
{

    /**
     * @var BrandFactory
     */
    private $brandFactory;

    /**
     * @var BrandFileInfo
     */
    private $brandFileInfo;

    /**
     * @var string[]
     */
    private $brand = [];

    /**
     * @param BrandFactory  $brandFactory
     * @param BrandFileInfo $brandFileInfo
     */
    public function __construct(
        BrandFactory $brandFactory,
        BrandFileInfo $brandFileInfo
    ) {
        $this->brandFactory = $brandFactory;
        $this->brandFileInfo = $brandFileInfo;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field       $field,
                    $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    ) {

        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        if (isset($value['brand'])) {
            $store = $context->getExtensionAttributes()->getStore();
            return $this->getBrandName($value['brand'], $store);
        }
        return [];
    }

    /**
     * Get image URL
     *
     * @param string|null $brandId
     * @param             $store
     *
     * @return string[]
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getBrandName($brandId, $store): array
    {
        if (!empty($this->brand)) {
            return $this->brand;
        }
        $brand = $this->brandFactory->create()->load($brandId);
        $this->brand = [
            "brand_id" => $brand->getId(),
            "name" => $brand->getName(),
            "code" => $brand->getCode(),
            "logo" => $this->getLogoUrl($brand->getLogo(), $store),
        ];
        return $this->brand;
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    public function getLogoUrl($image, $store): string
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->brandFileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->brandFileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(BrandFileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
}
