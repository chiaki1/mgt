<?php



namespace Coedo\PDFCustom\Controller\Guest;

class Invoice extends \Coedo\PDFCustom\Controller\Sales\Invoice
{
    protected function getRedirect()
    {
        return $this->_redirect('sales/guest/form');
    }
}
