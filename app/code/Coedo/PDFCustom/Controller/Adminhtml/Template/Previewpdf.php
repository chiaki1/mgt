<?php



namespace Coedo\PDFCustom\Controller\Adminhtml\Template;

class Previewpdf extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Coedo_PDFCustom::template';

    /**
     * @var \Coedo\PDFCustom\Model\PdfFactory
     */
    private $pdfFactory;

    /**
     * @var \Coedo\PDFCustom\Model\ComponentChecker
     */
    private $componentChecker;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Coedo\PDFCustom\Model\PdfFactory $pdfFactory,
        \Coedo\PDFCustom\Model\ComponentChecker $componentChecker
    ) {
        $this->pdfFactory = $pdfFactory;
        parent::__construct($context);
        $this->componentChecker = $componentChecker;
    }

    /**
     * Preview transactional email action
     *
     * @return \Magento\Framework\Controller\Result\Raw|\Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        if (!$this->componentChecker->isComponentsExist()) {
            $this->messageManager->addErrorMessage($this->componentChecker->getComponentsErrorMessage());

            return $this->_redirect('*/*/');
        }

        try {
            $html = $this->_view->getLayout()
                ->createBlock(\Coedo\PDFCustom\Block\Adminhtml\Template\Preview::class, 'preview.page.content')
                ->toHtml();

            /** @var \Coedo\PDFCustom\Model\Pdf $pdf */
            $pdf = $this->pdfFactory->create();
            $pdf->setHtml($html);
            $rawPdf = $pdf->render();

            /** @var \Magento\Framework\Controller\Result\Raw $raw */
            $raw = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
            $raw->setHeader('Content-type', "application/x-pdf");
            $raw->setHeader('Content-Security-Policy', "script-src 'none'");
            $raw->setHeader('Content-Disposition', "inline; filename=preview.pdf");
            $raw->setContents($rawPdf);

            return $raw;
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred. The PDF template can not be opened for preview.')
            );
            $this->_redirect('*/*/');
        }
    }
}
