<?php



namespace Coedo\PDFCustom\Model\Template;

use Amasty\Base\Model\MagentoVersion;
use Coedo\PDFCustom\Model\Template;
use Magento\Framework\Module\Manager;

class VariablesOptionRepository
{
    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * @var MagentoVersion
     */
    private $magentoVersion;

    public function __construct(
        Manager $moduleManager,
        MagentoVersion $magentoVersion
    ) {
        $this->moduleManager = $moduleManager;
        $this->magentoVersion = $magentoVersion;
    }

    /**
     * @param Template $template
     *
     * @return array
     */
    public function getAdditionalVariables($template)
    {
        $options = [];

        return $options;
    }
}
