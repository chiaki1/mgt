<?php



namespace Coedo\PDFCustom\Model;

class PdfProvider
{
    /**
     * @var Pdf[]
     */
    protected $invoicePdfStorage = [];

    /**
     * @var Order\Pdf\Invoice
     */
    private $invoicePdf;

    public function __construct(
        \Coedo\PDFCustom\Model\Order\Pdf\Invoice $invoicePdf
    ) {
        $this->invoicePdf = $invoicePdf;
    }

    /**
     * @param \Magento\Sales\Model\Order\Invoice $saleObject
     *
     * @return Pdf
     */
    public function getInvoicePdf(\Magento\Sales\Model\AbstractModel $saleObject)
    {
        if (!isset($this->invoicePdfStorage[$saleObject->getId()])) {
            $this->invoicePdfStorage[$saleObject->getId()] = $this->invoicePdf->getPdf([$saleObject]);
        }

        return $this->invoicePdfStorage[$saleObject->getId()];
    }

    /**
     * @param \Magento\Sales\Model\Order $saleObject
     *
     * @return Pdf
     */
    public function getOrderPdf(\Magento\Sales\Model\AbstractModel $saleObject)
    {
        if (!isset($this->orderPdfStorage[$saleObject->getId()])) {
            $this->orderPdfStorage[$saleObject->getId()] = $this->orderPdf->getPdf([$saleObject]);
        }

        return $this->orderPdfStorage[$saleObject->getId()];
    }

    /**
     * @param \Magento\Sales\Model\Order $saleObject
     *
     * @return Pdf
     */
    public function getShipmentPdf(\Magento\Sales\Model\AbstractModel $saleObject)
    {
        if (!isset($this->shipmentPdfStorage[$saleObject->getId()])) {
            $this->shipmentPdfStorage[$saleObject->getId()] = $this->shipmentPdf->getPdf([$saleObject]);
        }

        return $this->shipmentPdfStorage[$saleObject->getId()];
    }

    /**
     * @param \Magento\Sales\Model\Order $saleObject
     *
     * @return Pdf
     */
    public function getCreditmemoPdf(\Magento\Sales\Model\AbstractModel $saleObject)
    {
        if (!isset($this->creditmemoPdfStorage[$saleObject->getId()])) {
            $this->creditmemoPdfStorage[$saleObject->getId()] = $this->creditmemoPdf->getPdf([$saleObject]);
        }

        return $this->creditmemoPdfStorage[$saleObject->getId()];
    }
}
