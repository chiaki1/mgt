<?php



namespace Coedo\PDFCustom\Model\Source;

class PlaceForUse extends \Magento\Framework\DataObject implements \Magento\Framework\Option\ArrayInterface
{
    const TYPE_INVOICE = 2;

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => ''],
            ['value' => self::TYPE_INVOICE, 'label' => __('Invoice')],
        ];
    }
}
