<?php


require_once(BP.'/app/library/vendor/autoload.php');

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Coedo_PDFCustom',
    __DIR__
);
