<?php



namespace Coedo\PDFCustom\Setup\Operation;

use Magento\Email\Model\TemplateFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class AddVariablesToDefaultTemplates
{
    /**
     * @var TemplateFactory
     */
    private $templateFactory;

    public function __construct(TemplateFactory $templateFactory)
    {
        $this->templateFactory = $templateFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    public function execute(ModuleDataSetupInterface $setup)
    {
        $templatesArray = [
            'ampdf_invoice_template'    => __('Invoice default Template'),
        ];

        $connection = $setup->getConnection();
        $tableName = $setup->getTable('chiaki_pdf_template');
        foreach ($templatesArray as $templateCode => $templateName) {
            $template = $this->templateFactory->create();
            $template->setForcedArea($templateCode);
            $template->loadDefault($templateCode);

            $bind = [
                'orig_template_code' => $templateCode,
                'orig_template_variables' => $template->getData('orig_template_variables'),
            ];

            $connection->update($tableName, $bind, ['template_code = ?' => $templateName]);
        }
    }
}
