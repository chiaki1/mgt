<?php

namespace Coedo\SaleAdmin\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Type
 *
 * @package Coedo\SaleAdmin\Model\Source
 */
class Type implements ArrayInterface
{
    const TYPE_BASE = '0';
    const TYPE_SALE = '1';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::TYPE_BASE, 'label' => __('Base')],
            ['value' => self::TYPE_SALE, 'label' => __('Sale')],
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::TYPE_BASE => __('Base'),
            self::TYPE_SALE => __('Sale'),
        ];
    }
}
