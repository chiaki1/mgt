<?php

namespace Coedo\SaleAdmin\Model\Source;

use BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\Collection;
use Magento\Framework\Option\ArrayInterface;

/**
 * Class Type
 *
 * @package Coedo\SaleAdmin\Model\Source
 */
class Warehouse implements ArrayInterface
{

    protected $moduleManager;

    public function __construct(\Magento\Framework\Module\Manager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    public function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $options       = [];
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            $collection    = $objectManager->create(Collection::class);

            foreach ($collection as $item) {
                $options[] = ['value' => $item->getId(), 'label' => $item->getw_name()];
            }
        }
        array_unshift($options, ['value' => '0', 'label' => __('All Warehouse')]);

        return $options;
    }

    public function toArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $options       = [];
        $options['0']  = __('All Warehouse');
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            $collection    = $objectManager->create(Collection::class);

            foreach ($collection as $item) {
                $options[$item->getId()] = $item->getw_name();
            }
        }

        return $options;
    }
}
