<?php

namespace Coedo\SaleAdmin\Model;

use Magento\Framework\Exception\LocalizedException;

class User extends \Magento\User\Model\User
{
    /**
     * Retrieve user roles
     *
     * @return array
     * @throws LocalizedException
     */
    public function getCustomers()
    {
        return $this->_getResource()->getCustomers($this);
    }
}
