<?php

namespace Coedo\SaleAdmin\Model\ResourceModel\Order\Grid;

use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Sales\Model\ResourceModel\Order;
use Magento\User\Model\UserFactory;
use Psr\Log\LoggerInterface as Logger;

class Collection extends \Magento\Sales\Model\ResourceModel\Order\Grid\Collection
{
    protected $authSession;
    protected $userFactory;

    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        AuthSession $authSession,
        UserFactory $userFactory,
        $mainTable = 'sales_order_grid',
        $resourceModel = Order::class
    ) {
        $this->authSession = $authSession;
        $this->userFactory = $userFactory;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);
    }

    /**
     * @inheritdoc
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        if ($this->getCurrentUser()->getData('type') == 1) {
            $this->addFieldToFilter('customer_id', ['in' => $this->getCurrentUser()->getCustomers()]);
        }

        return $this;
    }

    public function getCurrentUser()
    {
        $user = $this->authSession->getUser();
        return $this->userFactory->create()->load($user->getUserId());
    }
}
