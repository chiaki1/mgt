<?php

namespace Coedo\SaleAdmin\Model\ResourceModel\Order\Customer;

class Collection extends \Magento\Sales\Model\ResourceModel\Order\Customer\Collection
{
    /**
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        if ($this->getCurrentUser()->getData('type') == 1) {
            $this->addFieldToFilter('entity_id', ['in' => $this->getCurrentUser()->getCustomers()]);
        }
        return $this;
    }

    public function getCurrentUser()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $user = $objectManager->create('Magento\Backend\Model\Auth\Session')->getUser();
        return $objectManager->create('Magento\User\Model\User')->load($user->getUserId());
    }
}
