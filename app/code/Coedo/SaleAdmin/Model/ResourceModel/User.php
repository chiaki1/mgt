<?php

namespace Coedo\SaleAdmin\Model\ResourceModel;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Model\AbstractModel;

class User extends \Magento\User\Model\ResourceModel\User
{
    /**
     * Get user roles
     *
     * @param AbstractModel $user
     *
     * @return array
     */
    public function getCustomers(AbstractModel $user)
    {
        if (!$user->getId()) {
            return [];
        }

        $table      = $this->getTable('admin_user_customer');
        $connection = $this->getConnection();

        $select = $connection->select()->from(
            $table,
            ['customer_id']
        )->where(
            "{$table}.user_id = :user_id"
        );

        $binds = ['user_id' => (int)$user->getId(),];

        $customers = $connection->fetchCol($select, $binds);

        if ($customers) {
            return $customers;
        }

        return [];
    }

    /**
     * @param AbstractModel $object
     *
     * @return User
     */
    protected function _afterSave(AbstractModel $object)
    {
        parent::_afterSave($object);
        $connection = $this->getConnection();
        $this->saveCustomers($object, $connection);

        return $this;
    }

    /**
     * @param AbstractModel    $object
     * @param AdapterInterface $connection
     *
     * @return $this
     */
    private function saveCustomers(AbstractModel $object, AdapterInterface $connection)
    {
        $customers = $object->getData('user_customers');
        if ($object->hasData('user_customers')) {
            $condition = ['user_id = ?' => $object->getUserId()];
            $connection->delete($this->getTable('admin_user_customer'), $condition);
            if (!empty($customers)) {
                $customersArray = explode('&', $customers);
                foreach ($customersArray as $customerId) {
                    $customerId     = explode('=', $customerId);
                    $customerInsert = ['user_id' => $object->getUserId(), 'customer_id' => $customerId[0]];
                    $connection->insert($this->getTable('admin_user_customer'), $customerInsert);
                }
            }
        }

        return $this;
    }
}
