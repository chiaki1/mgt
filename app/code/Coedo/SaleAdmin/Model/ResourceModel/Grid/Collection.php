<?php

namespace Coedo\SaleAdmin\Model\ResourceModel\Grid;

use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Customer\Model\ResourceModel\Customer;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\Locale\ResolverInterface;
use Magento\User\Model\UserFactory;
use Psr\Log\LoggerInterface as Logger;

class Collection extends \Magento\Customer\Model\ResourceModel\Grid\Collection
{
    protected $authSession;
    protected $userFactory;

    public function __construct(
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        ResolverInterface $localeResolver,
        AuthSession $authSession,
        UserFactory $userFactory,
        $mainTable = 'customer_grid_flat',
        $resourceModel = Customer::class
    ) {
        $this->authSession = $authSession;
        $this->userFactory = $userFactory;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $localeResolver, $mainTable, $resourceModel);
    }

    /**
     * @inheritdoc
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        if ($this->getCurrentUser()->getData('type') == 1) {
            $this->addFieldToFilter('entity_id', ['in' => $this->getCurrentUser()->getCustomers()]);
        }
        return $this;
    }

    public function getCurrentUser()
    {
        $user = $this->authSession->getUser();
        return $this->userFactory->create()->load($user->getUserId());
    }
}
