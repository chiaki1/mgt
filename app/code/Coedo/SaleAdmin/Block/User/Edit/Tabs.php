<?php

namespace Coedo\SaleAdmin\Block\User\Edit;

use Coedo\SaleAdmin\Block\User\Edit\Tab\Customer;
use Coedo\SaleAdmin\Block\User\Edit\Tab\Main;
use Magento\User\Block\User\Edit\Tab\Roles;

class Tabs extends \Magento\User\Block\User\Edit\Tabs
{
    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'main_section',
            [
                'label'   => __('User Info'),
                'title'   => __('User Info'),
                'content' => $this->getLayout()->createBlock(Main::class)->toHtml(),
                'active'  => true
            ]
        );

        $this->addTab(
            'roles_section',
            [
                'label'   => __('User Role'),
                'title'   => __('User Role'),
                'content' => $this->getLayout()->createBlock(
                    Roles::class,
                    'user.roles.grid'
                )->toHtml()
            ]
        );

        $this->addTab(
            'customer_section',
            [
                'label'   => __('Assign Customers'),
                'title'   => __('Assign Customers'),
                'content' => $this->getLayout()->createBlock(
                    Customer::class,
                    'user.customer.grid'
                )->toHtml()
            ]
        );
        return \Magento\Backend\Block\Widget\Tabs::_beforeToHtml();
    }
}
