<?php

namespace Coedo\SaleAdmin\Block\User\Edit\Tab;

use Coedo\SaleAdmin\Model\Source\Type;
use Coedo\SaleAdmin\Model\Source\Warehouse;
use Magento\Framework\Data\Form;
use Magento\Framework\Locale\OptionInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\View\Element\Template;
use Magento\User\Model\User;

class Main extends \Magento\User\Block\User\Edit\Tab\Main
{

    /**
     * Prepare form fields
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var $model User */
        $model = $this->_coreRegistry->registry('permissions_user');

        /** @var Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('user_');

        $baseFieldset = $form->addFieldset('base_fieldset', ['legend' => __('Account Information')]);

        if ($model->getUserId()) {
            $baseFieldset->addField('user_id', 'hidden', ['name' => 'user_id']);
        } else {
            if (!$model->hasData('is_active')) {
                $model->setIsActive(1);
            }
        }

        $baseFieldset->addField(
            'username',
            'text',
            [
                'name'     => 'username',
                'label'    => __('User Name'),
                'id'       => 'username',
                'title'    => __('User Name'),
                'required' => true
            ]
        );

        $baseFieldset->addField(
            'firstname',
            'text',
            [
                'name'     => 'firstname',
                'label'    => __('First Name'),
                'id'       => 'firstname',
                'title'    => __('First Name'),
                'required' => true
            ]
        );

        $baseFieldset->addField(
            'lastname',
            'text',
            [
                'name'     => 'lastname',
                'label'    => __('Last Name'),
                'id'       => 'lastname',
                'title'    => __('Last Name'),
                'required' => true
            ]
        );

        $baseFieldset->addField(
            'email',
            'text',
            [
                'name'     => 'email',
                'label'    => __('Email'),
                'id'       => 'customer_email',
                'title'    => __('User Email'),
                'class'    => 'required-entry validate-email',
                'required' => true
            ]
        );

        $isNewObject = $model->isObjectNew();
        if ($isNewObject) {
            $passwordLabel = __('Password');
        } else {
            $passwordLabel = __('New Password');
        }
        $confirmationLabel = __('Password Confirmation');
        $this->_addPasswordFields($baseFieldset, $passwordLabel, $confirmationLabel, $isNewObject);

        $deployedLocales = $objectManager->get(OptionInterface::class);
        $baseFieldset->addField(
            'interface_locale',
            'select',
            [
                'name'   => 'interface_locale',
                'label'  => __('Interface Locale'),
                'title'  => __('Interface Locale'),
                'values' => $deployedLocales->getOptionLocales(),
                'class'  => 'select'
            ]
        );

        if ($this->_authSession->getUser()->getId() != $model->getUserId()) {
            $baseFieldset->addField(
                'is_active',
                'select',
                [
                    'name'    => 'is_active',
                    'label'   => __('This account is'),
                    'id'      => 'is_active',
                    'title'   => __('Account Status'),
                    'class'   => 'input-select',
                    'options' => ['1' => __('Active'), '0' => __('Inactive')]
                ]
            );
        }

        $type = $objectManager->get(Type::class);
        $baseFieldset->addField(
            'type',
            'select',
            [
                'name'   => 'type',
                'label'  => __('Type'),
                'title'  => __('Type'),
                'values' => $type->toArray(),
                'class'  => 'select'
            ]
        );

        $moduleManager = $objectManager->get(Manager::class);
        if ($moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            $warehouse = $objectManager->get(Warehouse::class);
            $baseFieldset->addField(
                'warehouse_id',
                'select',
                [
                    'name'   => 'warehouse_id',
                    'label'  => __('Warehouse'),
                    'title'  => __('Warehouse'),
                    'values' => $warehouse->toArray(),
                    'class'  => 'select'
                ]
            );
        }

        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                Template::class
            )->setTemplate('Coedo_SaleAdmin::user/form_js.phtml')
        );

        $baseFieldset->addField('user_roles', 'hidden', ['name' => 'user_roles', 'id' => '_user_roles']);
        $baseFieldset->addField('user_customers', 'hidden', ['name' => 'user_customers', 'id' => '_user_customers']);

        $currentUserVerificationFieldset = $form->addFieldset(
            'current_user_verification_fieldset',
            ['legend' => __('Current User Identity Verification')]
        );
        $currentUserVerificationFieldset->addField(
            self::CURRENT_USER_PASSWORD_FIELD,
            'password',
            [
                'name'     => self::CURRENT_USER_PASSWORD_FIELD,
                'label'    => __('Your Password'),
                'id'       => self::CURRENT_USER_PASSWORD_FIELD,
                'title'    => __('Your Password'),
                'class'    => 'validate-current-password required-entry',
                'required' => true
            ]
        );

        $data = $model->getData();
        unset($data['password']);
        unset($data[self::CURRENT_USER_PASSWORD_FIELD]);
        $form->setValues($data);

        $this->setForm($form);
        return \Magento\Backend\Block\Widget\Form\Generic::_prepareForm();
    }
}
