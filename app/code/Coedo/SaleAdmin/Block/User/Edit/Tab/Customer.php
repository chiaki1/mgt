<?php

namespace Coedo\SaleAdmin\Block\User\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Registry;
use Magento\User\Model\User;

class Customer extends Extended
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var CollectionFactory
     */
    protected $_customerCollectionFactory;

    /**
     * @var EncoderInterface
     */
    protected $_jsonEncoder;

    public function __construct(
        Context $context,
        Data $backendHelper,
        EncoderInterface $jsonEncoder,
        CollectionFactory $customerCollectionFactory,
        Registry $coreRegistry,
        array $data = []
    ) {
        $this->_jsonEncoder               = $jsonEncoder;
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->_coreRegistry              = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('assignCustomerToUserGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('asc');
        $this->setTitle(__('Assign Customers Information'));
        $this->setUseAjax(true);
    }

    /**
     * @param Column $column
     *
     * @return $this|Customer
     * @throws LocalizedException
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'assigned_customer') {
            $customerIds = $this->getSelectedCustomers();
            if (empty($customerIds)) {
                $customerIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $customerIds]);
            } else {
                if ($customerIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $customerIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Prepares collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        if ($this->getUser()->getUserId()) {
            $this->setDefaultFilter(['assigned_customer' => 1]);
        }
        $collection = $this->_customerCollectionFactory->create();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepares columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'assigned_customer',
            [
                'header_css_class' => 'data-grid-actions-cell',
                'header'           => __('Assigned'),
                'type'             => 'checkbox',
                'html_name'        => 'customers[]',
                'values'           => $this->getSelectedCustomers(),
                'align'            => 'center',
                'index'            => 'entity_id'
            ]
        );

        $this->addColumn('customer_id', ['header' => __('ID'), 'index' => 'entity_id']);
        $this->addColumn('customer_firstname', ['header' => __('First Name'), 'index' => 'firstname']);
        $this->addColumn('customer_lastname', ['header' => __('Last Name'), 'index' => 'lastname']);
        $this->addColumn('customer_email', ['header' => __('Email'), 'index' => 'email']);

        return parent::_prepareColumns();
    }

    public function getUser()
    {
        return $this->_coreRegistry->registry('permissions_user');
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        $userPermissions = $this->_coreRegistry->registry('permissions_user');
        return $this->getUrl('*/*/customersGrid', ['user_id' => $userPermissions->getUserId()]);
    }

    /**
     * Gets selected customer
     *
     * @param bool $json
     *
     * @return array|string
     */
    public function getSelectedCustomers($json = false)
    {
        $userCustomers = $this->getRequest()->getParam('customers');
        if ($userCustomers) {
            if ($json) {
                $result = json_decode($userCustomers);
                return $result ? $this->_jsonEncoder->encode($result) : '{}';
            }
            return $this->escapeJs($this->escapeHtml($userCustomers));
        }
        /* @var $user User */
        $user = $this->_coreRegistry->registry('permissions_user');
        //checking if we have this data and we
        //don't need load it through resource model
        if ($user->hasData('customers')) {
            $userCustomers = $user->getData('customers');
        } else {
            $userCustomers = $user->getCustomers();
        }

        if ($json) {
            $jsonCustomers = [];
            foreach ($userCustomers as $customerId) {
                $jsonCustomers[$customerId] = 0;
            }
            return $this->_jsonEncoder->encode((object)$jsonCustomers);
        } else {
            return $userCustomers;
        }
    }
}
