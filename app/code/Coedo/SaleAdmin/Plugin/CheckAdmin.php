<?php

namespace Coedo\SaleAdmin\Plugin;

use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Customer\Controller\Adminhtml\Index\Edit;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;

class CheckAdmin
{
    /**
     * @var AuthSession
     */
    protected $authSession;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var MessageManagerInterface
     */
    protected $messageManager;

    /**
     * CheckAdmin constructor.
     *
     * @param AuthSession             $authSession
     * @param RedirectFactory         $resultRedirectFactory
     * @param MessageManagerInterface $messageManager
     */
    public function __construct(
        AuthSession $authSession,
        RedirectFactory $resultRedirectFactory,
        MessageManagerInterface $messageManager
    ) {
        $this->authSession           = $authSession;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager        = $messageManager;
    }

    /**
     * @param Edit     $subject
     * @param callable $proceed
     *
     * @return mixed
     */
    public function aroundExecute(
        Edit $subject,
        callable $proceed
    ) {
        $customerId = (int)$subject->getRequest()->getParam('id');
        if ($this->getCurrentUser()->getData('type') == 1) {
            if (!in_array($customerId, $this->getCurrentUser()->getCustomers())) {
                $this->messageManager->addErrorMessage(__("Can not access to this customer."));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('customer/*/index');
                return $resultRedirect;
            }
        }
        return $proceed();
    }

    public function getCurrentUser()
    {
        return $this->authSession->getUser();
    }
}
