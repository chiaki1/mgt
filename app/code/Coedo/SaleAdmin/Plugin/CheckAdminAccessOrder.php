<?php

namespace Coedo\SaleAdmin\Plugin;

use Magento\Backend\Model\Auth\Session as AuthSession;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\Adminhtml\Order\View;

class CheckAdminAccessOrder
{

    /**
     * @var AuthSession
     */
    protected $authSession;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var MessageManagerInterface
     */
    protected $messageManager;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * CheckAdminAccessOrder constructor.
     *
     * @param AuthSession              $authSession
     * @param RedirectFactory          $resultRedirectFactory
     * @param MessageManagerInterface  $messageManager
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        AuthSession $authSession,
        RedirectFactory $resultRedirectFactory,
        MessageManagerInterface $messageManager,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->authSession           = $authSession;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager        = $messageManager;
        $this->orderRepository       = $orderRepository;
    }

    /**
     * @param View     $subject
     * @param callable $proceed
     */
    public function aroundExecute(View $subject, callable $proceed)
    {
        $id = $subject->getRequest()->getParam('order_id');
        try {
            $order = $this->orderRepository->get($id);
            if ($order->getEntityId()) {
                if ($this->getCurrentUser()->getData('type') == 1) {
                    if (!in_array($order->getCustomerId(), $this->getCurrentUser()->getCustomers())) {
                        $resultRedirect = $this->resultRedirectFactory->create();
                        $this->messageManager->addErrorMessage(__('Can not access to this order.'));
                        $resultRedirect->setPath('sales/order/index');
                        return $resultRedirect;
                    }
                }
            }
        } catch (NoSuchEntityException $e) {

        }
        return $proceed();
    }

    public function getCurrentUser()
    {
        return $this->authSession->getUser();
    }
}
