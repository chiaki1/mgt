<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\CustomBoostMyShop\Model\WarehouseStock\Import;

use BoostMyShop\AdvancedStock\Model\Warehouse\ProductsImportHandlerFactory;
use Coedo\CustomBoostMyShop\Api\Data\WarehouseStockImportInfoInterface;
use Magento\Framework\Notification\NotifierInterface;
use Psr\Log\LoggerInterface;

/**
 * Consumer for export message.
 */
class Consumer
{
    /**
     * @var NotifierInterface
     */
    private $notifier;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProductsImportHandlerFactory
     */
    private $productsImportHandler;

    /**
     * @param LoggerInterface              $logger
     * @param NotifierInterface            $notifier
     * @param ProductsImportHandlerFactory $productsImportHandler
     */
    public function __construct(
        LoggerInterface              $logger,
        NotifierInterface            $notifier,
        ProductsImportHandlerFactory $productsImportHandler
    ) {
        $this->logger                = $logger;
        $this->notifier              = $notifier;
        $this->productsImportHandler = $productsImportHandler;
    }

    public function process(WarehouseStockImportInfoInterface $importInfo)
    {
        try {
            $importHandler = $this->productsImportHandler->create();
            $importHandler->importFromCsvFile($importInfo->getStockId(), $importInfo->getImportFile(), $importInfo->getSeparator());
            $result = $importHandler->getResult();

            $this->notifier->addMajor(
                __("Result Import Stock Warehouse Id %1", $importInfo->getStockId()),
                json_encode($result)
            );

        } catch (\Exception $exception) {

            $this->notifier->addCritical(
                __('Error during export process occurred'),
                __('Error during export process occurred. Please check logs for detail')
            );
            $this->logger->critical('Something went wrong while export process. ' . $exception->getMessage());
        }
    }
}
