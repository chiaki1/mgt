<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\CustomBoostMyShop\Model\WarehouseStock\Import\Entity;

use Coedo\CustomBoostMyShop\Api\Data\WarehouseStockImportInfoInterface;

/**
 * Class WarehouseStockImportInfo
 *
 * @package Coedo\CustomBoostMyShop\Model\WarehouseStock\Import\Entity
 */
class WarehouseStockImportInfo implements WarehouseStockImportInfoInterface
{

    private $separator;
    private $import_file;
    private $stock_id;

    /**
     * Return separator
     *
     * @return string
     * @since 100.3.2
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param $separator
     *
     * @return void
     * @since 100.3.2
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * Return import_file
     *
     * @return string
     * @since 100.3.2
     */
    public function getImportFile()
    {
        return $this->import_file;
    }

    /**
     * @param $import_file
     *
     * @return void
     * @since 100.3.2
     */
    public function setImportFile($import_file)
    {
        $this->import_file = $import_file;
    }

    /**
     * Return stock_id
     *
     * @return string
     * @since 100.3.2
     */
    public function getStockId()
    {
        return $this->stock_id;
    }

    /**
     * @param $stockId
     *
     * @return void
     * @since 100.3.2
     */
    public function setStockId($stockId)
    {
        $this->stock_id = $stockId;
    }
}
