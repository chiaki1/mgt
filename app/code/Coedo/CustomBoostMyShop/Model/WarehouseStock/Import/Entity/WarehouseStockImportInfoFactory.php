<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\CustomBoostMyShop\Model\WarehouseStock\Import\Entity;

use Coedo\CustomBoostMyShop\Api\Data\WarehouseStockImportInfoInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class WarehouseStockImportInfoFactory
 *
 * @package Coedo\CustomBoostMyShop\Model\WarehouseStock\Import\Entity
 */
class WarehouseStockImportInfoFactory
{
    /**
     * Object Manager
     *
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * ImportInfoFactory constructor.
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;
    }

    /**
     * @param $separator
     * @param $import_file
     * @param $stock_id
     *
     * @return WarehouseStockImportInfoInterface
     */
    public function create(
        $separator, $import_file, $stock_id
    ) {
        /** @var WarehouseStockImportInfoInterface $importInfo */
        $importInfo = $this->objectManager->create(WarehouseStockImportInfoInterface::class);
        $importInfo->setSeparator($separator);
        $importInfo->setImportFile($import_file);
        $importInfo->setStockId($stock_id);

        return $importInfo;
    }
}
