<?php

namespace Coedo\CustomBoostMyShop\Model;

class StockMovement extends \BoostMyShop\AdvancedStock\Model\StockMovement
{
    const INCREASE = '1';
    const DECREASE = '2';
    const TRANSFER = '3';

    public function beforeSave()
    {
        if (!$this->getsm_from_warehouse_id() && $this->getsm_to_warehouse_id()) {
            $this->setsm_movement_description(self::INCREASE);
        } elseif ($this->getsm_from_warehouse_id() && !$this->getsm_to_warehouse_id()) {
            $this->setsm_movement_description(self::DECREASE);
        } elseif ($this->getsm_from_warehouse_id() && $this->getsm_to_warehouse_id()) {
            $this->setsm_movement_description(self::TRANSFER);
        }
        return parent::beforeSave();
    }
}
