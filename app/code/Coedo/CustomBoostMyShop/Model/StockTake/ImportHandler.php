<?php

namespace Coedo\CustomBoostMyShop\Model\StockTake;

class ImportHandler extends \BoostMyShop\AdvancedStock\Model\StockTake\ImportHandler
{
    public function importFromCsvFile($stockTake, $filePath)
    {
        if (!($filePath)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }

        //perform checks
        $this->csvProcessor->setDelimiter(',');
        $rows = $this->csvProcessor->getData($filePath);
        if (!isset($rows[0]))
            throw new \Exception('The file is empty');
        $columns = $rows[0];
        $this->checkColumns($columns);

        //import rows
        $stockTakeItems = $stockTake->getItems();
        $count = 0;
        $errors = [];
        foreach ($rows as $rowIndex => $rowData) {
            // skip headers
            if ($rowIndex == 0) {
                continue;
            }

            try
            {
                $this->_importRow($rowData, $stockTakeItems);
                $count++;
            }
            catch(\Exception $ex)
            {
                $errors[] = $ex->getMessage();
            }

        }

        return ['success' => $count, 'errors' => $errors];
    }
}
