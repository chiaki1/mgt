<?php

namespace Coedo\CustomBoostMyShop\Model;

use BoostMyShop\AdvancedStock\Helper\Logger;
use BoostMyShop\AdvancedStock\Model\Routing\Store\Mode;

class Router extends \BoostMyShop\AdvancedStock\Model\Router
{
    public function getWarehouseIdForOrderItem($order, $orderItem)
    {
        $routingLevel = $this->_config->getSetting('general/routing_level');
        $store        = $this->getStore($order->getStoreId());
        $groupId      = $store->getGroupId();
        $storeId      = $store->getStoreId();
        if ($routingLevel == 0) {
            $groupId = 0;
            $storeId = 0;
        } elseif ($routingLevel == 1) {
            $storeId = 0;
        }

        $routingMode = $this->getRoutingModeCustom($store->getWebsiteId(), $groupId, $storeId);
        $this->_logger->log('Routing mode for ' . $order->getId() . '/' . $orderItem->getId() . ' is ' . $routingMode, Logger::kLogRouting);
        $productId = $orderItem->getproduct_id();

        $warehouseId = null;
        switch ($routingMode) {
            case Mode::alwaysBestPriority:
                foreach ($this->getWarehousesForShipment($store->getWebsiteId(), $groupId, $storeId) as $id => $data) {
                    if ($data['rsw_priority'] == 1) {
                        $this->_logger->log('Warehouse with priority 1 for  ' . $order->getId() . '/' . $orderItem->getId() . ' is ' . $id, Logger::kLogRouting);
                        $warehouseId = $id;
                    }
                }
                break;
            case Mode::withStockOrderByPriority:
                $store = $this->getStore($order->getStoreId());
                foreach ($this->getWarehousesForShipment($store->getWebsiteId(), $groupId, $storeId) as $id => $data) {
                    if (!$warehouseId) {
                        $warehouseItem = $this->_warehouseItemFactory->create()->loadByProductWarehouse($productId, $id);
                        if ($warehouseItem->getwi_available_quantity() >= $orderItem->getqty_ordered()) {
                            $this->_logger->log('Warehouse #' . $id . ' has stock for ' . $order->getId() . '/' . $orderItem->getId(), Logger::kLogRouting);
                            $warehouseId = $id;
                        } else
                            $this->_logger->log('Warehouse #' . $id . ' has not enough stock (' . $warehouseItem->getwi_available_quantity() . ') for ' . $order->getId() . '/' . $orderItem->getId(), Logger::kLogRouting);
                    }
                }
                break;
        }

        if (!$warehouseId) {
            if ($this->getPrimaryWarehouse())
                $warehouseId = $this->getPrimaryWarehouse()->getId();
            if (!$warehouseId)
                $warehouseId = $this->getWarehouses()->getFirstItem()->getId();
            $this->_logger->log('Can not determinate warehouse for ' . $order->getId() . '/' . $orderItem->getId() . ', use warehouse ' . $warehouseId, Logger::kLogRouting);
        }

        return $warehouseId;
    }

    public function getWarehousesForShipment($websiteId, $groupId = 0, $storeId = 0)
    {

        $warehouses = [];

        foreach ($this->getWarehouses() as $warehouse) {
            $config = $this->getStoreWarehouseConfiguration($websiteId, $groupId, $storeId, $warehouse->getId());
            if ($config) {
                if (isset($config['rsw_use_for_shipments']) && $config['rsw_use_for_shipments'])
                    $warehouses[$warehouse->getId()] = $config;
            }
        }

        uasort($warehouses, array('\BoostMyShop\AdvancedStock\Model\Router', 'sortWarehousesPerPriority'));

        return $warehouses;
    }

    public function getRoutingModeCustom($websiteId, $groupId, $storeId)
    {
        $conf = $this->getStoreConfiguration($websiteId, $groupId, $storeId);
        return $conf->getrs_routing_mode();
    }
}
