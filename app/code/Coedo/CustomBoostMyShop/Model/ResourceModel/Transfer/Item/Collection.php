<?php

namespace Coedo\CustomBoostMyShop\Model\ResourceModel\Transfer\Item;

class Collection extends \BoostMyShop\AdvancedStock\Model\ResourceModel\Transfer\Item\Collection
{
    /**
     * @return \BoostMyShop\AdvancedStock\Model\ResourceModel\Transfer\Item\Collection
     */
    protected function _initSelect()
    {
        $this->getSelect()->from(['main_table' => $this->getMainTable()]);
        $this->getSelect()->join(['p' => $this->getResource()->getTable('catalog_product_entity')], 'main_table.st_product_id = p.entity_id', ['sku']);
        $this->getSelect()->join(
            ['n' => $this->getResource()->getTable('catalog_product_entity_varchar')],
            'p.row_id = n.row_id and n.store_id = 0 and n.attribute_id = ' . $this->_attributeFactory->create()->loadByCode('catalog_product', 'name')->getId(),
            ['product_name' => 'value']
        );

        return $this;
    }
}
