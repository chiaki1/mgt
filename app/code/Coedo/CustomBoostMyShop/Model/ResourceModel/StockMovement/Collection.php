<?php

namespace Coedo\CustomBoostMyShop\Model\ResourceModel\StockMovement;

class Collection extends \BoostMyShop\AdvancedStock\Model\ResourceModel\StockMovement\Collection
{
    /**
     * Add attribute to filter
     *
     * @param string $attribute
     * @param array|null $condition
     * @param string $joinType
     * @return \BoostMyShop\AdvancedStock\Model\ResourceModel\StockMovement\Collection
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner')
    {
        switch ($attribute) {
            case 'sm_id':
            case 'sm_created_at':
            case 'sm_product_id':
            case 'sm_from_warehouse_id':
            case 'sm_to_warehouse_id':
            case 'sm_qty':
            case 'sm_category':
            case 'sm_comments':
            case 'sm_user_id':
            case 'sm_ui':
            case 'sm_movement_description':
            case 'sm_parent_id':
                $conditionSql = $this->_getConditionSql($attribute, $condition);
                $this->getSelect()->where($conditionSql);
                break;
            default:
                \Magento\Catalog\Model\ResourceModel\Product\Collection::addAttributeToFilter($attribute, $condition, $joinType);
                break;
        }
        return $this;
    }
}
