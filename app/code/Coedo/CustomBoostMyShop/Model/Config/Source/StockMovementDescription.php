<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coedo\CustomBoostMyShop\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class StockMovementDescription implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Increase')],
            ['value' => 2, 'label' => __('Decrease')],
            ['value' => 3, 'label' => __('Transfer')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [1 => __('Increase'), 2 => __('Decrease'), 3 => __('Transfer')];
    }
}
