<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Coedo\CustomBoostMyShop\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class RoutingLevel implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('Website')], ['value' => 1, 'label' => __('Store')], ['value' => 2, 'label' => __('Store View')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('Website'), 1 => __('Store'), 2 => __('Store View')];
    }
}
