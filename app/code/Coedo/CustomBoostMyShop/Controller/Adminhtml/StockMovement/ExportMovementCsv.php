<?php

namespace Coedo\CustomBoostMyShop\Controller\Adminhtml\StockMovement;

use BoostMyShop\AdvancedStock\Controller\Adminhtml\StockMovement;
use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class ExportMovementCsv extends StockMovement
{

    /**
     * @return ResponseInterface|ResultInterface
     * @throws Exception
     */
    public function execute()
    {
        //prevent magento to redirect to dashboard
        $this->_auth->getAuthStorage()->setIsFirstPageAfterLogin(false);

        $fileName = 'stock_movement.csv';
        $content  = $this->_view->getLayout()->createBlock(
            'BoostMyShop\AdvancedStock\Block\StockMovement\Grid'
        )->getCsv();

        return $this->_fileFactory->create($fileName, $content, DirectoryList::VAR_DIR);
    }
}
