<?php

namespace Coedo\CustomBoostMyShop\Controller\Adminhtml\Report\Product;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Reports\Controller\Adminhtml\Report\Product;

class Configurablestock extends Product implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Coedo_CustomBoostMyShop::configurable_products_stock';

    /**
     * Low stock action
     *
     * @return void
     */
    public function execute()
    {
        $this->_initAction()->_setActiveMenu(
            'Coedo_CustomBoostMyShop::configurable_products_stock'
        )->_addBreadcrumb(
            __('Configurable Product Stock'),
            __('Configurable Product Stock')
        );
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Configurable Product Stock'));
        $this->_view->renderLayout();
    }
}
