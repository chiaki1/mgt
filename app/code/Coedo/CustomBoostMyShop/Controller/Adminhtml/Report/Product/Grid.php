<?php

namespace Coedo\CustomBoostMyShop\Controller\Adminhtml\Report\Product;

use BoostMyShop\AdvancedStock\Controller\Adminhtml\MassStockEditor;

class Grid extends MassStockEditor
{

    /**
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();

    }
}
