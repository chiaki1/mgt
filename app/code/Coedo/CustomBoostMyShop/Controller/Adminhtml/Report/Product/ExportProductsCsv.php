<?php

namespace Coedo\CustomBoostMyShop\Controller\Adminhtml\Report\Product;

use BoostMyShop\AdvancedStock\Controller\Adminhtml\Warehouse;

class ExportProductsCsv extends Warehouse
{

    public function execute()
    {
        $fileName = 'configurable_product_stock.xlsx';
        $content     = $this->_view->getLayout()->createBlock(
            'Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Grid'
        )->getXlsx();

        return $this->_fileFactory->create(
            $fileName,
            $content,
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR
        );
    }
}
