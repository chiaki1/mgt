<?php

namespace Coedo\CustomBoostMyShop\Controller\Adminhtml\Warehouse;

use BoostMyShop\AdvancedStock\Model\WarehouseFactory;
use Coedo\CustomBoostMyShop\Model\WarehouseStock\Import\Entity\WarehouseStockImportInfo;
use Coedo\CustomBoostMyShop\Model\WarehouseStock\Import\Entity\WarehouseStockImportInfoFactory;
use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\UploaderFactory;
use Magento\Framework\HTTP\Adapter\FileTransferFactory;
use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\LayoutFactory;

class Save extends \BoostMyShop\AdvancedStock\Controller\Adminhtml\Warehouse\Save
{
    /**
     * @var PublisherInterface
     */
    protected $messagePublisher;

    /**
     * @var WarehouseStockImportInfoFactory
     */
    protected $warehouseStockImportInfoFactory;

    /**
     * @param Context                         $context
     * @param Registry                        $coreRegistry
     * @param LayoutFactory                   $resultLayoutFactory
     * @param WarehouseFactory                $warehouseFactory
     * @param UploaderFactory                 $uploaderFactory
     * @param FileTransferFactory             $httpFactory
     * @param FileFactory                     $fileFactory
     * @param Product                         $product
     * @param DirectoryList                   $dir
     * @param PublisherInterface              $messagePublisher
     * @param WarehouseStockImportInfoFactory $warehouseStockImportInfoFactory
     */
    public function __construct(
        Context                         $context,
        Registry                        $coreRegistry,
        LayoutFactory                   $resultLayoutFactory,
        WarehouseFactory                $warehouseFactory,
        UploaderFactory                 $uploaderFactory,
        FileTransferFactory             $httpFactory,
        FileFactory                     $fileFactory,
        Product                         $product,
        DirectoryList                   $dir,
        PublisherInterface              $messagePublisher,
        WarehouseStockImportInfoFactory $warehouseStockImportInfoFactory
    ) {
        $this->messagePublisher                = $messagePublisher;
        $this->warehouseStockImportInfoFactory = $warehouseStockImportInfoFactory;
        parent::__construct($context, $coreRegistry, $resultLayoutFactory, $warehouseFactory, $uploaderFactory, $httpFactory, $fileFactory, $product, $dir);
    }

    public function execute()
    {

        $stockId = (int)$this->getRequest()->getParam('w_id');
        $data    = $this->getRequest()->getPostValue();

        if (!$data) {
            $this->_redirect('adminhtml/*/');
            return;
        }

        $model = $this->_warehouseFactory->create()->load($stockId);
        if ($stockId && $model->isObjectNew()) {
            $this->messageManager->addError(__('This warehouse no longer exists.'));
            $this->_redirect('adminhtml/*/');
            return;
        }

        $model->setData($data);

        try {
            $model->save();

            $this->checkImport($stockId);

            $this->messageManager->addSuccess(__('You saved the warehouse.'));
            $this->_redirect('*/*/Edit', ['w_id' => $model->getId()]);

        } catch (\Magento\Framework\Validator\Exception $e) {
            $messages = $e->getMessages();
            $this->messageManager->addMessages($messages);
            $this->redirectToEdit($model, $data);
        } catch (LocalizedException $e) {
            if ($e->getMessage()) {
                $this->messageManager->addError($e->getMessage());
            }
            $this->redirectToEdit($model, $data);
        }
    }

    protected function checkImport($stockId)
    {
        try {
            $adapter = $this->_httpFactory->create();
            if ($adapter->isValid('import_file')) {
                $destinationFolder = $this->_dir->getPath(DirectoryList::VAR_DIR);
                $uploader          = $this->_uploaderFactory->create(array('fileId' => 'import_file'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setAllowedExtensions(['csv', 'txt']);
                $uploader->setFilesDispersion(true);
                $uploader->setAllowCreateFolders(true);
                $result   = $uploader->save($destinationFolder);
                $fullPath = $result['path'] . $result['file'];

                $postValue = $this->getRequest()->getPostValue();
                $separator = isset($postValue['separator']) ? $postValue['separator'] : ';';

                /** @var WarehouseStockImportInfo $dataObject */
                $dataObject = $this->warehouseStockImportInfoFactory->create(
                    $separator, $fullPath, $stockId
                );
                $this->messagePublisher->publish('warehouse_stock.import', $dataObject);
                $this->messageManager->addSuccess(
                    __(
                        'Message is added to queue, wait to get your file soon.'
                        . ' Make sure your cron job is running to import the file'
                    )
                );
            }

        } catch (Exception $ex) {
            //nothing
            $this->messageManager->addError(__('An error occured during import : %1', $ex->getMessage()));
        }

        return false;
    }
}
