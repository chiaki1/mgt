<?php

namespace Coedo\CustomBoostMyShop\Plugin;

use BoostMyShop\UltimateReport\Block\Reports;
use BoostMyShop\UltimateReport\Model\Config;
use Magento\Framework\Module\Manager;

class FixReportBlock
{
    protected $_config;

    /**
     * @var Manager
     */
    protected $moduleManager;

    public function __construct(
        Config $config,
        Manager $moduleManager
    ) {
        $this->_config = $config;
        $this->moduleManager = $moduleManager;
    }

    /**
     * @param Reports                                   $subject
     * @param callable                                  $proceed
     * @param                                           $reportCode
     * @SuppressWarnings(PHPMD)
     */
    public function aroundAddReport(Reports $subject, callable $proceed, $reportCode)
    {
        $reports = $this->_config->getDashboardReports();
        if (in_array($reportCode, $reports) && $this->moduleManager->isOutputEnabled('BoostMyShop_UltimateReport')) {
            return $proceed($reportCode);
        }
    }
}
