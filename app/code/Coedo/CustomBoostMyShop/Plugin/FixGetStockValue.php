<?php

namespace Coedo\CustomBoostMyShop\Plugin;

use BoostMyShop\UltimateReport\Model\Report\Source\Warehouse\StockValue;
use BoostMyShop\UltimateReport\Model\ResourceModel\Report;
use Magento\Framework\Module\Manager;

class FixGetStockValue
{
    /**
     * @var Report
     */
    protected $_reportResource;

    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * FixGetStockValue constructor.
     *
     * @param Report  $reportResource
     * @param Manager $moduleManager
     */
    public function __construct(
        Report $reportResource,
        Manager $moduleManager
    ) {
        $this->_reportResource = $reportResource;
        $this->moduleManager = $moduleManager;
    }

    /**
     * @param StockValue $subject
     * @param callable   $proceed
     * @param            $warehouseId
     * @SuppressWarnings(PHPMD)
     */
    public function aroundGetStockValue(
        StockValue $subject, callable $proceed, $warehouseId
    ) {
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_UltimateReport')) {
            $costAttributeId = 81;

            $sql = $this->_reportResource->getDbConnection()->select()
                                         ->from(array('tbl_warehouse_item' => $this->_reportResource->getTableName('bms_advancedstock_warehouse_item')),
                                                array('value' => 'sum(wi_physical_quantity * value)')
                                         )
                                         ->join($this->_reportResource->getTableName('sequence_product'), 'sequence_product.sequence_value = wi_product_id')
                                         ->join($this->_reportResource->getTableName('catalog_product_entity'), 'sequence_product.sequence_value = catalog_product_entity.entity_id')
                                         ->join($this->_reportResource->getTableName('catalog_product_entity_decimal'), 'catalog_product_entity_decimal.row_id = catalog_product_entity.row_id')
                                         ->where('store_id = 0')
                                         ->where('wi_warehouse_id = ' . $warehouseId)
                                         ->where('attribute_id = ' . $costAttributeId);

            $value = $this->_reportResource->getDbConnection()->fetchOne($sql);
            if ($value < 0)
                $value = 0;
            return $value;
        }
        return $proceed($warehouseId);
    }
}
