<?php

namespace Coedo\CustomBoostMyShop\Plugin;

use BoostMyShop\AdvancedStock\Block\StockMovement\Renderer\Direction;
use Magento\Framework\DataObject;

class AddTransferImage
{
    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * CustomExportButton constructor.
     *
     * @param \Magento\Framework\Module\Manager $moduleManager
     */
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
    }

    /**
     * @param Direction  $subject
     * @param            $result
     * @param DataObject $row
     *
     * @return mixed|string
     */
    public function afterRender(
        Direction $subject, $result, DataObject $row
    ) {
        if (empty($result) && $this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            return '<img src="' . $subject->getViewFileUrl('Coedo_CustomBoostMyShop::images/transfer.png') . '" style="height: 15px; width: 15px; max-width: 15px;" />';
        }
        return $result;
    }
}
