<?php

namespace Coedo\CustomBoostMyShop\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    private $moduleManager;

    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            if (version_compare($context->getVersion(), '1.0.1', '<')) {

                $setup->getConnection()->addColumn(
                    $setup->getTable('bms_advancedstock_stock_movement'),
                    'sm_movement_description',
                    [
                        'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'length'   => 10,
                        'nullable' => true,
                        'comment'  => 'Movement Description'
                    ]
                );
            }
        }

        $setup->endSetup();
    }
}
