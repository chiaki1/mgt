<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\CustomBoostMyShop\Setup\Patch\Data;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class UpdateOrganizerConfig implements DataPatchInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $configWriter;

    private $moduleManager;

    /**
     * UpdateOrganizerConfig constructor.
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param WriterInterface          $configWriter
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        WriterInterface $configWriter,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configWriter    = $configWriter;
        $this->moduleManager   = $moduleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            $categories = [
                '_1516262823992_992' => ['categories' => 'Sales'],
                '_1516262832967_967' => ['categories' => 'Purchasing'],
                '_1516262846095_95'  => ['categories' => 'Marketing'],
                '_1516262854302_302' => ['categories' => 'Customer service'],
            ];

            $statuses = [
                '_1516262823992_992' => ['statuses' => 'New'],
                '_1516262832967_967' => ['statuses' => 'Pending'],
                '_1516262846095_95'  => ['statuses' => 'In progress'],
                '_1516262854302_302' => ['statuses' => 'Done'],
            ];

            $priorities = [
                '_1516262823992_992' => ['priorities' => 'Low'],
                '_1516262832967_967' => ['priorities' => 'Normal'],
                '_1516262846095_95'  => ['priorities' => 'High'],
                '_1516262854302_302' => ['priorities' => 'Blocking'],
            ];
            $this->configWriter->save('organizer/general/categories', json_encode($categories));
            $this->configWriter->save('organizer/general/statuses', json_encode($statuses));
            $this->configWriter->save('organizer/general/priorities', json_encode($priorities));
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }
}

