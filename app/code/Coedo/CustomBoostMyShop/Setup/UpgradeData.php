<?php

namespace Coedo\CustomBoostMyShop\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $moduleManager;

    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            if (version_compare($context->getVersion(), '1.0.1') < 0) {
                $sql1 = 'UPDATE ' . $setup->getTable('bms_advancedstock_stock_movement') . ' SET sm_movement_description = 1 WHERE sm_from_warehouse_id = 0';
                $setup->getConnection()->query($sql1);
                $sql2 = 'UPDATE ' . $setup->getTable('bms_advancedstock_stock_movement') . ' SET sm_movement_description = 2 WHERE sm_to_warehouse_id = 0';
                $setup->getConnection()->query($sql2);
                $sql3 = 'UPDATE ' . $setup->getTable('bms_advancedstock_stock_movement') . ' SET sm_movement_description = 3 WHERE sm_from_warehouse_id != 0 AND sm_to_warehouse_id != 0';
                $setup->getConnection()->query($sql3);
            }
        }

        $setup->endSetup();
    }

}
