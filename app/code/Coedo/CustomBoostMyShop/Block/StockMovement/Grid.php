<?php

namespace Coedo\CustomBoostMyShop\Block\StockMovement;

use BoostMyShop\AdvancedStock\Model\Config;
use BoostMyShop\AdvancedStock\Model\ResourceModel\StockMovement\CollectionFactory as StockMovementCollectionFactory;
use BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory;
use BoostMyShop\AdvancedStock\Model\StockMovement\Category;
use Coedo\CustomBoostMyShop\Model\Config\Source\StockMovementDescription;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Authorization\PolicyInterface;
use Magento\Framework\Registry;
use Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

class Grid extends \BoostMyShop\AdvancedStock\Block\StockMovement\Grid
{
    /**
     * @var StockMovementDescription
     */
    protected $movementDescription;

    /**
     * Grid constructor.
     *
     * @param Context                        $context
     * @param Data                           $backendHelper
     * @param UserCollectionFactory          $userCollectionFactory
     * @param StockMovementCollectionFactory $stockMovementCollectionFactory
     * @param CollectionFactory              $warehouseCollectionFactory
     * @param Category                       $categories
     * @param PolicyInterface                $policyInterface
     * @param Session                        $authSession
     * @param Registry                       $coreRegistry
     * @param Config                         $config
     * @param StockMovementDescription       $movementDescription
     * @param array                          $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        UserCollectionFactory $userCollectionFactory,
        StockMovementCollectionFactory $stockMovementCollectionFactory,
        CollectionFactory $warehouseCollectionFactory,
        Category $categories,
        PolicyInterface $policyInterface,
        Session $authSession,
        Registry $coreRegistry,
        Config $config,
        StockMovementDescription $movementDescription,
        array $data = []
    ) {
        $this->movementDescription = $movementDescription;
        parent::__construct($context, $backendHelper, $userCollectionFactory, $stockMovementCollectionFactory, $warehouseCollectionFactory, $categories, $policyInterface, $authSession, $coreRegistry, $config, $data);
    }

    protected function _prepareColumns()
    {
        $this->addColumn('sm_created_at', ['header' => __('Date'), 'index' => 'sm_created_at', 'type' => 'datetime']);
        $this->addColumn('sku', ['header' => __('Sku'), 'index' => 'sku', 'renderer' => 'BoostMyShop\AdvancedStock\Block\StockMovement\Renderer\Sku']);
        $this->addColumn('name', ['header' => __('Product'), 'index' => 'name']);
        $this->addColumn('sm_from_warehouse_id', ['header' => __('From'), 'align' => 'center', 'index' => 'sm_from_warehouse_id', 'type' => 'options', 'options' => $this->getWarehouseOptions()]);
        $this->addColumn('sm_to_warehouse_id', ['header' => __('To'), 'align' => 'center', 'index' => 'sm_to_warehouse_id', 'type' => 'options', 'options' => $this->getWarehouseOptions()]);
        $this->addColumn('sm_qty', ['header' => __('Qty'), 'align' => 'center', 'type' => 'number', 'index' => 'sm_qty']);
        $this->addColumn('sm_direction', ['header' => __(' '), 'index' => ' ', 'filter' => false, 'align' => 'center', 'sortable' => 'false', 'renderer' => 'BoostMyShop\AdvancedStock\Block\StockMovement\Renderer\Direction']);
        $this->addColumn('sm_movement_description', ['header' => __('Movement'), 'index' => 'sm_movement_description', 'type' => 'options', 'options' => $this->movementDescription->toArray()]);
        $this->addColumn('sm_category', ['header' => __('Category'), 'index' => 'sm_category', 'type' => 'options', 'options' => $this->_categories->toOptionArray()]);
        $this->addColumn('sm_comments', ['header' => __('Comments'), 'index' => 'sm_comments']);
        $this->addColumn('sm_user_id', ['header' => __('Users'), 'index' => 'sm_user_id', 'type' => 'options', 'options' => $this->_getManagerAsOptions()]);
        if ($this->_config->displayAdvancedLog()) {
            $this->addColumn('sm_ui', ['header' => __('UID'), 'index' => 'sm_ui']);
            $this->addColumn('sm_logs', ['header' => __('Logs'), 'index' => 'sm_id', 'filter' => false, 'align' => 'center', 'sortable' => 'false', 'renderer' => 'BoostMyShop\AdvancedStock\Block\StockMovement\Renderer\Logs']);
        }

        if ($this->canDeleteStockMovement())
            $this->addColumn('delete_sm', ['header' => __('Delete'), 'index' => 'sm_id', 'filter' => false, 'align' => 'center', 'sortable' => 'false', 'renderer' => 'BoostMyShop\AdvancedStock\Block\StockMovement\Renderer\Delete']);

        $this->_eventManager->dispatch('bms_advanced_stock_stock_movement_grid_prepare_columns', ['grid' => $this]);

        $this->addExportType('*/*/exportMovementCsv', __('CSV'));
        return Extended::_prepareColumns();
    }

    public function getCsv()
    {
        $csv             = '';
        $this->_isExport = true;
        $this->_prepareGrid();
        $this->getCollection()->getSelect()->limit();
        $this->getCollection()->setPageSize(0);
        $this->getCollection()->load();
        $this->_afterLoadCollection();

        $data = [];
        foreach ($this->getColumns() as $column) {
            if (!$column->getIsSystem() && $column->getId() != 'delete_sm') {
                $data[] = '"' . $column->getExportHeader() . '"';
            }
        }
        $csv .= implode(',', $data) . "\n";

        foreach ($this->getCollection() as $item) {
            $data = [];
            foreach ($this->getColumns() as $column) {
                if (!$column->getIsSystem() && $column->getId() != 'delete_sm') {
                    $data[] = '"' . str_replace(
                            ['"', '\\'],
                            ['""', '\\\\'],
                            $column->getRowFieldExport($item)
                        ) . '"';
                }
            }
            $csv .= implode(',', $data) . "\n";
        }

        if ($this->getCountTotals()) {
            $data = [];
            foreach ($this->getColumns() as $column) {
                if (!$column->getIsSystem() && $column->getId() != 'delete_sm') {
                    $data[] = '"' . str_replace(
                            ['"', '\\'],
                            ['""', '\\\\'],
                            $column->getRowFieldExport($this->getTotals())
                        ) . '"';
                }
            }
            $csv .= implode(',', $data) . "\n";
        }

        return $csv;
    }
}
