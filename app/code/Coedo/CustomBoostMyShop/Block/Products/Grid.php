<?php

namespace Coedo\CustomBoostMyShop\Block\Products;

class Grid extends \BoostMyShop\Erp\Block\Products\Grid
{
    /**
     * @return Grid|\Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {

        $this->addColumn('image', ['header' => __('Image'),'filter' => false, 'sortable' => false, 'type' => 'renderer', 'renderer' => '\BoostMyShop\Erp\Block\Products\Renderer\Image']);
        $this->addColumn('id', ['header' => __('ID'), 'index' => 'entity_id', 'type' => 'number']);
        $this->addColumn('type_id', ['header' => __('Type'), 'index' => 'type_id']);
        $this->addColumn('sku', ['header' => __('Sku'), 'index' => 'sku', 'renderer' => '\BoostMyShop\Erp\Block\Products\Renderer\Sku']);

        if ($this->_orderPreparationConfig->getMpnAttribute())
            $this->addColumn($this->_orderPreparationConfig->getMpnAttribute(), ['header' => __('MPN'), 'index' => $this->_orderPreparationConfig->getMpnAttribute()]);

        if ($this->_config->getBarcodeAttribute())
            $this->addColumn($this->_config->getBarcodeAttribute(), ['header' => __('Barcode'), 'index' => $this->_config->getBarcodeAttribute()]);

        if ($this->_config->getManufacturerAttribute())
            $this->addColumn($this->_config->getManufacturerAttribute(), ['header' => __('Manufacturer'), 'index' => $this->_config->getManufacturerAttribute(), 'type' => 'options', 'options' => $this->getManufacturerOptions()]);


        $this->addColumn('name', ['header' => __('Product'), 'index' => 'name']);
        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status', 'type' => 'options', 'options' => [1 => 'Enabled', 2 => 'Disabled']]);
        $this->addColumn('supply_discontinued', ['header' => __('Discontinued'), 'index' => 'supply_discontinued', 'type' => 'options', 'options' => [0 => __('No'), 1 => __('Yes')]]);
        $this->addColumn('cost', ['header' => __('Cost'), 'index' => 'cost', 'type' => 'number']);

        if($this->_bmsHelper->advancedStockModuleIsInstalled())
            $this->addColumn('stock_details', ['header' => __('Stock details'), 'filter_index' => 'entity_id', 'sortable' => false, 'index' => 'entity_id', 'align' => 'left', 'renderer' => 'BoostMyShop\Erp\Block\Products\Renderer\StockDetails', 'filter' => 'BoostMyShop\AdvancedStock\Block\Widget\Grid\Filter\StockDetails']);
        else
            $this->addColumn('stock_details', ['header' => __('Stock details'), 'filter' => false, 'sortable' => false, 'index' => 'entity_id', 'align' => 'left', 'renderer' => 'BoostMyShop\Erp\Block\Products\Renderer\StockDetails']);

        if ($this->hasMultipleWebsites())
            $this->addColumn('websites', ['header' => __('Websites'), 'index' => 'entity_id', 'sortable' => false, 'align' => 'left', 'renderer' => 'BoostMyShop\Erp\Block\Products\Renderer\Website', 'filter' => 'BoostMyShop\Erp\Block\Products\Filter\Website']);

        //$this->addColumn('sales_history', ['header' => __('Sales History'), 'index' => 'entity_id', 'sortable' => false, 'filter' => false, 'align' => 'center', 'renderer' => 'BoostMyShop\Erp\Block\Renderer\History']);
        $this->addColumn('suppliers', ['header' => __('Suppliers'), 'index' => 'entity_id', 'sortable' => false, 'align' => 'left', 'renderer' => 'BoostMyShop\Supplier\Block\Replenishment\Renderer\Suppliers', 'filter' => 'BoostMyShop\Supplier\Block\Replenishment\Filter\Suppliers']);

        $this->addColumn('old_sku', ['header' => __('Old Sku'), 'index' => 'old_sku']);
        //$this->addColumn('expected_po', ['header' => __('Expected PO'), 'index' => 'entity_id', 'filter' => false, 'sortable' => false, 'align' => 'left', 'renderer' => 'BoostMyShop\Erp\Block\Products\Renderer\ExpectedPo']);

        $this->_eventManager->dispatch('bms_erp_product_grid', ['grid' => $this]);

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareColumns();
    }

    /**
     * @return Grid|\Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('status');
        $collection->addAttributeToSelect('thumbnail');
        $collection->addAttributeToSelect('cost');
        $collection->addAttributeToSelect('supply_discontinued');
        $collection->addAttributeToSelect('old_sku');

        if ($this->_config->getBarcodeAttribute())
            $collection->addAttributeToSelect($this->_config->getBarcodeAttribute());

        if ($this->_config->getManufacturerAttribute())
            $collection->addAttributeToSelect($this->_config->getManufacturerAttribute());

        if ($this->_orderPreparationConfig->getMpnAttribute())
            $collection->addAttributeToSelect($this->_orderPreparationConfig->getMpnAttribute());

        $this->_eventManager->dispatch('bms_erp_product_grid_prepare_collection', ['collection' => $collection]);

        $this->setCollection($collection);

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
    }
}
