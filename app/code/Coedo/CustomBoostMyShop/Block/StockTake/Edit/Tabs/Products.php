<?php

namespace Coedo\CustomBoostMyShop\Block\StockTake\Edit\Tabs;

class Products extends \BoostMyShop\AdvancedStock\Block\StockTake\Edit\Tabs\Products
{
    /**
     * @return Products|\Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'stai_sku',
            [
                'header'   => __('Sku'),
                'index'    => 'stai_sku',
                'renderer' => '\BoostMyShop\AdvancedStock\Block\StockTake\Widget\Grid\Column\Renderer\Edit\Tabs\Products\Sku'
            ]
        );
        $this->addColumn(
            'old_sku',
            [
                'header'   => __('Old Sku'),
                'index'    => 'stai_sku',
                'renderer' => '\Coedo\CustomBoostMyShop\Block\StockTake\Widget\Grid\Column\Renderer\Edit\Tabs\Products\OldSku'
            ]
        );
        $this->addColumn('stai_name', ['header' => __('Name'), 'index' => 'stai_name']);
        $this->addColumn('stai_location', ['header' => __('Location'), 'index' => 'stai_location']);
        $this->addColumn('stai_expected_qty', ['header' => __('Expected Qty'), 'index' => 'stai_expected_qty', 'type' => 'range']);
        $this->addColumn(
            'stai_scanned_qty',
            [
                'header'   => __('Scanned Qty'),
                'index'    => 'stai_scanned_qty',
                'type'     => 'range',
                'renderer' => '\BoostMyShop\AdvancedStock\Block\StockTake\Widget\Grid\Column\Renderer\Edit\Tabs\Products\ScannedQty'
            ]
        );
        $this->addColumn(
            'stai_status',
            [
                'header'  => __('Status'),
                'index'   => 'stai_status',
                'type'    => 'options',
                'options' => $this->_stockTakeItemFactory->create()->getStatuses()
            ]
        );

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareColumns();
    }
}
