<?php

namespace Coedo\CustomBoostMyShop\Block\StockTake\Edit\Tabs;

class ImportExport extends \BoostMyShop\AdvancedStock\Block\StockTake\Edit\Tabs\ImportExport
{
    protected $_template = 'Coedo_CustomBoostMyShop::advanced_stock/stock_take/edit/tab/import_export.phtml';
}
