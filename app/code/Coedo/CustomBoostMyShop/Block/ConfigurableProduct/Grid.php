<?php

namespace Coedo\CustomBoostMyShop\Block\ConfigurableProduct;

use Coedo\CustomBoostMyShop\Model\ResourceModel\ConfigurableProduct\CollectionFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Eav\Model\Config;
use Magento\Framework\DataObject;
use Magento\Framework\Registry;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Grid extends Extended
{
    protected $_warehouseCollectionFactory;
    protected $_coreRegistry;
    protected $_configurableProductCollectionFactory;
    protected $_config;
    protected $_eavConfig;

    /**
     * Grid constructor.
     *
     * @param Context                                                                    $context
     * @param Data                                                                       $backendHelper
     * @param \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory $warehouseCollectionFactory
     * @param CollectionFactory                                                          $configurableProductCollectionFactory
     * @param \BoostMyShop\AdvancedStock\Model\Config                                    $config
     * @param Config                                                                     $eavConfig
     * @param Registry                                                                   $coreRegistry
     * @param array                                                                      $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        \BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\CollectionFactory $warehouseCollectionFactory,
        CollectionFactory $configurableProductCollectionFactory,
        \BoostMyShop\AdvancedStock\Model\Config $config,
        Config $eavConfig,
        Registry $coreRegistry,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
        $this->_warehouseCollectionFactory           = $warehouseCollectionFactory;
        $this->_configurableProductCollectionFactory = $configurableProductCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_config       = $config;
        $this->_eavConfig = $eavConfig;
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('configurableProductStockGrid');
        $this->setDefaultSort('sm_id');
        $this->setDefaultDir('DESC');
        $this->setTitle(__('Configurable Product Stock'));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_configurableProductCollectionFactory->create()
                                                                  ->addFieldToFilter('type_id', ['in' => ['configurable']]);

        if ($this->_config->getManufacturerAttribute())
            $collection->addAttributeToSelect($this->_config->getManufacturerAttribute());

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', ['header' => __('ID'), 'index' => 'entity_id', 'type' => 'number']);
        $this->addColumn('sku', ['header' => __('Sku'), 'index' => 'sku', 'renderer' => '\Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Renderer\Sku']);

        if ($this->_config->getBarcodeAttribute())
            $this->addColumn($this->_config->getBarcodeAttribute(), ['header' => __('Barcode'), 'index' => $this->_config->getBarcodeAttribute()]);

        $this->addColumn('name', ['header' => __('Product'), 'index' => 'name']);

        if ($this->_config->getManufacturerAttribute())
            $this->addColumn($this->_config->getManufacturerAttribute(), ['header' => __('Manufacturer'), 'index' => $this->_config->getManufacturerAttribute(), 'type' => 'options', 'options' => $this->getManufacturerOptions()]);

        $this->addColumn('status', ['header' => __('Status'), 'index' => 'status', 'type' => 'options', 'align' => 'center', 'options' => $this->getStatusesOptions()]);
        $this->addColumn('wi_warehouse_id', ['header' => __('Warehouse'), 'index' => 'wi_warehouse_id', 'align' => 'center', 'type' => 'options', 'options' => $this->getWarehouseOptions()]);
        $this->addColumn('wi_physical_quantity', ['header' => __('Qty in warehouse'), 'type' => 'number', 'align' => 'center', 'index' => 'wi_physical_quantity', 'renderer' => '\Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Renderer\PhysicalQuantity']);
        $this->addColumn('wi_quantity_to_ship', ['header' => __('Qty to ship'), 'type' => 'number', 'align' => 'center', 'index' => 'wi_quantity_to_ship', 'renderer' => '\Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Renderer\QuantityToShip']);
        $this->addColumn('wi_available_quantity', ['header' => __('Available Qty'), 'type' => 'number', 'align' => 'center', 'index' => 'wi_available_quantity', 'renderer' => '\Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Renderer\AvailableQuantity']);

        $this->addExportType('*/*/exportProductsCsv', __('Xlsx'));

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid');
    }

    public function getRowUrl($item)
    {
        //empty to not get link to #
    }

    public function getWarehouseOptions()
    {
        $options     = [];
        $options[''] = ' ';
        foreach ($this->_warehouseCollectionFactory->create()->addActiveFilter() as $item) {
            $options[$item->getId()] = $item->getw_name();
        }
        return $options;
    }

    public function getStatusesOptions()
    {
        $options    = [];
        $options[1] = __('Enabled');
        $options[2] = __('Disabled');
        return $options;
    }

    public function addExportType($url, $label)
    {
        $this->_exportTypes[] = new DataObject(
            ['url' => $this->getUrl($url), 'label' => $label]
        );
        return $this;
    }

    public function getManufacturerOptions()
    {
        $options = array();

        $attribute        = $this->_eavConfig->getAttribute('catalog_product', $this->_config->getManufacturerAttribute());
        $attributeOptions = $attribute->getSource()->getAllOptions();

        foreach ($attributeOptions as $item) {
            $options[$item['value']] = $item['label'];
        }

        return $options;
    }

    /**
     * @return array|string
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function getXlsx()
    {
        $this->_isExport = true;
        $this->_prepareGrid();
        $this->getCollection()->getSelect()->limit();
        $this->getCollection()->setPageSize(0);
        $this->getCollection()->load();
        $this->_afterLoadCollection();

        $exportData = [];
        foreach ($this->getColumns() as $column) {
            if (!$column->getIsSystem()) {
                $exportData[0][] = $column->getExportHeader();
            }
        }

        foreach ($this->getCollection() as $item) {
            $data = [];
            /** @var \Magento\Backend\Block\Widget\Grid\Column $column */
            foreach ($this->getColumns() as $column) {
                if (!$column->getIsSystem()) {
                    if ($column->getData('type') == 'number') {
                        $data[] = !empty($column->getRowFieldExport($item)) ? $column->getRowFieldExport($item) : "0";
                    } else {
                        $data[] = $column->getRowFieldExport($item);
                    }
                }
            }
            $exportData[] = $data;
        }
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($exportData, null, 'A1' );

        $spreadsheet->setActiveSheetIndex(0);

        $name = md5(microtime());
        $file = $this->_path . '/' . $name . '.xlsx';
        $this->_directory->create($this->_path);
        $filepath = $this->_directory->getAbsolutePath($file);
        $writer = new Xlsx($spreadsheet);
        $writer->save($filepath);
        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true  // can delete file after use
        ];
    }
}


