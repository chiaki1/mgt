<?php

namespace Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Renderer;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Coedo\CustomBoostMyShop\Model\ResourceModel\ConfigurableProduct\CollectionFactory;

class PhysicalQuantity extends AbstractRenderer
{

    protected $_linkManagement;

    protected $_collectionFactory;

    public function __construct(
        Context $context,
        LinkManagementInterface $linkManagement,
        CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->_linkManagement = $linkManagement;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    public function render(DataObject $row)
    {
        $sku = $row->getSku();
        $whId = $row->getData('wi_warehouse_id');
        return $this->calculatorQty($sku, $whId) ?: parent::render($row);
    }

    public function renderExport(DataObject $row)
    {
        $sku = $row->getSku();
        $whId = $row->getData('wi_warehouse_id');
        return $this->calculatorQty($sku, $whId);
    }

    public function calculatorQty($sku, $whId)
    {
        $collection = $this->_collectionFactory->create()
                                               ->addFieldToFilter('wi_warehouse_id', $whId)
                                               ->addFieldToFilter('entity_id', ['in' => $this->getChildProductIds($sku)]);
        $total = 0;
        foreach ($collection as $item) {
            $total += $item->getData('wi_physical_quantity');
        }
        return $total;
    }

    public function getChildProductIds($sku)
    {
        $childIds = [];
        $child = $this->_linkManagement->getChildren($sku);
        foreach ($child as $_child) {
            $childIds[] = $_child->getId();
        }
        return $childIds;
    }

}
