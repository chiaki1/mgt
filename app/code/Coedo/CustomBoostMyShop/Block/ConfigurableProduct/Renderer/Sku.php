<?php

namespace Coedo\CustomBoostMyShop\Block\ConfigurableProduct\Renderer;

use BoostMyShop\AdvancedStock\Model\Config;
use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Sku extends AbstractRenderer
{
    protected $_config;

    public function __construct(
        Context $context,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_config = $config;
    }

    public function render(DataObject $row)
    {
        if ($this->_config->isErpIsInstalled())
            $url = $this->getUrl('erp/products/edit', ['id' => $row->getwi_product_id()]);
        else
            $url = $this->getUrl('catalog/product/edit', ['id' => $row->getwi_product_id()]);

        return '<a href="' . $url . '">' . $row->getsku() . '</a>';
    }

    public function renderExport(DataObject $row)
    {
        return $row->getsku();
    }

}
