<?php

namespace Coedo\CustomBoostMyShop\Block\Routing;

class Index extends \BoostMyShop\AdvancedStock\Block\Routing\Index
{
    protected $_template = 'Coedo_CustomBoostMyShop::routing/index.phtml';
}
