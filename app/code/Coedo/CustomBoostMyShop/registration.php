<?php

use Magento\Framework\Component\ComponentRegistrar;

require_once(BP.'/app/library/vendor/autoload.php');

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Coedo_CustomBoostMyShop', __DIR__);

