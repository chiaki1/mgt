<?php

declare(strict_types=1);

namespace Coedo\CustomBoostMyShop\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    public function isModuleOutputEnabled($moduleName = null)
    {
        return parent::isModuleOutputEnabled($moduleName);
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment\Item $item
     *
     * @return mixed
     */
    public function getWarehouse($item)
    {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $flatItem      = $objectManager->create(\BoostMyShop\AdvancedStock\Model\ExtendedSalesFlatOrderItem::class);
            $flatItem->loadByItemId($item->getOrderItemId());
            $warehouseId = $flatItem->getesfoi_warehouse_id();
            return $this->getWarehouseName($warehouseId);
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getWarehouseName($warehouseId)
    {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            return $objectManager->create(\BoostMyShop\AdvancedStock\Model\Warehouse::class)->load($warehouseId)->getw_name();
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param \Magento\Sales\Model\Order\Shipment\Item $item
     *
     * @return mixed
     */
    public function getPackedQty($item)
    {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $item          = $objectManager->create(\BoostMyShop\OrderPreparation\Model\InProgress\Item::class)->load($item->getOrderItemId(), 'ipi_order_item_id');
            return $item->getpacked_qty() ?? 0;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getConfig($path)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return int|void
     */
    public function getAllWarehouseInQuote(\Magento\Quote\Model\Quote $quote)
    {
        $warehouse = [];

        $items = $quote->getAllItems();
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($items as $item) {
            $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
            $warehouseItems = $objectManager->create(\BoostMyShop\AdvancedStock\Model\ResourceModel\Warehouse\Item\Collection::class);
            $warehouseItems->addProductFilter($item->getProduct()->getId());
            $warehouseItem = $warehouseItems->getFirstItem();
            if (!in_array($warehouseItem->getData('wi_warehouse_id'), $warehouse)) {
                $warehouse[] = $warehouseItem->getData('wi_warehouse_id');
            }
        }
        return count($warehouse);
    }
}
