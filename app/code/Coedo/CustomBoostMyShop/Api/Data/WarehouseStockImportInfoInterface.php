<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\CustomBoostMyShop\Api\Data;

/**
 * Basic interface with data needed for import operation.
 *
 * @api
 * @since 100.3.2
 */
interface WarehouseStockImportInfoInterface
{

    /**
     * Return separator
     *
     * @return string
     * @since 100.3.2
     */
    public function getSeparator();

    /**
     * @param $separator
     *
     * @return void
     * @since 100.3.2
     */
    public function setSeparator($separator);

    /**
     * Return import_file
     *
     * @return string
     * @since 100.3.2
     */
    public function getImportFile();

    /**
     * @param $import_file
     *
     * @return void
     * @since 100.3.2
     */
    public function setImportFile($import_file);

    /**
     * Return stock_id
     *
     * @return string
     * @since 100.3.2
     */
    public function getStockId();

    /**
     * @param $stockId
     *
     * @return void
     * @since 100.3.2
     */
    public function setStockId($stockId);

}
