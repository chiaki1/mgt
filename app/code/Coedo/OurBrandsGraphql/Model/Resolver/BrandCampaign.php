<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\OurBrandsGraphql\Model\Resolver;

use Coedo\OurBrands\Model\Banner\FileInfo;
use Coedo\OurBrands\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Model\Config;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Framework\UrlInterface;

class BrandCampaign implements ResolverInterface
{
    private const FILTER_EQUAL_TYPE = 'FilterEqualTypeInput';
    private const FILTER_RANGE_TYPE = 'FilterRangeTypeInput';
    private const FILTER_MATCH_TYPE = 'FilterMatchTypeInput';

    protected $mappingOperatorOptionsEqualType;

    protected $mappingOperatorOptionsRangeType;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var FileInfo
     */
    private $fileInfo;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * @var Config
     */
    protected $_eavConfig;

    /**
     * @param CollectionFactory $collectionFactory
     * @param FileInfo          $fileInfo
     * @param SerializerJson    $serializer
     * @param Config            $eavConfig
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        FileInfo          $fileInfo,
        SerializerJson    $serializer,
        Config            $eavConfig
    ) {
        $this->collectionFactory               = $collectionFactory;
        $this->fileInfo                        = $fileInfo;
        $this->serializer                      = $serializer;
        $this->_eavConfig                      = $eavConfig;
        $this->mappingOperatorOptionsEqualType = [
            '==' => 'eq',
            '()' => 'in',
        ];
        $this->mappingOperatorOptionsRangeType = [
            '>=' => 'from',
            '<=' => 'to',
            '>'  => 'from',
            '<'  => 'to',
        ];
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (isset($value[$field->getName()])) {
            return $value[$field->getName()];
        }
        $store = $context->getExtensionAttributes()->getStore();

        try {
            if (isset($args['filters'])) {
                $filters = $args['filters'];
                if (isset($filters['type'])) {
                    foreach ($filters['type'] as $condition => $value) {
                        $type = $value;
                        break;
                    }
                }
                if (isset($filters['show_home'])) {
                    foreach ($filters['show_home'] as $condition => $value) {
                        $show_home = $value;
                        break;
                    }
                }
            }
            $collection = $this->collectionFactory->create();
            if (isset($type)) {
                if ($type) {
                    $collection->addFieldToFilter('type', 1);
                } else {
                    $collection->addFieldToFilter('type', 0);
                }
            }
            if (isset($show_home)) {
                if ($show_home) {
                    $collection->addFieldToFilter('show_home', 1);
                } else {
                    $collection->addFieldToFilter('show_home', 0);
                }
            }
            $result = [];
            foreach ($collection as $item) {
                $result[] = [
                    "banner_id"            => $item->getBannerId(),
                    "name"                 => $item->getName(),
                    "banner_image"         => $this->getUrl($item->getBannerImage(), $store),
                    "banner_image_listing" => $this->getUrl($item->getBannerImageListing(), $store),
                    "type"                 => $item->getType(),
                    "show_home"            => $item->getShowHome(),
                    "data_filters"         => $this->convertDataFilter($item->getDataFilters())
                ];
            }
            return $result;
        } catch (InputException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        }
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    public function getUrl($image, $store): string
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->fileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->fileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(FileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    private function convertDataFilter($dataFilters)
    {
        try {
            if ($dataFilters) {
                $dataFilters    = $this->serializer->unserialize($dataFilters);
                $newDataFilters = [];
                foreach ($dataFilters as $key => $dataFilter) {
                    if ($dataFilter['type'] == 'Coedo\OurBrands\Model\Rule\Condition\Combine') {
                        unset($dataFilters[$key]);
                        continue;
                    }
                    $newDataFilters[$dataFilter['attribute']][] = $dataFilter;
                }
                $filter = [
                    'filter' => []
                ];
                if (count($newDataFilters) > 0) {
                    $filter      = [];
                    $filterItems = [];
                    foreach ($newDataFilters as $_attributeCode => $newDataFilter) {
                        if ($_attributeCode == "category_ids") {
                            $dataFilterArray            = explode(',', $newDataFilter[0]['value']);
                            $filterItems['category_id'] = [
                                (count($dataFilterArray) > 1 ? "in" : "eq") => (count($dataFilterArray) > 1 ? $this->arrayToFilterArray($dataFilterArray) : ($newDataFilter[0]['value']))
                            ];
                        } elseif ($this->getFilterType($this->_getAttribute($_attributeCode)) == self::FILTER_RANGE_TYPE) {
                            if (count($newDataFilter) > 1) {
                                foreach ($newDataFilter as $k => $item) {
                                    if ($k < (count($newDataFilter) - 1)) {
                                        $filterItems[$_attributeCode] = [
                                            $this->convertOperator($this->_getAttribute($_attributeCode), $item['operator']) => $item['value']
                                        ];
                                    } else {
                                        $filterItems[$_attributeCode] = [
                                            $this->convertOperator($this->_getAttribute($_attributeCode), $item['operator']) => $item['value']
                                        ];
                                    }
                                }
                            } elseif (count($newDataFilter) == 1) {
                                if (isset($this->mappingOperatorOptionsEqualType[$newDataFilter[0]['operator']])) {
                                    $filterItems[$_attributeCode] = [
                                        "from" => $newDataFilter[0]['value'],
                                        "to"   => $newDataFilter[0]['value']
                                    ];
                                } else {
                                    $filterItems[$_attributeCode] = [
                                        $this->convertOperator($this->_getAttribute($_attributeCode), $newDataFilter[0]['operator']) => $newDataFilter[0]['value']
                                    ];
                                }
                            }
                        } elseif ($_attributeCode == "attribute_set_id") {
                            $filterItems["attribute_set_id"] = [
                                "eq" => $newDataFilter[0]['value']
                            ];
                        } else {
                            $dataFilterArray              = explode(',', $newDataFilter[0]['value']);
                            $filterItems[$_attributeCode] = [
                                $this->convertOperator($this->_getAttribute($_attributeCode), $newDataFilter[0]['operator']) => (count($dataFilterArray) > 1 ? $this->arrayToFilterArray($dataFilterArray) : ($newDataFilter[0]['value']))
                            ];
                        }
                    }
                    $filter["filter"] = $filterItems;
                }
                return json_encode($filter);
            }
        } catch (\Exception $e) {
            return '';
        }
    }

    private function arrayToFilterArray($array)
    {
        $result = [];
        foreach ($array as $item) {
            $result[] = trim($item);
        }
        return $result;
    }

    /**
     * @param $operator
     *
     * @return string
     */
    private function convertOperator($attribute, $operator)
    {
        if ($this->getFilterType($attribute) == self::FILTER_MATCH_TYPE) {
            return 'in';
        }
        if ($this->getFilterType($attribute) == self::FILTER_EQUAL_TYPE) {
            if (isset($this->mappingOperatorOptionsEqualType[$operator])) {
                return $this->mappingOperatorOptionsEqualType[$operator];
            }
            return 'eq';
        }
        if ($this->getFilterType($attribute) == self::FILTER_RANGE_TYPE) {
            if (isset($this->mappingOperatorOptionsRangeType[$operator])) {
                return $this->mappingOperatorOptionsRangeType[$operator];
            }
            return 'from';
        }
        return 'eq';
    }

    /**
     * Map attribute type to filter type
     *
     * @param Attribute $attribute
     *
     * @return string
     */
    private function getFilterType(Attribute $attribute): string
    {
        $filterTypeMap = [
            'price'       => self::FILTER_RANGE_TYPE,
            'date'        => self::FILTER_RANGE_TYPE,
            'select'      => self::FILTER_EQUAL_TYPE,
            'multiselect' => self::FILTER_EQUAL_TYPE,
            'boolean'     => self::FILTER_EQUAL_TYPE,
            'text'        => self::FILTER_MATCH_TYPE,
            'textarea'    => self::FILTER_MATCH_TYPE,
        ];

        return $filterTypeMap[$attribute->getFrontendInput()] ?? self::FILTER_MATCH_TYPE;
    }

    private function _getAttribute($attributeCode)
    {
        return $this->_eavConfig->getAttribute(Product::ENTITY, $attributeCode);
    }
}
