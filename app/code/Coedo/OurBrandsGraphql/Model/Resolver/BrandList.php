<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Coedo\OurBrandsGraphql\Model\Resolver;

use Coedo\OurBrands\Model\Banner\FileInfo;
use Coedo\OurBrands\Model\Brand\FileInfo as BrandFileInfo;
use Coedo\OurBrands\Model\BrandFactory;
use Coedo\OurBrands\Model\ResourceModel\Banner\CollectionFactory;
use Coedo\OurBrands\Model\ResourceModel\Brand\CollectionFactory as BrandCollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\CatalogGraphQl\Model\Category\CategoryFilter;
use Magento\CatalogGraphQl\Model\Resolver\Products\DataProvider\CategoryTree;
use Magento\CatalogGraphQl\Model\Resolver\Products\DataProvider\CategoryTree as CategoryTreeDataProvider;
use Magento\CatalogGraphQl\Model\Resolver\Products\DataProvider\ExtractDataFromCategoryTree;
use Magento\Eav\Model\Config;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ArgumentsProcessorInterface;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Framework\UrlInterface;

class BrandList implements ResolverInterface
{
    private const FILTER_EQUAL_TYPE = 'FilterEqualTypeInput';
    private const FILTER_RANGE_TYPE = 'FilterRangeTypeInput';
    private const FILTER_MATCH_TYPE = 'FilterMatchTypeInput';

    private $mappingOperatorOptionsEqualType;

    private $mappingOperatorOptionsRangeType;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var FileInfo
     */
    private $fileInfo;

    /**
     * @var SerializerJson
     */
    private $serializer;

    /**
     * @var Config
     */
    private $_eavConfig;

    /**
     * @var BrandFactory
     */
    private $brandFactory;

    /**
     * @var BrandFileInfo
     */
    private $brandFileInfo;

    /**
     * @var CategoryTree
     */
    private $categoryTree;

    /**
     * @var CategoryFilter
     */
    private $categoryFilter;

    /**
     * @var ExtractDataFromCategoryTree
     */
    private $extractDataFromCategoryTree;

    /**
     * @var ArgumentsProcessorInterface
     */
    private $argsSelection;

    /**
     * @var BrandCollectionFactory
     */
    private $brandCollectionFactory;

    /**
     * @param CollectionFactory           $collectionFactory
     * @param FileInfo                    $fileInfo
     * @param SerializerJson              $serializer
     * @param Config                      $eavConfig
     * @param BrandFactory                $brandFactory
     * @param BrandFileInfo               $brandFileInfo
     * @param CategoryTreeDataProvider    $categoryTree
     * @param ExtractDataFromCategoryTree $extractDataFromCategoryTree
     * @param CategoryFilter              $categoryFilter
     * @param ArgumentsProcessorInterface $argsSelection
     * @param BrandCollectionFactory      $brandCollectionFactory
     */
    public function __construct(
        CollectionFactory           $collectionFactory,
        FileInfo                    $fileInfo,
        SerializerJson              $serializer,
        Config                      $eavConfig,
        BrandFactory                $brandFactory,
        BrandFileInfo               $brandFileInfo,
        CategoryTree                $categoryTree,
        ExtractDataFromCategoryTree $extractDataFromCategoryTree,
        CategoryFilter              $categoryFilter,
        ArgumentsProcessorInterface $argsSelection,
        BrandCollectionFactory      $brandCollectionFactory
    ) {
        $this->collectionFactory               = $collectionFactory;
        $this->fileInfo                        = $fileInfo;
        $this->serializer                      = $serializer;
        $this->_eavConfig                      = $eavConfig;
        $this->brandFactory                    = $brandFactory;
        $this->brandFileInfo                   = $brandFileInfo;
        $this->categoryTree                    = $categoryTree;
        $this->extractDataFromCategoryTree     = $extractDataFromCategoryTree;
        $this->categoryFilter                  = $categoryFilter;
        $this->argsSelection                   = $argsSelection;
        $this->brandCollectionFactory          = $brandCollectionFactory;
        $this->mappingOperatorOptionsEqualType = [
            '==' => 'eq',
            '()' => 'in',
        ];
        $this->mappingOperatorOptionsRangeType = [
            '>=' => 'from',
            '<=' => 'to',
            '>'  => 'from',
            '<'  => 'to',
        ];
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (isset($value[$field->getName()])) {
            return $value[$field->getName()];
        }
        $store = $context->getExtensionAttributes()->getStore();

        try {
            $result     = [];
            $collection = $this->brandCollectionFactory->create();
            foreach ($collection as $brand) {
                $result[] = [
                    "brand_id"     => $brand->getBrandId(),
                    "name"         => $brand->getName(),
                    "code"         => $brand->getCode(),
                    "logo"         => $this->getLogoUrl($brand->getLogo(), $store),
                    "small_logo"   => $this->getLogoUrl($brand->getSmallLogo(), $store),
                    "small_banner" => $this->getLogoUrl($brand->getSmallBanner(), $store),
                    "campaigns"    => $this->getCampaignsBanner(1, $brand->getBrandId(), $store),
                    "banners"      => $this->getCampaignsBanner(0, $brand->getBrandId(), $store),
                    "category_ids" => !empty($brand->getCategoryIds()) ? explode(',', $brand->getCategoryIds()) : [],
                    "categories"   => $this->getCategories($brand->getCategoryIds(), $store, $info, $context)
                ];
            }
            return $result;
        } catch (InputException $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        }
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    public function getUrl($image, $store): string
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->fileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->fileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(FileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    public function getLogoUrl($image, $store): string
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->brandFileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->brandFileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(BrandFileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    private function convertDataFilter($dataFilters)
    {
        try {
            if ($dataFilters) {
                $dataFilters    = $this->serializer->unserialize($dataFilters);
                $newDataFilters = [];
                foreach ($dataFilters as $key => $dataFilter) {
                    if ($dataFilter['type'] == 'Coedo\OurBrands\Model\Rule\Condition\Combine') {
                        unset($dataFilters[$key]);
                        continue;
                    }
                    $newDataFilters[$dataFilter['attribute']][] = $dataFilter;
                }
                $filter = [
                    'filter' => []
                ];
                if (count($newDataFilters) > 0) {
                    $filter      = [];
                    $filterItems = [];
                    foreach ($newDataFilters as $_attributeCode => $newDataFilter) {
                        if ($_attributeCode == "category_ids") {
                            $dataFilterArray            = explode(',', $newDataFilter[0]['value']);
                            $filterItems['category_id'] = [
                                (count($dataFilterArray) > 1 ? "in" : "eq") => (count($dataFilterArray) > 1 ? $this->arrayToFilterArray($dataFilterArray) : ($newDataFilter[0]['value']))
                            ];
                        } elseif ($this->getFilterType($this->_getAttribute($_attributeCode)) == self::FILTER_RANGE_TYPE) {
                            if (count($newDataFilter) > 1) {
                                foreach ($newDataFilter as $k => $item) {
                                    if ($k < (count($newDataFilter) - 1)) {
                                        $filterItems[$_attributeCode] = [
                                            $this->convertOperator($this->_getAttribute($_attributeCode), $item['operator']) => $item['value']
                                        ];
                                    } else {
                                        $filterItems[$_attributeCode] = [
                                            $this->convertOperator($this->_getAttribute($_attributeCode), $item['operator']) => $item['value']
                                        ];
                                    }
                                }
                            } elseif (count($newDataFilter) == 1) {
                                if (isset($this->mappingOperatorOptionsEqualType[$newDataFilter[0]['operator']])) {
                                    $filterItems[$_attributeCode] = [
                                        "from" => $newDataFilter[0]['value'],
                                        "to"   => $newDataFilter[0]['value']
                                    ];
                                } else {
                                    $filterItems[$_attributeCode] = [
                                        $this->convertOperator($this->_getAttribute($_attributeCode), $newDataFilter[0]['operator']) => $newDataFilter[0]['value']
                                    ];
                                }
                            }
                        } elseif ($_attributeCode == "attribute_set_id") {
                            $filterItems["attribute_set_id"] = [
                                "eq" => $newDataFilter[0]['value']
                            ];
                        } else {
                            $dataFilterArray              = explode(',', $newDataFilter[0]['value']);
                            $filterItems[$_attributeCode] = [
                                $this->convertOperator($this->_getAttribute($_attributeCode), $newDataFilter[0]['operator']) => (count($dataFilterArray) > 1 ? $this->arrayToFilterArray($dataFilterArray) : ($newDataFilter[0]['value']))
                            ];
                        }
                    }
                    $filter["filter"] = $filterItems;
                }
                return json_encode($filter);
            }
        } catch (\Exception $e) {
            return '';
        }
    }

    private function arrayToFilterArray($array)
    {
        $result = [];
        foreach ($array as $item) {
            $result[] = trim($item);
        }
        return $result;
    }

    /**
     * @param $operator
     *
     * @return string
     */
    private function convertOperator($attribute, $operator)
    {
        if ($this->getFilterType($attribute) == self::FILTER_MATCH_TYPE) {
            return 'in';
        }
        if ($this->getFilterType($attribute) == self::FILTER_EQUAL_TYPE) {
            if (isset($this->mappingOperatorOptionsEqualType[$operator])) {
                return $this->mappingOperatorOptionsEqualType[$operator];
            }
            return 'eq';
        }
        if ($this->getFilterType($attribute) == self::FILTER_RANGE_TYPE) {
            if (isset($this->mappingOperatorOptionsRangeType[$operator])) {
                return $this->mappingOperatorOptionsRangeType[$operator];
            }
            return 'from';
        }
        return 'eq';
    }

    /**
     * Map attribute type to filter type
     *
     * @param Attribute $attribute
     *
     * @return string
     */
    private function getFilterType(Attribute $attribute): string
    {
        $filterTypeMap = [
            'price'       => self::FILTER_RANGE_TYPE,
            'date'        => self::FILTER_RANGE_TYPE,
            'select'      => self::FILTER_EQUAL_TYPE,
            'multiselect' => self::FILTER_EQUAL_TYPE,
            'boolean'     => self::FILTER_EQUAL_TYPE,
            'text'        => self::FILTER_MATCH_TYPE,
            'textarea'    => self::FILTER_MATCH_TYPE,
        ];

        return $filterTypeMap[$attribute->getFrontendInput()] ?? self::FILTER_MATCH_TYPE;
    }

    private function _getAttribute($attributeCode)
    {
        return $this->_eavConfig->getAttribute(Product::ENTITY, $attributeCode);
    }

    private function getCampaignsBanner($type, $brandId, $store)
    {
        $campaigns  = [];
        $constraint = 'relationship.brand_id=' . $brandId;
        $collection = $this->collectionFactory->create();
        $collection->getSelect()->joinRight(
            ['relationship' => $collection->getTable("brands_banner_relationship")],
            'relationship.banner_id = main_table.banner_id AND ' . $constraint,
            ['position']
        );
        $collection->addFieldToFilter('type', $type);
        $collection->addFieldToFilter('show_home', 0);
        foreach ($collection as $item) {
            $campaigns[] = [
                "banner_id"            => $item->getBannerId(),
                "name"                 => $item->getName(),
                "banner_image"         => $this->getUrl($item->getBannerImage(), $store),
                "banner_image_listing" => $this->getUrl($item->getBannerImageListing(), $store),
                "data_filters"         => $this->convertDataFilter($item->getDataFilters()),
                "position"             => $item->getPosition()
            ];
        }
        return $campaigns;
    }

    private function getCategories($category_ids, $store, $info, $context)
    {
        if ($category_ids) {
            $category_ids           = explode(',', $category_ids);
            $args['filters']['ids'] = ['in' => $category_ids];
            try {
                $processedArgs = $this->argsSelection->process($info->fieldName, $args);
                $filterResults = $this->categoryFilter->getResult($processedArgs, $store, [], $context);

                $rootCategoryIds = $filterResults['category_ids'];
            } catch (InputException $e) {
                return [];
            }

            return $this->fetchCategories($rootCategoryIds, $info);
        }
        return [];
    }

    /**
     * Fetch category tree data
     *
     * @param array       $categoryIds
     * @param ResolveInfo $info
     *
     * @return array
     */
    private function fetchCategories(array $categoryIds, ResolveInfo $info)
    {
        $fetchedCategories = [];
        foreach ($categoryIds as $categoryId) {
            $categoryTree = $this->categoryTree->getTree($info, $categoryId);
            if (empty($categoryTree)) {
                continue;
            }
            $fetchedCategories[] = current($this->extractDataFromCategoryTree->execute($categoryTree));
        }

        return $fetchedCategories;
    }
}
