<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Model\ResourceModel;

/**
 * Resource Model Tag.
 *
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Tag extends \VNG\Storelocator\Model\ResourceModel\AbstractDbManageStores
{
    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(\VNG\Storelocator\Setup\InstallSchema::SCHEMA_TAG, 'tag_id');
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreRelationTable()
    {
        return $this->getTable(\VNG\Storelocator\Setup\InstallSchema::SCHEMA_STORE_TAG);
    }
}
