<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Model;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Factory
{
    /**#@+
     * Allowed object types
     */
    const MODEL_SPECIALDAY = 'specialday';
    const MODEL_HOLIDAY = 'holiday';
    const MODEL_TAG = 'tag';
    const MODEL_SCHEDULE = 'schedule';

    /**
     * Map of types which are references to classes.
     *
     * @var array
     */
    protected $_typeMap = [
        self::MODEL_SPECIALDAY => 'VNG\Storelocator\Model\Specialday',
        self::MODEL_HOLIDAY => 'VNG\Storelocator\Model\Holiday',
        self::MODEL_TAG => 'VNG\Storelocator\Model\Tag',
        self::MODEL_SCHEDULE => 'VNG\Storelocator\Model\Schedule',
    ];

    /**
     * @var ObjectManagerInterface
     */
    private $_objectManager;

    /**
     * Constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param array                  $typeMap
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $typeMap = []
    ) {
        $this->_objectManager = $objectManager;
        $this->mergeTypes($typeMap);
    }

    /**
     * Add or override object types.
     *
     * @param array $typeMap
     */
    protected function mergeTypes(array $typeMap)
    {
        foreach ($typeMap as $typeInfo) {
            if (isset($typeInfo['type']) && isset($typeInfo['class'])) {
                $this->_typeMap[$typeInfo['type']] = $typeInfo['class'];
            }
        }
    }

    /**
     * @param $type
     * @param array $arguments
     *
     * @return mixed
     */
    public function create($type, array $arguments = [])
    {
        if (empty($this->_typeMap[$type])) {
            throw new \InvalidArgumentException('"' . $type . ': isn\'t allowed');
        }

        $instance = $this->_objectManager->create($this->_typeMap[$type], $arguments);
        if (!$instance instanceof \VNG\Storelocator\Model\AbstractModelManageStores) {
            throw new \InvalidArgumentException(
                get_class($instance)
                . ' isn\'t instance of \VNG\Storelocator\Model\AbstractModelManageStores'
            );
        }

        return $instance;
    }
}
