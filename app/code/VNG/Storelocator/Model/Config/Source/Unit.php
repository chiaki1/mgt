<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Model\Config\Source;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Unit implements \Magento\Framework\Option\ArrayInterface
{
    const UNIT_MILES = 0;
    const UNIT_KILOMETER = 1;

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::UNIT_MILES, 'label' => __('Miles')],
            ['value' => self::UNIT_KILOMETER, 'label' => __('Kilometers')],
        ];
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::UNIT_MILES => __('Miles'),
            self::UNIT_KILOMETER => __('Kilometers'),
        ];
    }
}
