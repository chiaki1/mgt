<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Model\Config\Comment;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Facebook extends \VNG\Storelocator\Model\Config\Comment\AbstractComment
{
    /**
     * Retrieve element comment by element value.
     *
     * @param string $elementValue
     *
     * @return string
     */
    public function getCommentText($elementValue)
    {
        return __(
            'To register a Facebook API key, please follow the guide <a href="%1">here</a>',
            $this->_url->getUrl('storelocatoradmin/guide')
        );
    }
}
