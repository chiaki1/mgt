<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Model;

/**
 * Model Specialday.
 *
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Specialday extends \VNG\Storelocator\Model\AbstractModelManageStores
{
    /**
     * Model construct that should be used for object initialization.
     */
    public function _construct()
    {
        $this->_init('VNG\Storelocator\Model\ResourceModel\Specialday');
    }

    /**
     * Processing object before save data.
     */
    public function beforeSave()
    {
        $this->_prepareSaveWorkingTime();

        return parent::beforeSave();
    }

    /*
     * prepare save working time of specialday
     */
    protected function _prepareSaveWorkingTime()
    {
        if (is_array($this->getData('time_open'))) {
            $this->setData('time_open', implode(':', $this->getData('time_open')));
        }

        if (is_array($this->getData('time_close'))) {
            $this->setData('time_close', implode(':', $this->getData('time_close')));
        }
    }
}
