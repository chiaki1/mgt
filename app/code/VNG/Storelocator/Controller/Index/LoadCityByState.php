<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class LoadCityByState extends \VNG\Storelocator\Controller\Index
{

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {

        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $response->setHeader('Content-type', 'text/plain');

        $regionId = $this->getRequest()->getParam('regionId');

        $listData = [['label' => __('Quận/huyện'), 'value' => '']];
        if ($regionId) {
            $cityCollection = $this->_city->create()->getCollection();
            $cityCollection->addFieldToFilter('region_id', ['eq' => $regionId]);
            foreach ($cityCollection as $city) {
                $listData[] = [
                    'label' => $city->getName(),
                    'value' => $city->getName(),
                ];
            }
        }

        $response->setContents(
            $this->_jsonHelper->jsonEncode(
                [
                    'districts' => $listData,
                ]
            )
        );

        return $response;
    }


}
