<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Controller\Index;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class View extends \VNG\Storelocator\Controller\Index
{
    /**
     * Execute action.
     */
    public function execute()
    {
        if (!$this->_systemConfig->isEnableFrontend()) {
            return $this->_getResultRedirectNoroute();
        }

        $storelocatorId = $this->getRequest()->getParam('storelocator_id');

        /** @var \VNG\Storelocator\Model\Store $store */
        $store = $this->_objectManager->create('VNG\Storelocator\Model\Store')->load($storelocatorId);

        if (!$store->getId() || !$store->isEnabled()) {
            return $this->_getResultRedirectNoroute();
        }

        /*
         * load base image of store
         */

        $this->_coreRegistry->register('storelocator_store', $store);

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->_initResultPage();
        $resultPage->getConfig()->getTitle()->set($store->getMetaTitle());
        $resultPage->getConfig()->setDescription($store->getMetaDescription());
        $resultPage->getConfig()->setKeywords($store->getMetaKeywords());

        return $resultPage;
    }
}
