<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use VNG\Storelocator\Model\Config\Source\OrderTypeStore;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class LoadStoreByState extends \VNG\Storelocator\Controller\Index
{
    /**
     * Default current page.
     */
    const DEFAULT_CURRENT_PAGINATION = 1;

    /**
     * Default range pagination.
     */
    const DEFAULT_RANGE_PAGINATION = 5;

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        /** @var \VNG\Storelocator\Model\ResourceModel\Store\Collection $collection */
        $collection = $this->_filterStoreCollection($this->_storeCollectionFactory->create());


        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $response->setHeader('Content-type', 'text/plain');
        $regionId = $this->getRequest()->getParam('state');

        $listData = [['label' => __('Quận/huyện'), 'value' => '']];
        if ($regionId) {
            $cityCollection = $this->_city->create()->getCollection();
            $cityCollection->addFieldToFilter('region_id', ['eq' => $regionId]);
            foreach ($cityCollection as $city) {
                $listData[] = [
                    'label' => $city->getName(),
                    'value' => $city->getName(),
                ];
            }
        }

        $response->setContents(
            $this->_jsonHelper->jsonEncode(
                [
                    'storesjson' => $collection->prepareJson(),
                    'num_store' => $collection->getSize(),
                    'districts' => $listData,
                ]
            )
        );
        return $response;
    }

    /**
     * filter store.
     *
     * @param \VNG\Storelocator\Model\ResourceModel\Store\Collection $collection
     *
     * @return \VNG\Storelocator\Model\ResourceModel\Store\Collection
     */
    protected function _filterStoreCollection(
        \VNG\Storelocator\Model\ResourceModel\Store\Collection $collection
    ) {
        $collection->addFieldToSelect([
            'store_name',
            'status',
            'address',
            'state_id',
            'city',
        ]);

        if ($productId = $this->getRequest()->getParam('productId')) {
            $collection->getSelect()
                ->joinInner(['store_product' => \VNG\Storelocator\Setup\UpgradeSchema::SCHEMA_STORE_PRODUCT],
                    'main_table.storelocator_id = store_product.storelocator_id', '')
                ->where('store_product.product_id = ?', $productId);
        }
        /*
         * Filter store enabled
         */
        $collection->addFieldToFilter('status', \VNG\Storelocator\Model\Status::STATUS_ENABLED);

        if ($state = $this->getRequest()->getParam('state')) {
            $collection->addFieldToFilter('state_id', $state);
        }
        if ($city = $this->getRequest()->getParam('city')) {
            $collection->addFieldToFilter('city', ['like' => "%$city%"]);
        }
        // Allow load base image for each store
        $collection->setLoadBaseImage(true);

        return $collection;
    }
}
