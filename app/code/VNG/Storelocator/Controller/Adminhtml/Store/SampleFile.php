<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Shopbybrand
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Controller\Adminhtml\Store;

use Magento\Framework\App\Filesystem\DirectoryList;

/**
 *
 *
 * @category VNG
 * @package  VNG_Shopbybrand
 * @module   Pdfinvoiceplus
 * @author   VNG Developer
 */
class SampleFile extends \VNG\Storelocator\Controller\Adminhtml\Store
{
    /**
     * Execute action
     */
    public function execute()
    {
        $fileName = 'storelocator.csv';

        /** @var \Magento\Framework\App\Response\Http\FileFactory $fileFactory */
        $fileFactory = $this->_objectManager->get('Magento\Framework\App\Response\Http\FileFactory');

        return $fileFactory->create(
            $fileName,
            $this->getStorelocatorSampleData(),
            DirectoryList::VAR_DIR
        );
    }

    public function getStorelocatorSampleData()
    {
        /** @var \Magento\Framework\Module\Dir $moduleReader */
        $moduleReader = $this->_objectManager->get('Magento\Framework\Module\Dir');
        /** @var \Magento\Framework\Filesystem\DriverPool $drivePool */
        $drivePool = $this->_objectManager->get('Magento\Framework\Filesystem\DriverPool');
        $drive = $drivePool->getDriver(\Magento\Framework\Filesystem\DriverPool::FILE);

        return $drive->fileGetContents($moduleReader->getDir('VNG_Storelocator')
            . DIRECTORY_SEPARATOR . '_fixtures' . DIRECTORY_SEPARATOR . 'storelocator.csv');
    }
}
