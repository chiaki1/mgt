<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Controller;

use Magento\Framework\Controller\ResultFactory;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
abstract class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \VNG\Storelocator\Model\SystemConfig
     */
    protected $_systemConfig;

    /**
     * @var \VNG\Storelocator\Model\ResourceModel\Store\CollectionFactory
     */
    protected $_storeCollectionFactory;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Action constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \VNG\Storelocator\Model\SystemConfig $systemConfig,
        \VNG\Storelocator\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Directory\Model\Region $region
    )
    {
        parent::__construct($context);
        $this->_systemConfig = $systemConfig;
        $this->_storeCollectionFactory = $storeCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_jsonHelper = $jsonHelper;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    protected function _getResultRedirectNoroute()
    {
        /* @var \Magento\Framework\Controller\Result\Redirect $resultLayout */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('cms/noroute');

        return $resultRedirect;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    protected function _initResultPage()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__($this->_systemConfig->getPageTitpe()));

        return $resultPage;
    }
}
