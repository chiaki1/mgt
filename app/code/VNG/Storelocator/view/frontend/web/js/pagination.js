/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */
define(["jquery", "jquery/ui"], function($) {
  $.widget("vng.pagination", {
    options: {},
    _create: function() {
      var self = this,
        options = this.options;
      self.addChangePageEvent();
    },

    addChangePageEvent: function() {
      var self = this,
        options = this.options;
      $(self.element).on("click", ".page-item:not(.disabled)", function() {
        if ($(this).data("first-page")) {
          $(self.element).trigger("changePage", {
            newPage: $(this).data("first-page")
          });
        } else if ($(this).data("last-page")) {
          $(self.element).trigger("changePage", {
            newPage: $(this).data("last-page")
          });
        } else if (!$(this).hasClass("active")) {
          $(self.element).trigger("changePage", {
            newPage: $(this).data("page-id")
          });
        }
      });
    },

    getCurPage: function() {
      return $(this)
        .find(".page-item.active")
        .data("page-id");
    }
  });

  return $.vng.pagination;
});
