/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */
define([
    'jquery',
    'VNG_Storelocator/js/utilities',
    'VNG_Storelocator/js/weekdaytime'
], function($, MAGE_UTIL, WeekdayTime) {
    var openTime = new WeekdayTime(
            '#specialday_time_open_hour',
            '#specialday_time_open_minute'
        ),
        closeTime = new WeekdayTime(
            '#specialday_time_close_hour',
            '#specialday_time_close_minute'
        );

    openTime.callbackChange = function() {
        if(openTime.getStringTime() > closeTime.getStringTime()) {
            closeTime.setWeekdayTime(openTime);
        }
    }

    closeTime.callbackChange = function() {
        if(closeTime.getStringTime() < openTime.getStringTime()) {
            openTime.setWeekdayTime(closeTime);
        }
    }
});
