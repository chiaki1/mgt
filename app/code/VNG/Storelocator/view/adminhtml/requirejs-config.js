/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */
var config = {
    map: {
       '*': {
            'vng/weekdaytime' : 'VNG_Storelocator/js/weekdaytime',
            'vng/utilities' : 'VNG_Storelocator/js/utilities',
            'vng/schedule' : 'VNG_Storelocator/js/form/schedule',
            'vng/specialday' : 'VNG_Storelocator/js/form/specialday',
            'vng/map': 'VNG_Storelocator/js/store/map',
            'vng/baseimage': 'VNG_Storelocator/js/gallery/base-image-uploader',
       }
    },
    paths: {
    },
};
