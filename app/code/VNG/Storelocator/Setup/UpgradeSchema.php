<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use VNG\Storelocator\Setup\InstallSchema as StorelocatorShema;

/**
 *
 *
 * @category VNG
 * @package  VNG_Pdfinvoiceplus
 * @module   Pdfinvoiceplus
 * @author   VNG Developer
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    const SCHEMA_STORE = 'vng_storelocator_store';
    const SCHEMA_STORE_PRODUCT = 'vng_storelocator_store_product';
    const SCHEMA_PRODUCT = 'catalog_product_entity';

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->changeColumnImage($setup);
        }
        if (version_compare($context->getVersion(), '1.0.2', '<')) {

            /* Add new columns to table vng_storelocator_store */
            if ($setup->tableExists('vng_storelocator_store')) {

                $targetNewColumn = [
                    'state_id' => Table::TYPE_INTEGER,
                    'city_id' => Table::TYPE_INTEGER,
                    'subdistrict' => Table::TYPE_TEXT,
                    'subdistrict_id' => Table::TYPE_INTEGER
                ];

                foreach ($targetNewColumn as $_colName => $_type) {
                    $setup->getConnection()->addColumn(
                        $setup->getTable('vng_storelocator_store'),
                        $_colName,
                        [
                            'type' => $_type,
                            'nullable' => true,
                            'default' => null,
                            'comment' => $_colName
                        ]
                    );
                }
            }

        }

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->createStoreProductLink($setup);
        }

        $installer->endSetup();
    }

    /**
     *
     * rename column storelocator_id in table vng_storelocator_image to locator_id
     *
     * @param SchemaSetupInterface $setup
     */
    public function changeColumnImage(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->dropForeignKey(
            $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
            $setup->getFkName(
                StorelocatorShema::SCHEMA_IMAGE,
                'storelocator_id',
                StorelocatorShema::SCHEMA_STORE,
                'storelocator_id'
            )
        );

        $setup->getConnection()->dropIndex(
            $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
            $setup->getIdxName(
                $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
                ['storelocator_id'],
                AdapterInterface::INDEX_TYPE_INDEX
            )
        );

        $setup->getConnection()->changeColumn(
            $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
            'storelocator_id',
            'locator_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => null,
                'comment' => 'Storelocator Id',
                'unsigned' => true
            ]
        );

        $setup->getConnection()->addIndex(
            $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
            $setup->getIdxName(
                $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
                ['locator_id'],
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['locator_id'],
            AdapterInterface::INDEX_TYPE_INDEX
        );

        $setup->getConnection()->addForeignKey(
            $setup->getFkName(
                StorelocatorShema::SCHEMA_IMAGE,
                'locator_id',
                StorelocatorShema::SCHEMA_STORE,
                'storelocator_id'
            ),
            $setup->getTable(StorelocatorShema::SCHEMA_IMAGE),
            'locator_id',
            $setup->getTable(StorelocatorShema::SCHEMA_STORE),
            'storelocator_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

    }

    public function createStoreProductLink(SchemaSetupInterface $installer)
    {

        /*
         * Create table vng_storelocator_store_product
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable(self::SCHEMA_STORE_PRODUCT)
        )->addColumn(
            'storelocator_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Storelocator Id'
        )->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Product ID'
        )->addIndex(
            $installer->getIdxName(
                $installer->getTable(self::SCHEMA_STORE_PRODUCT),
                ['product_id'],
                AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['product_id'],
            ['type' => AdapterInterface::INDEX_TYPE_INDEX]
        )->setComment(
            'Store Tag Table'
        );

        $installer->getConnection()->createTable($table);

    }
}