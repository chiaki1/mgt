<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Block\ListStore;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class SearchBox extends \VNG\Storelocator\Block\AbstractBlock
{
    protected $_template = 'VNG_Storelocator::liststore/searchbox.phtml';

    /**
     * @var \Magento\Config\Model\Config\Source\Locale\Country
     */
    protected $_localCountry;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $_directoryHelper;

    /**
     * Block constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array                                            $data
     */
    public function __construct(
        \VNG\Storelocator\Block\Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Config\Model\Config\Source\Locale\Country $localCountry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_directoryHelper = $directoryHelper;
        $this->_localCountry = $localCountry;
    }

    /**
     * @return string
     */
    public function getRegionJson()
    {
        return $this->_directoryHelper->getRegionJson();
    }

    /**
     * get tag icon.
     *
     * @param \VNG\Storelocator\Model\Tag $tag
     *
     * @return string
     */
    public function getTagIcon(\VNG\Storelocator\Model\Tag $tag)
    {
        return $tag->getTagIcon() ? $this->_imageHelper->getMediaUrlImage($tag->getTagIcon())
        : $this->getViewFileUrl('VNG_Storelocator::images/Hospital_icon.png');
    }

    /**
     * @param \VNG\Storelocator\Model\Tag $tag
     *
     * @return string
     */
    public function getTagHtml(\VNG\Storelocator\Model\Tag $tag)
    {
        $tagFormat = '<li data-tag-id="%s" class="tag-icon icon-filter text-center">';
        $tagFormat .= '<img src="%s" class="img-responsive"/><p>%s</p></li>';

        return sprintf($tagFormat, $tag->getId(), $this->getTagIcon($tag), $tag->getTagName());
    }

    /**
     * @return array
     */
    public function getCountryOption()
    {
        return $this->_localCountry->toOptionArray();
    }
}
