<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Block;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class AbstractBlock extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \VNG\Storelocator\Model\SystemConfig
     */
    protected $_systemConfig;

    /**
     * @var \VNG\Storelocator\Helper\Image
     */
    protected $_imageHelper;

    /**
     * @var \VNG\Storelocator\Model\ResourceModel\Store\CollectionFactory
     */
    protected $_storeCollectionFactory;

    /**
     * @var \VNG\Storelocator\Model\ResourceModel\Tag\CollectionFactory
     */
    protected $_tagCollectionFactory;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Url                      $customerUrl
     * @param array                                            $data
     */
    public function __construct(
        \VNG\Storelocator\Block\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_systemConfig = $context->getSystemConfig();
        $this->_imageHelper = $context->getImageHelper();
        $this->_storeCollectionFactory = $context->getStoreCollectionFactory();
        $this->_tagCollectionFactory = $context->getTagCollectionFactory();
        $this->_coreRegistry = $context->getCoreRegistry();
    }

    /**
     * @return \VNG\Storelocator\Model\SystemConfig
     */
    public function getSystemConfig()
    {
        return $this->_systemConfig;
    }

    /**
     * Render block HTML.
     *
     * @return string
     */
    protected function _toHtml()
    {
        return $this->_systemConfig->isEnableFrontend() ? parent::_toHtml() : '';
    }

    /**
     * @return \VNG\Storelocator\Model\ResourceModel\Store\Collection
     */
    public function getStoreCollection()
    {
        return $this->_storeCollectionFactory->create();
    }

    /**
     * @return \VNG\Storelocator\Model\ResourceModel\Tag\Collection
     */
    public function getTagCollection()
    {
        return $this->_tagCollectionFactory->create();
    }

    public function getMediaUrlImage($imagePath = '')
    {
        return $this->_imageHelper->getMediaUrlImage($imagePath);
    }
}
