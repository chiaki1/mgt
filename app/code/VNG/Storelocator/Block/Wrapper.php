<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Block;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Wrapper extends \VNG\Storelocator\Block\AbstractBlock
{
    protected $_template = 'VNG_Storelocator::wrapper.phtml';

    /**
     * Block constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array                                            $data
     */
    public function __construct(
        \VNG\Storelocator\Block\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        $this->addChild('storelocator.mapbox', 'VNG\Storelocator\Block\ListStore\MapBox');
        $this->addChild('storelocator.searchbox', 'VNG\Storelocator\Block\ListStore\SearchBox');
        $this->addChild('storelocator.liststorebox', 'VNG\Storelocator\Block\ListStore\ListStoreBox');

        return parent::_prepareLayout();
    }
}
