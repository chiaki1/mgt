<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Block\Adminhtml\Widget\Grid\Column;

/**
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
abstract class AbstractCheckboxes extends \Magento\Backend\Block\Widget\Grid\Column
{
    /**
     * @var \VNG\Storelocator\Helper\Data
     */
    protected $_storelocatorHelper;

    /**
     * @var \VNG\Storelocator\Model\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \VNG\Storelocator\Helper\Data $storelocatorHelper,
        \VNG\Storelocator\Model\StoreFactory $storeFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_storelocatorHelper = $storelocatorHelper;
        $this->_storeFactory = $storeFactory;

        $this->_filterTypes['checkbox'] = 'VNG\Storelocator\Block\Adminhtml\Widget\Grid\Column\Filter\Checkbox';
    }

    /**
     * values.
     *
     * @return mixed
     */
    public function getValues()
    {
        if (!$this->hasData('values')) {
            $this->setData('values', $this->getSelectedValues());
        }

        return $this->getData('values');
    }

    /**
     * get selected rows.
     *
     * @return array
     */
    abstract public function getSelectedValues();
}
