<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 *
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */
namespace VNG\Storelocator\Block\Adminhtml\Widget\Grid\Column;

use VNG\Storelocator\Block\Adminhtml\Widget\Grid\Column\AbstractCheckboxes;

/**
 * @category VNG
 * @module   Storelocator
 *
 * @author   VNG Developer
 */
class StoreCheckboxes extends AbstractCheckboxes
{
    /**
     * {@inheritdoc}
     */
    public function getSelectedValues()
    {
        return $this->_storelocatorHelper->getTreeSelectedStores();
    }
}
