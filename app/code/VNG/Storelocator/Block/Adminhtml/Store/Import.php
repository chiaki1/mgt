<?php
/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Giftvoucher
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */
namespace VNG\Storelocator\Block\Adminhtml\Store;

/**
 * Adminhtml Giftvoucher Import Block
 *
 * @category VNG
 * @package  VNG_Giftvoucher
 * @module   Giftvoucher
 * @author   VNG Developer
 */
class Import extends \Magento\Backend\Block\Widget\Form\Container
{

    public function _construct()
    {
        parent::_construct();
        $this->_blockGroup = 'VNG_Storelocator';
        $this->_controller = 'adminhtml_store';
        $this->_mode = 'import';
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
        $this->updateButton('save', 'label', __('Import Stores'));
    }
}
