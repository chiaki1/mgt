<?php

/**
 * VNG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Block\Adminhtml\Store\Edit\Tab\GmapTab;

/**
 * Class City
 * @package VNG\Storelocator\Block\Adminhtml\Store\Edit\Tab\GmapTab
 */
class City extends \Magento\Backend\Block\Template
{
    protected $_template = 'VNG_Storelocator::store/city.phtml';

    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $_directoryHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Directory\Helper\Data          $directoryHelper
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_directoryHelper = $directoryHelper;
    }

    /**
     * @return string
     */
    public function getRegionJson()
    {
        return $this->_directoryHelper->getRegionJson();
    }

    /**
     * get registry model.
     *
     * @return \VNG\Storelocator\Model\Store
     */
    public function getStore()
    {
        return $this->getParentBlock()->getRegistryModel();
    }
}
