<?php

/**
 * VNG.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the VNG.com license that is
 * available through the world-wide-web at this URL:
 * http://www.vinagiay.vn/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    VNG
 * @package     VNG_Storelocator
 * @copyright   Copyright (c) 2012 VNG (http://www.vinagiay.vn/)
 * @license     http://www.vinagiay.vn/license-agreement.html
 */

namespace VNG\Storelocator\Block\Adminhtml\Tag\Edit;

use VNG\Storelocator\Model\Factory;

/**
 * Tag Edit Tabs.
 *
 * @category VNG
 * @package  VNG_Storelocator
 * @module   Storelocator
 * @author   VNG Developer
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('tag_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Tag Information'));
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->addTab('general_section', 'tag_edit_tab_general');

        // add stores tab
        $this->addTab(
            'stores_section',
            [
                'label' => __('Stores of Tag'),
                'title' => __('Stores of Tag'),
                'class' => 'ajax',
                'url' => $this->getUrl(
                    'storelocatoradmin/ajaxtabgrid_store',
                    [
                        'entity_type' => Factory::MODEL_TAG,
                        'enitity_id' => $this->getRequest()->getParam('tag_id'),
                        'serialized_name' => 'serialized_stores',
                    ]
                ),
            ]
        );

        return $this;
    }
}
