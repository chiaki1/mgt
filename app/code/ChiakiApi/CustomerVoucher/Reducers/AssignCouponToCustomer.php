<?php

namespace ChiakiApi\CustomerVoucher\Reducers;

use Chiaki\CustomerVoucher\Helper\Data;
use Chiaki\CustomerVoucher\Model\VoucherFactory;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\SalesRule\Model\RuleFactory;
use Magento\User\Model\UserFactory;

class AssignCouponToCustomer extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @var VoucherFactory
     */
    private $voucherFactory;

    /**
     * @var Data
     */
    private $couponHelper;

    /**
     * @var RuleFactory
     */
    private $ruleFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @param ObjectManagerInterface      $objectManager
     * @param IzRetailResponseFactory     $izRetailResponseFactory
     * @param UserContextInterface        $userContext
     * @param VoucherFactory              $voucherFactory
     * @param Data                        $couponHelper
     * @param RuleFactory                 $ruleFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param UserFactory                 $userFactory
     */
    public function __construct(
        ObjectManagerInterface      $objectManager,
        IzRetailResponseFactory     $izRetailResponseFactory,
        UserContextInterface        $userContext,
        VoucherFactory              $voucherFactory,
        Data                        $couponHelper,
        RuleFactory                 $ruleFactory,
        CustomerRepositoryInterface $customerRepository,
        UserFactory                 $userFactory
    ) {
        $this->userContext        = $userContext;
        $this->voucherFactory     = $voucherFactory;
        $this->couponHelper       = $couponHelper;
        $this->ruleFactory        = $ruleFactory;
        $this->customerRepository = $customerRepository;
        $this->userFactory        = $userFactory;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface   $action
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface $response
     *
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'assign-coupon-to-customer':
                $userId = $this->userContext->getUserId();
                $data   = $this->assignCoupon($userId, $action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @param $userId
     *
     * @return array|mixed|null
     * @throws WebAPiException
     */
    public function assignCoupon($userId, $payload)
    {
        if (!isset($payload['source_id']) || empty($payload['source_id'])) {
            throw new WebAPiException(__('Specify the "source_id" value.'));
        }
        if (!isset($payload['customer_id']) || empty($payload['customer_id'])) {
            throw new WebAPiException(__('Specify the "customer_id" value.'));
        }
        if (!isset($payload['comment']) || empty($payload['comment'])) {
            throw new WebAPiException(__('Specify the "comment" value.'));
        }
        try {
            $this->validateCustomer($payload['customer_id']);
            $this->validateUser($userId);
            $voucher = $this->voucherFactory->create();
            $rule    = $this->ruleFactory->create()->load($payload['source_id']);
            if ($rule->getId()) {
                $coupons = $rule->getCoupons();
                foreach ($coupons as $coupon) {
                    if (!$this->isAssigned($coupon->getCode())) {
                        $couponCode = $coupon->getCode();
                        break;
                    }
                }
            }
            $data = [
                'code'        => $couponCode ?? $this->couponHelper->generateCode($payload['source_id']),
                'customer_id' => $payload['customer_id'],
                'pick_by'     => $userId,
                'source_type' => 0,
                'source_id'   => $payload['source_id'],
                'comment'     => $payload['comment']
            ];
            $voucher->setData($data)->save();
            return ['message' => 'Success'];
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    private function validateCustomer($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }

    /**
     * @param $couponCode
     *
     * @return bool
     */
    public function isAssigned($couponCode)
    {
        $collection = $this->voucherFactory->create()->getCollection();
        $collection->addFieldToFilter('main_table.code', $couponCode);
        return $collection->getSize() > 0;
    }

    public function validateUser($userId)
    {
        $user = $this->userFactory->create()->load($userId);
        if (!$user->getId()) {
            throw NoSuchEntityException::singleField('user_id', $userId);
        }
    }
}
