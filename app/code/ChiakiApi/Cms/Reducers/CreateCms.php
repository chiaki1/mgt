<?php

namespace ChiakiApi\Cms\Reducers;

use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;

class CreateCms extends IzRetailApiManagement implements IzRetailReducer
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * @var ResourceConnection
     */
    private $connection;

    /**
     * @param ObjectManagerInterface   $objectManager
     * @param IzRetailResponseFactory  $izRetailResponseFactory
     * @param PageFactory              $pageFactory
     * @param BlockFactory             $blockFactory
     * @param PageRepositoryInterface  $pageRepository
     * @param BlockRepositoryInterface $blockRepository
     */
    public function __construct(
        ObjectManagerInterface   $objectManager,
        IzRetailResponseFactory  $izRetailResponseFactory,
        PageFactory              $pageFactory,
        BlockFactory             $blockFactory,
        PageRepositoryInterface  $pageRepository,
        BlockRepositoryInterface $blockRepository,
        ResourceConnection       $connection
    ) {
        $this->pageFactory     = $pageFactory;
        $this->blockFactory    = $blockFactory;
        $this->pageRepository  = $pageRepository;
        $this->blockRepository = $blockRepository;
        $this->connection      = $connection;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * @param IzRetailActionInterface   $action
     * @param IzRetailResponseInterface $response
     *
     * @throws WebAPiException
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'create-cms-block':
                $data = $this->createCmsBlock($action->getPayload());
                break;
            case 'create-cms-page':
                $data = $this->createCmsPage($action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @param $payload
     *
     * @return array
     * @throws WebAPiException
     */
    private function createCmsBlock($payload)
    {
        if (empty($payload['identifier'])) {
            throw new WebAPiException(__('Specify the "identifier" value.'));
        }
        if (empty($payload['content'])) {
            throw new WebAPiException(__('Specify the "content" value.'));
        }
        if (empty($payload['user_id'])) {
            throw new WebAPiException(__('Specify the "user_id" value.'));
        }
        if (empty($payload['title'])) {
            throw new WebAPiException(__('Specify the "title" value.'));
        }
        if (!$this->getIsUniqueBlockToStores($payload['identifier'])) {
            throw new WebAPiException(__('A block identifier with the same properties already exists in the selected store.'));
        }

        try {
            $data  = [
                'title'      => $payload['title'],
                'identifier' => $payload['identifier'],
                'stores'     => ['0'],
                'is_active'  => 1,
                'content'    => $payload['content'],
                'user_id'    => $payload['user_id']
            ];
            $block = $this->blockFactory->create()->setData($data);
            $this->blockRepository->save($block);

            return ['block_id' => $block->getId()];
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    /**
     * @param $payload
     *
     * @return array
     * @throws WebAPiException
     */
    private function createCmsPage($payload)
    {
        if (empty($payload['identifier'])) {
            throw new WebAPiException(__('Specify the "identifier" value.'));
        }
        if (empty($payload['content'])) {
            throw new WebAPiException(__('Specify the "content" value.'));
        }
        if (empty($payload['user_id'])) {
            throw new WebAPiException(__('Specify the "user_id" value.'));
        }
        if (empty($payload['title'])) {
            throw new WebAPiException(__('Specify the "title" value.'));
        }
        if (!$this->getIsUniquePageToStores($payload['identifier'])) {
            throw new WebAPiException(__('A page identifier with the same properties already exists in the selected store.'));
        }
        try {
            $page = $this->pageFactory->create();
            $page->setTitle($payload['title'])
                 ->setIdentifier($payload['identifier'])
                 ->setIsActive(true)
                 ->setPageLayout('1column')
                 ->setStores([0])
                 ->setContent($payload['content'])
                 ->setUserId($payload['user_id']);
            $this->pageRepository->save($page);

            return ['page_id' => $page->getId()];
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    /**
     * Check for unique of identifier of block to selected store(s).
     *
     * @param $identifier
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    private function getIsUniqueBlockToStores($identifier)
    {
        $connection = $this->connection->getConnection();

        $select = $connection->select()
                             ->from(['cb' => $connection->getTableName('cms_block')])
                             ->join(
                                 ['cbs' => $connection->getTableName('cms_block_store')],
                                 'cb.row_id = cbs.row_id',
                                 []
                             )
                             ->where('cb.identifier = ?  ', $identifier)
                             ->where('cbs.store_id IN (?)', [0]);

        if ($connection->fetchRow($select)) {
            return false;
        }

        return true;
    }

    /**
     * Check for unique of identifier of page to selected store(s).
     *
     * @param $identifier
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    private function getIsUniquePageToStores($identifier)
    {
        $connection = $this->connection->getConnection();

        $select = $connection->select()
                             ->from(['cp' => $connection->getTableName('cms_page')])
                             ->join(
                                 ['cps' => $connection->getTableName('cms_page_store')],
                                 'cp.row_id = cps.row_id',
                                 []
                             )
                             ->where('cp.identifier = ?  ', $identifier)
                             ->where('cps.store_id IN (?)', [0]);

        if ($connection->fetchRow($select)) {
            return false;
        }

        return true;
    }
}
