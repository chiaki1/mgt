<?php

namespace ChiakiApi\Vnpay\Reducers;

use Chiaki\Vnpay\Helper\Data;
use Chiaki\Vnpay\Logger\Logger;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\StoreManagerInterface;
use Chiaki\Vnpay\Model\ResourceModel\Transaction\CollectionFactory as VnpayTransactionCollectionFactory;

class Vnpay extends IzRetailApiManagement implements IzRetailReducer
{

    const PAYMENT_CODE = "vnpay";

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var VnpayTransactionCollectionFactory
     */
    protected $_transactions;

    /**
     * Vnpay constructor.
     *
     * @param ObjectManagerInterface                  $objectManager
     * @param IzRetailResponseFactory                 $izRetailResponseFactory
     * @param OrderFactory                            $orderFactory
     * @param Data                                    $helper
     * @param StoreManagerInterface                   $storeManager
     * @param Logger                                  $logger
     * @param VnpayTransactionCollectionFactory $transactions
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        OrderFactory $orderFactory,
        Data $helper,
        StoreManagerInterface $storeManager,
        Logger $logger,
        VnpayTransactionCollectionFactory $transactions
    ) {
        $this->_helper       = $helper;
        $this->_orderFactory = $orderFactory;
        $this->_logger       = $logger;
        $this->_storeManager = $storeManager;
        $this->_transactions = $transactions;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface   $action
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface $response
     *
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'get-pay-url':
                $data = $this->getPayUrl($action->getPayload());
                break;
            case 'check-pay-success':
                $data = $this->checkPaySuccess($action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @param $payload
     *
     * @return array
     * @throws WebAPiException
     */
    public function getPayUrl($payload)
    {
        if (!isset($payload['order_number']) || empty($incrementID = $payload['order_number'])) {
            throw new WebAPiException(__('Specify the "order_number" value.'));
        }
        $returnUrl = $this->_storeManager->getStore()->getBaseUrl();
        $returnUrl = rtrim($returnUrl, "/");
        $returnUrl .= "/vnpay/order/response";
        if (isset($payload['callback_url'])) {
            if (filter_var($payload['callback_url'], FILTER_VALIDATE_URL)) {
                $returnUrl = $payload['callback_url'];
            } else {
                throw new WebAPiException(__('Invalid "callback_url" value.'));
            }
        }

        try {
            $order = $this->_orderFactory->create()->loadByIncrementId($incrementID);
            if ($order->getId()) {
                if (!$order->getVnpayTnxRef()) {
                    $inputData = [
                        "vnp_Version"    => $this->_helper->getVnpVersion(),
                        "vnp_TmnCode"    => $this->_helper->getMerchantId(self::PAYMENT_CODE),
                        "vnp_Amount"     => round($order->getTotalDue() * 100, 0),
                        "vnp_Command"    => "pay",
                        "vnp_CreateDate" => date('YmdHis'),
                        "vnp_CurrCode"   => $this->_helper->getCurrencyCode(),
                        "vnp_IpAddr"     => $order->getRemoteIp(),
                        "vnp_Locale"     => 'vn',
                        "vnp_OrderInfo"  => $incrementID,
                        "vnp_OrderType"  => 'other',
                        "vnp_ReturnUrl"  => $returnUrl,
                        "vnp_TxnRef"     => $this->_helper->getTxnRef(self::PAYMENT_CODE),
                    ];
                    ksort($inputData);
                    $this->_logger->info('######## Request Data To Vnpay For OrderId: ' . $incrementID . ' ########');
                    $this->_logger->info('######## List Request Parameter: ########');
                    $this->_helper->writeLog($inputData);
                    $this->_logger->info('############################################');
                    $url = $this->_helper->getCallUrl(self::PAYMENT_CODE, $inputData);
                    $this->_helper->saveVnpayTnxRef($order, $inputData["vnp_TxnRef"]);
                    return ['url' => $url];
                } else {
                    return ['url' => ''];
                }
            } else {
                throw new WebAPiException(__("Order not found."));
            }
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    public function checkPaySuccess($payload)
    {
        if (!isset($payload['order_number']) || empty($incrementID = $payload['order_number'])) {
            throw new WebAPiException(__('Specify the "order_number" value.'));
        }

        try {
            $result = [
                'status' => 'pending',
                'message' => 'Pending'
            ];
            $order = $this->_orderFactory->create()->loadByIncrementId($incrementID);
            if ($order->getId()) {
                $collection = $this->_transactions->create()->addFieldToFilter('vnpay_tnx_ref', $order->getVnpayTnxRef())->setOrder('created_at', 'DESC');
                $lastTransaction = $collection->getFirstItem();
                if ($lastTransaction->getId()) {
                    $additionalInfo = $lastTransaction->getData('additional_information');
                    $additionalInfo = json_decode($additionalInfo, true);
                    if (isset($additionalInfo['vnp_ResponseCode']) && $additionalInfo['vnp_ResponseCode'] == '00' && $order->hasInvoices() && $order->getTotalPaid() == $order->getGrandTotal()) {
                        $result = [
                            'status' => 'success',
                            'message' => $this->_helper->getResponseDescriptionPay($additionalInfo['vnp_ResponseCode'])
                        ];
                    } elseif (isset($additionalInfo['vnp_ResponseCode']) && $additionalInfo['vnp_ResponseCode'] != '00') {
                        $result = [
                            'status' => 'failed',
                            'message' => $this->_helper->getResponseDescriptionPay($additionalInfo['vnp_ResponseCode'])
                        ];
                    }
                }
                return $result;
            } else {
                throw new WebAPiException(__("Order not found."));
            }
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }
}
