<?php

namespace ChiakiApi\CustomerSizeQuestion\Reducers;

use Chiaki\CustomerSizeQuestion\Helper\Data;
use Chiaki\CustomerSizeQuestion\Model\AnswerFactory;
use Chiaki\CustomerSizeQuestion\Model\ImageProcessor;
use Chiaki\CustomerSizeQuestion\Model\QuestionSetFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Gallery\CollectionFactory as GalleryCollectionFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\CollectionFactory as QuestionCollectionFactory;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Exception;
use Magento\Authorization\Model\CompositeUserContext;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Framework\Webapi\Exception as WebAPiException;

class Question extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var QuestionCollectionFactory
     */
    protected $questionCollectionFactory;

    /**
     * @var GalleryCollectionFactory
     */
    protected $galleryCollectionFactory;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * @var ImageProcessor
     */
    protected $imageProcessor;

    /**
     * @var AnswerFactory
     */
    protected $answerFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var QuestionSetFactory
     */
    protected $questionSetFactory;

    /**
     * @var CompositeUserContext
     */
    protected $userContext;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * Question constructor.
     *
     * @param ObjectManagerInterface    $objectManager
     * @param IzRetailResponseFactory   $izRetailResponseFactory
     * @param QuestionCollectionFactory $questionCollectionFactory
     * @param GalleryCollectionFactory  $galleryCollectionFactory
     * @param SerializerJson            $serializer
     * @param ImageProcessor            $imageProcessor
     * @param AnswerFactory             $answerFactory
     * @param Data                      $helper
     * @param QuestionSetFactory        $questionSetFactory
     * @param CompositeUserContext      $userContext
     * @param CustomerFactory           $customerFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        QuestionCollectionFactory $questionCollectionFactory,
        GalleryCollectionFactory $galleryCollectionFactory,
        SerializerJson $serializer,
        ImageProcessor $imageProcessor,
        AnswerFactory $answerFactory,
        Data $helper,
        QuestionSetFactory $questionSetFactory,
        CompositeUserContext $userContext,
        CustomerFactory $customerFactory
    ) {
        $this->questionCollectionFactory = $questionCollectionFactory;
        $this->galleryCollectionFactory  = $galleryCollectionFactory;
        $this->serializer                = $serializer;
        $this->imageProcessor            = $imageProcessor;
        $this->answerFactory             = $answerFactory;
        $this->helper                    = $helper;
        $this->questionSetFactory        = $questionSetFactory;
        $this->userContext               = $userContext;
        $this->customerFactory           = $customerFactory;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param IzRetailActionInterface   $action
     * @param IzRetailResponseInterface $response
     *
     * @throws WebAPiException
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'get-all-questions':
                $data = $this->getQuestion();
                break;
            case 'get-enable-questions':
                $data = $this->getQuestion(true);
                break;
            case 'save-answer-of-questions':
                $data = $this->saveAnswer($action->getPayload());
                break;
            case 'get-set-of-questions':
                $data = $this->getSetOfQuestion();
                break;
            case 'set-sizes-to-customer':
                $data = $this->setSizeToCustomer($action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @param bool $isEnable
     *
     * @return array
     * @throws WebAPiException
     */
    public function getQuestion(bool $isEnable = false)
    {
        $results = [];
        try {
            if ($isEnable) {
                $items = $this->questionCollectionFactory->create()
                                                         ->addFieldToFilter('status', ['eq' => 1])
                                                         ->getItems();
            } elseif (!$isEnable) {
                $items = $this->questionCollectionFactory->create()
                                                         ->getItems();
            }

            foreach ($items as $model) {
                if (is_string($model->getData('options'))) {
                    $options    = $this->serializer->unserialize($model->getData('options'));
                    $newOptions = [];
                    foreach ($options as $option) {
                        if (isset($option['initialize'])) {
                            unset($option['initialize']);
                        }
                        $newOptions[] = $option;
                    }
                    $model->setData('options', $newOptions);
                }
                $galleryImages = $this->galleryCollectionFactory->create()->getImagesByQuestion($model->getData('question_id'));
                if (!empty($galleryImages)) {
                    $gallery_image = [];

                    foreach ($galleryImages as $image) {
                        $imgName = $image->getData('image_name');
                        array_push(
                            $gallery_image,
                            [
                                'name' => $imgName,
                                'url'  => $this->imageProcessor->getImageUrl(
                                    [ImageProcessor::CHIAKI_QUESTION_GALLERY_MEDIA_PATH, $model->getId(), $imgName]
                                )
                            ]
                        );
                    }
                    $model->setData('gallery_image', $gallery_image);
                }
                $result                   = $model->getData();
                $results[$model->getId()] = $result;
            }
            return $results;
        } catch (Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    /**
     * @param $payload
     *
     * @return array[]
     * @throws WebAPiException
     */
    public function saveAnswer($payload)
    {
        $customerId = $this->userContext->getUserId();
        if (!$customerId) {
            throw new WebAPiException(__('Please use Bearer Token.'));
        }
        if (!isset($payload['answers']) || empty($payload['answers']) || !is_array($payload['answers'])) {
            throw new WebAPiException(__('Specify the "answers" value is array not empty.'));
        }
        $payloadAnswers = $payload['answers'];
        $checkHeight    = false;
        $checkWeight    = false;
        $checkCollar    = '';
        $checkSleeve    = '';
        foreach ($payloadAnswers as $answer) {
            $questionId = $answer['question_id'];
            if ($questionId == $this->helper->getQuestionHeightId()) {
                $checkHeight = $answer['answer'];
            }
            if ($questionId == $this->helper->getQuestionWeightId()) {
                $checkWeight = $answer['answer'];
            }
            if ($questionId == $this->helper->getQuestionCollarId()) {
                $checkCollar = $answer['answer'];
            }
            if ($questionId == $this->helper->getQuestionSleeveId()) {
                $checkSleeve = $answer['answer'];
            }
        }
        if (!$checkHeight) {
            throw new WebAPiException(__('Specify the height question value.'));
        }
        if (!$checkWeight) {
            throw new WebAPiException(__('Specify the weight question value.'));
        }

        try {
            $output = $this->detectOutputData($customerId, $checkHeight, $checkWeight, $checkCollar, $checkSleeve);
            if (empty($output)) {
                if ($this->isRange($checkHeight)) {
                    $checkHeightArray = explode('-', (string)$checkHeight);
                    $checkHeight      = array_sum($checkHeightArray) / 2;
                    $output           = $this->detectOutputData($customerId, $checkHeight, $checkWeight, $checkCollar, $checkSleeve);
                    if (empty($output)) {
                        $output = $this->detectOutputData($customerId, $checkHeightArray[1], $checkWeight, $checkCollar, $checkSleeve);
                        if (empty($output)) {
                            if ($this->isRange($checkWeight)) {
                                $checkWeightArray = explode('-', (string)$checkWeight);
                                $checkWeight      = array_sum($checkWeightArray) / 2;
                                $output           = $this->detectOutputData($customerId, $checkHeight, $checkWeight, $checkCollar, $checkSleeve);
                                if (empty($output)) {
                                    $output = $this->detectOutputData($customerId, $checkHeightArray[1], $checkWeightArray[1], $checkCollar, $checkSleeve);
                                }
                            }
                        }
                    }

                }
            }
            $answers = $this->serializer->serialize($payload['answers']);
            $answer  = $this->answerFactory->create()->setData(
                [
                    'customer_id' => $customerId,
                    'value'       => $answers,
                    'name'        => $payload['name'],
                    'shirt_size'  => $output[10]['value'] ?? null,
                    'pants_size'  => $output[9]['value'] ?? null,
                    'active'      => 1
                ]
            )->save();
            $answer->activeAnswer();
            if (!empty($output)) {
                $customer = $this->customerFactory->create()->load($customerId);
                $customer->setData('shirt_size', $output[10]['value']);
                $customer->setData('pants_size', $output[9]['value']);
                $customer->save();
            }
            return $output;
        } catch (Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    public function getSetOfQuestion()
    {
        try {
            $results       = [];
            $selectedSetId = $this->helper->getSelectedQuestionSetId();
            $collection    = $this->questionCollectionFactory->create();
            $collection->getSelect()->joinRight(
                ['question_set_questions' => $collection->getTable("customer_size_question_set_questions")],
                'question_set_questions.question_id = main_table.question_id',
                ['*']
            )->where('question_set_questions.question_set_id=?', $selectedSetId);
            $collection->setOrder('position', 'ASC')->addFieldToFilter('status', ['eq' => 1]);
            $items = $collection->getItems();
            foreach ($items as $model) {
                if (is_string($model->getData('options'))) {
                    $options    = $this->serializer->unserialize($model->getData('options'));
                    $newOptions = [];
                    foreach ($options as $option) {
                        if (isset($option['initialize'])) {
                            unset($option['initialize']);
                        }
                        $newOptions[] = $option;
                    }
                    $model->setData('options', $newOptions);
                }
                $galleryImages = $this->galleryCollectionFactory->create()->getImagesByQuestion($model->getData('question_id'));
                if (!empty($galleryImages)) {
                    $gallery_image = [];

                    foreach ($galleryImages as $image) {
                        $imgName = $image->getData('image_name');
                        array_push(
                            $gallery_image,
                            [
                                'name' => $imgName,
                                'url'  => $this->imageProcessor->getImageUrl(
                                    [ImageProcessor::CHIAKI_QUESTION_GALLERY_MEDIA_PATH, $model->getId(), $imgName]
                                )
                            ]
                        );
                    }
                    $model->setData('gallery_image', $gallery_image);
                }
                $result    = $model->getData();
                $results[] = $result;
            }
            return $results;
        } catch (Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    public function detectOutputData($customerId, $height, $weight, $collar = "", $sleeve = "")
    {
        $data   = $this->helper->getSizeData();
        $result = [];
        foreach ($data as $row => $datum) {
            if ($row > 0) {
                $heightRange      = $datum[2];
                $weightRange      = $datum[3];
                $collarSleeveSize = $datum[1];
                if ($this->checkRangeData($height, $heightRange) && $this->checkRangeData($weight, $weightRange) && $this->checkCollarSleeveSize($collar, $sleeve, $collarSleeveSize)) {
                    $result = $datum;
                } elseif ($this->checkRangeData($height, $heightRange) && $this->checkRangeData($weight, $weightRange)) {
                    $result = $datum;
                }
            }
        }
        $output = [];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if ($key == 2) {
                    $value = $height;
                }
                if ($key == 3) {
                    $value = $weight;
                }
                $output[] = [
                    'label' => $data[0][$key],
                    'value' => $value
                ];
            }
        }

        if (!empty($output)) {
            $customer = $this->customerFactory->create()->load($customerId);
            $customer->setData('shirt_size', $output[10]['value']);
            $customer->setData('pants_size', $output[9]['value']);
            $customer->save();
        }

        return $output;
    }

    /**
     * @param $value
     * @param $range
     *
     * @return bool
     */
    public function checkRangeData($value, $range)
    {
        if ($this->isRange($value)) {
            return $value == $range;
        }
        $range = explode('-', (string)$range);
        if ($value <= $range[1] && $value >= $range[0]) {
            return true;
        }
        return false;
    }

    /**
     * @param $collar
     * @param $sleeve
     * @param $collarSleeveSize
     *
     * @return bool
     */
    public function checkCollarSleeveSize($collar, $sleeve, $collarSleeveSize)
    {
        $collarSleeveSize = explode('-', (string)$collarSleeveSize);
        if ($collar == $collarSleeveSize[0] && $sleeve == $collarSleeveSize[1]) {
            return true;
        }
        return false;
    }

    public function setSizeToCustomer($payload)
    {
        if (!isset($payload['shirt_size']) || empty($payload['shirt_size'])) {
            throw new WebAPiException(__('Specify the "shirt_size" value is array not empty.'));
        }
        if (!isset($payload['pants_size']) || empty($payload['pants_size'])) {
            throw new WebAPiException(__('Specify the "pants_size" value is array not empty.'));
        }
        if (!isset($payload['size_id']) || empty($payload['size_id'])) {
            throw new WebAPiException(__('Specify the "size_id" value is array not empty.'));
        }
        try {
            $customerId = $this->userContext->getUserId();
            if (!$customerId) {
                throw new WebAPiException(__('Please use Bearer Token.'));
            }
            $customer = $this->customerFactory->create()->load($customerId);
            $customer->setData('shirt_size', $payload['shirt_size']);
            $customer->setData('pants_size', $payload['pants_size']);
            $customer->save();
            $answer = $this->answerFactory->create()->load($payload['size_id']);
            if ($answer->getId()) {
                $answer->activeAnswer();
            }
            return ['status' => 'success'];
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function isRange($value)
    {
        $value = explode('-', (string)$value);
        return count($value) == 2;
    }
}
