<?php

namespace ChiakiApi\Address\Reducers;

use Chiaki\Address\Model\ResourceModel\IzAddressDistrict\CollectionFactory as IzAddressDistrictCollectionFactory;
use Chiaki\Address\Model\ResourceModel\IzAddressProvince\CollectionFactory as IzAddressProvinceCollectionFactory;
use Chiaki\Address\Model\ResourceModel\IzAddressWard\CollectionFactory as IzAddressWardCollectionFactory;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Chiaki\Address\Model\IzAddressDistrict;
use Chiaki\Address\Model\IzAddressProvince;
use Chiaki\Address\Model\IzAddressWard;

class Address extends IzRetailApiManagement implements IzRetailReducer
{
    /**
     * @var IzAddressProvinceCollectionFactory
     */
    protected $izAddressProvinceCollectionFactory;

    /**
     * @var IzAddressDistrictCollectionFactory
     */
    protected $izAddressDistrictCollectionFactory;

    /**
     * @var IzAddressWardCollectionFactory
     */
    protected $izAddressWardCollectionFactory;

    protected $addressData;

    /**
     * Address constructor.
     *
     * @param ObjectManagerInterface             $objectManager
     * @param IzRetailResponseFactory            $izRetailResponseFactory
     * @param IzAddressProvinceCollectionFactory $izAddressProvinceCollectionFactory
     * @param IzAddressDistrictCollectionFactory $izAddressDistrictCollectionFactory
     * @param IzAddressWardCollectionFactory     $izAddressWardCollectionFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        IzAddressProvinceCollectionFactory $izAddressProvinceCollectionFactory,
        IzAddressDistrictCollectionFactory $izAddressDistrictCollectionFactory,
        IzAddressWardCollectionFactory $izAddressWardCollectionFactory
    ) {
        parent::__construct($objectManager, $izRetailResponseFactory);
        $this->izAddressProvinceCollectionFactory = $izAddressProvinceCollectionFactory;
        $this->izAddressDistrictCollectionFactory = $izAddressDistrictCollectionFactory;
        $this->izAddressWardCollectionFactory     = $izAddressWardCollectionFactory;
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface   $action
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface $response
     *
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'get-address-data':
                $data = $this->getAddressData();
                break;
            case 'get-province-data':
                $data = $this->getProvinceData();
                break;
            case 'get-district-data':
                $data = $this->getDistrictData($action->getPayload());
                break;
            case 'get-ward-data':
                $data = $this->getWardData($action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     *
     * @return array
     * @throws WebAPiException
     */
    public function getAddressData()
    {
        try {
            if (!$this->addressData) {
                $result = [];
                $provinceCollection = $this->izAddressProvinceCollectionFactory->create();
                $provinceCollection->addFieldToFilter('country_id', 'vn');
                /** @var IzAddressProvince $province */
                foreach ($provinceCollection as $province) {
                    $result['provinces'][] = [
                        'id' => $province->getId(),
                        'code' => $province->getCode(),
                        'name' => $province->getName()
                    ];
                }
                $districtCollection = $this->izAddressDistrictCollectionFactory->create();
                /** @var IzAddressDistrict $district */
                foreach ($districtCollection as $district) {
                    $result['districts'][$district->getProvinceId()][] = [
                        'id' => $district->getId(),
                        'code' => $district->getCode(),
                        'name' => $district->getName()
                    ];
                }
                $wardCollection = $this->izAddressWardCollectionFactory->create();
                /** @var IzAddressWard $ward */
                foreach ($wardCollection as $ward) {
                    $result['wards'][$ward->getDistrictId()][] = [
                        'id' => $ward->getId(),
                        'code' => $ward->getCode(),
                        'name' => $ward->getName()
                    ];
                }
                $this->addressData = $result;

            }
            return $this->addressData;
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    public function getProvinceData()
    {
        $result = [];
        $provinceCollection = $this->izAddressProvinceCollectionFactory->create();
        $provinceCollection->addFieldToFilter('country_id', 'vn');
        /** @var IzAddressProvince $province */
        foreach ($provinceCollection as $province) {
            $result[] = [
                'province_id' => $province->getId(),
                'code' => $province->getCode(),
                'name' => $province->getName()
            ];
        }
        return $result;
    }

    public function getDistrictData($payload)
    {
        if (!isset($payload['province_id']) || empty($province_id = $payload['province_id'])) {
            throw new WebAPiException(__('Specify the "province_id" value.'));
        }
        $result = [];
        $districtCollection = $this->izAddressDistrictCollectionFactory->create();
        $districtCollection->addFieldToFilter('province_id', $province_id);
        /** @var IzAddressDistrict $district */
        foreach ($districtCollection as $district) {
            $result[] = [
                'district_id' => $district->getId(),
                'code' => $district->getCode(),
                'name' => $district->getName()
            ];
        }
        return $result;
    }

    public function getWardData($payload)
    {
        if (!isset($payload['district_id']) || empty($district_id = $payload['district_id'])) {
            throw new WebAPiException(__('Specify the "district_id" value.'));
        }

        $result = [];
        $wardCollection = $this->izAddressWardCollectionFactory->create();
        $wardCollection->addFieldToFilter('district_id', $district_id);
        /** @var IzAddressWard $ward */
        foreach ($wardCollection as $ward) {
            $result[] = [
                'ward_id' => $ward->getId(),
                'code' => $ward->getCode(),
                'name' => $ward->getName()
            ];
        }
        return $result;
    }
}
