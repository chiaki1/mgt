<?php

namespace ChiakiApi\Review\Reducers;

use Chiaki\Review\Model\ResourceModel\Rating\CollectionFactory as RatingCollectionFactory;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception;

class Rating extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var RatingCollectionFactory
     */
    protected $ratingCollectionFactory;

    /**
     * Rating constructor.
     *
     * @param ObjectManagerInterface  $objectManager
     * @param IzRetailResponseFactory $izRetailResponseFactory
     * @param RatingCollectionFactory $ratingCollectionFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        RatingCollectionFactory $ratingCollectionFactory
    ) {
        parent::__construct($objectManager, $izRetailResponseFactory);
        $this->ratingCollectionFactory = $ratingCollectionFactory;
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param IzRetailActionInterface   $action
     * @param IzRetailResponseInterface $response
     *
     * @throws Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'get-rating-answer':
                $data = $this->getRatingAnswer();
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @return array
     */
    public function getRatingAnswer()
    {
        $collection = $this->ratingCollectionFactory->create();
        $result     = [];
        foreach ($collection as $rating) {
            $result[$rating->getValue()] = $rating->getData();
        }
        return $result;
    }
}
