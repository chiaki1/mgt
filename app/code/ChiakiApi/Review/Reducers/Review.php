<?php

namespace ChiakiApi\Review\Reducers;

use Chiaki\Review\Model\ReviewFactory;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Exception;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\Sales\Model\OrderFactory;

class Review extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * Review constructor.
     *
     * @param ObjectManagerInterface  $objectManager
     * @param IzRetailResponseFactory $izRetailResponseFactory
     * @param ReviewFactory           $reviewFactory
     * @param OrderFactory            $orderFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        ReviewFactory $reviewFactory,
        OrderFactory $orderFactory
    ) {
        $this->reviewFactory = $reviewFactory;
        $this->orderFactory  = $orderFactory;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param IzRetailActionInterface   $action
     * @param IzRetailResponseInterface $response
     *
     * @throws WebAPiException
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'save-order-review':
                $data = $this->saveOrderReview($action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @return array
     * @throws WebAPiException
     */
    public function saveOrderReview($payload)
    {
        if (!isset($payload['order_number']) || empty($order_number = $payload['order_number'])) {
            throw new WebAPiException(__('Specify the "order_number" value.'));
        }
        $order = $this->orderFactory->create()->loadByIncrementId($order_number);
        if (!$order->getId()) {
            throw new WebAPiException(__('Order with "order_number" %1 not exist.', $order_number));
        }
        if ($order->getStatus() != 'complete') {
            throw new WebAPiException(__('Can not review this order.'));
        }
        $payload['order_id'] = $order->getId();
        if (!$this->checkReviewExist($order->getId())) {
            throw new WebAPiException(__('This order already has a review.'));
        }
        if (!isset($payload['rating']) || empty($payload['rating'])) {
            throw new WebAPiException(__('Specify the "rating" value.'));
        }
        if (isset($payload['rating']) && !empty($payload['rating']) && !in_array($payload['rating'], [1, 2, 3, 4, 5])) {
            throw new WebAPiException(__('The "rating" value is range of 1 to 5.'));
        }
        $reviewId = null;
        if (isset($payload['review_id'])) {
            $reviewId = $payload['review_id'];
        }
        if (isset($payload['answer'])) {
            if (is_array($payload['answer'])) {
                $payload['answer'] = implode(',', $payload['answer']);
            }
        }

        $model = $this->reviewFactory->create()->load($reviewId);
        if (!$model->getId() && $reviewId) {
            throw new WebAPiException(__('This review no longer exists.'));
        }
        $model->setData($payload);

        try {
            $model->save();
            return ['review_id' => $model->getReviewId()];
        } catch (Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    public function checkReviewExist($orderId)
    {
        $reviewCollection = $this->reviewFactory->create()->getCollection()->addFieldToFilter('order_id', $orderId);
        return $reviewCollection->count() < 1;
    }
}
