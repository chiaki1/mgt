<?php

namespace ChiakiApi\SocialLogin\Reducers;

use Chiaki\SocialLogin\Api\AccountRepositoryInterface;
use Chiaki\SocialLogin\Exception\CustomerConvertException;
use Chiaki\SocialLogin\Model\Customer\AccountManagement;
use Chiaki\SocialLogin\Model\Provider\Account\ConverterInterface;
use Chiaki\SocialLogin\Model\Provider\AccountInterface as ProviderAccountInterface;
use Chiaki\SocialLogin\Model\Provider\Customer\ConverterInterface as CustomerConverterInterface;
use Chiaki\SocialLogin\Model\Provider\Customer\ValidatorInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Event\ManagerInterface as EventManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Chiaki\SocialLogin\Model\ProviderManagement;
use Psr\Log\LoggerInterface;
use Magento\Integration\Model\Oauth\TokenFactory;

class SocialLogin extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var ProviderManagement
     */
    protected $_providerManagement;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var AccountRepositoryInterface
     */
    protected $accountRepository;

    /**
     * @var AccountManagement
     */
    protected $accountManagement;

    /**
     * @var AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var ConverterInterface
     */
    protected $converter;

    /**
     * @var CustomerConverterInterface
     */
    protected $customerConverter;

    /**
     * @var TokenFactory
     */
    protected $tokenModelFactory;

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * SocialLogin constructor.
     *
     * @param ObjectManagerInterface      $objectManager
     * @param IzRetailResponseFactory     $izRetailResponseFactory
     * @param ProviderManagement          $providerManagement
     * @param LoggerInterface             $logger
     * @param AccountRepositoryInterface  $accountRepository
     * @param AccountManagement           $accountManagement
     * @param ConverterInterface          $converter
     * @param CustomerConverterInterface  $customerConverter
     * @param AccountManagementInterface  $customerAccountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param TokenFactory                $tokenModelFactory
     * @param EventManagerInterface       $eventManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        ProviderManagement $providerManagement,
        LoggerInterface $logger,
        AccountRepositoryInterface $accountRepository,
        AccountManagement $accountManagement,
        ConverterInterface $converter,
        CustomerConverterInterface $customerConverter,
        AccountManagementInterface $customerAccountManagement,
        CustomerRepositoryInterface $customerRepository,
        TokenFactory $tokenModelFactory,
        EventManagerInterface $eventManager
    ) {
        parent::__construct($objectManager, $izRetailResponseFactory);
        $this->_providerManagement       = $providerManagement;
        $this->_logger                   = $logger;
        $this->accountRepository         = $accountRepository;
        $this->accountManagement         = $accountManagement;
        $this->converter                 = $converter;
        $this->customerConverter         = $customerConverter;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerRepository        = $customerRepository;
        $this->tokenModelFactory         = $tokenModelFactory;
        $this->eventManager              = $eventManager;
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface   $action
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface $response
     *
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'social-login':
                $data = $this->socialLoginProcess($action->getPayload());
                break;
            default:
                $data = ['token' => ''];
        }

        $response->setData($data);
    }

    /**
     * @param $payload
     *
     * @return array
     * @throws WebAPiException
     */
    public function socialLoginProcess($payload)
    {
        $result = [];
        if (!isset($payload['provider']) || empty($providerCode = $payload['provider'])) {
            throw new WebAPiException(__('Specify the "provider" value.'));
        }

        if (!isset($payload['token']) || empty($accessToken = $payload['token'])) {
            throw new WebAPiException(__('Specify the "token" value.'));
        }

        if (!isset($payload['expires_in']) || empty($expiresIn = $payload['expires_in'])) {
            throw new WebAPiException(__('Specify the "expires_in" value.'));
        }

        try {
            $factory = $this->_providerManagement->getEnabledFactory($providerCode);
            $service = $factory->createService();

            $account = $factory->createCallbackRequestProcessor()
                               ->processWithToken($service, $this->getRequest(), $accessToken, $expiresIn);

            try {
                $socialAccount = $this->accountRepository->getBySocialId($account->getType(), $account->getSocialId());
            } catch (\Exception $e) {
                try {
                    $customer      = $this->customerConverter->convert($account);
                    $customer      = $this->initCustomer($customer);
                    $socialAccount = $this->linkAccount($account, $customer);
                } catch (CustomerConvertException $exception) {
                    $error = $exception->getErrors();
                    if (count($error) == 1 && $error[0]['type'] == ValidatorInterface::ERROR_TYPE_INVALID_FIELD && $error[0]['message'] == 'Customer with this email already exist') {
                        $customer = $this->customerRepository->get($error[0]['data']['customer_email']);
                        $socialAccount = $this->linkAccount($account, $customer);
                    }

                }
            }
            $customer = $this->accountManagement->authenticate($socialAccount);
            $this->eventManager->dispatch('customer_login', ['customer' => $customer]);
            $customerToken   = $this->tokenModelFactory->create();
            $tokenKey        = $customerToken->createCustomerToken($customer->getId())->getToken();
            $result['token'] = $tokenKey;
            $this->accountRepository->save($socialAccount->updateLastSignedAt());
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
            throw new WebAPiException(__($e->getMessage()));
        }
        return $result;
    }

    /**
     * @param ProviderAccountInterface $providerAccount
     * @param CustomerInterface        $customer
     *
     * @return \Chiaki\SocialLogin\Api\Data\AccountInterface
     */
    protected function linkAccount(ProviderAccountInterface $providerAccount, CustomerInterface $customer)
    {
        $account = $this->converter->convert($providerAccount);
        $account->setCustomerId($customer->getId());
        return $this->accountRepository->save($account);
    }

    /**
     * Init customer
     *
     * @param CustomerInterface $customer
     *
     * @return CustomerInterface
     */
    protected function initCustomer(CustomerInterface $customer)
    {
        try {
            $customer = $this->customerRepository->get($customer->getEmail());
        } catch (NoSuchEntityException $e) {
            $customer = $this->customerAccountManagement->createAccount($customer);
        }
        return $customer;
    }
}
