<?php


namespace ChiakiApi\ApiBase\Model\Data;


use ChiakiApi\ApiBase\Api\Data\IzRetailDataInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class IzRetailData extends AbstractSimpleObject implements IzRetailDataInterface
{
}
