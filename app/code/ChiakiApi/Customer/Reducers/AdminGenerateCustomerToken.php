<?php

namespace ChiakiApi\Customer\Reducers;

use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Authorization\Model\Acl\AclRetriever;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Authorization\PolicyInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\User\Model\UserFactory;

class AdminGenerateCustomerToken extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var AclRetriever
     */
    protected $aclRetriever;

    /**
     * @var PolicyInterface
     */
    protected $aclPolicy;

    /**
     * @var TokenFactory
     */
    protected $tokenModelFactory;

    protected $customerRepository;

    /**
     * AdminUserRole constructor.
     *
     * @param ObjectManagerInterface  $objectManager
     * @param IzRetailResponseFactory $izRetailResponseFactory
     * @param UserContextInterface    $userContext
     * @param UserFactory             $userFactory
     * @param AclRetriever            $aclRetriever
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        UserContextInterface $userContext,
        UserFactory $userFactory,
        AclRetriever $aclRetriever,
        PolicyInterface $aclPolicy,
        TokenFactory $tokenModelFactory,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->userContext       = $userContext;
        $this->userFactory       = $userFactory;
        $this->aclRetriever      = $aclRetriever;
        $this->aclPolicy         = $aclPolicy;
        $this->tokenModelFactory = $tokenModelFactory;
        $this->customerRepository = $customerRepository;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface   $action
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface $response
     *
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'admin-generate-customer-token':
                $userId = $this->userContext->getUserId();
                $data   = $this->getAdminUserRole($userId, $action->getPayload());
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @param $userId
     *
     * @return array|mixed|null
     * @throws WebAPiException
     */
    public function getAdminUserRole($userId, $payload)
    {
        if (!isset($payload['customer_id']) || empty($customer_id = $payload['customer_id'])) {
            throw new WebAPiException(__('Specify the "customer_id" value.'));
        }
        try {
            $customer = $this->customerRepository->getById($customer_id);
            $user   = $this->userFactory->create()->load($userId);
            $role   = $user->getRole();
            if ($this->aclPolicy->isAllowed($role->getId(), "Magento_Customer::manage")) {
                $customerToken   = $this->tokenModelFactory->create();
                $tokenKey        = $customerToken->createCustomerToken($customer->getId())->getToken();
                $result['token'] = $tokenKey;
                return $result;
            } else {
                throw new WebAPiException(__("You can not permission to create customer token."));
            }
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }
}
