<?php

namespace ChiakiApi\Customer\Reducers;

use Chiaki\Sms\Helper\Data;
use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Exception;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\Event\ManagerInterface as EventManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\Integration\Api\CustomerTokenServiceInterface;
use Magento\Integration\Model\Oauth\TokenFactory;

class GenerateCustomerTokenBySms extends IzRetailApiManagement implements IzRetailReducer
{
    /**
     * @var CustomerTokenServiceInterface
     */
    private $customerTokenService;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var TokenFactory
     */
    protected $tokenModelFactory;

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * @var CollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @param ObjectManagerInterface        $objectManager
     * @param IzRetailResponseFactory       $izRetailResponseFactory
     * @param CustomerTokenServiceInterface $customerTokenService
     * @param Data                          $helper
     * @param CustomerRepositoryInterface   $customerRepository
     * @param TokenFactory                  $tokenModelFactory
     * @param EventManagerInterface         $eventManager
     * @param CollectionFactory             $customerCollectionFactory
     */
    public function __construct(
        ObjectManagerInterface        $objectManager,
        IzRetailResponseFactory       $izRetailResponseFactory,
        CustomerTokenServiceInterface $customerTokenService,
        Data                          $helper,
        CustomerRepositoryInterface   $customerRepository,
        TokenFactory                  $tokenModelFactory,
        EventManagerInterface         $eventManager,
        CollectionFactory             $customerCollectionFactory
    ) {
        parent::__construct($objectManager, $izRetailResponseFactory);
        $this->customerTokenService      = $customerTokenService;
        $this->helper                    = $helper;
        $this->customerRepository        = $customerRepository;
        $this->tokenModelFactory         = $tokenModelFactory;
        $this->eventManager              = $eventManager;
        $this->customerCollectionFactory = $customerCollectionFactory;
    }

    /**
     * @param IzRetailActionInterface   $action
     * @param IzRetailResponseInterface $response
     *
     * @return array
     * @throws WebAPiException
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        if (empty($action->getPayload()['retail_telephone'])) {
            throw new WebAPiException(__('Specify the "retail_telephone" value.'));
        }

        try {
            $collection = $this->customerCollectionFactory->create();
            $customer   = $collection->addAttributeToSelect('*')
                                     ->addAttributeToFilter(\Chiaki\Sms\Setup\UpgradeData::RETAIL_TELEPHONE, $action->getPayload()['retail_telephone'])
                                     ->getFirstItem();
            if (!$customer->getId()) {
                $customer = $this->helper->createCustomer($action->getPayload()['retail_telephone']);
            }
            $this->eventManager->dispatch('customer_login', ['customer' => $customer]);
            $customerToken = $this->tokenModelFactory->create();
            $tokenKey      = $customerToken->createCustomerToken($customer->getId())->getToken();

            return ['token' => $tokenKey];
        } catch (Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }
}
