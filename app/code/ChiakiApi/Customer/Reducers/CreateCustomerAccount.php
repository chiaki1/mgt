<?php

namespace ChiakiApi\Customer\Reducers;

use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\CustomerGraphQl\Model\Customer\ExtractCustomerData;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Integration\Model\Oauth\TokenFactory;
use Magento\Newsletter\Model\Config;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class CreateCustomerAccount extends IzRetailApiManagement implements IzRetailReducer
{
    /**
     * @var ExtractCustomerData
     */
    private $extractCustomerData;
    /**
     * @var \Magento\CustomerGraphQl\Model\Customer\CreateCustomerAccount
     */
    private $createCustomerAccount;
    /**
     * @var Config
     */
    private $newsLetterConfig;

    /**
     * @var TokenFactory
     */
    private $tokenFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        ObjectManagerInterface                                        $objectManager,
        \ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory         $izRetailResponseFactory,
        ExtractCustomerData                                           $extractCustomerData,
        \Magento\CustomerGraphQl\Model\Customer\CreateCustomerAccount $createCustomerAccount,
        Config                                                        $newsLetterConfig,
        TokenFactory                                                  $tokenFactory,
        StoreManagerInterface                                         $storeManager
    ) {
        parent::__construct($objectManager, $izRetailResponseFactory);
        $this->extractCustomerData   = $extractCustomerData;
        $this->createCustomerAccount = $createCustomerAccount;
        $this->newsLetterConfig      = $newsLetterConfig;
        $this->tokenFactory          = $tokenFactory;
        $this->storeManager          = $storeManager;
    }

    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response): array
    {
        $payload = $action->getPayload();

        if (!$this->newsLetterConfig->isActive(ScopeInterface::SCOPE_STORE)) {
            $payload['is_subscribed'] = false;
        }
        if (isset($payload['date_of_birth'])) {
            $payload['dob'] = $payload['date_of_birth'];
        }
        if (isset($payload['telephone'])) {
            $payload['retail_telephone'] = $payload['telephone'];
        }
        try {
            $customer = $this->createCustomerAccount->execute(
                $payload,
                $this->getContext()->getStore() ?? $this->storeManager->getStore()
            );
        } catch (GraphQlInputException $e) {
            throw new \Magento\Framework\Webapi\Exception(__($e->getMessage()));
        }

        try {
            $data          = $this->extractCustomerData->execute($customer);
            $customerToken = $this->tokenFactory->create();
            $tokenKey      = $customerToken->createCustomerToken($customer->getId())->getToken();
            $data['token'] = $tokenKey;
        } catch (LocalizedException $e) {
            throw new \Magento\Framework\Webapi\Exception(__($e->getMessage()));
        }

        return ['customer' => $data];
    }
}
