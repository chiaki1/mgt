<?php


namespace ChiakiApi\Customer\Reducers;


use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\Integration\Api\CustomerTokenServiceInterface;

class GenerateCustomerToken extends IzRetailApiManagement implements IzRetailReducer
{
    /**
     * @var CustomerTokenServiceInterface
     */
    private $customerTokenService;

    public function __construct(
        ObjectManagerInterface $objectManager,
        \ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory $izRetailResponseFactory,
        CustomerTokenServiceInterface $customerTokenService
    )
    {
        parent::__construct($objectManager, $izRetailResponseFactory);
        $this->customerTokenService = $customerTokenService;
    }

    /**
     * @param IzRetailActionInterface $action
     * @param IzRetailResponseInterface $response
     * @return array
     * @throws WebAPiException
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        if (empty($action->getPayload()['email'])) {
            throw new WebAPiException(__('Specify the "email" value.'));
        }

        if (empty($action->getPayload()['password'])) {
            throw new WebAPiException(__('Specify the "password" value.'));
        }
        try {
            $token = $this->customerTokenService->createCustomerAccessToken($action->getPayload()['email'], $action->getPayload()['password']);

            return ['token' => $token];
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }
}
