<?php

namespace ChiakiApi\AdminUser\Reducers;

use ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface;
use ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface;
use ChiakiApi\ApiBase\Api\IzRetailReducer;
use ChiakiApi\ApiBase\Model\Data\IzRetailResponseFactory;
use ChiakiApi\ApiBase\Model\IzRetailApiManagement;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\User\Model\UserFactory;
use Magento\Authorization\Model\Acl\AclRetriever;

class AdminUserRole extends IzRetailApiManagement implements IzRetailReducer
{

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var AclRetriever
     */
    protected $aclRetriever;

    /**
     * AdminUserRole constructor.
     *
     * @param ObjectManagerInterface  $objectManager
     * @param IzRetailResponseFactory $izRetailResponseFactory
     * @param UserContextInterface    $userContext
     * @param UserFactory             $userFactory
     * @param AclRetriever            $aclRetriever
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        IzRetailResponseFactory $izRetailResponseFactory,
        UserContextInterface $userContext,
        UserFactory $userFactory,
        AclRetriever $aclRetriever
    ) {
        $this->userContext = $userContext;
        $this->userFactory = $userFactory;
        $this->aclRetriever = $aclRetriever;
        parent::__construct($objectManager, $izRetailResponseFactory);
    }

    /**
     * POST for ChiakiApi Management api
     *
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailActionInterface   $action
     * @param \ChiakiApi\ApiBase\Api\Data\IzRetailResponseInterface $response
     *
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function reduce(IzRetailActionInterface $action, IzRetailResponseInterface $response)
    {
        switch ($action->getType()) {
            case 'get-admin-user-role':
                $userId = $this->userContext->getUserId();
                $data = $this->getAdminUserRole($userId);
                break;
            default:
                $data = [];
        }

        $response->setData($data);
    }

    /**
     * @param $userId
     *
     * @return array|mixed|null
     * @throws WebAPiException
     */
    public function getAdminUserRole($userId)
    {
        try {
            $user = $this->userFactory->create()->load($userId);
            $role = $user->getRole();
            $result = $role->getData();
            if ($role->getId()) {
                $result['resources'] = $this->aclRetriever->getAllowedResourcesByRole($role->getId());
            }
            return $result;
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }
}
