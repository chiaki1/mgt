<?php
/**
 *
 * @author Khoi Le - mr.vjcspy@gmail.com
 * @time 7/12/20 5:03 PM
 *
 */

namespace Iz\Store\Observer;


use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Store\Model\Store;

class RefreshStoreLocator implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Store\Model\ResourceModel\Store\CollectionFactory
     */
    private $storeCollectionFactory;
    /**
     * @var \VNG\Storelocator\Model\ResourceModel\Store\CollectionFactory
     */
    private $storeLocatorCollectionFactory;
    /**
     * @var \VNG\Storelocator\Model\StoreFactory
     */
    private $storeLocatorModelFactory;
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    public function __construct(
        \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        \VNG\Storelocator\Model\ResourceModel\Store\CollectionFactory $storeLocatorCollectionFactory,
        \VNG\Storelocator\Model\StoreFactory $storeLocatorModelFactory,
        ResourceConnection $resourceConnection
    )
    {
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->storeLocatorCollectionFactory = $storeLocatorCollectionFactory;
        $this->storeLocatorModelFactory = $storeLocatorModelFactory;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        $object = $observer->getData('object');
        if ($object instanceof Store) {
            $stores = $this->getStoreCollection()->setLoadDefault(true)->getItems();
            foreach ($stores as $store) {
                /** @var Store $store */
                $storeLocator = $this->getStoreLocatorCollection()->addFieldToFilter('store_id', $store->getId());

                if ($storeLocator->getSize() === 0) {
                    $this->createNewStoreLocator($store);
                } elseif ($storeLocator->getSize() === 1) {
                    // Do nothing
                } elseif ($storeLocator->getSize() > 1) {
                    $ids = [];
                    foreach ($storeLocator as $item) {
                        $ids[] = $item->getData('store_id');
                    }
                    $table = $this->resourceConnection->getTableName('vng_storelocator_store');
                    $this->resourceConnection->getConnection()->delete($table, ['store_id IN (?)' => $ids]);

                    $this->createNewStoreLocator($store);
                }
            }
        }
    }

    /**
     * @param Store $store
     * @return \VNG\Storelocator\Model\Store
     * @throws \Exception
     */
    protected function createNewStoreLocator($store)
    {
        $newStoreLocator = $this->getStoreLocatorModel();
        $newStoreLocator->addData([
            'address' => 'Please fill information',
            'country_id' => 'VN',
            'latitude' => '0.00000000',
            'longitude' => '0.00000000',
            'status' => '2',
            'store_name' => $store->getName(),
            'store_id' => $store->getId(),
            'link' => $store->getCode()
        ]);
        $newStoreLocator->save();

        return $newStoreLocator;
    }

    /**
     * @return \Magento\Store\Model\ResourceModel\Store\Collection
     */
    protected function getStoreCollection()
    {
        return $this->storeCollectionFactory->create();
    }

    /**
     * @return \VNG\Storelocator\Model\ResourceModel\Store\Collection
     */
    protected function getStoreLocatorCollection()
    {
        return $this->storeLocatorCollectionFactory->create();
    }

    /**
     * @return \VNG\Storelocator\Model\Store
     */
    protected function getStoreLocatorModel()
    {
        return $this->storeLocatorModelFactory->create();
    }
}
