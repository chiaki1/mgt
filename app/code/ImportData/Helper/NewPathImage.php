<?php

namespace ImportData\Helper;

class NewPathImage
{
    protected $_logger;

    public function __construct(
        \ImportData\ImportSimpleProduct\Logger\Logger $logger
    ) {
        $this->_logger = $logger;
    }

    public function getNewPathImage($imagePath)
    {
        $newPath = $this->renameImage($imagePath);
        try {
            $isCopy = copy($imagePath, $newPath);
        } catch (\Exception $e) {
            $this->_logger->info('The image do not exist ' . $imagePath);
        }
        if (isset($isCopy)) {
            return $newPath;
        }
        return;
    }

    public function renameImage($image)
    {
        $path = substr($image, 0, strlen($image) - strlen(basename($image)));
        $newPath = $path . time() . basename($image);
        return $newPath;
    }
}
