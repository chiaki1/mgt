<?php

namespace ImportData\Helper;

use Magento\Framework\Filesystem;

require(dirname(__FILE__) . '/../../../library/PHPExcel/Classes/PHPExcel/IOFactory.php');

class ReadFile
{

    private $fileSystem;

    /**
     * ImportData constructor.
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     * @param Filesystem $fileSystem
     */
    public function __construct(
        Filesystem $fileSystem
    ) {
        $this->fileSystem = $fileSystem;
    }

    public function readFile($sheetNumber)
    {
        $filePath = $this->getFilePath();
        if (!is_null($filePath)) {
            try {
                $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);
                $objPHPExcel = $objReader->load($filePath);
            } catch (Exception $e) {
                die('Error, can not read the file "' . pathinfo($filePath, PATHINFO_BASENAME) . '": ' . $e->getMessage());
            }
            $sheetCategory = $objPHPExcel->getSheet($sheetNumber);
//            unlink($filePath);
            return $this->prepareData($sheetCategory);
        }
    }

    public function getFilePath()
    {
        $scanDirectory = scandir($this->getPath());
        if (isset($scanDirectory[2])) {
            $filePath = $this->getPath() . '/' . $scanDirectory[2];
            return $filePath;
        } else {
            return;
        }
    }

    public function getPath()
    {
        return $this->fileSystem
            ->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR)
            ->getAbsolutePath('/import_file');
    }

    public function prepareData($sheet)
    {
        $dataOutput = [];
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 1; $row <= $highestRow; $row++) {
            // Lấy dữ liệu từng dòng và đưa vào mảng $rowData

            $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, '', true, false);
        }
        $key = array_slice($rowData, 0, 1);
        array_shift($rowData);
        foreach ($rowData as $row) {
            $data[] = array_combine($key[0][0], $row[0]);
        }

        foreach ($data as $d) {
            unset($d['']);
            $dataOutput[] = $d;
        }

        if (isset($dataOutput)) {
            return $dataOutput;
        }
    }
}
