<?php
namespace ImportData\ImportCategory\Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
class ImportCategory extends Command
{
    private $readFile;

    protected $categoryFactory;

    protected $categoryLinkManagement;

    private $_objectManager;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    /**
     * ImportData constructor.
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \ImportData\Helper\ReadFile $readFile,
        \Magento\Framework\App\State $state,
        string $name = null
    ) {
        parent::__construct($name);
        $this->categoryFactory = $categoryFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->_objectManager = $objectmanager;
        $this->readFile = $readFile;
        $this->state = $state;
    }
    protected function configure()
    {
        $this->setName('import:category');
        $this->setDescription('import category');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        }
        $dataCategory = $this->readFile->readFile(2);

        if (isset($dataCategory)) {
            $this->createMenuData($dataCategory);
        }
    }


    public function createMenuData($categorys = [])
    {
        if (isset($categorys)) {
            foreach ($categorys as $cate) {
                if (!empty($cate['name'])) {
                    if (isset($cate['category_parent']) && $cate['category_parent'] == null) {
                        var_dump(' create root category ' . $cate['category_parent']);
                        $this->createRootCategory($cate);
                    } else {
                        var_dump('create category ' . $cate['name']);
                        $this->createCategory($cate);
                    }
                }
            }
        }
    }



    public function createRootCategory($data)
    {
        $rootCategory = $this->categoryFactory->create();
        $rootCategory->setName($data['name'])
            ->setIsActive(true)
            ->setParentId(1)
            ->setPath(1)
            ->setUrlKey(rand(10, 100000) . time())
            ->save();
        if(isset($data['image']) && $data['image'] != null) {
            $categoryobj = $this->_objectManager->get('Magento\Catalog\Model\Category')->load($rootCategory->getId());
            $categoryobj->setImage('media/image_product/' . trim($data['image']), ['small_image', 'image', 'thumbnail'], true, false);
            $categoryobj->setCustomAttribute('name_on_app', (isset($data['name_on_app']) && $data['name_on_app'] != null) ? $data['name_on_app']: '');
            $categoryobj->save();
        }
    }

    public function createCategory($data)
    {
        $category = $this->categoryFactory->create();
        $_category = $this->_objectManager->create('Magento\Catalog\Model\Category');
        $parentCategory = $_category->getCollection()
            ->addAttributeToFilter('name', $data['category_parent'])
            ->getFirstItem();

        if ($parentCategory->getId()) {
            $categoryExist = $_category->getCollection()
                ->addAttributeToFilter('name', $data['name'])
                ->addFieldToFilter('parent_id', $parentCategory->getId())
                ->getFirstItem();
            if (!$categoryExist->getId()) {
                $category->setName($data['name'])
                    ->setIsActive(true)
                    ->setParentId($parentCategory->getId())
                    ->setPath($parentCategory->getPath())
                    ->setCustomAttribute('name_on_app', (isset($data['name_on_app']) && $data['name_on_app'] != null) ? $data['name_on_app']: '')
                    ->setUrlKey(rand(10, 100000) . time())
                    ->save();
                if(isset($data['image']) && $data['image'] != null)
                {
                    $categoryobj = $this->_objectManager->get('Magento\Catalog\Model\Category')->load($category->getId());
                    $categoryobj->setImage('media/image_product/' . trim($data['image']), ['small_image','image','thumbnail'], true, false);
                    $categoryobj->save();
                }

            } else {
                // update category
                $categoryobj = $this->_objectManager->get('Magento\Catalog\Model\Category')->load($categoryExist->getId());
                $categoryobj->setCustomAttribute('name_on_app', (isset($data['name_on_app']) && $data['name_on_app'] != null) ? $data['name_on_app']: '');
                if(isset($data['image']) && $data['image'] != null)
                {
                    $categoryobj = $this->_objectManager->get('Magento\Catalog\Model\Category')->load($category->getId());
                    $categoryobj->setImage('media/image_product/' . trim($data['image']), ['small_image','image','thumbnail'], true, false);
                }
                $categoryobj->save();

            }
        } else {
            var_dump("Parent category " . $data['category_parent'] . " do not exist. Please check your data");
            die();
        }
    }
}
