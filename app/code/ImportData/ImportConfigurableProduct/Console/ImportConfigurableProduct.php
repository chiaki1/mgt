<?php

namespace ImportData\ImportConfigurableProduct\Console;

use ImportData\Helper\NewPathImage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportConfigurableProduct extends Command
{
    private $readFile;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    protected $_productFactory;

    protected $_entityAttributeFactory;

    protected $_optionsFactory;

    protected $productRepository;

    protected $logger;

    protected $productLinkFactory;

    protected $categoryLinkRepository;

    private $_objectManager;

    protected $_file;

    protected $newPathImage;

    const attributeSetDefault = [
        'name',
        'sku',
        'image',
        'menu',
        'attribute_codes',
        'sku_simple',
        'position',
        'visibility',
        'description'
    ];

    protected $cacheAttributeOption = [];


    /**
     * ImportData constructor.
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Eav\Model\Entity\AttributeFactory $entityAttributeFactory,
        \Magento\ConfigurableProduct\Helper\Product\Options\Factory $optionsFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \ImportData\Helper\ReadFile $readFile,
        \ImportData\Helper\NewPathImage $newPathImage,
        \ImportData\ImportConfigurableProduct\Logger\Logger $logger,
        \Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\App\State $state,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->readFile = $readFile;
        $this->state = $state;
        $this->_productFactory = $productFactory;
        $this->_entityAttributeFactory = $entityAttributeFactory;
        $this->_optionsFactory = $optionsFactory;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->productLinkFactory = $productLinkFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->_file                    = $file;
        $this->newPathImage             = $newPathImage;
        $this->_objectManager = $objectmanager;
    }

    protected function configure()
    {
        $this->setName('import:configurable');
        $this->setDescription('import category');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        }
        $dataConfigurable = $this->readFile->readFile(3);
        $attributes = array_diff(array_keys($dataConfigurable[0]), self::attributeSetDefault);
        $this->cacheAttributeOptions($attributes);
        $this->createConfigurableProduct($dataConfigurable, $attributes);
    }

    protected function createConfigurableProduct($dataConfigurable, $attributes)
    {
        $cacheCategory = [];
        $dir = $this->_objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        foreach ($dataConfigurable as $data)
        {
            $configurableProduct = $this->_productFactory->create();
            $configurableProduct->setSku($data['sku']);
            $configurableProduct->setName($data['name']);
            $configurableProduct->setTypeId('configurable');
            if($data['visibility'] == 1) {
                $configurableProduct->setVisibility(1);
            } else {
                $configurableProduct->setVisibility(4);
            }
            $configurableProduct->setAttributeSetId(4);
            $configurableProduct->setUrlKey($data['sku']);
            $configurableProduct->setShortDescription(isset($data['description']) ? $data['description']: '');
            $configurableProduct->setStoreId(0);
            $configurableProduct->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock' => 1, //manage stock
                    'is_in_stock' => 1, //Stock Availability
                )
            );
            if (isset($data['image']) && $data['image'] != null) {
                $imagePath = explode(',', $data['image']);
                foreach ($imagePath as $index => $image) {
                    $newPath = $this->newPathImage->getNewPathImage($dir->getPath('media') . '/image_product/' . trim($image));
                    if (trim($image) != '') {
                        if ($index == 0) {
                            try {
                                $configurableProduct->addImageToMediaGallery($newPath, ['small_image','image','thumbnail'], false,false);
                                $this->_file->deleteFile($newPath);

                            } catch (\Exception $e) {
                                $this->logger->info('the image p do not exist ' . $image);
                            }
                        } else {
                            try {
                                $configurableProduct->addImageToMediaGallery($newPath, null, false, false);
                                $this->_file->deleteFile($newPath);
                            } catch (\Exception $e) {
                                $this->logger->info('the image path do not exist ' . $image);
                            }
                        }
                    }
                }
            }

            // set customAttributes of configurable.
            $attributeCodes = explode(',', $data['attribute_codes']);
            $attributeId = [];
            $customerAttribute = [];
            $values = [];
            $optionsFact = [];
            foreach ($attributeCodes as $attriCode) {
                if (trim($attriCode) != '') {
                    $attributeData = $this->_entityAttributeFactory->create()->loadByCode('catalog_product', trim($attriCode));
                    if (!empty($attributeData->getData())) {
                        $attributeId[] = $attributeData->getId();
                        $customerAttribute[] = [
                            "attribute_code" => $attriCode,
                            "value" => $attributeData->getDefaultValue()
                        ];
                        $options = $attributeData->getOptions();
                        foreach ($options as $option){
                            $values[] = [
                                "value_index" => $option->getValue()
                            ];
                        }
                        $optionsFact[] = [
                            "attribute_id" => $attributeData->getId(),
                            "label" => $attributeData->getDefaultFrontendLabel(),
                            "position" => 0,
                            "values" => $values
                        ];
                    } else {
                        $this->logger->info('The attribute code ' . $attriCode .'do not exist, please check again.');
                    }
                }
            }

            $configurableProduct->setCustomAttributes($customerAttribute);

            foreach ($attributes as $attribute) {
                $type = $this->checkTypeInput($attribute);
                if ($type == 'multiselect' || $type == 'select') {
                    $optionId = $this->getOptionId($attribute, $data[$attribute]);
                    if (!is_null($optionId)) {
                        $configurableProduct->setCustomAttribute($attribute, $optionId);
                    }
                } elseif ($type == 'text') {
                    $configurableProduct->setCustomAttribute($attribute, $data[$attribute]);
                } elseif ($type == 'date') {
                    $configurableProduct->setCustomAttribute($attribute,date($data[$attribute]));
                }
            }

            //Assign simple product
            $skuProducts = explode(',', $data['sku_simple']);
            $productIds = [];
            foreach ($skuProducts as $sku) {
                var_dump('sku simple product ' . $sku);
                if (trim($sku) != '') {
                    $pro = $this->_productFactory->create();
                    $productId = $pro->load($pro->getIdBySku(trim($sku)))->getId();
                    var_dump($productId);
                    if (isset($productId)) {
                        $productIds[] = $productId;
                    } else {
                        $this->logger->info("The product do not exist with sku " . trim($sku));
                    }
                }
            }
            $extensionAttrs = $configurableProduct->getExtensionAttributes();
            $extensionAttrs->setConfigurableProductLinks($productIds);
            $optionsFactory = $this->_optionsFactory->create($optionsFact);
            $extensionAttrs->setConfigurableProductOptions($optionsFactory);
            $configurableProduct->setExtensionAttributes($extensionAttrs);

            $this->productRepository->save($configurableProduct);

            // set product
            if ($data['menu'] != '') {
                $menu = explode(',' ,$data['menu']);
                foreach ($menu as $m) {
                    if (!isset($cacheCategory[$m])) {
                        $_category = $this->_objectManager->create('Magento\Catalog\Model\Category');
                        $cate = $_category->getCollection()
                            ->addAttributeToFilter('name', trim($m))
                            ->getFirstItem();

                        if (isset($cate)) {
                            $cacheCategory[$m] = $cate->getId();
                        } else {
                            $this->logger->info('Do not exist the category ' . $m);
                            return null;
                        }
                    }
                    $this->getCategoryLinkManagement($configurableProduct->getSku(), $cacheCategory[$m], $data['position']);
                }
            }
        }
    }
    public function getCategoryLinkManagement($sku, $categoryId, $position)
    {

        $categoryProductLink = $this->productLinkFactory->create();
        $categoryProductLink->setSku($sku);
        $categoryProductLink->setCategoryId($categoryId);
        $categoryProductLink->setPosition($position);
        $this->getCategoryLinkRepository()->save($categoryProductLink);


    }

    private function getCategoryLinkRepository()
    {
        if (null === $this->categoryLinkRepository) {
            $this->categoryLinkRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Catalog\Api\CategoryLinkRepositoryInterface::class);
        }
        return $this->categoryLinkRepository;
    }

    protected function getOptionId($attributeCode, $label)
    {
        $output = array_first(array_filter($this->cacheAttributeOption[$attributeCode]->getSource()->getAllOptions(), function ($_i) use ($label) {
            return strtolower(trim($_i['label'])) == strtolower(trim($label));
        }, ARRAY_FILTER_USE_BOTH));
        return $output['value'];
    }

    protected function checkTypeInput($attribute)
    {
        return $this->cacheAttributeOption[$attribute]->getData()['frontend_input'];
    }

    public function cacheAttributeOptions($attributeCode)
    {
        foreach ($attributeCode as $att) {
            if (isset($this->cacheAttributeOption[$att])) {
                continue;
            } else {
                $attribute = $this->_entityAttributeFactory->create()->loadByCode('catalog_product', $att);
                $this->cacheAttributeOption[$att] = $attribute;
            }
        }
        return $this->cacheAttributeOption;
    }
}


