<?php

namespace ImportData\ImportAttribute\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class ImportAttribute extends Command
{

    private $readFile;

    protected $categoryFactory;

    protected $categoryLinkManagement;

    private $_objectManager;

    protected $eavConfig;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    const NAME = 'update';

    /**
     * ImportData constructor.
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Eav\Model\Config $eavConfig,
        \ImportData\Helper\ReadFile $readFile,
        \Magento\Framework\App\State $state,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->categoryFactory = $categoryFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->_objectManager = $objectmanager;
        $this->readFile = $readFile;
        $this->state = $state;
        $this->eavConfig = $eavConfig;
    }

    protected function configure()
    {
        $this->setName('import:attribute');
        $this->setDescription('import attribute');
        $this->addOption(
            self::NAME,
            null,
            InputOption::VALUE_OPTIONAL,
            'Update'
        );
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        }
        $data = $this->readFile->readFile(1);
        $attributeData = $this->retrieveAttrData($data);
        $this->prepareCreateAttribute($attributeData, $input->getOption(self::NAME));
    }

    protected $_attributes = [];

    protected function retrieveAttrData($data)
    {
        if (count($data) === 0) {
            return $this->_attributes;
        }

        $attr = array_first($data);

        $code = $attr['code'];

        $attrs = array_filter($data, function ($_i) use ($code) {
            return $_i['code'] === $code;
        }, ARRAY_FILTER_USE_BOTH);

        $attr['name'] = array_map(function ($_i) {
            return $_i['name'] . '';
        }, $attrs);
        $this->_attributes[] = $attr;

        return $this->retrieveAttrData(array_filter($data, function ($_i) use ($code) {
            return $_i['code'] !== $code;
        }, ARRAY_FILTER_USE_BOTH));
    }

    public function prepareCreateAttribute($data, $isUpdate)
    {
        foreach ($data as $d) {
            $attribute = $this->eavConfig->getAttribute('catalog_product', $d['code']);
            if (is_null($attribute->getId())) {
                $this->createAttribute($d);
            } else {
                if ($isUpdate == 1) {
                    $this->removeAttribute($d);
                    $this->createAttribute($d);
                }
            }
        }
    }

    public function createAttribute($data)
    {
        $options = [];
        $setup = $this->_objectManager->get('Magento\Framework\Setup\ModuleDataSetupInterface');
        $eav_setup_factory = $this->_objectManager->get('Magento\Eav\Setup\EavSetupFactory');
        $setup->startSetup();
        $eavSetup = $eav_setup_factory->create(['setup' => $setup]);
        if($data['type'] == 'multiselect' || $data['type'] == 'select') {
            $options = [
                'option' => ['values' => $data['name']],
                'filterable' => true,
                'searchable' => true,
                'type' => 'int'
            ];
        } elseif ($data['type'] == 'date') {
            $options = [
                'filterable' => false,
                'type' => 'text',
                'searchable' => false,
            ];
        } else {
            $options = [
                'filterable' => false,
                'type' => 'text',
                'searchable' => true,
            ];
        }
        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, $data['code'],
            array_merge(
                [
                    'group' => 'Product Details',/* Group name in which you want to display your custom attribute */
                    'backend' => '',
                    'frontend' => '',
                    'label' => $data['label'], /* lablel of your attribute*/
                    'input' => $data['type'],
                    'class' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,  /*Scope of your attribute */
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => '',
                    'comparable' => false,
                    'visible_on_front' => true,
                    'used_in_product_listing' => true,
                    'unique' => false
                ], $options)
        );
    }

    public function removeAttribute($data)
    {
        $setup = $this->_objectManager->get('Magento\Framework\Setup\ModuleDataSetupInterface');
        $eav_setup_factory = $this->_objectManager->get('Magento\Eav\Setup\EavSetupFactory');
        $setup->startSetup();
        $eavSetup = $eav_setup_factory->create(['setup' => $setup]);

        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            $data['code']);
    }
}
