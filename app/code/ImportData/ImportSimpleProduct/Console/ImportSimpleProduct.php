<?php

namespace ImportData\ImportSimpleProduct\Console;

use ImportData\Helper\NewPathImage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class ImportSimpleProduct extends Command
{

    protected $productRepository;
    private $readFile;

    private $_objectManager;
    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    protected $productFactory;

    const NAME = 'update';

    protected $cacheAttributeOption = [];

    const attributeSetDefault = [
        'name',
        'sku',
        'price',
        'image',
        'menu',
        'visibility',
        'quantity',
        'description',
        'position'
    ];

    protected $_file;

    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;

    protected $stockRegistry;

    protected $categoryFactory;

    protected $categoryLinkManagement;

    protected $productLinkFactory;

    protected $categoryLinkRepository;
    protected $_entityAttributeFactory;
    protected $output = null;
    protected $_logger;
    protected $newPathImage;

    /**
     * ImportData constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \ImportData\Helper\ReadFile $readFile,
        \ImportData\Helper\NewPathImage $newPathImage,
        \Magento\Framework\App\State $state,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        \Magento\Eav\Model\Entity\AttributeFactory $entityAttributeFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository,
        \ImportData\ImportSimpleProduct\Logger\Logger $logger,
        \Magento\Framework\Filesystem\Driver\File $file,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->_objectManager = $objectmanager;
        $this->readFile = $readFile;
        $this->state = $state;
        $this->productFactory = $productFactory;
        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
        $this->_entityAttributeFactory = $entityAttributeFactory;
        $this->productRepository = $productRepository;
        $this->stockRegistry = $stockRegistry;
        $this->categoryFactory = $categoryFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->productLinkFactory = $productLinkFactory;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->_file                    = $file;
        $this->newPathImage             = $newPathImage;
        $this->_logger = $logger;
    }

    protected function configure()
    {
        $this->setName('import:simple');
        $this->setDescription('import attribute');
        $this->addOption(
            self::NAME,
            null,
            InputOption::VALUE_OPTIONAL,
            'Update'
        );
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Exception $e) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        }
        $data = $this->readFile->readFile(0);

        // lấy hết tất cả att k thuộc attribute set default.
        $attributeCode = array_diff(array_keys($data[0]), self::attributeSetDefault);

        //cache lại attribute options.
        $this->cacheAttributeOptions($attributeCode);

        // create simple product.
        $this->createSimpleProduct($data, $attributeCode);

    }

    public function cacheAttributeOptions($attributeCode)
    {
        foreach ($attributeCode as $att) {
            if (isset($this->cacheAttributeOption[$att])) {
                continue;
            } else {
                $attribute = $this->_entityAttributeFactory->create()->loadByCode('catalog_product', $att);
                $this->cacheAttributeOption[$att] = $attribute;
            }
        }
        return $this->cacheAttributeOption;
    }

    protected function createSimpleProduct($data, $attributeCode)
    {
        $cacheCategory = [];
        $dir = $this->_objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        foreach ($data as $pro) {
            $product = $this->productFactory->create();
            var_dump('create product ' . $pro['name']);
            $product->setName($pro['name']);
            $product->setSku($pro['sku'] . '');
            $product->setTypeId('simple');
            $product->setWeight(0.1);
            if($pro['visibility'] == 1) {
                $product->setVisibility(1);
            } else {
                $product->setVisibility(4);
            }
            $product->setStoreId(0);
            $product->setPrice($pro['price']);
            $product->setShortDescription(isset($pro['description']) ? $pro['description']: '');
            $product->setUrlKey($pro['sku']);
            $product->setStockData(null);
            //string to array
            if (isset($pro['image']) && $pro['image'] != null) {
                $imagePath = explode(',', $pro['image']);
                foreach ($imagePath as $index => $image) {
                    $newPath = $this->newPathImage->getNewPathImage($dir->getPath('media') . '/image_product/' . trim($image));
                    if ($newPath) {
                        if ($index == 0) {
                            try {
                                $product->addImageToMediaGallery($newPath, ['small_image','image','thumbnail'], false, false);
                                $this->_file->deleteFile($newPath);
                            } catch (\Exception $e) {
                                $this->_logger->info('the image p do not exist ' . $image);
                            }
                        } else {
                            try {
                                $product->addImageToMediaGallery($newPath, null, false, false);
                                $this->_file->deleteFile($newPath);
                            } catch (\Exception $e) {
                                $this->_logger->info('the image path do not exist ' . $image);
                            }
                        }
                    }
                }
            }
            foreach ($attributeCode as $attribute) {
                $type = $this->checkTypeInput($attribute);
                if ($type == 'multiselect' || $type == 'select') {
                    $optionId = $this->getOptionId($attribute, $pro[$attribute]);
                    if (!is_null($optionId)) {
                        $product->setCustomAttribute($attribute, $optionId);
                    }
                } elseif ($type == 'text') {
                    $product->setCustomAttribute($attribute, $pro[$attribute]);
                } elseif ($type == 'date') {
                    $product->setCustomAttribute($attribute,date($pro[$attribute]));
                }
            }

            $product->setAttributeSetId(4);
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            try {
                $this->productRepository->save($product);
            } catch (\Exception $e) {
                var_dump($e->getMessage());
                return $e->getMessage();
            }
            $stockItem = $this->stockRegistry->getStockItemBySku($product->getSku());
            $stockItem->setIsInStock(1);
            $stockItem->setQty(isset($pro['quantity']) ? $pro['quantity']:999);
            $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);

            if ($pro['menu'] != '') {
                $menu = explode(',' ,$pro['menu']);
                foreach ($menu as $m) {
                    if (!isset($cacheCategory[$m])) {
                        $_category = $this->_objectManager->create('Magento\Catalog\Model\Category');
                        $cate = $_category->getCollection()
                            ->addAttributeToFilter('name', trim($m))
                            ->getFirstItem();

                        if (isset($cate)) {
                            $cacheCategory[$m] = $cate->getId();
                        } else {
                            $this->_logger->info('Do not exist the category ' . $m);
                            return null;
                        }
                    }
                    $this->getCategoryLinkManagement($product->getSku(), $cacheCategory[$m], $pro['position']);
                }
            }
        }
    }

    public function getCategoryLinkManagement($sku, $categoryId, $position)
    {

        $categoryProductLink = $this->productLinkFactory->create();
        $categoryProductLink->setSku($sku);
        $categoryProductLink->setCategoryId($categoryId);
        $categoryProductLink->setPosition($position);
        $this->getCategoryLinkRepository()->save($categoryProductLink);


    }

    private function getCategoryLinkRepository()
    {
        if (null === $this->categoryLinkRepository) {
            $this->categoryLinkRepository = \Magento\Framework\App\ObjectManager::getInstance()
                ->get(\Magento\Catalog\Api\CategoryLinkRepositoryInterface::class);
        }
        return $this->categoryLinkRepository;
    }

    protected function getOptionId($attributeCode, $label)
    {
        $output = array_first(array_filter($this->cacheAttributeOption[$attributeCode]->getSource()->getAllOptions(), function ($_i) use ($label) {
            return strtolower(trim($_i['label'])) == strtolower(trim($label));
        }, ARRAY_FILTER_USE_BOTH));
        return $output['value'];
    }

    protected function checkTypeInput($attribute)
    {
        return $this->cacheAttributeOption[$attribute]->getData()['frontend_input'];
    }
}
