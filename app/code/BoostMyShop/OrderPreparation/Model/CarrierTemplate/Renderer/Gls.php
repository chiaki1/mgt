<?php namespace BoostMyShop\OrderPreparation\Model\CarrierTemplate\Renderer;

class Gls extends RendererAbstract
{
    public function getShippingLabelFile($orderInProgress, $carrierTemplate)
    {
        $pdfs = [];
        $raiseErrors = (count($orderInProgress) == 1);

        foreach($orderInProgress as $orderInProgress) {
            $labelPdf = false;
            $shipment = $orderInProgress->getShipment();

            try
            {
                $data = $this->getLabelPdf($shipment, $orderInProgress->getip_total_weight());

                if (isset($data['tracking_number'])) {
                    $this->attachTrackingToShipment($shipment, $data['tracking_number']);
                }

                if (!isset($data['pdf']))
                    throw new \Exception(__('GLS label not available.'));

                $labelPdf = $data['pdf'];
                if (!$labelPdf) {
                    throw new \Exception(__('GLS label not available'));
                }

                $pdfs[] = $labelPdf;
            }
            catch(\Exception $ex)
            {
                if ($raiseErrors)
                    throw new \Exception($ex->getMessage());
            }
        }

        //render
        if (count($pdfs) == 1)
            return $pdfs[0];
        else
        {
            //merge labels in a single pdf document
            return $this->mergePdfs($pdfs);
        }
    }


    public function getLabelPdf($shipment, $weight)
    {
        $glsLabel = $this->getObjectManager()->create('BoostMyShop\GlsShipping\Helper\Config');
        return $glsLabel->getShippingLabelByShipment($shipment, $weight);
    }

    protected function attachTrackingToShipment($shipment, $trackingNumber)
    {
        $track = $this->getObjectManager()->create('Magento\Sales\Model\Order\Shipment\Track');

        $shippingMethod = $shipment->getOrder()->getShippingMethod();
        $carrierCode = explode('_', $shippingMethod);
        $data = array(
            'carrier_code' => isset($carrierCode[0])?$carrierCode[0]:$shippingMethod,
            'title' => $shipment->getOrder()->getShippingDescription(),
            'number' => $trackingNumber
        );
        $track->addData($data);
        $shipment->addTrack($track);
        $track->save();
    }

    protected function mergePdfs($pdfs)
    {
        $mergedPdf = new \Zend_Pdf();

        foreach($pdfs as $pdf)
        {
            $pdf1 = \Zend_Pdf::parse($pdf, 1);
            $template = clone $pdf1->pages[0];
            $page1 = new \Zend_Pdf_Page($template);
            $mergedPdf->pages[] = $page1;
        }

        return $mergedPdf->render();
    }
}
