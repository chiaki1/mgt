<?php namespace BoostMyShop\OrderPreparation\Model\CarrierTemplate\Renderer;

class ChronopostLabel extends RendererAbstract
{
    public function getShippingLabelFile($ordersInProgress, $carrierTemplate)
    {
        $labelPdf = false;

        foreach($ordersInProgress as $orderInProgress) {
            $shipment = $orderInProgress->getShipment();
            $labelPdf = $this->getLabelPdf($shipment);
            if(!$labelPdf){
                throw new \Exception(__('An error occurred during the generation of the Chronopost label'));
            }
        }

        if($labelPdf){
            if (is_string($labelPdf))
            {
                return $labelPdf;
            }
            else
            {
                //merge different PDF
                $merged = new \Zend_Pdf();
                foreach ($labelPdf as $value) {
                    $tcpdf = \Zend_Pdf::parse($value);
                    foreach ($tcpdf->pages as $p) {
                        $merged->pages[] = clone $p;
                    }
                }
                return $merged->render();
            }
        }
        else
            throw new \Exception('No label to print');

    }

    public function getLabelPdf($shipment)
    {
        $labelHelper = $this->getObjectManager()->create('Chronopost\Chronorelais\Helper\Shipment');
        return $labelHelper->getEtiquetteUrl($shipment);
    }

}
