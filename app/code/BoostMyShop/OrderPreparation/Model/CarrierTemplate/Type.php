<?php

namespace BoostMyShop\OrderPreparation\Model\CarrierTemplate;

class Type implements \Magento\Framework\Option\ArrayInterface
{

    protected $moduleManager;
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager
    )
    {
        $this->moduleManager = $moduleManager;
    }

    /**
     * Return array of carriers.
     * If $isActiveOnlyFlag is set to true, will return only active carriers
     *
     * @param bool $isActiveOnlyFlag
     * @return array
     */
    public function toOptionArray()
    {
        $methods = [];

        $methods['order_details_export'] = __('Order details');
        $methods['simple_address_label'] = __('Address label');
        $methods['dpdstation3'] = __('Dpd Station');

        if ($this->moduleManager->isEnabled('Colissimo_Label')) {
            $methods['colissimo_label'] = __('Colissimo Label');
        }

        if ($this->moduleManager->isEnabled('Chronopost_Chronorelais')) {
            $methods['chronopost_label'] = __('Chronopost Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_Shippo')) {
            $methods['shippo'] = __('Shippo Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_Boxtal')) {
            $methods['boxtal'] = __('Boxtal Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_AmazonLabel')) {
            $methods['amazon'] = __('Amazon Label');
        }

        if ($this->moduleManager->isEnabled('MondialRelay_Shipping')) {
            $methods['mondial_relay'] = __('Mondial Relay');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_Delivengo')) {
            $methods['delivengo'] = __('Delivengo Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_DpdFrance')) {
            $methods['dpdfrance'] = __('DpdFrance Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_UpsLabel')) {
            $methods['upsoffline'] = __('UPS Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_GlsShipping')) {
            $methods['gls'] = __('GLS Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_LaPoste')) {
            $methods['laposte'] = __('LaPoste Label');
        }

        if ($this->moduleManager->isEnabled('BoostMyShop_Tnt')) {
            $methods['tnt'] = __('Tnt Label');
        }

        $methods['dpdstation3'] = __('Dpd Station');

        return $methods;
    }
}
