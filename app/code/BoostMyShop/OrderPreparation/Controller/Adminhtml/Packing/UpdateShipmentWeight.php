<?php

namespace BoostMyShop\OrderPreparation\Controller\Adminhtml\Packing;

class UpdateShipmentWeight extends \BoostMyShop\OrderPreparation\Controller\Adminhtml\Packing
{

    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();

        $id = $this->getRequest()->getParam('id');
        try
        {
            $shipmentWeight = $this->getRequest()->getParam('shipment_weight');
            $inProgress = $this->_inProgressFactory->create()->load($id);
            $inProgress->setip_total_weight($shipmentWeight)->save();
            if ($inProgress->getip_shipment_id())
            {
                $shipment = $this->_shipmentRepository->get($inProgress->getip_shipment_id());
                $shipment->setTotalWeight($shipmentWeight)->save();
            }

            $this->messageManager->addSuccess(__('Shipment total weight updated'));
        }
        catch(\Exception $ex)
        {
            $this->messageManager->addError($ex->getMessage());
        }

        $this->_redirect('*/*/Index', ['order_id' => $id]);

    }
}
