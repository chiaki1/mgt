<?php
namespace BoostMyShop\OrderPreparation\Block\Preparation;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected $_coreRegistry;

    protected $_template = 'Magento_Backend::widget/tabshoriz.phtml';

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('page_tabs');
        $this->setDestElementId('tab_container');
        $this->setTitle(__('Order Preparation'));

    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $tabs = ['instock' => 'In stock', 'outofstock' => 'Backorder', 'holded' => 'Holded'];

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\Tab\InStock');
        $html = $block->toHtml();
        $this->addTab(
            'tab_instock',
            [
                'label' => __('In Stock')." (".$block->getCollection()->getSize().")",
                'title' => __('In Stock')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\Tab\BackOrder');
        $html = $block->toHtml();
        $this->addTab(
            'tab_backorder',
            [
                'label' => __('Backorder')." (".$block->getCollection()->getSize().")",
                'title' => __('Backorder')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\Tab\Holded');
        $html = $block->toHtml();
        $this->addTab(
            'tab_holded',
            [
                'label' => __('On Hold')." (".$block->getCollection()->getSize().")",
                'title' => __('On Hold')." (".$block->getCollection()->getSize().")",
                'content' => $html
            ]
        );

        $block =  $this->getLayout()->createBlock('BoostMyShop\OrderPreparation\Block\Preparation\InProgress');
        $html = $block->toHtml();
        $this->addTab(
            'tab_progress',
            [
                'label' => __('In progress')." (".$block->getCollection()->getSize().")",
                'title' => __('In progress')." (".$block->getCollection()->getSize().")",
                'content' => $html,
                'active' => true
            ]
        );

        return parent::_beforeToHtml();
    }
}