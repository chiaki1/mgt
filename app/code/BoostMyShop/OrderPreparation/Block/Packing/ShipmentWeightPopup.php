<?php
namespace BoostMyShop\OrderPreparation\Block\Packing;

class ShipmentWeightPopup extends AbstractBlock
{
    protected $_template = 'OrderPreparation/Packing/ShipmentWeightPopup.phtml';

    protected $_catalogProduct;


    public function getInProgress()
    {
        return $this->_coreRegistry->registry('current_inprogress');
    }

    public function getCurrentWeight()
    {
        return $this->getInProgress()->getip_total_weight();
    }

    public function getUpdateShipmentWeightUrl()
    {
        return $this->getUrl('*/*/updateShipmentWeight', ['id' => $this->getInProgress()->getId()]);
    }

}