<?php

namespace BoostMyShop\AdvancedStock\Model\ResourceModel;

class SalesHistory extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('bms_advancedstock_sales_history', 'sh_id');
    }

    public function updateForProductWarehouse($warehouseItemId, $ranges)
    {
        $this->getConnection()->delete($this->getMainTable(), 'sh_warehouse_item_id='.$warehouseItemId);

        $data = ['sh_warehouse_item_id' => $warehouseItemId, 'sh_range_1' => 0, 'sh_range_2' => 0, 'sh_range_3' => 0];

        $fromDate = array();
        for($i=1;$i<=3;$i++)
        {
            $fromDate[$i] = date('Y-m-d', time() - (3600 * 24 * 7) * $ranges[$i - 1]);
        }

        $qtyInvoicedByRange = $this->calculateHistory($warehouseItemId, $fromDate);

        for($i=1;$i<=3;$i++)
        {
            $data['sh_range_'.$i] = (int) $qtyInvoicedByRange['range_'.$i];
        }

        $this->getConnection()->insert($this->getMainTable(), $data);

        return $this;
    }

    public function calculateHistory($warehouseItemId, $fromDate)
    {
        $connection = $this->getConnection();

        $sql = "SELECT
                    SUM(case when created_at >= '".$fromDate[1]."' then qty_invoiced else 0 end) AS range_1,
                    SUM(case when created_at >= '".$fromDate[2]."' then qty_invoiced else 0 end) AS range_2,
                    SUM(case when created_at >= '".$fromDate[3]."' then qty_invoiced else 0 end) AS range_3
                FROM ".$this->getTable('sales_order_item')." 
                    JOIN ".$this->getTable('bms_advancedstock_extended_sales_flat_order_item')." ON item_id = esfoi_order_item_id
                    JOIN ".$this->getTable('bms_advancedstock_warehouse_item')." ON esfoi_warehouse_id = wi_warehouse_id AND product_id = wi_product_id
                WHERE wi_id = " . $warehouseItemId;

        $result = $connection->fetchAll($sql);

        return $result[0];
    }
}