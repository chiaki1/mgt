<?php

namespace BoostMyShop\Supplier\Block\Order\Edit\Tab;

use Magento\Backend\Block\Widget\Grid\Column;

/**
 * Class History
 * @package BoostMyShop\Supplier\Block\Order\Edit\Tab
 */
class History extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * @var \BoostMyShop\Supplier\Model\ResourceModel\Order\History\CollectionFactory|null
     */
    protected $_orderHistoryFactory = null;

    /**
     * History constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \BoostMyShop\Supplier\Model\ResourceModel\Order\History\CollectionFactory $orderHistoryFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \BoostMyShop\Supplier\Model\ResourceModel\Order\History\CollectionFactory $orderHistoryFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_orderHistoryFactory = $orderHistoryFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('orderHistoryGrid');
        $this->setDefaultSort('poh_date');
        $this->setDefaultDir('desc');
        $this->setTitle(__('History'));
        $this->setUseAjax(true);
        $this->setPagerVisibility(false);
        $this->setFilterVisibility(false);
    }

    /**
     * @return mixed
     */
    protected function getOrder()
    {
        return $this->_coreRegistry->registry('current_purchase_order');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_orderHistoryFactory->create();
        $collection->addPoFilter($this->getOrder()->getId());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn('poh_date', ['filter' => false, 'sortable' => true, 'align' => 'center', 'header' => __('Date'), 'index' => 'poh_date', 'type' => 'datetime']);
        $this->addColumn('poh_username', ['filter' => false, 'sortable' => true, 'align' => 'center', 'header' => __('User'), 'index' => 'poh_username', 'type' => 'text']);
        $this->addColumn('poh_description', ['filter' => false, 'sortable' => false, 'align' => 'left', 'header' => __('Details'), 'index' => 'poh_description', 'type' => 'text']);


        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('supplier/order_history/grid', ['po_id' => $this->getOrder()->getId()]);
    }

}
