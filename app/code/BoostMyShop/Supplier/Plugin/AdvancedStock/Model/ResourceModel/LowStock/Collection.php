<?php

namespace BoostMyShop\Supplier\Plugin\AdvancedStock\Model\ResourceModel\LowStock;

class Collection extends \BoostMyShop\AdvancedStock\Model\ResourceModel\LowStock\Collection
{

    protected function _joinFields()
    {
        parent::_joinFields();

        $this->addAttributeToSelect('supply_discontinued');

        //add lead time expression
        $this->getSelect()->joinLeft(
            [$this->getTable('bms_supplier_product')],
            'sp_product_id = e.entity_id and sp_primary = 1',
            []
        );
        $this->getSelect()->joinLeft(
            [$this->getTable('bms_supplier')],
            'sp_sup_id = sup_id',
            ['sup_shipping_delay', 'sup_supply_delay']
        );
        $this->getSelect()->columns(new \Zend_Db_Expr('(sup_shipping_delay + sup_supply_delay) as lead_time'));

        return $this;
    }

}