<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\SalesGraphql\Model\Resolver;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\OrderFactory;

/**
 * ReOrder customer order
 */
class CancelOrder implements ResolverInterface
{

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var OrderManagementInterface
     */
    private $orderManagement;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param OrderManagementInterface $orderManagement
     * @param OrderFactory             $orderFactory
     * @param ScopeConfigInterface     $scopeConfig
     */
    public function __construct(
        OrderManagementInterface $orderManagement,
        OrderFactory             $orderFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->orderFactory    = $orderFactory;
        $this->orderManagement = $orderManagement;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritDoc
     */
    public function resolve(
        Field       $field,
                    $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    ) {
        /** @var ContextInterface $context */
        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The current customer isn\'t authorized.'));
        }

        $currentUserId = $context->getUserId();
        $orderNumber   = $args['orderNumber'] ?? '';
        $storeId       = (string)$context->getExtensionAttributes()->getStore()->getId();

        $order = $this->orderFactory->create()->loadByIncrementIdAndStoreId($orderNumber, $storeId);
        if ((int)$order->getCustomerId() !== $currentUserId) {
            throw new GraphQlInputException(
                __('Order number "%1" doesn\'t belong to the current customer', $orderNumber)
            );
        }

        try {
            if ($this->checkAllowStatus($order->getStatus())) {
                $this->orderManagement->cancel($order->getId());
                return [
                    'message' => __("Cancel order successfully."),
                ];
            } else {
                return [
                    'message' => __("Can not cancel this order."),
                ];
            }
        } catch (Exception $e) {
            return [
                'message' => __($e->getMessage()),
            ];
        }
    }

    private function checkAllowStatus($orderStatus)
    {
        $statuses = $this->scopeConfig->getValue('sales/cancelOrder/allow_attribute', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($statuses) {
            $statuses = explode(',', $statuses);
            if (in_array($orderStatus, $statuses)) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
