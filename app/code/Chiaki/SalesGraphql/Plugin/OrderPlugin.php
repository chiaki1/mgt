<?php

namespace Chiaki\SalesGraphql\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\SalesGraphQl\Model\Formatter\Order;

class OrderPlugin
{

    /**
     * @param Order          $subject
     * @param callable       $proceed
     * @param OrderInterface $orderModel
     *
     * @return array
     */
    public function aroundFormat(
        Order $subject, callable $proceed, OrderInterface $orderModel
    ) {
        $result                = $proceed($orderModel);
        $result['status_code'] = $orderModel->getStatus();
        return $result;
    }
}
