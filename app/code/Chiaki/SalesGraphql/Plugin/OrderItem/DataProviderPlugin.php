<?php

namespace Chiaki\SalesGraphql\Plugin\OrderItem;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Image;
use Magento\SalesGraphQl\Model\OrderItem\DataProvider;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;

class DataProviderPlugin
{

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Image
     */
    private $imageHelper;

    /**
     * @var Emulation
     */
    private $appEmulation;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * DataProviderPlugin constructor.
     *
     * @param StoreManagerInterface      $storeManager
     * @param ProductRepositoryInterface $productRepository
     * @param Emulation                  $appEmulation
     * @param Image                      $imageHelper
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        Emulation $appEmulation,
        Image $imageHelper
    ) {
        $this->storeManager      = $storeManager;
        $this->productRepository = $productRepository;
        $this->appEmulation      = $appEmulation;
        $this->imageHelper       = $imageHelper;
    }

    /**
     * @param DataProvider $subject
     * @param              $result
     *
     * @return array
     */
    public function afterGetOrderItemById(DataProvider $subject, $result): array
    {
        if (!empty($result)) {
            try {
                $productSku = $result['product_sku'];
                $storeId    = $this->storeManager->getStore()->getId();
                $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
                $product         = $this->productRepository->get($productSku);
                $result['image'] = $this->imageHelper->init($product, 'product_base_image')->getUrl();
                $this->appEmulation->stopEnvironmentEmulation();
                //$result['status_code'] = $this->getStatusCode($result['status']);
            } catch (\Exception $e) {
                return $result;
            }
        }
        return $result;
    }

    public function getStatusCode($label)
    {

    }
}
