<?php

namespace Chiaki\SalesGraphql\Plugin;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Model\QuoteFactory;

class CartRewardPoints
{

    /**
     * @var QuoteFactory
     */
    private $quoteFactory;

    /**
     * @param QuoteFactory $quoteFactory
     */
    public function __construct(
        QuoteFactory $quoteFactory
    ) {
        $this->quoteFactory = $quoteFactory;
    }

    /**
     * @param \Magento\RewardGraphQl\Model\Resolver\CartRewardPoints $subject
     * @param Field                                                  $field
     * @param ContextInterface                                       $context
     * @param ResolveInfo                                            $info
     * @param array|null                                             $value
     * @param array|null                                             $args
     *
     * @return array
     */
    public function beforeResolve(
        \Magento\RewardGraphQl\Model\Resolver\CartRewardPoints $subject, Field $field, $context, ResolveInfo $info, array $value = null, array $args = null
    ) {
        if (isset($value['model'])) {
            $quote          = $value['model'];
            $quote          = $this->quoteFactory->create()->load($quote->getId());
            $value['model'] = $quote;
        }
        return [$field, $context, $info, $value, $args];
    }
}
