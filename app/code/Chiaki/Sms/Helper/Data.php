<?php
namespace Chiaki\Sms\Helper;

use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 *
 * @package Sparsh\MobileNumberLogin\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @param Context               $context
     * @param StoreManagerInterface $storeManager
     * @param CustomerFactory       $customerFactory
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
    }

    /**
     * Get config value
     *
     * @param $config
     * @param $scopeCode = null
     * @return string
     */
    public function getConfig($config, $scopeCode = null)
    {
        return $this->scopeConfig->getValue($config, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $scopeCode);
    }

    /**
     * Is module active
     *
     * @param $scopeCode
     * @return bool
     */
    public function getSuffix($scopeCode = null)
    {
        return $this->getConfig('sms/default/suffix', $scopeCode);
    }

    /**
     * Retrieve login mode.
     *
     * @param $scopeCode
     * @return string
     */
    public function getLastname($scopeCode = null)
    {
        return $this->getConfig('sms/default/lastname', $scopeCode);
    }

    /**
     * @param string $sms
     *
     * @return \Magento\Customer\Model\Customer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createCustomer($sms)
    {
        // Get Website ID
        $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();

        // Instantiate object (this is the most important part)
        $customer   = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        // Preparing data for new customer
        $customer->setEmail($sms . $this->getSuffix());
        $customer->setFirstname($sms);
        $customer->setLastname($this->getLastname());
        $customer->setPassword($sms . $this->getSuffix());
        $customer->setData('retail_telephone', $sms);
        // Save data
        $customer->save();
        try {
            $customer->sendNewAccountEmail();
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
        }

        return $customer;
    }
}
