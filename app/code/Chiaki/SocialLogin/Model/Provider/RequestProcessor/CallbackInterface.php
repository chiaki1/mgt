<?php

namespace Chiaki\SocialLogin\Model\Provider\RequestProcessor;

use Chiaki\SocialLogin\Model\Provider\AccountInterface;
use Chiaki\SocialLogin\Model\Provider\Service\ServiceInterface;

/**
 * Interface CallbackInterface
 */
interface CallbackInterface
{
    /**
     * @param ServiceInterface                        $service
     * @param \Magento\Framework\App\RequestInterface $request
     *
     * @return AccountInterface
     */
    public function process(ServiceInterface $service, \Magento\Framework\App\RequestInterface $request);

    /**
     * @param ServiceInterface                        $service
     * @param \Magento\Framework\App\RequestInterface $request
     * @param string                                  $token
     * @param int                                     $expiresIn
     *
     * @return AccountInterface
     */
    public function processWithToken(
        ServiceInterface $service, \Magento\Framework\App\RequestInterface $request, $token, $expiresIn
    );
}
