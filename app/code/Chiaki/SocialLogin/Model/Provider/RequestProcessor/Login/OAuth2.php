<?php

namespace Chiaki\SocialLogin\Model\Provider\RequestProcessor\Login;

use Chiaki\SocialLogin\Model\Provider\RequestProcessor\Login;
use Chiaki\SocialLogin\Model\Provider\Service\ServiceInterface;
use OAuth\Common\Http\Uri\UriInterface;

/**
 * Class OAuth2 login processor
 */
class OAuth2 extends Login
{
    /**
     * {@inheritdoc}
     */
    public function process(ServiceInterface $service, \Magento\Framework\App\RequestInterface $request)
    {
        $authUrl = $service->getAuthorizationUri();
        return $this->buildRedirect($authUrl);
    }

    /**
     * @param ServiceInterface $service
     *
     * @return UriInterface
     */
    public function processApi(ServiceInterface $service)
    {
        return $service->getAuthorizationUri();
    }
}
