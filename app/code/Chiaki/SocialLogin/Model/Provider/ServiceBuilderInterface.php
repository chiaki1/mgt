<?php

namespace Chiaki\SocialLogin\Model\Provider;

use Chiaki\SocialLogin\Model\Provider\Service\ServiceInterface;

/**
 * Interface ServiceBuilderInterface
 */
interface ServiceBuilderInterface
{
    /**
     * @return ServiceInterface
     */
    public function build();
}
