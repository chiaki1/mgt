<?php

namespace Chiaki\SocialLogin\Model\Provider\Account;

use Chiaki\SocialLogin\Model\Provider\AccountInterface;
use Chiaki\SocialLogin\Model\Provider\Service\ServiceInterface;

interface RetrieverInterface
{
    /**
     * @param ServiceInterface $service
     *
     * @return AccountInterface
     */
    public function retrieve(ServiceInterface $service);
}
