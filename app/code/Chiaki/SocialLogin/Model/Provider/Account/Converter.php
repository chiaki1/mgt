<?php

namespace Chiaki\SocialLogin\Model\Provider\Account;

use Chiaki\SocialLogin\Model\Provider\AccountInterface as ProviderAccountInterface;
use Chiaki\SocialLogin\Api\Data\AccountInterface;

/**
 * Class Converter
 */
class Converter implements ConverterInterface
{
    /**
     * @var \Chiaki\SocialLogin\Model\AccountFactory
     */
    protected $accountFactory;

    /**
     * @param \Chiaki\SocialLogin\Model\AccountFactory $accountFactory
     */
    public function __construct(
        \Chiaki\SocialLogin\Model\AccountFactory $accountFactory
    ) {
        $this->accountFactory = $accountFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function convert(ProviderAccountInterface $providerAccount)
    {
        $account = $this->initAccount();

        $account->setType($providerAccount->getType())
                ->setFirstName($providerAccount->getFirstName())
                ->setLastName($providerAccount->getLastName())
                ->setEmail($providerAccount->getEmail())
                ->setSocialId($providerAccount->getSocialId());

        //@TODO image upload
        $account->setImagePath($providerAccount->getImageUrl());

        return $account;
    }

    /**
     * Init account model
     *
     * @return AccountInterface
     */
    protected function initAccount()
    {
        return $this->accountFactory->create();
    }
}
