<?php

namespace Chiaki\SocialLogin\Model\Provider\Account;

use Chiaki\SocialLogin\Model\Provider\AccountInterface as ProviderAccountInterface;
use Chiaki\SocialLogin\Api\Data\AccountInterface;

/**
 * Interface ConverterInterface
 */
interface ConverterInterface
{
    /**
     * Convert provider account to social account
     *
     * @param ProviderAccountInterface $providerAccount
     *
     * @return AccountInterface
     */
    public function convert(ProviderAccountInterface $providerAccount);
}
