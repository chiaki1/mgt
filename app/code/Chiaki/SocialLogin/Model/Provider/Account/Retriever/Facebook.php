<?php

namespace Chiaki\SocialLogin\Model\Provider\Account\Retriever;

use Chiaki\SocialLogin\Model\Provider\Account\AbstractRetriever;
use Chiaki\SocialLogin\Model\Provider\AccountInterface;
use Chiaki\SocialLogin\Model\Provider\Service\ServiceInterface;
use OAuth\Common\Http\Uri\Uri;

class Facebook extends AbstractRetriever
{
    /**
     * Get account method
     */
    const API_METHOD_ACCOUNT_GET = '/me?locale=en_US&fields=id,first_name,last_name,email,picture';

    /**
     * {@inheritdoc}
     */
    protected function requestData(ServiceInterface $service)
    {
        /** @var \Chiaki\SocialLogin\Model\Provider\Service\Facebook $service */
        $response     = $service->request(self::API_METHOD_ACCOUNT_GET);
        $responseData = $this->decodeJson($response);
        return $this->createDataObject()->setData($responseData);
    }

    /**
     * {@inheritdoc}
     */
    protected function requestDataWithToken(ServiceInterface $service, $token, $expiresIn)
    {
        /** @var \Chiaki\SocialLogin\Model\Provider\Service\Facebook $service */
        $response     = $service->requestWithToken(self::API_METHOD_ACCOUNT_GET, $service, $token, $expiresIn);
        $responseData = $this->decodeJson($response);

        return $this->createDataObject()->setData($responseData);
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareResponseData(\Magento\Framework\DataObject $responseData)
    {
        return [
            AccountInterface::TYPE       => AccountInterface::TYPE_FACEBOOK,
            AccountInterface::SOCIAL_ID  => $responseData->getData('id'),
            AccountInterface::FIRST_NAME => $responseData->getData('first_name'),
            AccountInterface::LAST_NAME  => $responseData->getData('last_name'),
            AccountInterface::IMAGE_URL  => $responseData->getData('picture/data/url'),
            AccountInterface::EMAIL      => $responseData->getData('email')
        ];
    }
}
