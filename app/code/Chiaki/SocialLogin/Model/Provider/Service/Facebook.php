<?php

namespace Chiaki\SocialLogin\Model\Provider\Service;

use OAuth\Common\Exception\Exception;
use OAuth\Common\Http\Exception\TokenResponseException;
use OAuth\Common\Http\Uri\Uri;
use OAuth\Common\Http\Uri\UriInterface;
use OAuth\Common\Token\AbstractToken;
use OAuth\Common\Token\Exception\ExpiredTokenException;
use OAuth\Common\Token\TokenInterface;
use OAuth\OAuth2\Token\StdOAuth2Token;

/**
 * Class Facebook
 */
class Facebook extends \OAuth\OAuth2\Service\Facebook implements ServiceInterface
{

    /**
     * {@inheritdoc}
     * @throws \Zend_Json_Exception
     */
    protected function parseAccessTokenResponse($responseBody)
    {
        $data = \Zend_Json::decode($responseBody, true);

        if (!$data) {
            parse_str($responseBody, $data);
        }

        if (null === $data || !is_array($data)) {
            throw new TokenResponseException('Unable to parse response.');
        } elseif (isset($data['error'])) {
            throw new TokenResponseException('Error in retrieving token: "' . $data['error'] . '"');
        }

        $token = new StdOAuth2Token();
        $token->setAccessToken($data['access_token']);

        if (isset($data['expires'])) {
            $token->setLifeTime($data['expires']);
        }

        if (isset($data['refresh_token'])) {
            $token->setRefreshToken($data['refresh_token']);
            unset($data['refresh_token']);
        }

        unset($data['access_token']);
        unset($data['expires']);

        $token->setExtraParams($data);

        return $token;
    }

    /**
     * Sends an authenticated API request to the path provided.
     * If the path provided is not an absolute URI, the base API Uri (must be passed into constructor) will be used.
     *
     * @param string|UriInterface $path
     * @param string              $method       HTTP method
     * @param array               $body         Request body if applicable.
     * @param array               $extraHeaders Extra headers if applicable. These will override service-specific
     *                                          any defaults.
     * @param string              $token
     * @param int                 $expiresIn
     *
     * @return string
     *
     * @throws ExpiredTokenException
     * @throws Exception
     */
    public function requestWithToken(
        $path, ServiceInterface $service, $token, $expiresIn, $method = 'GET', $body = null,
        array $extraHeaders = array()
    ) {
        $uri   = $this->determineRequestUriFromPath($path, $this->baseApiUri);
        $token = $service->getTokenInfo($token, $expiresIn);

        if ($token->getEndOfLife() !== TokenInterface::EOL_NEVER_EXPIRES
            && $token->getEndOfLife() !== TokenInterface::EOL_UNKNOWN
            && time() > $token->getEndOfLife()
        ) {
            throw new ExpiredTokenException(
                sprintf(
                    'Token expired on %s at %s',
                    date('m/d/Y', $token->getEndOfLife()),
                    date('h:i:s A', $token->getEndOfLife())
                )
            );
        }

        // add the token where it may be needed
        if (static::AUTHORIZATION_METHOD_HEADER_OAUTH === $this->getAuthorizationMethod()) {
            $extraHeaders = array_merge(array('Authorization' => 'OAuth ' . $token->getAccessToken()), $extraHeaders);
        } elseif (static::AUTHORIZATION_METHOD_QUERY_STRING === $this->getAuthorizationMethod()) {
            $uri->addToQuery('access_token', $token->getAccessToken());
        } elseif (static::AUTHORIZATION_METHOD_QUERY_STRING_V2 === $this->getAuthorizationMethod()) {
            $uri->addToQuery('oauth2_access_token', $token->getAccessToken());
        } elseif (static::AUTHORIZATION_METHOD_QUERY_STRING_V3 === $this->getAuthorizationMethod()) {
            $uri->addToQuery('apikey', $token->getAccessToken());
        } elseif (static::AUTHORIZATION_METHOD_QUERY_STRING_V4 === $this->getAuthorizationMethod()) {
            $uri->addToQuery('auth', $token->getAccessToken());
        } elseif (static::AUTHORIZATION_METHOD_HEADER_BEARER === $this->getAuthorizationMethod()) {
            $extraHeaders = array_merge(array('Authorization' => 'Bearer ' . $token->getAccessToken()), $extraHeaders);
        }

        $extraHeaders = array_merge($this->getExtraApiHeaders(), $extraHeaders);

        return $this->httpClient->retrieveResponse($uri, $body, $extraHeaders, $method);
    }

    /**
     * {@inheritdoc}
     */
    public function getTokenInfoEndpoint($access_token)
    {
        return new Uri('https://graph.facebook.com/me?access_token=' . $access_token);
    }

    public function getTokenInfo($access_token, $expiresIn)
    {
        $responseBody = $this->httpClient->retrieveResponse(
            $this->getTokenInfoEndpoint($access_token),
            [],
            $this->getExtraOAuthHeaders(),
            'GET'
        );
        return $this->parseAccessTokenResponseFromInfo($access_token, $responseBody, $expiresIn);
    }

    /**
     * {@inheritdoc}
     */
    protected function parseAccessTokenResponseFromInfo($access_token, $responseBody, $expiresIn)
    {
        $data = json_decode($responseBody, true);

        if (null === $data || !is_array($data)) {
            throw new TokenResponseException('Unable to parse response.');
        } elseif (isset($data['error'])) {
            throw new TokenResponseException('Error in retrieving token: "' . $data['error'] . '"');
        }

        $token = new StdOAuth2Token();
        $token->setAccessToken($access_token);
        $token->setLifetime($expiresIn);

        if (isset($data['refresh_token'])) {
            $token->setRefreshToken($data['refresh_token']);
            unset($data['refresh_token']);
        }

        $token->setExtraParams($data);

        return $token;
    }
}
