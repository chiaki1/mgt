<?php

namespace Chiaki\SocialLogin\Model\Serialize;

use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class Serializer
 *
 * @package Chiaki\SocialLogin\Model\Serialize
 */
class Serializer
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
    }

    /**
     * Serialize data into string
     *
     * @param string|int|float|bool|array|null $data
     *
     * @return string|bool
     * @throws \InvalidArgumentException
     */
    public function serialize($data)
    {
        return $this->serializer->serialize($data);
    }

    /**
     * Unserialize the given string
     *
     * @param string $string
     *
     * @return string|int|float|bool|array|null
     * @throws \InvalidArgumentException
     */
    public function unserialize($string)
    {
        return $this->serializer->unserialize($string);
    }
}
