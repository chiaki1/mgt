<?php

namespace Chiaki\SocialLogin\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class InvalidCustomerException
 */
class InvalidCustomerException extends LocalizedException
{

}
