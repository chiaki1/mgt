<?php

namespace Chiaki\SocialLogin\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class InvalidRetrievedDataException
 */
class InvalidRetrievedDataException extends LocalizedException
{

}
