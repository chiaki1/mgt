<?php

namespace Chiaki\SocialLogin\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class InvalidTemplateException
 */
class InvalidTemplateException extends LocalizedException
{

}
