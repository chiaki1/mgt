<?php

namespace Chiaki\SocialLogin\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class UnknownProviderException
 */
class UnknownProviderException extends LocalizedException
{

}
