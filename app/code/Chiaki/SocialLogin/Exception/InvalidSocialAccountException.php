<?php

namespace Chiaki\SocialLogin\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class InvalidSocialAccountException
 */
class InvalidSocialAccountException extends LocalizedException
{

}
