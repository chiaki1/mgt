<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\HomeBannerGraphql\Model\Resolver;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Model\DataProvider\FileInfo;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\CollectionFactory;
use Chiaki\HomeBanner\Model\ResourceModel\Tag\CollectionFactory as TagCollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Model\Config;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Framework\UrlInterface;
use Magento\Framework\Webapi\Exception as WebAPiException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Home banner field resolver, used for GraphQL request processing.
 */
class HomeBanner implements ResolverInterface
{
    private const FILTER_EQUAL_TYPE = 'FilterEqualTypeInput';
    private const FILTER_RANGE_TYPE = 'FilterRangeTypeInput';
    private const FILTER_MATCH_TYPE = 'FilterMatchTypeInput';

    protected $mappingOperatorOptionsEqualType;

    protected $mappingOperatorOptionsRangeType;

    /**
     * @var BannerRepositoryInterface
     */
    protected $bannerRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var FileInfo
     */
    protected $fileInfo;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var TagCollectionFactory
     */
    protected $tagCollectionFactory;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * @var Config
     */
    protected $_eavConfig;

    /**
     * HomeBanner constructor.
     *
     * @param CollectionFactory         $collectionFactory
     * @param BannerRepositoryInterface $bannerRepository
     * @param StoreManagerInterface     $storeManager
     * @param FileInfo                  $fileInfo
     * @param TagCollectionFactory      $tagCollectionFactory
     * @param SerializerJson            $serializer
     * @param Config                    $eavConfig
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        BannerRepositoryInterface $bannerRepository,
        StoreManagerInterface $storeManager,
        FileInfo $fileInfo,
        TagCollectionFactory $tagCollectionFactory,
        SerializerJson $serializer,
        Config $eavConfig
    ) {
        $this->bannerRepository                = $bannerRepository;
        $this->collectionFactory               = $collectionFactory;
        $this->fileInfo                        = $fileInfo;
        $this->storeManager                    = $storeManager;
        $this->tagCollectionFactory            = $tagCollectionFactory;
        $this->serializer                      = $serializer;
        $this->_eavConfig                      = $eavConfig;
        $this->mappingOperatorOptionsEqualType = [
            '==' => 'eq',
            '()' => 'in',
        ];
        $this->mappingOperatorOptionsRangeType = [
            '>=' => 'from',
            '<=' => 'to',
            '>'  => 'from',
            '<'  => 'to',
        ];
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        return $this->getAllBanner();
    }

    /**
     *
     * @return array
     * @throws WebAPiException
     */
    private function getAllBanner()
    {
        $results = [];
        try {
            $items = $this->collectionFactory->create()
                                             ->addFieldToFilter('status', ['eq' => 1])
                                             ->setOrder('banner_position', 'ASC')
                                             ->getItems();

            foreach ($items as $model) {
                $model     = $this->getBanner($model->getBannerId());
                $result    = $model->getData();
                $result    = $this->convertValues('banner_image', $result);
                $result    = $this->convertValues('banner_video', $result);
                $result    = $this->convertDataFilter('data_filters', $result);
                $results[] = $result;
            }
            return $results;
        } catch (\Exception $e) {
            throw new WebAPiException(__($e->getMessage()));
        }
    }

    private function convertValues($attributeCode, $bannerData): array // @codingStandardsIgnoreLine
    {
        if (isset($bannerData[$attributeCode]) && $bannerData[$attributeCode]) {
            $fileName = $bannerData[$attributeCode];
            if ($this->fileInfo->isExist($fileName)) {
                $bannerData[$attributeCode] = $this->getUrl($fileName);
            } elseif (!$this->fileInfo->isExist($fileName)) {
                $bannerData[$attributeCode] = '';
            }
        }

        return $bannerData;
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    private function getUrl($image): string // @codingStandardsIgnoreLine
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $store        = $this->storeManager->getStore();
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->fileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->fileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(FileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    /**
     * @param $bannerId
     *
     * @return BannerInterface|null
     */
    private function getBanner($bannerId)
    {
        try {
            $model = $this->bannerRepository->get($bannerId);
        } catch (NoSuchEntityException $e) {
            $model = null;
        }

        return $model;
    }

    private function convertDataFilter($attributeCode, $bannerData)
    {
        try {
            if (isset($bannerData[$attributeCode]) && $bannerData[$attributeCode]) {
                $dataFilters    = $bannerData[$attributeCode];
                $dataFilters    = $this->serializer->unserialize($dataFilters);
                $newDataFilters = [];
                foreach ($dataFilters as $key => $dataFilter) {
                    if ($dataFilter['type'] == 'Chiaki\HomeBanner\Model\Rule\Condition\Combine') {
                        unset($dataFilters[$key]);
                        continue;
                    }
                    $newDataFilters[$dataFilter['attribute']][] = $dataFilter;
                }
                $filter = [
                    'filter' => []
                ];
                if (count($newDataFilters) > 0) {
                    $filter = [];
                    $filterItems = [];
                    foreach ($newDataFilters as $_attributeCode => $newDataFilter) {
                        if ($_attributeCode == "category_ids") {
                            $dataFilterArray = explode(',', $newDataFilter[0]['value']);
                            $filterItems['category_id'] = [
                                (count($dataFilterArray) > 1 ? "in" : "eq") => (count($dataFilterArray) > 1 ? $this->arrayToFilterArray($dataFilterArray) : ($newDataFilter[0]['value']))
                            ];
                        } elseif ($this->getFilterType($this->_getAttribute($_attributeCode)) == self::FILTER_RANGE_TYPE) {
                            if (count($newDataFilter) > 1) {
                                foreach ($newDataFilter as $k => $item) {
                                    if ($k < (count($newDataFilter) - 1)) {
                                        $filterItems[$_attributeCode] = [
                                            $this->convertOperator($this->_getAttribute($_attributeCode), $item['operator']) => $item['value']
                                        ];
                                    } else {
                                        $filterItems[$_attributeCode] = [
                                            $this->convertOperator($this->_getAttribute($_attributeCode), $item['operator']) => $item['value']
                                        ];
                                    }
                                }
                            } elseif (count($newDataFilter) == 1) {
                                if (isset($this->mappingOperatorOptionsEqualType[$newDataFilter[0]['operator']])) {
                                    $filterItems[$_attributeCode] = [
                                        "from" => $newDataFilter[0]['value'],
                                        "to" => $newDataFilter[0]['value']
                                    ];
                                } else {
                                    $filterItems[$_attributeCode] = [
                                        $this->convertOperator($this->_getAttribute($_attributeCode), $newDataFilter[0]['operator']) => $newDataFilter[0]['value']
                                    ];
                                }
                            }
                        } elseif ($_attributeCode == "attribute_set_id") {
                            $filterItems["attribute_set_id"] = [
                                "eq" => $newDataFilter[0]['value']
                            ];
                        } else {
                            $dataFilterArray = explode(',', $newDataFilter[0]['value']);
                            $filterItems[$_attributeCode] = [
                                $this->convertOperator($this->_getAttribute($_attributeCode), $newDataFilter[0]['operator']) => (count($dataFilterArray) > 1 ? $this->arrayToFilterArray($dataFilterArray) : ($newDataFilter[0]['value']))
                            ];
                        }
                    }
                    $filter["filter"] = $filterItems;
                }

                $bannerData[$attributeCode] = json_encode($filter);
            }
            return $bannerData;
        } catch (\Exception $e) {
            return '';
        }
    }

    private function arrayToFilterArray($array)
    {
        $result = [];
        foreach ($array as $item) {
            $result[] = trim($item);
        }
        return $result;
    }

    /**
     * @param $operator
     *
     * @return string
     */
    private function convertOperator($attribute, $operator)
    {
        if ($this->getFilterType($attribute) == self::FILTER_MATCH_TYPE) {
            return 'in';
        }
        if ($this->getFilterType($attribute) == self::FILTER_EQUAL_TYPE) {
            if (isset($this->mappingOperatorOptionsEqualType[$operator])) {
                return $this->mappingOperatorOptionsEqualType[$operator];
            }
            return 'eq';
        }
        if ($this->getFilterType($attribute) == self::FILTER_RANGE_TYPE) {
            if (isset($this->mappingOperatorOptionsRangeType[$operator])) {
                return $this->mappingOperatorOptionsRangeType[$operator];
            }
            return 'from';
        }
        return 'eq';
    }

    /**
     * Map attribute type to filter type
     *
     * @param Attribute $attribute
     *
     * @return string
     */
    private function getFilterType(Attribute $attribute): string
    {
        $filterTypeMap = [
            'price'       => self::FILTER_RANGE_TYPE,
            'date'        => self::FILTER_RANGE_TYPE,
            'select'      => self::FILTER_EQUAL_TYPE,
            'multiselect' => self::FILTER_EQUAL_TYPE,
            'boolean'     => self::FILTER_EQUAL_TYPE,
            'text'        => self::FILTER_MATCH_TYPE,
            'textarea'    => self::FILTER_MATCH_TYPE,
        ];

        return $filterTypeMap[$attribute->getFrontendInput()] ?? self::FILTER_MATCH_TYPE;
    }

    private function _getAttribute($attributeCode)
    {
        return $this->_eavConfig->getAttribute(Product::ENTITY, $attributeCode);
    }
}
