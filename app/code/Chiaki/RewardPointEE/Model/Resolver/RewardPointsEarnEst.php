<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\RewardPointEE\Model\Resolver;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Model\Quote;
use Magento\Reward\Model\RewardFactory;
use Magento\Tax\Model\Config;

/**
 * @inheritdoc
 */
class RewardPointsEarnEst implements ResolverInterface
{
    /**
     * Reward data
     *
     * @var \Magento\Reward\Helper\Data
     */
    private $rewardData = null;

    /**
     * @var RewardFactory
     */
    private $rewardFactory;

    /**
     * @var \Magento\Tax\Model\Config|null
     */
    private $taxConfig = null;

    /**
     * @param \Magento\Reward\Helper\Data $rewardData
     * @param RewardFactory               $rewardFactory
     */
    public function __construct(
        \Magento\Reward\Helper\Data $rewardData,
        RewardFactory               $rewardFactory
    ) {
        $this->rewardData    = $rewardData;
        $this->rewardFactory = $rewardFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }
        $quote = $value['model'];

        return $this->getRewardPointsEarnValues($quote);
    }

    /**
     * Get Discount Values
     *
     * @param Quote $quote
     *
     * @return array
     */
    private function getRewardPointsEarnValues(Quote $quote)
    {
        if (!$this->rewardData->isOrderAllowed($quote->getStore()->getWebsiteId())) {
            return null;
        }

        if ($quote->getGrandTotal()) {
            // known issue: no support for multishipping quote
            $address = $quote->getIsVirtual() ? $quote->getBillingAddress() : $quote->getShippingAddress();
            // use only money customer spend - shipping & tax
            if ($this->getTaxConfig()->priceIncludesTax($quote->getStoreId())) {
                $monetaryAmount = $quote->getBaseGrandTotal() -
                                  $address->getBaseShippingInclTax();
            } else {
                $monetaryAmount = $quote->getBaseGrandTotal() -
                                  $address->getBaseShippingAmount() -
                                  $address->getBaseTaxAmount();
            }
            $monetaryAmount = $monetaryAmount < 0 ? 0 : $monetaryAmount;
            $pointsDelta    = $this->rewardFactory->create()->getRateToPoints()->calculateToPoints((double)$monetaryAmount);
            return [
                'money'  => [
                    'currency' => $quote->getQuoteCurrencyCode(),
                    'value'    => $this->rewardFactory->create()->getRateToCurrency()->calculateToCurrency($pointsDelta)
                ],
                'points' => $pointsDelta
            ];
        }
        return null;
    }

    /**
     * Get tax config
     *
     * @return \Magento\Tax\Model\Config|mixed|null
     * @deprecated 101.0.2
     */
    private function getTaxConfig()
    {
        if ($this->taxConfig === null) {
            $this->taxConfig = ObjectManager::getInstance()->get(Config::class);
        }
        return $this->taxConfig;
    }
}
