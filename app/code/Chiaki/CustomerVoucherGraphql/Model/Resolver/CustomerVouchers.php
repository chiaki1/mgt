<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\CustomerVoucherGraphql\Model\Resolver;

use Chiaki\CustomerVoucher\Model\ResourceModel\Voucher\CollectionFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\SalesRule\Model\Rule;

/**
 * Orders data resolver
 */
class CustomerVouchers implements ResolverInterface
{

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * CustomerSizes constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The current customer isn\'t authorized.'));
        }
        $userId     = $context->getUserId();
        $vouchers      = $this->collectionFactory->create()->addFieldToFilter('customer_id', $userId);
        $vouchersArray = [];
        foreach ($vouchers as $voucher) {
            $used = (int)$voucher->getData('times_used');
            $vouchersArray[] = [
                'voucher_id'  => $voucher->getData('voucher_id'),
                'code'       => $voucher->getData('code'),
                'source_id'     => $voucher->getData('source_id'),
                'source_type' => $voucher->getData('source_type'),
                'comment' => $voucher->getData('comment'),
                'pick_date' => $voucher->getData('pick_date'),
                'pick_by' => $voucher->getData('pick_by'),
                'used' => !empty($used),
                'name' => $voucher->getData('name'),
                'description' => $voucher->getData('description'),
                'from_date' => $voucher->getData('from_date'),
                'to_date' => $voucher->getData('to_date'),
                'simple_action' => $voucher->getData('simple_action'),
                'simple_action_label' => $this->getActionName($voucher->getData('simple_action')),
                'discount_amount' => $voucher->getData('discount_amount')
            ];
        }
        return [
            'items' => $vouchersArray,
        ];
    }

    private function getActionName($action)
    {
        $default = [
            Rule::BY_PERCENT_ACTION => __('Percent of product price discount'),
            Rule::BY_FIXED_ACTION => __('Fixed amount discount'),
            Rule::CART_FIXED_ACTION => __('Fixed amount discount for whole cart'),
            Rule::BUY_X_GET_Y_ACTION => __('Buy X get Y free (discount amount is Y)')
        ];
        $option = array_merge($default, \Amasty\Rules\Helper\Data::staticGetDiscountTypes());
        return $option[$action] ?? "";
    }
}
