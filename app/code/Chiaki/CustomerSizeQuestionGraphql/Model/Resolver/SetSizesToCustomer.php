<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestionGraphql\Model\Resolver;

use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\Customer\Model\CustomerFactory;
use Chiaki\CustomerSizeQuestion\Model\AnswerFactory;

/**
 * Class SetSizesToCustomer
 *
 * @package Chiaki\CustomerSizeQuestionGraphql\Model\Resolver
 */
class SetSizesToCustomer implements ResolverInterface
{

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @var AnswerFactory
     */
    private $answerFactory;

    /**
     * SetSizesToCustomer constructor.
     *
     * @param CustomerFactory $customerFactory
     * @param AnswerFactory   $answerFactory
     */
    public function __construct(
        CustomerFactory $customerFactory,
        AnswerFactory $answerFactory
    ) {
        $this->customerFactory = $customerFactory;
        $this->answerFactory   = $answerFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var ContextInterface $context */
        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The current customer isn\'t authorized.'));
        }

        if (empty($args['shirt_size'])) {
            throw new GraphQlInputException(__('"shirt_size" value should be specified'));
        }
        if (empty($args['pants_size'])) {
            throw new GraphQlInputException(__('"pants_size" value should be specified'));
        }
        if (empty($args['size_id'])) {
            throw new GraphQlInputException(__('"size_id" value should be specified'));
        }

        try {
            $customer = $this->customerFactory->create()->load($context->getUserId());
            $customer->setData('shirt_size', $args['shirt_size']);
            $customer->setData('pants_size', $args['pants_size']);
            $customer->save();
            $answer = $this->answerFactory->create()->load($args['size_id']);
            if ($answer->getId()) {
                $answer->activeAnswer();
            }
            return ['status' => 'success'];
        } catch (\Exception $e) {
            throw new \Exception(__($e->getMessage()));
        }
    }
}
