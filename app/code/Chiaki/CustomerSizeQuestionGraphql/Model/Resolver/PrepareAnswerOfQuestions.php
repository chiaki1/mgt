<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestionGraphql\Model\Resolver;

use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\GraphQl\Model\Query\ContextInterface;
use Chiaki\CustomerSizeQuestion\Helper\Data;
use Magento\Customer\Model\CustomerFactory;

/**
 * Class PrepareAnswerOfQuestions
 *
 * @package Chiaki\CustomerSizeQuestionGraphql\Model\Resolver
 */
class PrepareAnswerOfQuestions implements ResolverInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * SaveAnswerOfQuestions constructor.
     *
     * @param Data            $helper
     * @param CustomerFactory $customerFactory
     */
    public function __construct(
        Data $helper,
        CustomerFactory $customerFactory
    ) {
        $this->helper          = $helper;
        $this->customerFactory = $customerFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var ContextInterface $context */
        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The current customer isn\'t authorized.'));
        }

        if (empty($args['input']) || !is_array($args['input'])) {
            throw new GraphQlInputException(__('"input" value should be specified'));
        }
        $input        = $args['input'];
        $inputAnswers = $input['answers'];
        $checkHeight  = false;
        $checkWeight  = false;
        $checkCollar  = '';
        $checkSleeve  = '';

        foreach ($inputAnswers as $answer) {
            $questionId = $answer['question_id'];
            if ($questionId == $this->helper->getQuestionHeightId()) {
                $checkHeight = $answer['answer'];
            }
            if ($questionId == $this->helper->getQuestionWeightId()) {
                $checkWeight = $answer['answer'];
            }
            if ($questionId == $this->helper->getQuestionCollarId()) {
                $checkCollar = $answer['answer'];
            }
            if ($questionId == $this->helper->getQuestionSleeveId()) {
                $checkSleeve = $answer['answer'];
            }
        }
        if (!$checkHeight) {
            throw new GraphQlInputException(__('"height" value should be specified'));
        }
        if (!$checkWeight) {
            throw new GraphQlInputException(__('"weight" value should be specified'));
        }

        try {
            $output  = $this->detectOutputData($context->getUserId(), $checkHeight, $checkWeight, $checkCollar, $checkSleeve);
            if (empty($output)) {
                if ($this->isRange($checkHeight)) {
                    $checkHeightArray = explode('-', (string)$checkHeight);
                    $checkHeight = array_sum($checkHeightArray) / 2;
                    $output = $this->detectOutputData($context->getUserId(), $checkHeight, $checkWeight, $checkCollar, $checkSleeve);
                    if (empty($output)) {
                        $output = $this->detectOutputData($context->getUserId(), $checkHeightArray[1], $checkWeight, $checkCollar, $checkSleeve);
                        if (empty($output)) {
                            if ($this->isRange($checkWeight)) {
                                $checkWeightArray = explode('-', (string)$checkWeight);
                                $checkWeight = array_sum($checkWeightArray) / 2;
                                $output = $this->detectOutputData($context->getUserId(), $checkHeight, $checkWeight, $checkCollar, $checkSleeve);
                                if (empty($output)) {
                                    $output = $this->detectOutputData($context->getUserId(), $checkHeightArray[1], $checkWeightArray[1], $checkCollar, $checkSleeve);
                                }
                            }
                        }
                    }

                }
            }
            return $output;
        } catch (\Exception $e) {
            throw new GraphQlInputException(__($e->getMessage()));
        }
    }

    /**
     * @param        $customerId
     * @param        $height
     * @param        $weight
     * @param string $collar
     * @param string $sleeve
     *
     * @return array
     * @throws \Exception
     */
    public function detectOutputData($customerId, $height, $weight, $collar = "", $sleeve = "")
    {
        $data   = $this->helper->getSizeData();
        $result = [];
        foreach ($data as $row => $datum) {
            if ($row > 0) {
                $heightRange      = $datum[2];
                $weightRange      = $datum[3];
                $collarSleeveSize = $datum[1];
                if ($this->checkRangeData($height, $heightRange) && $this->checkRangeData($weight, $weightRange) && $this->checkCollarSleeveSize($collar, $sleeve, $collarSleeveSize)) {
                    $result = $datum;
                } elseif ($this->checkRangeData($height, $heightRange) && $this->checkRangeData($weight, $weightRange)) {
                    $result = $datum;
                }
            }
        }
        $output = [];
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if ($key == 2) {
                    $value = $height;
                }
                if ($key == 3) {
                    $value = $weight;
                }
                $output[] = [
                    'label' => $data[0][$key],
                    'value' => $value
                ];
            }
        }

        if (!empty($output)) {
            $customer = $this->customerFactory->create()->load($customerId);
            $customer->setData('shirt_size', $output[10]['value']);
            $customer->setData('pants_size', $output[9]['value']);
            $customer->save();
        }

        return $output;
    }

    /**
     * @param $value
     * @param $range
     *
     * @return bool
     */
    public function checkRangeData($value, $range)
    {
        if ($this->isRange($value)) {
            return $value == $range;
        }
        $range = explode('-', (string)$range);
        if ($value <= $range[1] && $value >= $range[0]) {
            return true;
        }
        return false;
    }

    /**
     * @param $collar
     * @param $sleeve
     * @param $collarSleeveSize
     *
     * @return bool
     */
    public function checkCollarSleeveSize($collar, $sleeve, $collarSleeveSize)
    {
        $collarSleeveSize = explode('-', (string)$collarSleeveSize);
        if ($collar == $collarSleeveSize[0] && $sleeve == $collarSleeveSize[1]) {
            return true;
        }
        return false;
    }


    /**
     * @param $value
     *
     * @return bool
     */
    public function isRange($value)
    {
        $value = explode('-', (string)$value);
        return count($value) == 2;
    }
}
