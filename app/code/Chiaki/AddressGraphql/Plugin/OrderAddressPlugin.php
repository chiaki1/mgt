<?php

namespace Chiaki\AddressGraphql\Plugin;

use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\SalesGraphQl\Model\Order\OrderAddress;

class OrderAddressPlugin
{
    /**
     * @param OrderAddress   $subject
     * @param callable       $proceed
     * @param OrderInterface $order
     *
     * @return array
     */
    public function aroundGetOrderShippingAddress(OrderAddress $subject, callable $proceed, OrderInterface $order
    ): array {
        $shippingAddress = null;
        if ($order->getShippingAddress()) {
            $shippingAddress = $this->formatAddressData($order->getShippingAddress());
        }
        return $shippingAddress;
    }

    /**
     * @param OrderAddress   $subject
     * @param callable       $proceed
     * @param OrderInterface $order
     *
     * @return array
     */
    public function aroundGetOrderBillingAddress(OrderAddress $subject, callable $proceed, OrderInterface $order
    ): array {
        $billingAddress = null;
        if ($order->getBillingAddress()) {
            $billingAddress = $this->formatAddressData($order->getBillingAddress());
        }
        return $billingAddress;
    }

    /**
     * Customer Order address data formatter
     *
     * @param OrderAddressInterface $orderAddress
     * @return array
     */
    private function formatAddressData(
        OrderAddressInterface $orderAddress
    ): array {
        return
            [
                'firstname' => $orderAddress->getFirstname(),
                'lastname' => $orderAddress->getLastname(),
                'middlename' => $orderAddress->getMiddlename(),
                'postcode' => $orderAddress->getPostcode(),
                'prefix' => $orderAddress->getPrefix(),
                'suffix' => $orderAddress->getSuffix(),
                'street' => $orderAddress->getStreet(),
                'country_code' => $orderAddress->getCountryId(),
                'city' => $orderAddress->getCity(),
                'company' => $orderAddress->getCompany(),
                'fax' => $orderAddress->getFax(),
                'telephone' => $orderAddress->getTelephone(),
                'vat_id' => $orderAddress->getVatId(),
                'region_id' => $orderAddress->getRegionId(),
                'region' => $orderAddress->getRegion(),
                'iz_address_province'=> $orderAddress->getData('iz_address_province'),
                'iz_address_district'=> $orderAddress->getData('iz_address_district'),
                'iz_address_ward'=> $orderAddress->getData('iz_address_ward')
            ];
    }
}
