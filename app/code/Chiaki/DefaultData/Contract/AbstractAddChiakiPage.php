<?php


namespace Chiaki\DefaultData\Contract;


use Chiaki\DefaultData\Setup\Patch\Data\InitDefaultAccountLoginPage;
use Magento\Framework\Setup\Patch\DataPatchInterface;

abstract class AbstractAddChiakiPage implements DataPatchInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Chiaki\Page\Model\ChiakiPage
     */
    private $chiakiPage;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Chiaki\Page\Model\ChiakiPage $chiakiPage
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->chiakiPage = $chiakiPage;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    protected function _getUserId(): string
    {
        return 'default';
    }

    protected function _getStoreId(): string
    {
        return 'default';
    }

    protected abstract function _getUrl(): string;

    protected abstract function _getConfigData(): string;

    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $existed = $this->chiakiPage->getCollection()
            ->addFieldToFilter('user_id', $this->_getUserId())
            ->addFieldToFilter('store_id', $this->_getStoreId())
            ->addFieldToFilter('type', 'CHIAKI_PAGE')
            ->addFieldToFilter('url', $this->_getUrl())
            ->getFirstItem();

        if ($existed->getId()) {
            $this->chiakiPage->getResource()->delete($existed);
        }

        $this->chiakiPage->setData([
            'type' => 'CHIAKI_PAGE',
            'user_id' => $this->_getUserId(),
            'store_id' => $this->_getStoreId(),
            'url' => $this->_getUrl(),
            'config_data' => $this->_getConfigData()])
            ->save();
        $this->moduleDataSetup->endSetup();
    }

    public function getAliases(): array
    {
        return [];
    }
}
