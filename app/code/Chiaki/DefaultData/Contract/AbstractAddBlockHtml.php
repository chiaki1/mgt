<?php


namespace Chiaki\DefaultData\Contract;


use Magento\Framework\Setup\Patch\DataPatchInterface;

abstract class AbstractAddBlockHtml implements DataPatchInterface
{

    /**
     * @var \Magento\Cms\Model\BlockRepository
     */
    private $blockRepository;
    /**
     * @var \Magento\Cms\Api\Data\BlockInterfaceFactory
     */
    private $blockDataFactory;
    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $blockFactory;
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddLoginTermBlockHtml constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\BlockRepository $blockRepository
     * @param \Magento\Cms\Api\Data\BlockInterfaceFactory $blockInterfaceFactory
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\BlockRepository $blockRepository,
        \Magento\Cms\Api\Data\BlockInterfaceFactory $blockInterfaceFactory,
        \Magento\Cms\Model\BlockFactory $blockFactory
    )
    {
        $this->blockRepository = $blockRepository;
        $this->blockDataFactory = $blockInterfaceFactory;
        $this->blockFactory = $blockFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    protected abstract function _getBlockHtml(): string;

    protected function _getStores()
    {
        return [0];
    }

    protected function _getUserId(): string
    {
        return 'default';
    }

    protected abstract function _getIdentifier(): string;

    protected abstract function _getTitle(): string;

    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $existed = $this->blockFactory->create()
            ->getCollection()
            ->addFieldToFilter('identifier', $this->_getIdentifier())
            ->addFieldToFilter('user_id', $this->_getUserId())
            ->getFirstItem();

        if ($existed->getId()) {
            $this->blockFactory->create()->getResource()->delete($existed);
        }

        $this->blockFactory->create()->setTitle($this->_getTitle())
            ->setIdentifier($this->_getIdentifier())
            ->setContent(
                $this->_getBlockHtml()
            )->setIsActive(
                1
            )->setStores(
                $this->_getStores()
            )->setUserId($this->_getUserId())
            ->save();

        $this->moduleDataSetup->endSetup();
    }
}
