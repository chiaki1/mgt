<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Magento\Framework\Setup\Patch\DataPatchInterface;

class InitDefaultCategoryPage implements DataPatchInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Chiaki\Page\Model\ChiakiPage
     */
    private $chiakiPage;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Chiaki\Page\Model\ChiakiPage $chiakiPage
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->chiakiPage = $chiakiPage;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function apply(): InitDefaultCategoryPage
    {
        $this->moduleDataSetup->startSetup();
        $existed = $this->chiakiPage->getCollection()
            ->addFieldToFilter('user_id', 'default')
            ->addFieldToFilter('store_id', 'default')
            ->addFieldToFilter('type', 'CATEGORY')
            ->getFirstItem();

        if ($existed->getId()) {
            $this->chiakiPage->getResource()->delete($existed);
        }

        $this->chiakiPage->setData([
            'type' => 'CATEGORY',
            'user_id' => 'default',
            'store_id' => 'default',
            'config_data' => '{"uiId":"DEFAULT_ROOT","extensionDataConfigs":[{"forHookId":"header","extensionDataConfigs":[{"uiId":"HEADER"},{"uiId":"NAVIGATOR"}]},{"forHookId":"main","extensionDataConfigs":[{"uiId":"PRODUCTS","extensionDataConfigs":[{"forHookId":"products","extensionDataConfigs":[{"uiId":"PRODUCTS_BREADCRUMBS"},{"uiId":"PRODUCTS_CATEGORY_INFO"},{"uiId":"PRODUCTS_AGGREGATIONS"},{"uiId":"PRODUCTS_FILTERS"},{"uiId":"PRODUCT_LISTING"}]}]}]},{"forHookId":"footer","extensionDataConfigs":[{"uiId":"PCMS_PAGE_OR_DEFAULT","additionalData":[{"key":"urlKey","value":"custom_footer"}]}]}]}'
        ])->save();
        $this->moduleDataSetup->endSetup();

        return $this;
    }

    public function getAliases(): array
    {
        return [];
    }
}
