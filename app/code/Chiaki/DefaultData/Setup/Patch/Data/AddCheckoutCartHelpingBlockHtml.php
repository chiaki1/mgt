<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Chiaki\DefaultData\Contract\AbstractAddBlockHtml;

class AddCheckoutCartHelpingBlockHtml extends AbstractAddBlockHtml
{

    protected function _getBlockHtml(): string
    {
        return '<div class="gl-vspace-bpall-medium">
                    <h4 class="gl-label gl-label--l gl-label--bold">
                      NEED HELP?
                    </h4>
                    <div>
                      <p>
                        <span class="gl-link">Delivery</span>
                      </p>
                      <p>
                        <span class="gl-link">Return &amp; Refund</span>
                      </p>
                      <p>
                        <span class="gl-link">Ordering &amp; Payment</span>
                      </p>
                      <p>
                        <span class="gl-link">
                          Promotions &amp; Vouchers
                        </span>
                      </p>
                    </div>
                  </div>';
    }

    protected function _getIdentifier(): string
    {
        return 'chiaki_checkout_cart_helping';
    }

    protected function _getTitle(): string
    {
        return 'chiaki_checkout_cart_helping';
    }
}
