<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Magento\Framework\Setup\Patch\DataPatchInterface;

class InitDefaultProductPage implements DataPatchInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Chiaki\Page\Model\ChiakiPage
     */
    private $chiakiPage;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Chiaki\Page\Model\ChiakiPage $chiakiPage
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->chiakiPage = $chiakiPage;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function apply(): InitDefaultProductPage
    {
        $this->moduleDataSetup->startSetup();
        $existed = $this->chiakiPage->getCollection()
            ->addFieldToFilter('user_id', 'default')
            ->addFieldToFilter('store_id', 'default')
            ->addFieldToFilter('type', 'PRODUCT')
            ->getFirstItem();

        if ($existed->getId()) {
            $this->chiakiPage->getResource()->delete($existed);
        }

        $this->chiakiPage->setData([
            'type' => 'PRODUCT',
            'user_id' => 'default',
            'store_id' => 'default',
            'config_data' => '{"uiId":"DEFAULT_ROOT","extensionDataConfigs":[{"forHookId":"header","extensionDataConfigs":[{"uiId":"HEADER"},{"uiId":"NAVIGATOR"}]},{"forHookId":"main","extensionDataConfigs":[{"uiId":"PRODUCT"}]}]}'
        ])->save();
        $this->moduleDataSetup->endSetup();

        return $this;
    }

    public function getAliases(): array
    {
        return [];
    }
}
