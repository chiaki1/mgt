<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddUserIdDefaultForAllExistingBlock implements DataPatchInterface
{
    /**
     * @var \Magento\Cms\Model\BlockRepository
     */
    private $blockRepository;
    /**
     * @var \Magento\Cms\Api\Data\BlockInterfaceFactory
     */
    private $blockDataFactory;
    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $blockFactory;
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddLoginTermBlockHtml constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Cms\Model\BlockRepository $blockRepository
     * @param \Magento\Cms\Api\Data\BlockInterfaceFactory $blockInterfaceFactory
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Cms\Model\BlockRepository $blockRepository,
        \Magento\Cms\Api\Data\BlockInterfaceFactory $blockInterfaceFactory,
        \Magento\Cms\Model\BlockFactory $blockFactory
    )
    {
        $this->blockRepository = $blockRepository;
        $this->blockDataFactory = $blockInterfaceFactory;
        $this->blockFactory = $blockFactory;
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $existed = $this->blockFactory->create()
            ->getCollection()
            ->addFieldToFilter('user_id', [
                'in' => [null, '']
            ]);


        foreach ($existed as $item) {
            $this->blockFactory->create()->load($item->getData('block_id'))->setData('user_id', 'default')->save();
        }
        $this->moduleDataSetup->endSetup();
        return $this;
    }
}
