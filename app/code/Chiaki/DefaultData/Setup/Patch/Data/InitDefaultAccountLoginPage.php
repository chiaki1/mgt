<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Magento\Framework\Setup\Patch\DataPatchInterface;

class InitDefaultAccountLoginPage implements DataPatchInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var \Chiaki\Page\Model\ChiakiPage
     */
    private $chiakiPage;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Chiaki\Page\Model\ChiakiPage $chiakiPage
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->chiakiPage = $chiakiPage;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function apply(): InitDefaultAccountLoginPage
    {
        $this->moduleDataSetup->startSetup();
        $existed = $this->chiakiPage->getCollection()
            ->addFieldToFilter('user_id', 'default')
            ->addFieldToFilter('store_id', 'default')
            ->addFieldToFilter('type', 'CHIAKI_PAGE')
            ->addFieldToFilter('url', 'account-login')
            ->getFirstItem();

        if ($existed->getId()) {
            $this->chiakiPage->getResource()->delete($existed);
        }

        $this->chiakiPage->setData([
            'type' => 'CHIAKI_PAGE',
            'user_id' => 'default',
            'store_id' => 'default',
            'url' => 'account-login',
            'config_data' => '{"uiId":"DEFAULT_ROOT","extensionDataConfigs":[{"forHookId":"header","extensionDataConfigs":[{"uiId":"HEADER"},{"uiId":"NAVIGATOR"}]},{"forHookId":"main","extensionDataConfigs":[{"uiId":"TWO_COLUMNS","extensionDataConfigs":[{"forHookId":"columnOne","extensionDataConfigs":[{"uiId":"STACK","extensionDataConfigs":[{"uiId":"ACCOUNT_CHECK_AUTH"},{"uiId":"LOGIN_PASSWORD_FORM"},{"uiId":"PCMS_BLOCK","additionalData":[{"key":"identifier","value":"login_term_and_conditions"}]},{"uiId":"ACCOUNT_SOCIAL_LOGIN"}]}]},{"forHookId":"columnTwo","extensionDataConfigs":[{"uiId":"STACK","extensionDataConfigs":[{"uiId":"PCMS_BLOCK","additionalData":[{"key":"identifier","value":"login-data"}]}]}]}]}]}]}'
        ])->save();
        $this->moduleDataSetup->endSetup();

        return $this;
    }

    public function getAliases(): array
    {
        return [];
    }
}
