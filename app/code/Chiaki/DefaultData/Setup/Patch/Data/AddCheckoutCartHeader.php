<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Chiaki\DefaultData\Contract\AbstractAddBlockHtml;

class AddCheckoutCartHeader extends AbstractAddBlockHtml
{

    protected function _getBlockHtml(): string
    {
        return '<div class="ui-checkoutPage__head">
      <div class="container">
        <div class="row checkout-header__container">
          <div class="col-12 col-sm-3">
            <img
              class="df"
              src="https://vinagiay.cdn.vccloud.vn/pub/static/frontend/VNG/default/vi_VN/images/logo.svg"
              title=""
              alt="giay nam, giay nu, "
              width="164"
              height="36"
            />
          </div>
          <div class="col-12 col-sm-9">
            <div class="d-flex align-items-center justify-content-end">
              <div class="questions__phone d-flex align-items-center pr-3">
                <svg class="gl-icon gl-icon--size-communication">
                  <use href="#contact-phone">
                    <svg id="contact-phone" viewBox="0 0 9 19">
                      <title>contact-phone</title>
                      <g
                        fill="none"
                        stroke="currentColor"
                        strokeMiterlimit="10"
                      >
                        <path d="M.5 2.5h8v14h-8zm0 11h8m-8-9h8" />
                        <circle cx="4.5" cy="14.5" r="1" />
                      </g>
                    </svg>
                  </use>
                </svg>
                <span class="questions__text-content">
                  <div class="d-flex">
                    <div class="text-uppercase pr-2">Câu hỏi? </div>
                    <a class="gl-link" href="tel:+44 1244 747261">
                      <strong class="">1900636562</strong>
                    </a>
                  </div>
                </span>
              </div>
              <div class="opening d-flex">
                <span class="opening__hours">
                  <div>Thứ Hai đến Thứ Sáu: 8 giờ sáng - 10 giờ tối</div>
                </span>
                <span class="opening__hours pl-3">
                  <div>T7 - CN: 9h sáng - 9h tối</div>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
    }

    protected function _getIdentifier(): string
    {
        return 'chiaki_checkout_cart_header';
    }

    protected function _getTitle(): string
    {
        return 'chiaki_checkout_cart_header';
    }
}
