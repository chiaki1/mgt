<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;

use Chiaki\DefaultData\Contract\AbstractAddBlockHtml;

class AddLoginTermBlockHtml extends AbstractAddBlockHtml
{
    protected function _getBlockHtml(): string
    {
        return '<div className="block-password">
        <div className="gl-vspace-bpall-small">
          <span className="f-16">
            <span>I have read and accepted </span>
            <span>
              <a className="gl-link gl-link" href="#" target="_blank">
                Creators Club Terms &amp; Conditions
              </a>
            </span>
            <span> and the </span>
            <span>
              <a className="gl-link" href="#" target="_blank">
                adidas Privacy Notice
              </a>
            </span>
            <span />
          </span>
        </div>
      </div>
      ';
    }

    protected function _getIdentifier(): string
    {
        return 'login_term_and_conditions';
    }

    protected function _getTitle(): string
    {
        return 'login_term_and_conditions';
    }
}
