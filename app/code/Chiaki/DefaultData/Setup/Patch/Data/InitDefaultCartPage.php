<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Chiaki\DefaultData\Contract\AbstractAddChiakiPage;

class InitDefaultCartPage extends AbstractAddChiakiPage
{

    protected function _getUrl(): string
    {
        return 'cart';
    }

    protected function _getConfigData(): string
    {
        return '{"uiId":"DEFAULT_ROOT","extensionDataConfigs":[{"forHookId":"header","extensionDataConfigs":[{"uiId":"PCMS_BLOCK","additionalData":[{"key":"identifier","value":"chiaki_checkout_cart_header"}]}]},{"forHookId":"main","extensionDataConfigs":[{"uiId":"CHECKOUT_CART","extensionDataConfigs":[{"forHookId":"left","extensionDataConfigs":[{"uiId":"STACK","extensionDataConfigs":[{"uiId":"CHECKOUT_CART_ITEMS"}]}]},{"forHookId":"right","extensionDataConfigs":[{"uiId":"STACK","extensionDataConfigs":[{"uiId":"CHECKOUT_BUTTON_CHECKOUT"},{"uiId":"CHECKOUT_LOGIN"},{"uiId":"CHECKOUT_CART_SUMMARY"},{"uiId":"CHECKOUT_CART_COUPON"},{"uiId":"PCMS_BLOCK","additionalData":[{"key":"identifier","value":"chiaki_checkout_cart_helping"}]},{"uiId":"PCMS_BLOCK","additionalData":[{"key":"identifier","value":"chiaki_checkout_cart_accept_payment_method"}]}]}]}]}]}]}';
    }
}
