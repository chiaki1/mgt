<?php


namespace Chiaki\DefaultData\Setup\Patch\Data;


use Chiaki\DefaultData\Contract\AbstractAddBlockHtml;

class AddCheckoutCartAcceptPaymentMethod extends AbstractAddBlockHtml
{

    protected function _getBlockHtml(): string
    {
        return '<div class="gl-vspace-bpall-medium bottom-content">
                    <h4 class="gl-label gl-label--l gl-label--bold">
                      Accepted payment methods
                    </h4>
                    <img
                      alt=""
                      src="https://brand.assets.adidas.com/image/upload/f_auto,q_auto,fl_lossy/enGB/Images/visa-amex-master-maestro-pp-klarna-gc_tcm143-454282.png"
                    />
                  </div>';
    }

    protected function _getIdentifier(): string
    {
        return 'chiaki_checkout_cart_accept_payment_method';
    }

    protected function _getTitle(): string
    {
        return 'chiaki_checkout_cart_accept_payment_method';
    }
}
