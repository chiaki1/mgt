<?php
/**
 *
 * @author Khoi Le - mr.vjcspy@gmail.com
 * @time 9/5/20 8:45 PM
 *
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Chiaki_CatalogGraphql', __DIR__);
