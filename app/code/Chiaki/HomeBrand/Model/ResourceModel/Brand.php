<?php

declare(strict_types=1);

namespace Chiaki\HomeBrand\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Brand extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('home_brand', 'brand_id');
    }
}

