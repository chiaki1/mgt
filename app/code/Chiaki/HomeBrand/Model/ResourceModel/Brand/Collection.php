<?php

declare(strict_types=1);

namespace Chiaki\HomeBrand\Model\ResourceModel\Brand;

use Chiaki\HomeBrand\Model\ResourceModel\Brand;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'brand_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\HomeBrand\Model\Brand::class,
            Brand::class
        );
    }
}

