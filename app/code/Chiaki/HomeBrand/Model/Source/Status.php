<?php

namespace Chiaki\HomeBrand\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class
 */
class Status implements ArrayInterface
{
    const STATUS_DISABLED = 0;

    const STATUS_ENABLED = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::STATUS_DISABLED, 'label' => __('Disabled')],
            ['value' => self::STATUS_ENABLED, 'label' => __('Enable')],
        ];
    }
}
