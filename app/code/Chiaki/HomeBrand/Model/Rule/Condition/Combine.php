<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\HomeBrand\Model\Rule\Condition;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Rule\Model\Condition\Context;

/**
 * Combination of product conditions
 */
class Combine extends \Magento\Rule\Model\Condition\Combine
{
    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * {@inheritdoc}
     */
    protected $elementName = 'parameters';

    /**
     * @var array
     */
    private $excludedAttributes;

    /**
     * Combine constructor.
     *
     * @param Context        $context
     * @param ProductFactory $conditionFactory
     * @param array          $data
     * @param array          $excludedAttributes
     */
    public function __construct(
        Context        $context,
        ProductFactory $conditionFactory,
        array          $data = [],
        array          $excludedAttributes = []
    ) {
        $this->productFactory = $conditionFactory;
        parent::__construct($context, $data);
        $this->setType(Combine::class);
        $this->excludedAttributes = $excludedAttributes;
    }

    /**
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $productAttributes = $this->productFactory->create()->loadAttributeOptions()->getAttributeOption();
        $attributes        = [];
        foreach ($productAttributes as $code => $label) {
            if (!in_array($code, $this->excludedAttributes)) {
                $attributes[] = [
                    'value' => Product::class . '|' . $code,
                    'label' => $label,
                ];
            }
        }
        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive(
            $conditions,
            [
                ['label' => __('Product Attribute'), 'value' => $attributes]
            ]
        );
        return $conditions;
    }

    /**
     * Collect validated attributes for Product Collection
     *
     * @param Collection $productCollection
     *
     * @return $this
     */
    public function collectValidatedAttributes($productCollection)
    {
        foreach ($this->getConditions() as $condition) {
            $condition->collectValidatedAttributes($productCollection);
        }
        return $this;
    }

    /**
     * Load value options
     *
     * @return \Magento\Rule\Model\Condition\Combine
     */
    public function loadValueOptions()
    {
        $this->setValueOption([1 => __('TRUE')]);
        return $this;
    }

    /**
     * Load aggregation options
     *
     * @return \Magento\Rule\Model\Condition\Combine
     */
    public function loadAggregatorOptions()
    {
        $this->setAggregatorOption(['all' => __('ALL')]);
        return $this;
    }
}
