<?php

declare(strict_types=1);

namespace Chiaki\HomeBrand\Model\Data;

use Chiaki\HomeBrand\Api\Data\BrandInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Brand extends AbstractSimpleObject implements BrandInterface
{

    /**
     * Get brand_id
     *
     * @return string|null
     */
    public function getBrandId()
    {
        return $this->_get(self::BRAND_ID);
    }

    /**
     * Set brand_id
     *
     * @param string $brandId
     *
     * @return BrandInterface
     */
    public function setBrandId($brandId)
    {
        return $this->setData(self::BRAND_ID, $brandId);
    }

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return BrandInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BrandInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get brand_position
     *
     * @return string|null
     */
    public function getBrandPosition()
    {
        return $this->_get(self::BRAND_POSITION);
    }

    /**
     * Set brand_position
     *
     * @param string $brandPosition
     *
     * @return BrandInterface
     */
    public function setBrandPosition($brandPosition)
    {
        return $this->setData(self::BRAND_POSITION, $brandPosition);
    }

    /**
     * Get brand_image
     *
     * @return string|null
     */
    public function getBrandImage()
    {
        return $this->_get(self::BRAND_IMAGE);
    }

    /**
     * Set brand_image
     *
     * @param string $brandImage
     *
     * @return BrandInterface
     */
    public function setBrandImage($brandImage)
    {
        return $this->setData(self::BRAND_IMAGE, $brandImage);
    }

    /**
     * Get data_filters
     *
     * @return string|null
     */
    public function getDataFilters()
    {
        return $this->_get(self::DATA_FILTERS);
    }

    /**
     * Set data_filters
     *
     * @param string $dataFilters
     *
     * @return BrandInterface
     */
    public function setDataFilters($dataFilters)
    {
        return $this->setData(self::DATA_FILTERS, $dataFilters);
    }

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     *
     * @param string $createdAt
     *
     * @return BrandInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}

