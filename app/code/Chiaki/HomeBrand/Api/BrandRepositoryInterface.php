<?php

declare(strict_types=1);

namespace Chiaki\HomeBrand\Api;

use Chiaki\HomeBrand\Api\Data\BrandInterface;
use Chiaki\HomeBrand\Api\Data\BrandSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface BrandRepositoryInterface
{

    /**
     * Save Brand
     *
     * @param BrandInterface $brand
     *
     * @return BrandInterface
     * @throws LocalizedException
     */
    public function save(
        BrandInterface $brand
    );

    /**
     * Retrieve Brand
     *
     * @param string $brandId
     *
     * @return BrandInterface
     * @throws LocalizedException
     */
    public function get($brandId);

    /**
     * Retrieve Brand matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return BrandSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Brand
     *
     * @param BrandInterface $brand
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        BrandInterface $brand
    );

    /**
     * Delete Brand by ID
     *
     * @param string $brandId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($brandId);
}

