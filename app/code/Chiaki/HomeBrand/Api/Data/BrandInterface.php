<?php

declare(strict_types=1);

namespace Chiaki\HomeBrand\Api\Data;

interface BrandInterface
{

    const DATA_FILTERS   = 'data_filters';
    const BRAND_POSITION = 'brand_position';
    const TITLE          = 'title';
    const CREATED_AT     = 'created_at';
    const BRAND_IMAGE    = 'brand_image';
    const BRAND_ID       = 'brand_id';
    const STATUS         = 'status';

    /**
     * Get brand_id
     *
     * @return string|null
     */
    public function getBrandId();

    /**
     * Set brand_id
     *
     * @param string $brandId
     *
     * @return BrandInterface
     */
    public function setBrandId($brandId);

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return BrandInterface
     */
    public function setStatus($status);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BrandInterface
     */
    public function setTitle($title);

    /**
     * Get brand_position
     *
     * @return string|null
     */
    public function getBrandPosition();

    /**
     * Set brand_position
     *
     * @param string $brandPosition
     *
     * @return BrandInterface
     */
    public function setBrandPosition($brandPosition);

    /**
     * Get brand_image
     *
     * @return string|null
     */
    public function getBrandImage();

    /**
     * Set brand_image
     *
     * @param string $brandImage
     *
     * @return BrandInterface
     */
    public function setBrandImage($brandImage);

    /**
     * Get data_filters
     *
     * @return string|null
     */
    public function getDataFilters();

    /**
     * Set data_filters
     *
     * @param string $dataFilters
     *
     * @return BrandInterface
     */
    public function setDataFilters($dataFilters);

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     *
     * @param string $createdAt
     *
     * @return BrandInterface
     */
    public function setCreatedAt($createdAt);
}

