<?php

declare(strict_types=1);

namespace Chiaki\HomeBrand\Controller\Adminhtml\Brand;

use Chiaki\HomeBrand\Model\Brand;
use Exception;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

class Delete extends \Chiaki\HomeBrand\Controller\Adminhtml\Brand
{

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('brand_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(Brand::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Brand.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['brand_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Brand to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

