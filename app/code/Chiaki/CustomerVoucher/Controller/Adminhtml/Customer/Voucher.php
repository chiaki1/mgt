<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Reward admin customer controller
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
namespace Chiaki\CustomerVoucher\Controller\Adminhtml\Customer;

abstract class Voucher extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Chiaki_CustomerVoucher::manage_voucher';
}
