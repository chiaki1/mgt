<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Chiaki\CustomerVoucher\Controller\Adminhtml\Customer\Voucher;

/**
 * @codeCoverageIgnore
 */
class VoucherGrid extends \Chiaki\CustomerVoucher\Controller\Adminhtml\Customer\Voucher
{
    /**
     * History Grid Ajax Action
     *
     * @return void
     *
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
