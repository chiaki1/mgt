<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Chiaki\CustomerVoucher\Controller\Adminhtml\Customer\Voucher;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Chiaki\CustomerVoucher\Controller\Adminhtml\Customer\Voucher as VoucherAction;

/**
 * Used for accordion on a customer page, requires POST because of the accordion mechanism.
 *
 * @codeCoverageIgnore
 */
class Voucher extends VoucherAction implements HttpPostActionInterface
{
    /**
     * History Ajax Action
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}
