<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Model;

use Chiaki\CustomerVoucher\Api\Data\VoucherInterfaceFactory;
use Chiaki\CustomerVoucher\Api\Data\VoucherSearchResultsInterfaceFactory;
use Chiaki\CustomerVoucher\Api\VoucherRepositoryInterface;
use Chiaki\CustomerVoucher\Model\ResourceModel\Voucher as ResourceVoucher;
use Chiaki\CustomerVoucher\Model\ResourceModel\Voucher\CollectionFactory as VoucherCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class VoucherRepository implements VoucherRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $voucherCollectionFactory;

    protected $dataVoucherFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $voucherFactory;


    /**
     * @param ResourceVoucher $resource
     * @param VoucherFactory $voucherFactory
     * @param VoucherInterfaceFactory $dataVoucherFactory
     * @param VoucherCollectionFactory $voucherCollectionFactory
     * @param VoucherSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceVoucher $resource,
        VoucherFactory $voucherFactory,
        VoucherInterfaceFactory $dataVoucherFactory,
        VoucherCollectionFactory $voucherCollectionFactory,
        VoucherSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->voucherFactory = $voucherFactory;
        $this->voucherCollectionFactory = $voucherCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataVoucherFactory = $dataVoucherFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Chiaki\CustomerVoucher\Api\Data\VoucherInterface $voucher
    ) {
        /* if (empty($voucher->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $voucher->setStoreId($storeId);
        } */
        
        $voucherData = $this->extensibleDataObjectConverter->toNestedArray(
            $voucher,
            [],
            \Chiaki\CustomerVoucher\Api\Data\VoucherInterface::class
        );
        
        $voucherModel = $this->voucherFactory->create()->setData($voucherData);
        
        try {
            $this->resource->save($voucherModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the voucher: %1',
                $exception->getMessage()
            ));
        }
        return $voucherModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($voucherId)
    {
        $voucher = $this->voucherFactory->create();
        $this->resource->load($voucher, $voucherId);
        if (!$voucher->getId()) {
            throw new NoSuchEntityException(__('Voucher with id "%1" does not exist.', $voucherId));
        }
        return $voucher->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->voucherCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Chiaki\CustomerVoucher\Api\Data\VoucherInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Chiaki\CustomerVoucher\Api\Data\VoucherInterface $voucher
    ) {
        try {
            $voucherModel = $this->voucherFactory->create();
            $this->resource->load($voucherModel, $voucher->getVoucherId());
            $this->resource->delete($voucherModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Voucher: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($voucherId)
    {
        return $this->delete($this->get($voucherId));
    }
}

