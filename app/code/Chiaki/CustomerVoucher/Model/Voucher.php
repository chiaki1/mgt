<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Model;

use Chiaki\CustomerVoucher\Api\Data\VoucherInterface;
use Chiaki\CustomerVoucher\Api\Data\VoucherInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Voucher extends \Magento\Framework\Model\AbstractModel
{

    protected $voucherDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'customer_voucher';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param VoucherInterfaceFactory $voucherDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Chiaki\CustomerVoucher\Model\ResourceModel\Voucher $resource
     * @param \Chiaki\CustomerVoucher\Model\ResourceModel\Voucher\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        VoucherInterfaceFactory $voucherDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Chiaki\CustomerVoucher\Model\ResourceModel\Voucher $resource,
        \Chiaki\CustomerVoucher\Model\ResourceModel\Voucher\Collection $resourceCollection,
        array $data = []
    ) {
        $this->voucherDataFactory = $voucherDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve voucher model with voucher data
     * @return VoucherInterface
     */
    public function getDataModel()
    {
        $voucherData = $this->getData();

        $voucherDataObject = $this->voucherDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $voucherDataObject,
            $voucherData,
            VoucherInterface::class
        );

        return $voucherDataObject;
    }
}

