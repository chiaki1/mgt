<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Model\ResourceModel;

class Voucher extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_voucher', 'voucher_id');
    }
}

