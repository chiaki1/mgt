<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Model\ResourceModel\Voucher;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'voucher_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\CustomerVoucher\Model\Voucher::class,
            \Chiaki\CustomerVoucher\Model\ResourceModel\Voucher::class
        );
    }

    /**
     *
     * @param string $customerId
     * @return $this
     */
    public function addCustomerFilter($customerId)
    {
        if ($customerId) {
            $this->getSelect()->where('main_table.customer_id = ?', $customerId);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()->joinLeft(['coupon' => $this->getTable('salesrule_coupon')], 'main_table.code = coupon.code', ['times_used']);
        $this->getSelect()->joinLeft(['rule' => $this->getTable('salesrule')], 'coupon.rule_id = rule.rule_id', ['name', 'description', 'from_date', 'to_date', 'simple_action', 'discount_amount', 'rule_id']);
        return $this;
    }
}

