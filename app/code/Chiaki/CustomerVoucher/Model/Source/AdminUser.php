<?php

namespace Chiaki\CustomerVoucher\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\User\Model\ResourceModel\User\CollectionFactory;

class AdminUser implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_userCollectionFactory;

    /**
     * @param CollectionFactory $userCollectionFactory
     */
    public function __construct(
        CollectionFactory $userCollectionFactory
    ) {
        $this->_userCollectionFactory = $userCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        /** @var \Magento\User\Model\ResourceModel\User\Collection $collection */
        $collection = $this->_userCollectionFactory->create();
        $result = [];
        foreach ($collection as $item) {
            $result[] = [
                'value' => $item->getId(),
                'label' => $item->getUserName()
            ];
        }
        return $result;
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     * phpcs:disable Magento2.Functions.StaticFunction
     */
    public function getOptionArray()
    {
        /** @var \Magento\User\Model\ResourceModel\User\Collection $collection */
        $collection = $this->_userCollectionFactory->create();
        $result = [];
        foreach ($collection as $item) {
            $result[$item->getId()] = $item->getUserName();
        }
        return $result;
    }
}
