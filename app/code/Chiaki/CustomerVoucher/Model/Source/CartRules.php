<?php

namespace Chiaki\CustomerVoucher\Model\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory;

/**
 * Class
 */
class CartRules implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_ruleCollectionFactory;

    /**
     * @param CollectionFactory $ruleCollection
     */
    public function __construct(
        CollectionFactory $ruleCollectionFactory
    ) {
        $this->_ruleCollectionFactory = $ruleCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        /** @var \Magento\SalesRule\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_ruleCollectionFactory->create();
        $collection->getSelect()->where('is_active = 1 AND coupon_type = 3')->orWhere('is_active = 1 AND coupon_type = 2 AND use_auto_generation = 1');
        $result = [];
        $result[] = [
            'value' => '',
            'label' => __("Please select sale rule.")
        ];
        foreach ($collection as $item) {
            $result[] = [
                'value' => $item->getRuleId(),
                'label' => $item->getName()
            ];
        }
        return $result;
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     * phpcs:disable Magento2.Functions.StaticFunction
     */
    public function getOptionArray()
    {
        /** @var \Magento\SalesRule\Model\ResourceModel\Rule\Collection $collection */
        $collection = $this->_ruleCollectionFactory->create();
        $collection->getSelect()->where('is_active = 1 AND coupon_type = 3')->orWhere('is_active = 1 AND coupon_type = 2 AND use_auto_generation = 1');
        $result = [];
        $result[''] = __("Please select sale rule.");
        foreach ($collection as $item) {
            $result[$item->getRuleId()] = $item->getName();
        }
        return $result;
    }
}
