<?php

namespace Chiaki\CustomerVoucher\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class
 */
class SourceType implements ArrayInterface
{
    const TYPE_CART_RULE = 0;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::TYPE_CART_RULE, 'label' => __('Cart Price Rule')],
        ];
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     * phpcs:disable Magento2.Functions.StaticFunction
     */
    public function getOptionArray()
    {
        return [self::TYPE_CART_RULE => __('Cart Price Rule')];
    }
}
