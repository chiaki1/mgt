<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Model\Data;

use Chiaki\CustomerVoucher\Api\Data\VoucherInterface;

class Voucher extends \Magento\Framework\Api\AbstractExtensibleObject implements VoucherInterface
{

    /**
     * Get voucher_id
     * @return string|null
     */
    public function getVoucherId()
    {
        return $this->_get(self::VOUCHER_ID);
    }

    /**
     * Set voucher_id
     * @param string $voucherId
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setVoucherId($voucherId)
    {
        return $this->setData(self::VOUCHER_ID, $voucherId);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Chiaki\CustomerVoucher\Api\Data\VoucherExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\CustomerVoucher\Api\Data\VoucherExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get source_id
     * @return string|null
     */
    public function getSourceId()
    {
        return $this->_get(self::SOURCE_ID);
    }

    /**
     * Set source_id
     * @param string $sourceId
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setSourceId($sourceId)
    {
        return $this->setData(self::SOURCE_ID, $sourceId);
    }

    /**
     * Get source_type
     * @return string|null
     */
    public function getSourceType()
    {
        return $this->_get(self::SOURCE_TYPE);
    }

    /**
     * Set source_type
     * @param string $sourceType
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setSourceType($sourceType)
    {
        return $this->setData(self::SOURCE_TYPE, $sourceType);
    }

    /**
     * Get comment
     * @return string|null
     */
    public function getComment()
    {
        return $this->_get(self::COMMENT);
    }

    /**
     * Set comment
     * @param string $comment
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * Get pick_date
     * @return string|null
     */
    public function getPickDate()
    {
        return $this->_get(self::PICK_DATE);
    }

    /**
     * Set pick_date
     * @param string $pickDate
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setPickDate($pickDate)
    {
        return $this->setData(self::PICK_DATE, $pickDate);
    }

    /**
     * Get pick_by
     * @return string|null
     */
    public function getPickBy()
    {
        return $this->_get(self::PICK_BY);
    }

    /**
     * Set pick_by
     * @param string $pickBy
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setPickBy($pickBy)
    {
        return $this->setData(self::PICK_BY, $pickBy);
    }
}

