<?php

namespace Chiaki\CustomerVoucher\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\SalesRule\Api\Data\CouponGenerationSpecInterface;
use Magento\SalesRule\Helper\Coupon;
use Magento\SalesRule\Model\CouponGenerator;
use Magento\SalesRule\Model\RuleFactory;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_SALES_RULE_COUPON_QTY = 'promo/auto_generated_coupon_codes/qty';

    /**
     * @var RuleFactory
     */
    protected $ruleFactory;

    /**
     * @var Coupon
     */
    protected $_couponHelper;

    /**
     * @var CouponGenerator
     */
    protected $_couponGenerator;

    /**
     * @param Context         $context
     * @param RuleFactory     $ruleFactory
     * @param Coupon          $couponHelper
     * @param CouponGenerator $couponGenerator
     */
    public function __construct(
        Context         $context,
        RuleFactory     $ruleFactory,
        Coupon          $couponHelper,
        CouponGenerator $couponGenerator
    ) {
        parent::__construct($context);
        $this->ruleFactory      = $ruleFactory;
        $this->_couponHelper    = $couponHelper;
        $this->_couponGenerator = $couponGenerator;
    }

    /**
     * @param $ruleId
     *
     * @return string|string[]
     * @throws NoSuchEntityException
     */
    public function generateCode($ruleId)
    {
        $rule = $this->ruleFactory->create()->load($ruleId);
        if ($rule->getId()) {
            $data  = [
                'rule_id' => $ruleId,
                'qty'     => $this->getDefaultQty(),
                'length'  => $this->_couponHelper->getDefaultLength(),
                'format'  => CouponGenerationSpecInterface::COUPON_FORMAT_ALPHANUMERIC,
                'dash'    => $this->_couponHelper->getDefaultDashInterval()
            ];
            $codes = $this->_couponGenerator->generateCodes($data);
            if (!empty($codes)) {
                return $codes[0];
            }
            return $codes;
        } else {
            throw new NoSuchEntityException(__('Rule with id "%1" does not exist.', $ruleId));
        }
    }

    /**
     * Get default coupon code length
     *
     * @return int
     */
    public function getDefaultQty()
    {
        return (int)$this->scopeConfig->getValue(
            self::XML_PATH_SALES_RULE_COUPON_QTY,
            ScopeInterface::SCOPE_STORE
        );
    }
}
