<?php

namespace Chiaki\CustomerVoucher\Observer\Html;

/**
 * Sales rule coupon new columns (customer).
 */
class BeforeBlockToHtml implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return null
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $grid = $observer->getBlock();

        /**
         * \Magento\SalesRule\Block\Adminhtml\Promo\Quote\Edit\Tab\Coupons\Grid
         */
        if ($grid instanceof \Magento\SalesRule\Block\Adminhtml\Promo\Quote\Edit\Tab\Coupons\Grid) {
            $grid->addColumnAfter(
                'customer_id',
                [
                    'header' => __('Customer Assigned'),
                    'index' => 'code',
                    'type' => 'text',
                    'default' => '',
                    'align' => 'center',
                    'width' => '160',
                    'renderer' => \Chiaki\CustomerVoucher\Block\Adminhtml\Promo\Quote\Edit\Tab\Coupons\Grid\Column\Renderer\Customer::class
                ],
                'times_used'
            );
        }
    }
}
