<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Observer;

use Chiaki\CustomerVoucher\Helper\Data;
use Chiaki\CustomerVoucher\Model\VoucherFactory;
use Magento\Backend\Model\Auth\Session;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\SalesRule\Model\RuleFactory;

class SaveVoucher implements ObserverInterface
{
    /**
     * Customer converter
     *
     * @var CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * Reward factory
     *
     * @var VoucherFactory
     */
    protected $_voucherFactory;

    /**
     * @var Data
     */
    protected $_couponHelper;

    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * @var RuleFactory
     */
    protected $_ruleFactory;

    /**
     * @param VoucherFactory   $voucherFactory
     * @param CustomerRegistry $customerRegistry
     * @param Data             $couponHelper
     * @param Session          $authSession
     * @param RuleFactory      $ruleFactory
     */
    public function __construct(
        VoucherFactory   $voucherFactory,
        CustomerRegistry $customerRegistry,
        Data             $couponHelper,
        Session          $authSession,
        RuleFactory      $ruleFactory
    ) {
        $this->_voucherFactory  = $voucherFactory;
        $this->customerRegistry = $customerRegistry;
        $this->_couponHelper    = $couponHelper;
        $this->_authSession     = $authSession;
        $this->_ruleFactory     = $ruleFactory;
    }

    /**
     * pick coupon for customer
     *
     * @param Observer $observer
     *
     * @return $this|void
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        $data    = $request->getPost('voucher');
        if ($data) {
            /** @var CustomerInterface $customer */
            $customer = $observer->getEvent()->getCustomer();

            if (isset($data['source_type']) && $data['source_type'] == 0 && isset($data['source_id']) && $data['source_id']) {
                $rule = $this->_ruleFactory->create()->load($data['source_id']);
                if ($rule->getId()) {
                    $coupons = $rule->getCoupons();
                    foreach ($coupons as $coupon) {
                        if (!$this->isAssigned($coupon->getCode())) {
                            $couponCode = $coupon->getCode();
                            break;
                        }
                    }
                }
                $data['code']        = $couponCode ?? $this->_couponHelper->generateCode($data['source_id']);
                $data['customer_id'] = $customer->getId();
                $data['pick_by']     = $this->_authSession->getUser()->getId();
                $voucher             = $this->_voucherFactory->create();
                $voucher->addData($data);
                $voucher->save();
            }
        }

        return $this;
    }

    /**
     * @param $couponCode
     *
     * @return bool
     */
    public function isAssigned($couponCode)
    {
        $collection = $this->_voucherFactory->create()->getCollection();
        $collection->addFieldToFilter('main_table.code', $couponCode);
        return $collection->getSize() > 0;
    }
}
