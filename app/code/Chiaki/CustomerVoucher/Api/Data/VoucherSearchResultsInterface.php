<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Api\Data;

interface VoucherSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Voucher list.
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface[]
     */
    public function getItems();

    /**
     * Set code list.
     * @param \Chiaki\CustomerVoucher\Api\Data\VoucherInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

