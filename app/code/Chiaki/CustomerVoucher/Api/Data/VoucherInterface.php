<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Api\Data;

interface VoucherInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SOURCE_TYPE = 'source_type';
    const PICK_DATE = 'pick_date';
    const SOURCE_ID = 'source_id';
    const COMMENT = 'comment';
    const VOUCHER_ID = 'voucher_id';
    const PICK_BY = 'pick_by';
    const CODE = 'code';

    /**
     * Get voucher_id
     * @return string|null
     */
    public function getVoucherId();

    /**
     * Set voucher_id
     * @param string $voucherId
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setVoucherId($voucherId);

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setCode($code);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Chiaki\CustomerVoucher\Api\Data\VoucherExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\CustomerVoucher\Api\Data\VoucherExtensionInterface $extensionAttributes
    );

    /**
     * Get source_id
     * @return string|null
     */
    public function getSourceId();

    /**
     * Set source_id
     * @param string $sourceId
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setSourceId($sourceId);

    /**
     * Get source_type
     * @return string|null
     */
    public function getSourceType();

    /**
     * Set source_type
     * @param string $sourceType
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setSourceType($sourceType);

    /**
     * Get comment
     * @return string|null
     */
    public function getComment();

    /**
     * Set comment
     * @param string $comment
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setComment($comment);

    /**
     * Get pick_date
     * @return string|null
     */
    public function getPickDate();

    /**
     * Set pick_date
     * @param string $pickDate
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setPickDate($pickDate);

    /**
     * Get pick_by
     * @return string|null
     */
    public function getPickBy();

    /**
     * Set pick_by
     * @param string $pickBy
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     */
    public function setPickBy($pickBy);
}

