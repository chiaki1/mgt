<?php
declare(strict_types=1);

namespace Chiaki\CustomerVoucher\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface VoucherRepositoryInterface
{

    /**
     * Save Voucher
     * @param \Chiaki\CustomerVoucher\Api\Data\VoucherInterface $voucher
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Chiaki\CustomerVoucher\Api\Data\VoucherInterface $voucher
    );

    /**
     * Retrieve Voucher
     * @param string $voucherId
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($voucherId);

    /**
     * Retrieve Voucher matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Chiaki\CustomerVoucher\Api\Data\VoucherSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Voucher
     * @param \Chiaki\CustomerVoucher\Api\Data\VoucherInterface $voucher
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Chiaki\CustomerVoucher\Api\Data\VoucherInterface $voucher
    );

    /**
     * Delete Voucher by ID
     * @param string $voucherId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($voucherId);
}

