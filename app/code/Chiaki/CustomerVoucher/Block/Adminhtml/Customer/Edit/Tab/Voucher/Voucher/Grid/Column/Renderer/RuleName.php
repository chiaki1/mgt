<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\CustomerVoucher\Block\Adminhtml\Customer\Edit\Tab\Voucher\Voucher\Grid\Column\Renderer;

class RuleName extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Render "Expired / not expired" reward "Reason" field
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    protected function _getValue(\Magento\Framework\DataObject $row)
    {
        return "<a href='". $this->getUrl('sales_rule/promo_quote/edit', ['id' => $row->getData('rule_id')]) ."'>" . $row->getData($this->getColumn()->getIndex()) . "</a>";
    }
}
