<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\CustomerVoucher\Block\Adminhtml\Customer\Edit\Tab\Voucher\Voucher\Grid\Column\Renderer;

class SourceType extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * Render "Expired / not expired" reward "Reason" field
     *
     * @param   \Magento\Framework\DataObject $row
     * @return  string
     */
    protected function _getValue(\Magento\Framework\DataObject $row)
    {
        $sourceType = '';
        if ($row->getData('source_type') == 0) {
            $sourceType = '<span>' . __('Cart Price Rules') . '</span> ';
        }
        return $sourceType;
    }
}
