<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\CustomerVoucher\Block\Adminhtml\Customer\Edit\Tab\Voucher\Management;

use Magento\Customer\Controller\RegistryConstants;

/**
 * Reward update points form
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Update extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Core system store model
     *
     * @var \Magento\Store\Model\System\StoreFactory
     */
    protected $_storeFactory;

    /**
     * @var \Magento\Customer\Model\CustomerRegistry
     */
    protected $customerRegistry;

    /**
     * @var \Chiaki\CustomerVoucher\Model\Source\CartRules
     */
    protected $_cartRules;

    /**
     * @var \Chiaki\CustomerVoucher\Model\Source\SourceType
     */
    protected $_sourceType;

    /**
     * @param \Magento\Backend\Block\Template\Context         $context
     * @param \Magento\Framework\Registry                     $registry
     * @param \Magento\Framework\Data\FormFactory             $formFactory
     * @param \Magento\Store\Model\System\StoreFactory        $storeFactory
     * @param \Magento\Customer\Model\CustomerRegistry        $customerRegistry
     * @param \Chiaki\CustomerVoucher\Model\Source\CartRules  $cartRules
     * @param \Chiaki\CustomerVoucher\Model\Source\SourceType $sourceType
     * @param array                                           $data
     *
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context         $context,
        \Magento\Framework\Registry                     $registry,
        \Magento\Framework\Data\FormFactory             $formFactory,
        \Magento\Store\Model\System\StoreFactory        $storeFactory,
        \Magento\Customer\Model\CustomerRegistry        $customerRegistry,
        \Chiaki\CustomerVoucher\Model\Source\CartRules  $cartRules,
        \Chiaki\CustomerVoucher\Model\Source\SourceType $sourceType,
        array                                           $data = []
    ) {
        $this->_storeFactory    = $storeFactory;
        $this->customerRegistry = $customerRegistry;
        $this->_cartRules       = $cartRules;
        $this->_sourceType      = $sourceType;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Getter
     *
     * @return \Magento\Customer\Model\Customer
     * @codeCoverageIgnore
     */
    public function getCustomer()
    {
        $customerId = $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
        return $this->customerRegistry->retrieve($customerId);
    }

    /**
     * Prepare form before rendering HTML
     *
     * @return $this
     * @codeCoverageIgnore
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('voucher_');
        $form->setFieldNameSuffix('voucher');
        $fieldset = $form->addFieldset('pick_fieldset', ['legend' => __('Pick voucher')]);

        $fieldset->addField(
            'source_type',
            'select',
            [
                'name'           => 'source_type',
                'title'          => __('Source Type'),
                'label'          => __('Source Type'),
                'values'         => $this->_sourceType->getOptionArray(),
                'data-form-part' => $this->getData('target_form')
            ]
        );

        $fieldset->addField(
            'source_id',
            'select',
            [
                'name'           => 'source_id',
                'title'          => __('Sale Rules'),
                'label'          => __('Sale Rules'),
                'values'         => $this->_cartRules->getOptionArray(),
                'data-form-part' => $this->getData('target_form')
            ]
        );

        $fieldset->addField(
            'comment',
            'text',
            [
                'name'           => 'comment',
                'title'          => __('Comment'),
                'label'          => __('Comment'),
                'data-form-part' => $this->getData('target_form')
            ]
        );

        $this->updateFromSession($form, $this->getCustomer()->getId());

        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Update form elements from session data
     *
     * @param \Magento\Framework\Data\Form $form
     * @param int                          $customerId
     *
     * @return void
     */
    protected function updateFromSession(\Magento\Framework\Data\Form $form, $customerId)
    {
        $data = $this->_backendSession->getCustomerFormData();
        if (!empty($data)) {
            $dataCustomerId = isset($data['customer']['entity_id']) ? $data['customer']['entity_id'] : null;
            if (isset($data['voucher']) && $customerId == $dataCustomerId) {
                $form->addValues($data['voucher']);
            }
        }
    }
}
