<?php

namespace Chiaki\CustomerVoucher\Block\Adminhtml\Customer\Edit\Tab\Voucher\Voucher;

/**
 * @api
 * @codeCoverageIgnore
 * @since 100.0.2
 */
class Grid extends \Magento\Backend\Block\Widget\Grid
{
    /**
     * Prepare grid collection object
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $customerId = $this->getRequest()->getParam('id', 0);
        $this->getCollection()->addCustomerFilter($customerId);
        return parent::_prepareCollection();
    }
}
