<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Reward management container
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 * @codeCoverageIgnore
 */
namespace Chiaki\CustomerVoucher\Block\Adminhtml\Customer\Edit\Tab\Voucher;

class Management extends \Magento\Backend\Block\Template
{
    /**
     * Reward management template
     *
     * @var string
     */
    protected $_template = 'customer/edit/management.phtml';

    /**
     * Prepare layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {

        $update = $this->getLayout()->createBlock(
            \Chiaki\CustomerVoucher\Block\Adminhtml\Customer\Edit\Tab\Voucher\Management\Update::class,
            '',
            [
                'data' => [
                    'target_form' => $this->getData('target_form'),
                ]
            ]
        );

        $this->setChild('update', $update);

        return parent::_prepareLayout();
    }
}
