<?php

namespace Chiaki\CustomerVoucher\Block\Adminhtml\Promo\Quote\Edit\Tab\Coupons\Grid\Column\Renderer;

use Chiaki\CustomerVoucher\Model\ResourceModel\Voucher\CollectionFactory;
use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\Text;
use Magento\Framework\DataObject;

class Customer extends Text
{

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param Context           $context
     * @param CollectionFactory $collectionFactory
     * @param array             $data
     */
    public function __construct(
        Context           $context,
        CollectionFactory $collectionFactory,
        array             $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param DataObject $row
     *
     * @return string
     */
    public function render(DataObject $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        return $this->getCustomerId($value);
    }

    /**
     * @param $couponCode
     *
     * @return string
     */
    public function getCustomerId($couponCode)
    {
        $customerId = '';
        $collection = $this->_collectionFactory->create()->addFieldToFilter('main_table.code', $couponCode);
        foreach ($collection as $voucher) {
            $customerId = $voucher->getCustomerId();
            break;
        }
        return $customerId;
    }
}
