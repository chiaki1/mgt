<?php

namespace Chiaki\Customer\Console\Command;

use Magento\Quote\Model\Quote;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory;

class ClearCustomerCart extends Command
{
    /**
     * @var CollectionFactory
     */
    protected  $_collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     * @param string|null       $name
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        string $name = null
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($name);
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('chiaki:cart:clean');
        $this->setDescription('Clear all active customer cart after import product');
        parent::configure();
    }

    /**
     * CLI command description
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $collection = $this->_collectionFactory->create();
        /** @var Quote $quote */
        foreach ($collection as $quote) {
            if ($quote->getIsActive()) {
                $quote->delete();
            }
        }
    }
}
