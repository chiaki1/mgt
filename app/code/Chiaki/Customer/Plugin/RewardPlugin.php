<?php

namespace Chiaki\Customer\Plugin;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Reward\Model\Total\Quote\Reward;

class RewardPlugin
{
    /**
     * Reward data
     *
     * @var \Magento\Reward\Helper\Data
     */
    protected $_rewardData = null;

    /**
     * Reward factory
     *
     * @var \Magento\Reward\Model\RewardFactory
     */
    protected $_rewardFactory;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @param \Magento\Reward\Helper\Data $rewardData
     * @param \Magento\Reward\Model\RewardFactory $rewardFactory
     * @param PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Reward\Helper\Data $rewardData,
        \Magento\Reward\Model\RewardFactory $rewardFactory,
        PriceCurrencyInterface $priceCurrency
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->_rewardData = $rewardData;
        $this->_rewardFactory = $rewardFactory;
    }

    /**
     * @param Reward                      $subject
     * @param callable                    $proceed
     * @param Quote                       $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total                       $total
     */
    public function aroundCollect(
        Reward                      $subject,
        callable                    $proceed,
        Quote                       $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total                       $total
    ) {
        if ($quote->getRewardAmountApply()) {

            if (!$this->_rewardData->isEnabledOnFront($quote->getStore()->getWebsiteId())) {
                return $proceed($quote, $shippingAssignment, $total);
            }

            $total->setRewardPointsBalance(0)->setRewardCurrencyAmount(0)->setBaseRewardCurrencyAmount(0);

            if ($total->getBaseGrandTotal() >= 0 && $quote->getCustomer()->getId() && $quote->getUseRewardPoints()) {
                /* @var $reward \Magento\Reward\Model\Reward */
                $reward = $quote->getRewardInstance();
                if (!$reward || !$reward->getId()) {
                    $customer = $quote->getCustomer();
                    $reward = $this->_rewardFactory->create()->setCustomer($customer);
                    $reward->setCustomerId($quote->getCustomer()->getId());
                    $reward->setWebsiteId($quote->getStore()->getWebsiteId());
                    $reward->loadByCustomer();
                }
                $pointsLeft = $reward->getPointsBalance() - $quote->getRewardPointsBalance();
                $rewardCurrencyAmountLeft = $this->priceCurrency->convert(
                        $reward->getCurrencyAmount(),
                        $quote->getStore()
                    ) - $quote->getRewardCurrencyAmount();
                $baseRewardCurrencyAmountLeft = $reward->getCurrencyAmount() - $quote->getBaseRewardCurrencyAmount();
                $amountUse = $quote->getRewardAmountApply();
                $amountUse = $this->convertPointsToCurrency($amountUse, $reward);
                if ($amountUse > $baseRewardCurrencyAmountLeft) {
                    $amountUse = $baseRewardCurrencyAmountLeft;
                    $quote->setRewardAmountApply($amountUse);
                }
                if ($baseRewardCurrencyAmountLeft >= $total->getBaseGrandTotal()) {
                    if ($amountUse <= $baseRewardCurrencyAmountLeft && $amountUse >= $total->getBaseGrandTotal()) {
                        $pointsBalanceUsed = $reward->getPointsEquivalent($total->getBaseGrandTotal());
                        $pointsCurrencyAmountUsed = $total->getGrandTotal();
                        $basePointsCurrencyAmountUsed = $total->getBaseGrandTotal();

                        $total->setGrandTotal(0);
                        $total->setBaseGrandTotal(0);
                    } elseif ($amountUse <= $baseRewardCurrencyAmountLeft && $amountUse < $total->getBaseGrandTotal()) {
                        $pointsBalanceUsed = $reward->getPointsEquivalent($amountUse);
                        $pointsCurrencyAmountUsed = $amountUse;
                        $basePointsCurrencyAmountUsed = $amountUse;
                        $total->setGrandTotal($total->getGrandTotal() - $amountUse);
                        $total->setBaseGrandTotal($total->getGrandTotal() - $amountUse);
                    }
                } else {
                    $pointsBalanceUsed = $reward->getPointsEquivalent($baseRewardCurrencyAmountLeft);
                    if ($pointsBalanceUsed > $pointsLeft) {
                        $pointsBalanceUsed = $pointsLeft;
                    }
                    $pointsCurrencyAmountUsed = $rewardCurrencyAmountLeft;
                    $basePointsCurrencyAmountUsed = $baseRewardCurrencyAmountLeft;

                    $total->setGrandTotal($total->getGrandTotal() - $pointsCurrencyAmountUsed);
                    $total->setBaseGrandTotal($total->getBaseGrandTotal() - $basePointsCurrencyAmountUsed);
                }

                $quote->setRewardPointsBalance(round($quote->getRewardPointsBalance() + $pointsBalanceUsed));
                $quote->setRewardCurrencyAmount($quote->getRewardCurrencyAmount() + $pointsCurrencyAmountUsed);
                $quote->setBaseRewardCurrencyAmount($quote->getBaseRewardCurrencyAmount() + $basePointsCurrencyAmountUsed);

                $total->setRewardPointsBalance(round($pointsBalanceUsed));
                $total->setRewardCurrencyAmount($pointsCurrencyAmountUsed);
                $total->setBaseRewardCurrencyAmount($basePointsCurrencyAmountUsed);
            }
            return $this;
        }
        return $proceed($quote, $shippingAssignment, $total);
    }

    /**
     * Convert points to currency
     *
     * @param int $points
     * @param \Magento\Reward\Model\Reward $reward
     *
     * @return float
     */
    private function convertPointsToCurrency($points, $reward)
    {
        return $points && $reward->getRateToCurrency() ? (double)$reward->getRateToCurrency()->calculateToCurrency(
            $points
        ) : 0;
    }
}
