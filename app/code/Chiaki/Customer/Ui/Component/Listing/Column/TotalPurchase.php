<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Customer\Ui\Component\Listing\Column;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Directory\Model\Currency;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * Class TotalPurchase
 *
 * @package Chiaki\Customer\Ui\Component\Listing\Column
 */
class TotalPurchase extends Column
{
    /**
     * @var PriceCurrencyInterface
     */
    protected $priceFormatter;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var StoreManagerInterface|null
     */
    private $storeManager;

    /**
     * @var OrderCollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * TotalPurchase constructor.
     *
     * @param ContextInterface           $context
     * @param UiComponentFactory         $uiComponentFactory
     * @param PriceCurrencyInterface     $priceFormatter
     * @param OrderCollectionFactory     $orderCollectionFactory
     * @param array                      $components
     * @param array                      $data
     * @param Currency|null              $currency
     * @param StoreManagerInterface|null $storeManager
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        PriceCurrencyInterface $priceFormatter,
        OrderCollectionFactory $orderCollectionFactory,
        array $components = [],
        array $data = [],
        Currency $currency = null,
        StoreManagerInterface $storeManager = null
    ) {
        $this->priceFormatter = $priceFormatter;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->currency = $currency ?: ObjectManager::getInstance()
            ->get(Currency::class);
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()
            ->get(StoreManagerInterface::class);
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $store = $this->storeManager->getStore(
                $this->context->getFilterParam('store_id', \Magento\Store\Model\Store::DEFAULT_STORE_ID)
            );
            $currencyCode = $store->getBaseCurrency()->getCurrencyCode();
            $basePurchaseCurrency = $this->currency->load($currencyCode);
            foreach ($dataSource['data']['items'] as & $item) {
                $item['total_purchase'] = $basePurchaseCurrency
                    ->format($this->getTotalPurchase($item['entity_id']), [], false);
            }
        }

        return $dataSource;
    }

    /**
     * @param $customerId
     *
     * @return float|int|null
     */
    private function getTotalPurchase($customerId)
    {
        $total = 0;
        $orders = $this->orderCollectionFactory->create()
                                               ->addFieldToFilter('status', 'complete')
                                               ->addFieldToFilter('customer_id', $customerId);
        /** @var Order $order */
        foreach ($orders as $order) {
            $total += $order->getGrandTotal();
        }
        return $total;
    }
}
