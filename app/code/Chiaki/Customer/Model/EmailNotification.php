<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Customer\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Magento\Customer\Model\Data\CustomerSecure;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\CustomerRegistry;

/**
 * Customer email notification
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EmailNotification
{
    /**#@+
     * Configuration paths for email templates and identities
     */
    const XML_PATH_FORGOT_EMAIL_IDENTITY = 'customer/password/forgot_email_identity';

    const XML_PATH_CHANGE_EMAIL_AND_PASSWORD_TEMPLATE
        = 'customer/account_information/change_email_and_password_template';

    /**#@-*/

    /**#@-*/
    private $customerRegistry;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var CustomerViewHelper
     */
    protected $customerViewHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var SenderResolverInterface
     */
    private $senderResolver;

    /**
     * @param CustomerRegistry             $customerRegistry
     * @param StoreManagerInterface        $storeManager
     * @param TransportBuilder             $transportBuilder
     * @param CustomerViewHelper           $customerViewHelper
     * @param DataObjectProcessor          $dataProcessor
     * @param ScopeConfigInterface         $scopeConfig
     * @param SenderResolverInterface|null $senderResolver
     */
    public function __construct(
        CustomerRegistry        $customerRegistry,
        StoreManagerInterface   $storeManager,
        TransportBuilder        $transportBuilder,
        CustomerViewHelper      $customerViewHelper,
        DataObjectProcessor     $dataProcessor,
        ScopeConfigInterface    $scopeConfig,
        SenderResolverInterface $senderResolver = null
    ) {
        $this->customerRegistry   = $customerRegistry;
        $this->storeManager       = $storeManager;
        $this->transportBuilder   = $transportBuilder;
        $this->customerViewHelper = $customerViewHelper;
        $this->dataProcessor      = $dataProcessor;
        $this->scopeConfig        = $scopeConfig;
        $this->senderResolver     = $senderResolver ?? ObjectManager::getInstance()->get(SenderResolverInterface::class);
    }

    /**
     * Send corresponding email template
     *
     * @param CustomerInterface $customer
     * @param string            $template configuration path of email template
     * @param string            $sender   configuration path of email identity
     * @param array             $templateParams
     * @param int|null          $storeId
     * @param string            $email
     *
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    private function sendEmailTemplate(
        $customer,
        $template,
        $sender,
        $templateParams = [],
        $storeId = null,
        $email = null
    ): void {
        $templateId = $this->scopeConfig->getValue($template, ScopeInterface::SCOPE_STORE, $storeId);
        if ($email === null) {
            $email = $customer->getEmail();
        }

        /** @var array $from */
        $from = $this->senderResolver->resolve(
            $this->scopeConfig->getValue($sender, ScopeInterface::SCOPE_STORE, $storeId),
            $storeId
        );

        $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom($from)
                                            ->addTo($email, $this->customerViewHelper->getCustomerName($customer))
                                            ->getTransport();

        $transport->sendMessage();
    }

    /**
     * Create an object with data merged from Customer and CustomerSecure
     *
     * @param CustomerInterface $customer
     *
     * @return CustomerSecure
     */
    private function getFullCustomerObject($customer): CustomerSecure
    {
        // No need to flatten the custom attributes or nested objects since the only usage is for email templates and
        // object passed for events
        $mergedCustomerData = $this->customerRegistry->retrieveSecureData($customer->getId());
        $customerData       = $this->dataProcessor
            ->buildOutputDataArray($customer, CustomerInterface::class);
        $mergedCustomerData->addData($customerData);
        $mergedCustomerData->setData('name', $this->customerViewHelper->getCustomerName($customer));
        return $mergedCustomerData;
    }

    /**
     * Get either first store ID from a set website or the provided as default
     *
     * @param CustomerInterface $customer
     * @param int|string|null   $defaultStoreId
     *
     * @return int
     */
    private function getWebsiteStoreId($customer, $defaultStoreId = null): int
    {
        if ($customer->getWebsiteId() != 0 && empty($defaultStoreId)) {
            $storeIds       = $this->storeManager->getWebsite($customer->getWebsiteId())->getStoreIds();
            $defaultStoreId = reset($storeIds);
        }
        return $defaultStoreId;
    }

    /**
     * Send email with reset password confirmation link
     *
     * @param CustomerInterface $customer
     *
     * @return void
     */
    public function passwordResetConfirmationApp(CustomerInterface $customer): void
    {
        $storeId = $customer->getStoreId();
        if ($storeId === null) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
            $customer,
            'customer/password/forgot_email_graphql_template',
            self::XML_PATH_FORGOT_EMAIL_IDENTITY,
            ['customer' => $customerEmailData, 'store' => $this->storeManager->getStore($storeId)],
            $storeId
        );
    }

    /**
     * Send email with reset password confirmation link
     *
     * @param CustomerInterface $customer
     *
     * @return void
     */
    public function passwordResetConfirmationWeb(CustomerInterface $customer): void
    {
        $storeId = $customer->getStoreId();
        if ($storeId === null) {
            $storeId = $this->getWebsiteStoreId($customer);
        }

        $customerEmailData = $this->getFullCustomerObject($customer);

        $this->sendEmailTemplate(
            $customer,
            'customer/password/forgot_email_graphql_with_url_template',
            self::XML_PATH_FORGOT_EMAIL_IDENTITY,
            [
                'customer' => $customerEmailData,
                'store' => $this->storeManager->getStore($storeId),
                'redirect_url' => $this->getRedirectUrl() . '?' . http_build_query(
                    [
                        'token' => $customerEmailData->getRpToken(),
                        'email' => $customer->getEmail()
                    ]
                )
            ],
            $storeId
        );
    }

    private function getRedirectUrl()
    {
        $url = $this->scopeConfig->getValue('customer/password/url_forgot_pass');
        return $url ?: $this->storeManager->getStore()->getBaseUrl();
    }

}
