<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Customer\Setup\Patch\Data;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Customer\Api\CustomerMetadataInterface;

/**
 * Patch is mechanism, that allows to do atomic upgrade data changes
 */
class CreateCustomerAttribute implements
    DataPatchInterface,
    PatchRevertableInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CustomerSetupFactory     $customerSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->moduleDataSetup      = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $customerSetup->addAttribute(
            Customer::ENTITY,
            'shirt_size',
            [
                'type'         => 'varchar',
                'label'        => 'Shirt size',
                'input'        => 'text',
                'required'     => false,
                'sort_order'   => 999,
                'visible'      => true,
                'system'       => false,
                'user_defined' => true,
                'used_in_forms' => ['adminhtml_customer'],
            ]
        );
        $customerSetup->addAttribute(
            Customer::ENTITY,
            'pants_size',
            [
                'type'         => 'varchar',
                'label'        => 'Pants size',
                'input'        => 'text',
                'required'     => false,
                'sort_order'   => 999,
                'visible'      => true,
                'system'       => false,
                'user_defined' => true,
                'used_in_forms' => ['adminhtml_customer'],
            ]
        );
        $customerSetup->addAttribute(
            Customer::ENTITY,
            'notify',
            [
                'type'         => 'int',
                'label'        => 'Notify',
                'input'        => 'boolean',
                'required'     => false,
                'sort_order'   => 999,
                'visible'      => true,
                'system'       => false,
                'user_defined' => true,
                'source'       => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'used_in_forms' => ['adminhtml_customer'],
            ]
        );
        $customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'shirt_size'
        );
        $customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'pants_size'
        );
        $customerSetup->addAttributeToSet(
            CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            CustomerMetadataInterface::ATTRIBUTE_SET_ID_CUSTOMER,
            null,
            'notify'
        );

    }

    /**
     * @inheritdoc
     */
    public function revert()
    {
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
