<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\Cms\Api;

use Chiaki\Cms\Api\Data\TypeInterface;
use Chiaki\Cms\Api\Data\TypeSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;

/**
 * Page type CRUD interface
 *
 * @api
 * @since 100.0.2
 */
interface TypeRepositoryInterface
{
    /**
     * Save page type.
     *
     * @param TypeInterface $type
     *
     * @return TypeInterface
     * @throws LocalizedException
     */
    public function save(TypeInterface $type);

    /**
     * Get page type by type ID.
     *
     * @param int $id
     *
     * @return TypeInterface
     * @throws NoSuchEntityException If $typeId is not found
     * @throws LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve page types.
     *
     * The list of groups can be filtered to exclude the NOT_LOGGED_IN group using the first parameter and/or it can
     * be filtered by tax class.
     *
     * This call returns an array of objects, but detailed information about each object’s attributes might not be
     * included. See https://devdocs.magento.com/codelinks/attributes.html#GroupRepositoryInterface to determine
     * which call to use to get detailed information about all attributes for an object.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return TypeSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete page type.
     *
     * @param TypeInterface $type
     *
     * @return bool true on success
     * @throws StateException If page type cannot be deleted
     * @throws LocalizedException
     */
    public function delete(TypeInterface $type);

    /**
     * Delete page type by ID.
     *
     * @param int $id
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws StateException If page type cannot be deleted
     * @throws LocalizedException
     */
    public function deleteById($id);
}
