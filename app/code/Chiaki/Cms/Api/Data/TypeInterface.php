<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\Cms\Api\Data;

/**
 * page type interface.
 *
 * @api
 * @since 100.0.2
 */
interface TypeInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ID   = 'page_type_id';
    const CODE = 'page_type_code';

    const TYPE_CODE_MAX_LENGTH = 32;
    /**#@-*/

    /**
     * Get id
     *
     * @return int|null
     */
    public function getPageTypeId();

    /**
     * Set id
     *
     * @param int $id
     *
     * @return $this
     */
    public function setPageTypeId($id);

    /**
     * Get code
     *
     * @return string
     */
    public function getPageTypeCode();

    /**
     * Set code
     *
     * @param string $code
     *
     * @return $this
     */
    public function setPageTypeCode($code);
}
