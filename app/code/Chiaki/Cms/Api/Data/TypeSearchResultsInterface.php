<?php

declare(strict_types=1);

namespace Chiaki\Cms\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface TypeSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Type list.
     *
     * @return TypeInterface[]
     */
    public function getItems();

    /**
     * Set page_type_code list.
     *
     * @param TypeInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
