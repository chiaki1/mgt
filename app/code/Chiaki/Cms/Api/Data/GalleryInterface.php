<?php

declare(strict_types=1);

namespace Chiaki\Cms\Api\Data;

interface GalleryInterface
{

    const IMAGE_NAME  = 'image_name';
    const GALLERY_ID  = 'gallery_id';
    const PAGE_ID = 'page_id';

    /**
     * Get gallery_id
     *
     * @return string|null
     */
    public function getGalleryId();

    /**
     * Set gallery_id
     *
     * @param string $galleryId
     *
     * @return GalleryInterface
     */
    public function setGalleryId($galleryId);

    /**
     * Get image_name
     *
     * @return string|null
     */
    public function getImageName();

    /**
     * Set value
     *
     * @param string $image_name
     *
     * @return GalleryInterface
     */
    public function setImageName($image_name);

    /**
     * Get page_id
     *
     * @return int
     */
    public function getPageId();

    /**
     * Set page_id
     *
     * @param string $page_id
     *
     * @return GalleryInterface
     */
    public function setPageId($page_id);
}

