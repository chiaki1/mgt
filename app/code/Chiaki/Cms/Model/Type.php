<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\Cms\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Registry;
use Magento\Store\Model\StoresConfig;

/**
 * Customer group model
 *
 * @api
 * @method string getPageTypeCode()
 * @method Type setPageTypeCode(string $value)
 * @since 100.0.2
 */
class Type extends AbstractModel
{

    const ENTITY = 'page_type';

    const TYPE_CODE_MAX_LENGTH = 32;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'page_type';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'object';

    /**
     * @var StoresConfig
     */
    protected $_storesConfig;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * Constructor
     *
     * @param Context                        $context
     * @param Registry                             $registry
     * @param StoresConfig                       $storesConfig
     * @param DataObjectProcessor       $dataObjectProcessor
     * @param AbstractResource $resource
     * @param AbstractDb           $resourceCollection
     * @param array                                                   $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context                        $context,
        Registry                             $registry,
        StoresConfig                       $storesConfig,
        DataObjectProcessor       $dataObjectProcessor,
        AbstractResource $resource = null,
        AbstractDb           $resourceCollection = null,
        array                                                   $data = []
    ) {
        $this->_storesConfig       = $storesConfig;
        $this->dataObjectProcessor = $dataObjectProcessor;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Type::class);
    }

    /**
     * Alias for setPageTypeCode
     *
     * @param string $value
     *
     * @return $this
     */
    public function setCode($value)
    {
        return $this->setPageTypeCode($value);
    }

    /**
     * Alias for getPageTypeCode
     *
     * @return string
     */
    public function getCode()
    {
        return $this->getPageTypeCode();
    }

    /**
     * Prepare data before save
     *
     * @return $this
     */
    public function beforeSave()
    {
        $this->_prepareData();
        return parent::beforeSave();
    }

    /**
     * Prepare customer group data
     *
     * @return $this
     */
    protected function _prepareData()
    {
        $this->setCode(substr($this->getCode(), 0, self::TYPE_CODE_MAX_LENGTH));
        return $this;
    }
}
