<?php

declare(strict_types=1);

namespace Chiaki\Cms\Model\ResourceModel\Type;

use Chiaki\Cms\Model\ResourceModel\Type;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'page_type_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\Cms\Model\Type::class,
            Type::class
        );
    }
}
