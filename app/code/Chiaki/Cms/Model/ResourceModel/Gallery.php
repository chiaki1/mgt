<?php

declare(strict_types=1);

namespace Chiaki\Cms\Model\ResourceModel;

use Chiaki\Cms\Model\ImageProcessor;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Gallery extends AbstractDb
{
    /**
     * @var ImageProcessor
     */
    protected $imageProcessor;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var File
     */
    protected $ioFile;

    public function __construct(
        ImageProcessor $imageProcessor,
        Filesystem $filesystem,
        File $ioFile,
        Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->imageProcessor = $imageProcessor;
        $this->filesystem     = $filesystem;
        $this->ioFile         = $ioFile;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('page_gallery', 'gallery_id');
    }

    protected function _afterSave(AbstractModel $object)
    {
        $data = $object->getData();

        if (isset($data['image_name']) && $object->isObjectNew()) {
            $this->imageProcessor->processImage(
                $data['image_name'],
                ImageProcessor::GALLERY_IMAGE_TYPE,
                $object->getPageId(),
                $object->getPageIsNew()
            );
        }
    }

    protected function _afterDelete(AbstractModel $object)
    {
        $data = $object->getData();

        if (isset($data['image_name'])) {
            $this->imageProcessor->setBasePaths(
                ImageProcessor::GALLERY_IMAGE_TYPE,
                $object->getPageId(),
                $object->getPageIsNew()
            );
            $this->imageProcessor->deleteImage($data['image_name']);
        }
    }
}

