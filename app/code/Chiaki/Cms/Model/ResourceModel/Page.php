<?php

namespace Chiaki\Cms\Model\ResourceModel;

use Chiaki\Cms\Model\GalleryFactory;
use Chiaki\Cms\Model\ResourceModel\Gallery\Collection as GalleryCollection;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\StoreManagerInterface;

class Page extends \Magento\Cms\Model\ResourceModel\Page
{
    /**
     * @var GalleryCollection
     */
    protected $galleryCollection;

    /**
     * @var Gallery
     */
    protected $galleryResource;

    /**
     * @var GalleryFactory
     */
    protected $galleryFactory;

    /**
     * @param Context                                 $context
     * @param StoreManagerInterface                   $storeManager
     * @param DateTime                                $dateTime
     * @param EntityManager                           $entityManager
     * @param MetadataPool                            $metadataPool
     * @param GalleryCollection                       $galleryCollection
     * @param GalleryFactory                          $galleryFactory
     * @param \Chiaki\Cms\Model\ResourceModel\Gallery $galleryResource
     * @param null                                    $connectionName
     */
    public function __construct(
        Context               $context,
        StoreManagerInterface $storeManager,
        DateTime              $dateTime,
        EntityManager         $entityManager,
        MetadataPool          $metadataPool,
        GalleryCollection     $galleryCollection,
        GalleryFactory        $galleryFactory,
        Gallery               $galleryResource,
                              $connectionName = null
    ) {
        parent::__construct($context, $storeManager, $dateTime, $entityManager, $metadataPool, $connectionName);
        $this->galleryCollection = $galleryCollection;
        $this->galleryFactory    = $galleryFactory;
        $this->galleryResource   = $galleryResource;
    }

    protected function _beforeDelete(AbstractModel $object)
    {
        $allImages = $this->galleryCollection->getImagesByPage($object->getId());

        foreach ($allImages as $image) {
            $this->galleryResource->delete($image);
        }
    }

    protected function _afterSave(AbstractModel $object)
    {
        if (!($object->getData('inlineEdit') || $object->getData('massAction'))) {
            $this->saveGallery($object->getData(), $object->isObjectNew());
        }
    }

    private function saveGallery($data, $isObjectNew = false)
    {
        $pageId    = $data['page_id'];
        $allImages = $this->galleryCollection->getImagesByPage($pageId);

        if (!isset($data['gallery_image'])) {
            foreach ($allImages as $image) {
                $this->galleryResource->delete($image);
            }
            return;
        }
        $galleryImages    = $data['gallery_image'];
        $imagesOfPage = [];

        foreach ($allImages as $image) {
            $imagesOfPage[$image->getData('image_name')] = $image;
        }

        foreach ($galleryImages as $galleryImage) {
            if (array_key_exists($galleryImage['name'], $imagesOfPage)) {
                unset($imagesOfPage[$galleryImage['name']]);
            }
            if (isset($galleryImage['tmp_name']) && isset($galleryImage['name'])) {
                $newImage = $this->galleryFactory->create();
                $newImage->addData(
                    [
                        'page_id'     => $pageId,
                        'image_name'  => $galleryImage['name'],
                        'page_is_new' => $isObjectNew
                    ]
                );
                $this->galleryResource->save($newImage);
            }
        }

        foreach ($imagesOfPage as $imageToDelete) {
            $this->galleryResource->delete($imageToDelete);
        }
    }
}
