<?php

declare(strict_types=1);

namespace Chiaki\Cms\Model;

use Chiaki\Cms\Api\Data\GalleryInterface;
use Chiaki\Cms\Api\Data\GalleryInterfaceFactory;
use Chiaki\Cms\Api\Data\GallerySearchResultsInterfaceFactory;
use Chiaki\Cms\Api\GalleryRepositoryInterface;
use Chiaki\Cms\Model\ResourceModel\Gallery as ResourceGallery;
use Chiaki\Cms\Model\ResourceModel\Gallery\CollectionFactory as GalleryCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class GalleryRepository implements GalleryRepositoryInterface
{

    protected $resource;

    protected $galleryFactory;

    protected $galleryCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataGalleryFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceGallery                      $resource
     * @param GalleryFactory                       $galleryFactory
     * @param GalleryInterfaceFactory              $dataGalleryFactory
     * @param GalleryCollectionFactory             $galleryCollectionFactory
     * @param GallerySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                     $dataObjectHelper
     * @param DataObjectProcessor                  $dataObjectProcessor
     * @param StoreManagerInterface                $storeManager
     * @param CollectionProcessorInterface         $collectionProcessor
     * @param JoinProcessorInterface               $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter        $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceGallery $resource,
        GalleryFactory $galleryFactory,
        GalleryInterfaceFactory $dataGalleryFactory,
        GalleryCollectionFactory $galleryCollectionFactory,
        GallerySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->galleryFactory                   = $galleryFactory;
        $this->galleryCollectionFactory         = $galleryCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataGalleryFactory               = $dataGalleryFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        GalleryInterface $gallery
    ) {
        /* if (empty($gallery->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $gallery->setStoreId($storeId);
        } */

        $galleryData = $this->extensibleDataObjectConverter->toNestedArray(
            $gallery,
            [],
            GalleryInterface::class
        );

        $galleryModel = $this->galleryFactory->create()->setData($galleryData);

        try {
            $this->resource->save($galleryModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the gallery: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $galleryModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($galleryId)
    {
        $gallery = $this->galleryFactory->create();
        $this->resource->load($gallery, $galleryId);
        if (!$gallery->getId()) {
            throw new NoSuchEntityException(__('Gallery with id "%1" does not exist.', $galleryId));
        }
        return $gallery->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->galleryCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            GalleryInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        GalleryInterface $gallery
    ) {
        try {
            $galleryModel = $this->galleryFactory->create();
            $this->resource->load($galleryModel, $gallery->getGalleryId());
            $this->resource->delete($galleryModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Gallery: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($galleryId)
    {
        return $this->delete($this->get($galleryId));
    }
}

