<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\Cms\Model\Source;

use Chiaki\Cms\Model\ResourceModel\Type\CollectionFactory;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var array
     */
    protected $_options = [];

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $collection = $this->_collectionFactory->create();
            foreach ($collection as $item) {
                $this->_options[] = [
                    'value' => $item->getPageTypeId(),
                    'label' => $item->getPageTypeCode()
                ];
            }
            array_unshift($this->_options, ['value' => '', 'label' => __('-- Please Select --')]);
        }

        return $this->_options;
    }
}
