<?php

declare(strict_types=1);

namespace Chiaki\Cms\Model;

use Chiaki\Cms\Api\Data\TypeInterface;
use Chiaki\Cms\Api\Data\TypeInterfaceFactory;
use Chiaki\Cms\Api\Data\TypeSearchResultsInterfaceFactory;
use Chiaki\Cms\Api\TypeRepositoryInterface;
use Chiaki\Cms\Model\ResourceModel\Type as ResourceType;
use Chiaki\Cms\Model\ResourceModel\Type\CollectionFactory as TypeCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class TypeRepository implements TypeRepositoryInterface
{

    protected $resource;

    protected $typeFactory;

    protected $typeCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataTypeFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceType                      $resource
     * @param TypeFactory                       $typeFactory
     * @param TypeInterfaceFactory              $dataTypeFactory
     * @param TypeCollectionFactory             $typeCollectionFactory
     * @param TypeSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                  $dataObjectHelper
     * @param DataObjectProcessor               $dataObjectProcessor
     * @param StoreManagerInterface             $storeManager
     * @param CollectionProcessorInterface      $collectionProcessor
     * @param JoinProcessorInterface            $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter     $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceType                      $resource,
        TypeFactory                       $typeFactory,
        TypeInterfaceFactory              $dataTypeFactory,
        TypeCollectionFactory             $typeCollectionFactory,
        TypeSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper                  $dataObjectHelper,
        DataObjectProcessor               $dataObjectProcessor,
        StoreManagerInterface             $storeManager,
        CollectionProcessorInterface      $collectionProcessor,
        JoinProcessorInterface            $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter     $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->typeFactory                      = $typeFactory;
        $this->typeCollectionFactory            = $typeCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataTypeFactory                  = $dataTypeFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(TypeInterface $type)
    {
        /* if (empty($type->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $type->setStoreId($storeId);
        } */

        $typeData = $this->extensibleDataObjectConverter->toNestedArray(
            $type,
            [],
            TypeInterface::class
        );

        $typeModel = $this->typeFactory->create()->setData($typeData);

        try {
            $this->resource->save($typeModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the type: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $typeModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($typeId)
    {
        $type = $this->typeFactory->create();
        $this->resource->load($type, $typeId);
        if (!$type->getId()) {
            throw new NoSuchEntityException(__('Type with id "%1" does not exist.', $typeId));
        }
        return $type->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->typeCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            TypeInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(TypeInterface $type)
    {
        try {
            $typeModel = $this->typeFactory->create();
            $this->resource->load($typeModel, $type->getPageTypeId());
            $this->resource->delete($typeModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Type: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($typeId)
    {
        return $this->delete($this->getById($typeId));
    }
}
