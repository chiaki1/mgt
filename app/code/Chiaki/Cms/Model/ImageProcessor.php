<?php

namespace Chiaki\Cms\Model;

use Exception;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\Image;
use Magento\Framework\ImageFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class ImageProcessor
{
    const CHIAKI_CMS_MEDIA_PATH = 'chiaki/cms';

    const CHIAKI_CMS_MEDIA_TMP_PATH = 'chiaki/cms/tmp';

    /**
     * Gallery area inside media folder
     */
    const CHIAKI_CMS_GALLERY_MEDIA_PATH = 'chiaki/cms/gallery';

    /**
     * Gallery temporary area inside media folder
     */
    const CHIAKI_CMS_GALLERY_MEDIA_TMP_PATH = 'chiaki/cms/gallery/tmp';

    /**
     * Type image option gallery_image
     */
    const GALLERY_IMAGE_TYPE = 'gallery_image';

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * @var ImageFactory
     */
    private $imageFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var WriteInterface
     */
    private $mediaDirectory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Allowed extensions
     *
     * @var array
     */
    protected $allowedExtensions = ['jpg', 'jpeg', 'gif', 'png'];

    public function __construct(
        Filesystem $filesystem,
        ImageUploader $imageUploader,
        ImageFactory $imageFactory,
        StoreManagerInterface $storeManager,
        ManagerInterface $messageManager,
        LoggerInterface $logger
    ) {
        $this->filesystem     = $filesystem;
        $this->imageUploader  = $imageUploader;
        $this->imageFactory   = $imageFactory;
        $this->storeManager   = $storeManager;
        $this->messageManager = $messageManager;
        $this->logger         = $logger;
    }

    /**
     * @return WriteInterface
     */
    private function getMediaDirectory()
    {
        if ($this->mediaDirectory === null) {
            $this->mediaDirectory = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        }

        return $this->mediaDirectory;
    }

    /**
     * @param string $imageName
     *
     * @return string
     */
    public function getImageRelativePath($imageName)
    {
        return $this->imageUploader->getBasePath() . DIRECTORY_SEPARATOR . $imageName;
    }

    /**
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public function getImageUrl($params = [])
    {
        return $this->getMediaUrl() . implode(DIRECTORY_SEPARATOR, $params);
    }

    /**
     * Move file from temporary directory
     *
     * @param string $imageName
     * @param string $imageType
     * @param        $pageId
     * @param        $pageIsNew
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function processImage($imageName, $imageType, $pageId, $pageIsNew)
    {
        $this->setBasePaths($imageType, $pageId, $pageIsNew);
        $this->imageUploader->moveFileFromTmp($imageName, true);

        $filename = $this->getMediaDirectory()->getAbsolutePath($this->getImageRelativePath($imageName));
        try {
            $this->prepareImage($filename, $imageType);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $this->messageManager->addErrorMessage(
                __($errorMessage)
            );
            $this->logger->critical($e);
        }
    }

    /**
     * @param string $filename
     * @param string $imageType
     * @param bool   $needResize
     */
    public function prepareImage($filename, $imageType, $needResize = false)
    {
        /** @var Image $imageProcessor */
        $imageProcessor = $this->imageFactory->create(['fileName' => $filename]);
        $imageProcessor->keepAspectRatio(true);
        $imageProcessor->keepFrame(true);
        $imageProcessor->keepTransparency(true);
        $imageProcessor->save();
    }

    /**
     * @param string $imageName
     */
    public function deleteImage($imageName)
    {
        if ($imageName && strpos($imageName, '.') !== false) {
            $this->getMediaDirectory()->delete(
                $this->getImageRelativePath($imageName)
            );
        }
    }

    /**
     * @param string $imageType
     * @param int    $questionId
     * @param bool   $questionIsNew
     */
    public function setBasePaths($imageType, $pageId, $pageIsNew)
    {
        // if location doesn't exist, we set 0 to tmp path
        $tmpLocationId = $pageIsNew ? 0 : $pageId;
        $tmpPath       = ImageProcessor::CHIAKI_CMS_MEDIA_TMP_PATH . DIRECTORY_SEPARATOR . $tmpLocationId;
        $this->imageUploader->setBaseTmpPath(
            $tmpPath
        );
        switch ($imageType) {

            case ImageProcessor::GALLERY_IMAGE_TYPE:
                $this->imageUploader->setBasePath(
                    ImageProcessor::CHIAKI_CMS_GALLERY_MEDIA_PATH . DIRECTORY_SEPARATOR . $pageId
                );
                break;
        }
    }
}
