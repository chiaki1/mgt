<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\Cms\Model\Data;

use Chiaki\Cms\Api\Data\TypeInterface;
use Magento\Framework\Api\AbstractSimpleObject;

/**
 * Page Type data model.
 */
class Type extends AbstractSimpleObject implements TypeInterface
{
    /**
     * Get ID
     *
     * @return int
     */
    public function getPageTypeId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getPageTypeCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set id
     *
     * @param int $id
     *
     * @return $this
     */
    public function setPageTypeId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return $this
     */
    public function setPageTypeCode($code)
    {
        return $this->setData(self::CODE, $code);
    }
}
