<?php

namespace Chiaki\Cms\Model\Page;

use Chiaki\Cms\Model\ImageProcessor;
use Chiaki\Cms\Model\ResourceModel\Gallery\Collection as GalleryCollection;
use Magento\Cms\Model\Page\CustomLayoutManagerInterface;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\AuthorizationInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

class DataProvider extends \Magento\Cms\Model\Page\DataProvider
{
    /**
     * @var ImageProcessor
     */
    protected $imageProcessor;

    /**
     * @var GalleryCollection
     */
    protected $galleryCollection;

    private $collectionFactory;

    private $request;

    public function __construct(
        $name, $primaryFieldName, $requestFieldName, CollectionFactory $pageCollectionFactory,
        ImageProcessor $imageProcessor,
        GalleryCollection $galleryCollection,
        DataPersistorInterface $dataPersistor, array $meta = [], array $data = [], PoolInterface $pool = null,
        ?AuthorizationInterface $auth = null, ?RequestInterface $request = null,
        ?CustomLayoutManagerInterface $customLayoutManager = null
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $pageCollectionFactory, $dataPersistor, $meta, $data, $pool, $auth, $request, $customLayoutManager);
        $this->imageProcessor = $imageProcessor;
        $this->galleryCollection = $galleryCollection;
        $this->collectionFactory = $pageCollectionFactory;
        $this->request = $request ?? ObjectManager::getInstance()->get(RequestInterface::class);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->collection = $this->collectionFactory->create();
        $items = $this->collection->getItems();
        /** @var $page \Magento\Cms\Model\Page */
        foreach ($items as $page) {
            $galleryImages = $this->galleryCollection->getImagesByPage($page->getData('page_id'));
            if (!empty($galleryImages)) {
                $gallery_image = [];

                foreach ($galleryImages as $image) {
                    $imgName = $image->getData('image_name');
                    array_push(
                        $gallery_image,
                        [
                            'name' => $imgName,
                            'url'  => $this->imageProcessor->getImageUrl(
                                [ImageProcessor::CHIAKI_CMS_GALLERY_MEDIA_PATH, $page->getData('page_id'), $imgName]
                            )
                        ]
                    );
                }
                $page->setData('gallery_image', $gallery_image);
            }
            $this->loadedData[$page->getId()] = $page->getData();
            if ($page->getCustomLayoutUpdateXml() || $page->getLayoutUpdateXml()) {
                //Deprecated layout update exists.
                $this->loadedData[$page->getId()]['layout_update_selected'] = '_existing_';
            }
        }

        $data = $this->dataPersistor->get('cms_page');
        if (!empty($data)) {
            $page = $this->collection->getNewEmptyItem();
            $page->setData($data);
            $galleryImages = $this->galleryCollection->getImagesByPage($page->getData('page_id'));
            if (!empty($galleryImages)) {
                $gallery_image = [];

                foreach ($galleryImages as $image) {
                    $imgName = $image->getData('image_name');
                    array_push(
                        $gallery_image,
                        [
                            'name' => $imgName,
                            'url'  => $this->imageProcessor->getImageUrl(
                                [ImageProcessor::CHIAKI_CMS_GALLERY_MEDIA_PATH, $page->getData('page_id'), $imgName]
                            )
                        ]
                    );
                }
                $page->setData('gallery_image', $gallery_image);
            }
            $this->loadedData[$page->getId()] = $page->getData();
            if ($page->getCustomLayoutUpdateXml() || $page->getLayoutUpdateXml()) {
                $this->loadedData[$page->getId()]['layout_update_selected'] = '_existing_';
            }
            $this->dataPersistor->clear('cms_page');
        }

        return $this->loadedData;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getMeta()
    {
        $this->meta = parent::getMeta();

        $pageId = (int)$this->request->getParam('page_id');

        $this->meta['content']['children']['gallery']['arguments']['data']['config']['uploaderConfig']['url']
            = 'cms/file/upload/type/gallery_image/id/' . $pageId;

        return $this->meta;
    }
}
