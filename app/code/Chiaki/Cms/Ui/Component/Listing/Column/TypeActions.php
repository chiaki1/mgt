<?php

declare(strict_types=1);

namespace Chiaki\Cms\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class TypeActions extends Column
{

    const URL_PATH_EDIT    = 'cms/type/edit';
    const URL_PATH_DELETE  = 'cms/type/delete';
    const URL_PATH_DETAILS = 'cms/type/details';
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory           $uiComponentFactory
     * @param UrlInterface                              $urlBuilder
     * @param array                                                        $components
     * @param array                                                        $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory           $uiComponentFactory,
        UrlInterface                              $urlBuilder,
        array                                                        $components = [],
        array                                                        $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['page_type_id'])) {
                    $item[$this->getData('name')] = [
                        'edit'   => [
                            'href'  => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    'page_type_id' => $item['page_type_id']
                                ]
                            ),
                            'label' => __('Edit')
                        ],
                        'delete' => [
                            'href'    => $this->urlBuilder->getUrl(
                                static::URL_PATH_DELETE,
                                [
                                    'page_type_id' => $item['page_type_id']
                                ]
                            ),
                            'label'   => __('Delete'),
                            'confirm' => [
                                'title'   => __('Delete "${ $.$data.page_type_code }"'),
                                'message' => __('Are you sure you wan\'t to delete a "${ $.$data.page_type_code }" record?')
                            ]
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}

