<?php

declare(strict_types=1);

namespace Chiaki\Cms\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class Type extends Action
{

    const ADMIN_RESOURCE = 'Chiaki_Cms::Type';
    protected $_coreRegistry;

    /**
     * @param Context $context
     * @param Registry         $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry         $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     *
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
                   ->addBreadcrumb(__('Page'), __('Page'))
                   ->addBreadcrumb(__('Type'), __('Type'));
        return $resultPage;
    }
}

