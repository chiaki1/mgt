<?php

declare(strict_types=1);

namespace Chiaki\Cms\Controller\Adminhtml\Type;

use Chiaki\Cms\Model\Type;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Chiaki\Cms\Controller\Adminhtml\Type
{

    protected $resultPageFactory;

    /**
     * @param Context        $context
     * @param Registry                $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context        $context,
        Registry                $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id    = $this->getRequest()->getParam('page_type_id');
        $model = $this->_objectManager->create(Type::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Type no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('page_type', $model);

        // 3. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Page Type') : __('New Page Type'),
            $id ? __('Edit Page Type') : __('New Page Type')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Page Types'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Page Type %1', $model->getId()) : __('New Page Type'));
        return $resultPage;
    }
}

