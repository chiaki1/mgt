<?php

declare(strict_types=1);

namespace Chiaki\Cms\Controller\Adminhtml\Type;

use Chiaki\Cms\Model\Type;
use Exception;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

class Delete extends \Chiaki\Cms\Controller\Adminhtml\Type
{

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('page_type_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(Type::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Type.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['page_type_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Type to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

