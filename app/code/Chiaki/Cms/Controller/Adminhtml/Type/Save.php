<?php

declare(strict_types=1);

namespace Chiaki\Cms\Controller\Adminhtml\Type;

use Chiaki\Cms\Model\Type;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends Action
{

    protected $dataPersistor;

    /**
     * @param Context                   $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context                   $context,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data           = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('page_type_id');

            $model = $this->_objectManager->create(Type::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Type no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Type.'));
                $this->dataPersistor->clear('page_type');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['page_type_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Type.'));
            }

            $this->dataPersistor->set('page_type', $data);
            return $resultRedirect->setPath('*/*/edit', ['page_type_id' => $this->getRequest()->getParam('page_type_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}

