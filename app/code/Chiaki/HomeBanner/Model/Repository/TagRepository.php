<?php

namespace Chiaki\HomeBanner\Model\Repository;

use Chiaki\HomeBanner\Api\Data\TagInterface;
use Chiaki\HomeBanner\Api\TagRepositoryInterface;
use Chiaki\HomeBanner\Model\ResourceModel\Tag as TagResource;
use Chiaki\HomeBanner\Model\ResourceModel\Tag\Collection;
use Chiaki\HomeBanner\Model\ResourceModel\Tag\CollectionFactory;
use Chiaki\HomeBanner\Model\Tag;
use Chiaki\HomeBanner\Model\TagFactory;
use Exception;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class TagRepository implements TagRepositoryInterface
{
    /**
     * @var TagFactory
     */
    private $tagFactory;

    /**
     * @var TagResource
     */
    private $tagResource;

    /**
     * Model data storage
     *
     * @var array
     */
    private $tags;

    /**
     * @var CollectionFactory
     */
    private $tagCollectionFactory;

    /**
     * TagRepository constructor.
     *
     * @param TagFactory        $tagFactory
     * @param TagResource       $tagResource
     * @param CollectionFactory $tagCollectionFactory
     */
    public function __construct(
        TagFactory $tagFactory,
        TagResource $tagResource,
        CollectionFactory $tagCollectionFactory
    ) {
        $this->tagFactory           = $tagFactory;
        $this->tagResource          = $tagResource;
        $this->tagCollectionFactory = $tagCollectionFactory;
    }

    /**
     * @param TagInterface $tag
     *
     * @return TagInterface
     * @throws CouldNotSaveException
     */
    public function save(TagInterface $tag)
    {
        try {
            if ($tag->getTagId()) {
                $tag = $this->getById($tag->getTagId())->addData($tag->getData());
            } else {
                $tag->setTagId(null);
            }
            $this->tagResource->save($tag);
            unset($this->tags[$tag->getTagId()]);
        } catch (Exception $e) {
            if ($tag->getTagId()) {
                throw new CouldNotSaveException(
                    __(
                        'Unable to save tag with ID %1. Error: %2',
                        [$tag->getTagId(), $e->getMessage()]
                    )
                );
            }
            throw new CouldNotSaveException(__('Unable to save new tag. Error: %1', $e->getMessage()));
        }

        return $tag;
    }

    /**
     * @param int $tagId
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($tagId)
    {
        if (!isset($this->tags[$tagId])) {
            /** @var Tag $tag */
            $tag = $this->tagFactory->create();
            $this->tagResource->load($tag, $tagId);
            if (!$tag->getTagId()) {
                throw new NoSuchEntityException(__('Tag with specified ID "%1" not found.', $tagId));
            }
            $this->tags[$tagId] = $tag;
        }

        return $this->tags[$tagId];
    }

    /**
     * @param TagInterface $tag
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(TagInterface $tag)
    {
        try {
            $this->tagResource->delete($tag);
            unset($this->tags[$tag->getTagId()]);
        } catch (Exception $e) {
            if ($tag->getTagId()) {
                throw new CouldNotDeleteException(
                    __(
                        'Unable to remove tag with ID %1. Error: %2',
                        [$tag->getTagId(), $e->getMessage()]
                    )
                );
            }
            throw new CouldNotDeleteException(__('Unable to remove tag. Error: %1', $e->getMessage()));
        }

        return true;
    }

    /**
     * @param int $tagId
     *
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($tagId)
    {
        $tagModel = $this->getById($tagId);
        $this->delete($tagModel);

        return true;
    }

    /**
     * @param string[] $tags
     *
     * @return Collection
     */
    public function getList($tags)
    {
        return $this->tagCollectionFactory->create()
                                          ->addFieldToFilter(TagInterface::NAME, ['in' => $tags]);
    }

    /**
     * @return Tag
     */
    public function getTagModel()
    {
        return $this->tagFactory->create();
    }

    /**
     * @return Collection
     */
    public function getTagCollection()
    {
        return $this->tagCollectionFactory->create();
    }

    /**
     * @param $bannerId
     *
     * @return Collection
     */
    public function getTagsByBanner($bannerId)
    {
        $tags = $this->tagCollectionFactory->create();
        $tags->addBannerFilter($bannerId);

        return $tags;
    }

    /**
     * @param array $tagsIds
     *
     * @return Collection
     * @throws NoSuchEntityException
     */
    public function getTagsByIds($tagsIds = [])
    {
        if (!is_array($tagsIds)) {
            $tagsIds = explode(',', $tagsIds);
        }
        $tags = $this->tagCollectionFactory->create();
        $tags->addIdFilter($tagsIds);

        return $tags;
    }

    /**
     * @return DataObject[]
     */
    public function getAllTags()
    {
        return $this->tagCollectionFactory->create()->getItems();
    }

    /**
     * Retrieve Tag
     *
     * @param string $tagId
     *
     * @return TagInterface
     * @throws LocalizedException
     */
    public function get($tagId)
    {
        $tags = $this->tagCollectionFactory->create();
        $tags->addIdFilter($tagId);

        return $tags;
    }
}
