<?php

namespace Chiaki\HomeBanner\Model\Repository;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Model\Banner;
use Chiaki\HomeBanner\Model\BannerFactory;
use Chiaki\HomeBanner\Model\ResourceModel\Banner as BannerResource;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\CollectionFactory;
use Exception;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class BannerRepository implements BannerRepositoryInterface
{
    /**
     * @var BannerFactory
     */
    private $bannerFactory;

    /**
     * @var BannerResource
     */
    private $bannerResource;

    /**
     * Model data storage
     *
     * @var array
     */
    private $banners;

    /**
     * @var CollectionFactory
     */
    private $bannerCollectionFactory;

    /**
     * BannerRepository constructor.
     *
     * @param BannerFactory     $bannerFactory
     * @param BannerResource    $bannerResource
     * @param CollectionFactory $bannerCollectionFactory
     */
    public function __construct(
        BannerFactory $bannerFactory,
        BannerResource $bannerResource,
        CollectionFactory $bannerCollectionFactory
    ) {
        $this->bannerFactory           = $bannerFactory;
        $this->bannerResource          = $bannerResource;
        $this->bannerCollectionFactory = $bannerCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function save(BannerInterface $banner)
    {
        try {
            if ($banner->getBannerId()) {
                $banner = $this->getById($banner->getBannerId())->addData($banner->getData());
            } else {
                $banner->setBannerId(null);
            }
            $this->bannerResource->save($banner);
            unset($this->banners[$banner->getBannerId()]);
        } catch (Exception $e) {
            if ($banner->getBannerId()) {
                throw new CouldNotSaveException(
                    __(
                        'Unable to save banner with ID %1. Error: %2',
                        [$banner->getBannerId(), $e->getMessage()]
                    )
                );
            }
            throw new CouldNotSaveException(__('Unable to save new banner. Error: %1', $e->getMessage()));
        }

        return $banner;
    }

    /**
     * @return Banner
     */
    public function getBanner()
    {
        return $this->bannerFactory->create();
    }

    /**
     * @param int $bannerId
     *
     * @return BannerInterface|mixed
     * @throws NoSuchEntityException
     */
    public function get($bannerId)
    {
        if (!isset($this->banners[$bannerId])) {
            /** @var Banner $banners */
            $banners = $this->bannerFactory->create();
            $this->bannerResource->load($banners, $bannerId);
            if (!$banners->getBannerId()) {
                throw new NoSuchEntityException(__('Banner with specified ID "%1" not found.', $bannerId));
            }
            $this->banners[$bannerId] = $banners;
        }

        return $this->banners[$bannerId];
    }

    /**
     * @param int $bannerId
     *
     * @return BannerInterface|mixed
     * @throws NoSuchEntityException
     */
    public function getById($bannerId)
    {
        if (!isset($this->banners[$bannerId])) {
            /** @var Banner $banners */
            $banners = $this->bannerFactory->create();
            $this->bannerResource->load($banners, $bannerId);
            if (!$banners->getBannerId()) {
                throw new NoSuchEntityException(__('Banner with specified ID "%1" not found.', $bannerId));
            }
            $this->banners[$bannerId] = $banners;
        }

        return $this->banners[$bannerId];
    }

    /**
     * @param BannerInterface $banner
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(BannerInterface $banner)
    {
        try {
            $this->bannerResource->delete($banner);
            unset($this->banners[$banner->getBannerId()]);
        } catch (Exception $e) {
            if ($banner->getBannerId()) {
                throw new CouldNotDeleteException(
                    __(
                        'Unable to remove banner with ID %1. Error: %2',
                        [$banner->getBannerId(), $e->getMessage()]
                    )
                );
            }
            throw new CouldNotDeleteException(__('Unable to remove banner. Error: %1', $e->getMessage()));
        }

        return true;
    }

    /**
     * @param int $bannerId
     *
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($bannerId)
    {
        $bannerModel = $this->getById($bannerId);
        $this->delete($bannerModel);

        return true;
    }

    /**
     * @param int $tagId
     *
     * @return BannerResource\Collection
     */
    public function getTaggedBanners($tagId)
    {
        return $this->bannerCollectionFactory->create()->addTagFilter($tagId);
    }

    /**
     * @return BannerResource\Collection
     */
    public function getBannerCollection()
    {
        return $this->bannerCollectionFactory->create();
    }
}
