<?php

namespace Chiaki\HomeBanner\Model;

use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Model\ResourceModel\Banner as BannerResource;
use Exception;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\File\Uploader;
use Magento\Framework\Filesystem;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Framework\UrlInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

class Banner extends AbstractModel implements IdentityInterface, BannerInterface
{
    const CACHE_TAG = 'current_banners';

    const PERSISTENT_NAME = 'current_banners';

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var ImageUploader
     */
    protected $videoUploader;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * Banner constructor.
     *
     * @param Context                    $context
     * @param Registry                   $registry
     * @param Filesystem                 $filesystem
     * @param SerializerJson             $serializer
     * @param AbstractResource|null      $resource
     * @param AbstractDb|null            $resourceCollection
     * @param ImageUploader|null         $imageUploader
     * @param VideoUpload|null           $videoUploader
     * @param StoreManagerInterface|null $storeManager
     * @param array                      $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Filesystem $filesystem,
        SerializerJson $serializer,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        ImageUploader $imageUploader = null,
        VideoUpload $videoUploader = null,
        StoreManagerInterface $storeManager = null,
        array $data = []
    ) {
        $this->_filesystem   = $filesystem;
        $this->serializer    = $serializer;
        $this->imageUploader = $imageUploader ?: ObjectManager::getInstance()->get(ImageUploader::class);
        $this->videoUploader = $videoUploader ?: ObjectManager::getInstance()->get(VideoUpload::class);
        $this->storeManager  = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function _construct()
    {
        parent::_construct();
        $this->_cacheTag = self::CACHE_TAG;
        $this->_init(BannerResource::class);
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->_getData(BannerInterface::TAGS);
    }

    /**
     * @return array
     */
    public function getTagIds()
    {
        return $this->_getData(BannerInterface::TAG_IDS);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        $identities = [
            self::CACHE_TAG . '_' . $this->getId()
        ];

        return $identities;
    }

    /**
     * @return int
     */
    public function getBannerId()
    {
        return (int)$this->_getData(BannerInterface::BANNER_ID);
    }

    /**
     * @param int $bannerId
     *
     * @return $this|BannerInterface
     */
    public function setBannerId($bannerId)
    {
        $this->setData(BannerInterface::BANNER_ID, $bannerId);

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->_getData(BannerInterface::STATUS);
    }

    /**
     * @param int $status
     *
     * @return $this|BannerInterface
     */
    public function setStatus($status)
    {
        $this->setData(BannerInterface::STATUS, $status);

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->_getData(BannerInterface::CREATED_AT);
    }

    /**
     * @param string $createdAt
     *
     * @return $this|BannerInterface
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(BannerInterface::CREATED_AT, $createdAt);

        return $this;
    }

    /**
     * Get banner_type
     *
     * @return string|null
     */
    public function getBannerType()
    {
        return $this->_getData(BannerInterface::BANNER_TYPE);
    }

    /**
     * Set banner_type
     *
     * @param string $bannerType
     *
     * @return BannerInterface
     */
    public function setBannerType($bannerType)
    {
        $this->setData(BannerInterface::BANNER_TYPE, $bannerType);

        return $this;
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_getData(BannerInterface::TITLE);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BannerInterface
     */
    public function setTitle($title)
    {
        $this->setData(BannerInterface::TITLE, $title);

        return $this;
    }

    /**
     * Get banner_position
     *
     * @return string|null
     */
    public function getBannerPosition()
    {
        return $this->_getData(BannerInterface::BANNER_POSITION);
    }

    /**
     * Set banner_position
     *
     * @param string $bannerPosition
     *
     * @return BannerInterface
     */
    public function setBannerPosition($bannerPosition)
    {
        $this->setData(BannerInterface::BANNER_POSITION, $bannerPosition);

        return $this;
    }

    /**
     * Get banner_image
     *
     * @return string|null
     */
    public function getBannerImage()
    {
        return $this->_getData(BannerInterface::BANNER_IMAGE);
    }

    /**
     * Set banner_image
     *
     * @param string $bannerImage
     *
     * @return BannerInterface
     */
    public function setBannerImage($bannerImage)
    {
        $this->setData(BannerInterface::BANNER_IMAGE, $bannerImage);

        return $this;
    }

    /**
     * Get banner_video
     *
     * @return string|null
     */
    public function getBannerVideo()
    {
        return $this->_getData(BannerInterface::BANNER_VIDEO);
    }

    /**
     * Set banner_video
     *
     * @param string $bannerVideo
     *
     * @return BannerInterface
     */
    public function setBannerVideo($bannerVideo)
    {
        $this->setData(BannerInterface::BANNER_VIDEO, $bannerVideo);

        return $this;
    }

    /**
     * Get data_filters
     *
     * @return string|null
     */
    public function getDataFilters()
    {
        return $this->_getData(BannerInterface::DATA_FILTERS);
    }

    /**
     * Set data_filters
     *
     * @param string $dataFilters
     *
     * @return BannerInterface
     */
    public function setDataFilters($dataFilters)
    {
        $this->setData(BannerInterface::DATA_FILTERS, $dataFilters);

        return $this;
    }

    /**
     * Gets image name from $value array.
     *
     * Will return empty string in a case when $value is not an array.
     *
     * @param array $value Attribute value
     *
     * @return string
     */
    private function getUploadedImageName($value)
    {
        if (is_array($value) && isset($value[0]['name'])) {
            return $value[0]['name'];
        }

        return '';
    }

    /**
     * Check that image name exists in catalog/category directory and return new image name if it already exists.
     *
     * @param string $imageName
     *
     * @return string
     */
    private function checkUniqueImageName(string $imageName): string // @codingStandardsIgnoreLine
    {
        $mediaDirectory    = $this->_filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $imageAbsolutePath = $mediaDirectory->getAbsolutePath(
            $this->imageUploader->getBasePath() . DIRECTORY_SEPARATOR . $imageName
        );

        // phpcs:ignore Magento2.Functions.DiscouragedFunction
        $imageName = call_user_func([Uploader::class, 'getNewFilename'], $imageAbsolutePath);

        return $imageName;
    }

    /**
     * Avoiding saving potential upload data to DB.
     *
     * Will set empty image attribute value if image was not uploaded.
     *
     * @return $this
     * @since 101.0.8
     */
    public function beforeSave()
    {
        /** @var StoreInterface $store */
        $store        = $this->storeManager->getStore();
        $baseMediaDir = $store->getBaseMediaDir();
        $bannerImg    = $this->getData('banner_image');

        if ($this->isTmpFileAvailable($bannerImg) && $imageName = $this->getUploadedImageName($bannerImg)) {
            try {
                $newImgRelativePath   = $this->imageUploader->moveFileFromTmp($imageName, true);
                $bannerImg[0]['url']  = '/' . $baseMediaDir . '/' . $newImgRelativePath;
                $bannerImg[0]['name'] = $bannerImg[0]['url'];
            } catch (Exception $e) {
                $this->_logger->critical($e);
            }
        } elseif ($this->fileResidesOutsideCategoryDir($bannerImg)) {
            // use relative path for image attribute so we know it's outside of category dir when we fetch it
            // phpcs:ignore Magento2.Functions.DiscouragedFunction
            $bannerImg[0]['url']  = parse_url($bannerImg[0]['url'], PHP_URL_PATH);
            $bannerImg[0]['name'] = $bannerImg[0]['url'];
        }

        if ($imageName = $this->getUploadedImageName($bannerImg)) {
            //if (!$this->fileResidesOutsideCategoryDir($bannerImg)) {
            $imageName = $this->checkUniqueImageName($imageName);
            //}
            $this->setData('banner_image', $imageName);
        } elseif (!is_string($bannerImg)) {
            $this->setData('banner_image', '');
        }

        $video = $this->getData('banner_video');
        if ($this->isTmpFileAvailable($video) && $videoName = $this->getUploadedImageName($video)) {
            try {
                $newImgRelativePath = $this->videoUploader->moveFileFromTmp($videoName, true);
                $video[0]['url']    = '/' . $baseMediaDir . '/' . $newImgRelativePath;
                $video[0]['name']   = $video[0]['url'];
            } catch (Exception $e) {
                $this->_logger->critical($e);
            }
        } elseif ($this->fileResidesOutsideCategoryDir($video)) {
            // use relative path for image attribute so we know it's outside of category dir when we fetch it
            // phpcs:ignore Magento2.Functions.DiscouragedFunction
            $video[0]['url']  = parse_url($video[0]['url'], PHP_URL_PATH);
            $video[0]['name'] = $video[0]['url'];
        }

        if ($videoName = $this->getUploadedImageName($video)) {
            //if (!$this->fileResidesOutsideCategoryDir($video)) {
            $videoName = $this->checkUniqueImageName($videoName);
            //}
            $this->setData('banner_video', $videoName);
        } elseif (!is_string($video)) {
            $this->setData('banner_video', null);
        }
        $parameters = $this->getData('parameters');
        if ($parameters && isset($parameters['data_filters']) && count($parameters['data_filters']) > 1) {
            $dataFilters = $this->serializer->serialize($parameters['data_filters']);
            $this->setDataFilters($dataFilters);
        }

        return parent::beforeSave();
    }

    /**
     * Check if temporary file is available for new image upload.
     *
     * @param array $value
     *
     * @return bool
     */
    private function isTmpFileAvailable($value)
    {
        return is_array($value) && isset($value[0]['tmp_name']);
    }

    /**
     * Check for file path resides outside of category media dir. The URL will be a path including pub/media if true
     *
     * @param array|null $value
     *
     * @return bool
     */
    private function fileResidesOutsideCategoryDir($value)
    {
        if (!is_array($value) || !isset($value[0]['url'])) {
            return false;
        }

        $fileUrl      = ltrim($value[0]['url'], '/');
        $baseMediaDir = $this->_filesystem->getUri(DirectoryList::MEDIA);

        if (!$baseMediaDir) {
            return false;
        }

        return strpos($fileUrl, $baseMediaDir) !== false;
    }

    /**
     * Returns image url
     *
     * @param string $attributeCode
     *
     * @return bool|string
     * @throws LocalizedException
     */
    public function getImageUrl($attributeCode = 'banner_image')
    {
        $url   = false;
        $image = $this->getData($attributeCode);
        if ($image) {
            if (is_string($image)) {
                $store = $this->storeManager->getStore();

                $isRelativeUrl = substr($image, 0, 1) === '/';

                $mediaBaseUrl = $store->getBaseUrl(
                    UrlInterface::URL_TYPE_MEDIA
                );

                if ($isRelativeUrl) {
                    $url = $image;
                } elseif (!$isRelativeUrl) {
                    $url = $mediaBaseUrl
                           . ltrim('/chiaki/homebanner', '/')
                           . '/'
                           . $image;
                }
            } elseif (!is_string($image)) {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
}
