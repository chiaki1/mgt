<?php

namespace Chiaki\HomeBanner\Model\ResourceModel;

use Chiaki\HomeBanner\Api\Data\TagInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Tag extends AbstractDb
{

    const TABLE_NAME = 'banner_tags';

    public function _construct()
    {
        $this->_init(self::TABLE_NAME, TagInterface::TAG_ID);
    }
}
