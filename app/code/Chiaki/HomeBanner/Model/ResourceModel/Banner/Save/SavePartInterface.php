<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Model\ResourceModel\Banner\Save;

use Chiaki\HomeBanner\Model\Banner;

interface SavePartInterface
{
    public function execute(Banner $model): void;
}
