<?php

namespace Chiaki\HomeBanner\Model\ResourceModel\Banner;

use Chiaki\HomeBanner\Model\Banner as BannerModel;
use Chiaki\HomeBanner\Model\ResourceModel\Banner as BannerResource;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Psr\Log\LoggerInterface;

class Collection extends AbstractCollection
{

    /**
     * @var array
     */
    protected $_map
        = [
            'fields' => [
                'banner_id' => 'main_table.banner_id'
            ]
        ];

    /**
     * @var string
     */
    protected $_idFieldName = 'banner_id';

    /**
     * Collection constructor.
     *
     * @param EntityFactoryInterface $entityFactory
     * @param LoggerInterface        $logger
     * @param FetchStrategyInterface $fetchStrategy
     * @param ManagerInterface       $eventManager
     * @param AdapterInterface|null  $connection
     * @param AbstractDb|null        $resource
     */
    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    public function _construct()
    {
        $this->_init(BannerModel::class, BannerResource::class);
    }

    /**
     * @param $tagId
     *
     * @return $this
     */
    public function addTagFilter($tagIds)
    {
        if (!is_array($tagIds)) {
            $tagIds = [$tagIds];
        }
        $this->getSelect()
             ->joinLeft(
                 ['tags' => $this->getTable('home_banner_tag')],
                 'main_table.banner_id = tags.banner_id',
                 []
             )
             ->where('tags.tag_id IN (?)', $tagIds)
             ->group('main_table.banner_id');

        return $this;
    }

    /**
     * Load data
     *
     * @param boolean $printQuery
     * @param boolean $logQuery
     *
     * @return $this
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if (!$this->isLoaded()) {
            parent::load($printQuery, $logQuery);
            $this->addLinkedTables();
        }

        return $this;
    }

    /**
     * @return void
     */
    private function addLinkedTables()
    {
        $this->addLinkedData('tag', 'tag_id', 'tag_ids');
    }

    /**
     * @param $linkedTable
     * @param $linkedField
     * @param $fieldName
     */
    private function addLinkedData($linkedTable, $linkedField, $fieldName)
    {
        $connection = $this->getConnection();

        $bannerId = $this->getColumnValues('banner_id');
        $linked   = [];
        if (!empty($bannerId)) {
            $inCond = $connection->prepareSqlCondition('banner_id', ['in' => $bannerId]);
            $select = $connection->select()
                                 ->from($this->getTable('home_banner_' . $linkedTable))->where($inCond);
            $result = $connection->fetchAll($select);
            foreach ($result as $row) {
                if (!isset($linked[$row['banner_id']])) {
                    $linked[$row['banner_id']] = [];
                }
                $linked[$row['banner_id']][] = $row[$linkedField];
            }
        }

        foreach ($this as $item) {
            if (isset($linked[$item->getId()])) {
                $item->setData($fieldName, $linked[$item->getId()]);
            } else {
                $item->setData($fieldName, []);
            }
        }
    }
}
