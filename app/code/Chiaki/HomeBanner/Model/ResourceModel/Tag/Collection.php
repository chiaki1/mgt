<?php

namespace Chiaki\HomeBanner\Model\ResourceModel\Tag;

use Chiaki\HomeBanner\Model\ResourceModel\Tag;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var bool
     */
    private $bannerDataJoined = false;

    /**
     * @var string
     */
    private $queryText;

    /**
     * @var string
     */
    protected $_idFieldName = 'tag_id';

    /**
     * @var array
     */
    protected $_map
        = [
            'fields' => [
                'tag_id' => 'main_table.tag_id'
            ]
        ];

    public function _construct()
    {
        $this->_init(\Chiaki\HomeBanner\Model\Tag::class, Tag::class);
    }

    /**
     * @return $this
     */
    public function setNameOrder()
    {
        $this->getSelect()->order('name ASC');

        return $this;
    }

    /**
     * @return $this
     */
    private function joinBannerData()
    {
        if ($this->bannerDataJoined) {
            return $this;
        }

        $this->bannerDataJoined = true;

        $bannerTagTable = $this->getTable('home_banner_tag');
        $this->getSelect()->join(['home_banner_tag' => $bannerTagTable], "home_banner_tag.tag_id = main_table.tag_id", []);

        return $this;
    }

    /**
     * @param $bannerIds
     *
     * @return $this
     */
    public function addBannerFilter($bannerIds)
    {
        $bannerIds = is_array($bannerIds) ? $bannerIds : [$bannerIds];

        $this->joinBannerData();
        $this->getSelect()->where('home_banner_tag.banner_id IN (?)', $bannerIds);

        return $this;
    }

    /**
     * @param array $tagIds
     *
     * @return $this
     */
    public function addIdFilter($tagIds = [])
    {
        if (!is_array($tagIds)) {
            $tagIds = [$tagIds];
        }
        $this->addFieldToFilter('tag_id', ['in' => $tagIds]);

        return $this;
    }

    /**
     * @return string
     */
    public function getQueryText()
    {
        return $this->queryText;
    }

    /**
     * @param $queryText
     *
     * @return $this
     */
    public function setQueryText($queryText)
    {
        $this->queryText = $queryText;

        return $this;
    }

    /**
     * @return $this
     */
    public function addCount()
    {
        $this->getSelect()
             ->joinLeft(
                 ['bannertag' => $this->getTable('home_banner_tag')],
                 'main_table.tag_id = bannertag.tag_id',
                 ['COUNT(bannertag.`tag_id`) as count']
             );
        $this->getSelect()->group('main_table.tag_id');

        return $this;
    }
}
