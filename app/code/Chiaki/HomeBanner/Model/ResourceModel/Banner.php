<?php

namespace Chiaki\HomeBanner\Model\ResourceModel;

use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Api\Data\TagInterface;
use Chiaki\HomeBanner\Api\TagRepositoryInterface;
use Chiaki\HomeBanner\Model\Banner as BannerModel;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\Save\SavePartInterface;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\Save\SavePartProcessorsPool;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class
 */
class Banner extends AbstractDb
{
    const TABLE_NAME = 'home_banner';

    const HOME_BANNER_TAGS_TABLE = 'home_banner_tag';

    const BANNER_TAGS_TABLE = 'banner_tags';

    /**
     * @var TagRepositoryInterface
     */
    private $tagRepository;

    /**
     * @var TagInterface
     */
    private $tagModel;

    /**
     * @var SavePartProcessorsPool
     */
    private $savePartProcessorsPool;

    /**
     * Banner constructor.
     *
     * @param Context                $context
     * @param TagRepositoryInterface $tagRepository
     * @param SavePartProcessorsPool $savePartProcessorsPool
     * @param null                   $connectionName
     */
    public function __construct(
        Context $context,
        TagRepositoryInterface $tagRepository,
        SavePartProcessorsPool $savePartProcessorsPool,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->tagRepository          = $tagRepository;
        $this->tagModel               = $tagRepository->getTagModel();
        $this->savePartProcessorsPool = $savePartProcessorsPool;
    }

    public function _construct()
    {
        $this->_init(self::TABLE_NAME, BannerInterface::BANNER_ID);
    }

    /**
     * @param BannerInterface $object
     *
     * @return Banner|AbstractDb
     */
    protected function _afterLoad(AbstractModel $object)
    {
        parent::_afterLoad($object);

        return $this->loadTags($object);
    }

    /**
     * @param BannerInterface $bannerModel
     *
     * @return $this
     */
    private function loadTags(BannerInterface $bannerModel)
    {
        $bannerId = $bannerModel->getId();
        if ($bannerId) {
            $select = $this->getConnection()->select()
                           ->from(
                               ['banners_tags' => $this->getTable(Banner::HOME_BANNER_TAGS_TABLE)],
                               ['banners_tags.tag_id', 'tags.name']
                           )
                           ->joinLeft(
                               ['tags' => $this->getTable(self::BANNER_TAGS_TABLE)],
                               'banners_tags.tag_id = tags.tag_id',
                               []
                           )
                           ->where('banners_tags.banner_id = :banner_id');

            $tags = $this->getConnection()->fetchPairs($select, [':banner_id' => $bannerId]);
            $bannerModel->setData(BannerInterface::TAGS, implode(',', array_values($tags)));
            $bannerModel->setData(BannerInterface::TAG_IDS, implode(',', array_keys($tags)));
        }

        return $this;
    }

    /**
     * @param BannerModel $object
     *
     * @return AbstractDb
     * @throws FileSystemException
     * @throws LocalizedException
     */
    protected function _afterSave(AbstractModel $object)
    {
        $connection = $this->getConnection();
        $this->saveTags($object, $connection);

        /** @var SavePartInterface $savePartProcessor * */
        foreach ($this->savePartProcessorsPool as $savePartProcessor) {
            $savePartProcessor->execute($object);
        }

        return parent::_afterSave($object);
    }

    /**
     * @param BannerInterface  $banner
     * @param AdapterInterface $connection
     *
     * @return $this
     */
    private function saveTags(BannerInterface $banner, AdapterInterface $connection)
    {
        $tags      = $banner->getData('tags');
        $condition = [BannerInterface::BANNER_ID . ' = ?' => $banner->getBannerId()];
        $connection->delete($this->getTable(self::HOME_BANNER_TAGS_TABLE), $condition);
        if (!empty($tags)) {
            $tagsArray = explode(',', $tags);
            $tagsList  = $this->tagRepository->getList($tagsArray);
            $existTags = [];
            foreach ($tagsList as $tag) {
                $existTags[$tag->getId()] = $tag->getName();
            }

            foreach ($tagsArray as $tag) {
                if (!$tag) {
                    continue;
                }
                //insert exist tag or create new tag
                $this->tagModel->setData([])->isObjectNew(false);
                if (in_array($tag, $existTags)) {
                    $tagInsert = ['tag_id' => array_search($tag, $existTags), 'banner_id' => $banner->getBannerId()];
                } else {
                    $newTag = [
                        TagInterface::NAME => $tag,
                    ];
                    $this->tagModel->setData($newTag);
                    $this->tagRepository->save($this->tagModel);
                    $id        = $this->tagModel->getId();
                    $tagInsert = ['tag_id' => $id, 'banner_id' => $banner->getBannerId()];
                }

                $connection->insert($this->getTable(self::HOME_BANNER_TAGS_TABLE), $tagInsert);
            }
        }

        return $this;
    }
}
