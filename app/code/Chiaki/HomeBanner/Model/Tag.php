<?php

namespace Chiaki\HomeBanner\Model;

use Chiaki\HomeBanner\Api\Data\TagInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Tag extends AbstractModel implements IdentityInterface, TagInterface
{
    const PERSISTENT_NAME = 'banner_tags';

    const CACHE_TAG = 'banner_tags';

    public function _construct()
    {
        parent::_construct();
        $this->_cacheTag = self::CACHE_TAG;
        $this->_init(ResourceModel\Tag::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        $identities = [
            self::CACHE_TAG . '_' . $this->getId()
        ];

        return $identities;
    }

    /**
     * @return int
     */
    public function getTagId()
    {
        return (int)$this->_getData(TagInterface::TAG_ID);
    }

    /**
     * @param int $tagId
     *
     * @return $this|TagInterface
     */
    public function setTagId($tagId)
    {
        $this->setData(TagInterface::TAG_ID, $tagId);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->_getData(TagInterface::NAME);
    }

    /**
     * @param string|null $name
     *
     * @return $this|TagInterface
     */
    public function setName($name)
    {
        $this->setData(TagInterface::NAME, $name);

        return $this;
    }
}
