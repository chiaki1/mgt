<?php

namespace Chiaki\HomeBanner\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class
 */
class BannerType implements ArrayInterface
{
    const TYPE_IMAGE = 'image';

    const TYPE_VIDEO = 'video';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::TYPE_IMAGE, 'label' => __('Image')],
            ['value' => self::TYPE_VIDEO, 'label' => __('Video')],
        ];
    }
}
