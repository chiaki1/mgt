<?php

namespace Chiaki\HomeBanner\Model\DataProvider;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Ui\DataProvider\Modifier\PoolInterface as UiDataModifiersPool;

class BannerDataProvider extends AbstractDataProvider
{
    protected $loadedData;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var BannerRepositoryInterface
     */
    protected $bannerRepository;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var UiDataModifiersPool
     */
    protected $uiDataModifiersPool;

    /**
     * @var FileInfo
     */
    protected $fileInfo;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * BannerDataProvider constructor.
     *
     * @param string                    $name
     * @param string                    $primaryFieldName
     * @param string                    $requestFieldName
     * @param DataPersistorInterface    $dataPersistor
     * @param CollectionFactory         $collectionFactory
     * @param BannerRepositoryInterface $bannerRepository
     * @param UiDataModifiersPool       $uiDataModifiersPool
     * @param StoreManagerInterface     $storeManager
     * @param FileInfo                  $fileInfo
     * @param array                     $meta
     * @param array                     $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        DataPersistorInterface $dataPersistor,
        CollectionFactory $collectionFactory,
        BannerRepositoryInterface $bannerRepository,
        UiDataModifiersPool $uiDataModifiersPool,
        StoreManagerInterface $storeManager,
        FileInfo $fileInfo,
        array $meta = [],
        array $data = []
    ) {
        $this->collection          = $collectionFactory->create();
        $this->dataPersistor       = $dataPersistor;
        $this->bannerRepository    = $bannerRepository;
        $this->collectionFactory   = $collectionFactory;
        $this->uiDataModifiersPool = $uiDataModifiersPool;
        $this->fileInfo            = $fileInfo;
        $this->storeManager        = $storeManager;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
    }

    /**
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $model                             = $this->getBanner($model->getBannerId());
            $result                            = $model->getData();
            $result                            = $this->convertValues('banner_image', $result);
            $result                            = $this->convertValues('banner_video', $result);
            $this->loadedData[$model->getId()] = $result;
        }
        $data = $this->dataPersistor->get('current_banners');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $result                            = $model->getData();
            $result                            = $this->convertValues('banner_image', $result);
            $result                            = $this->convertValues('banner_video', $result);
            $this->loadedData[$model->getId()] = $result;
            $this->dataPersistor->clear('current_banners');
        }
        return $this->loadedData;
    }

    private function convertValues($attributeCode, $bannerData): array // @codingStandardsIgnoreLine
    {
        if (isset($bannerData[$attributeCode]) && $bannerData[$attributeCode] && is_string($bannerData[$attributeCode])) {
            $fileName = $bannerData[$attributeCode];
            unset($bannerData[$attributeCode]);
            if ($this->fileInfo->isExist($fileName)) {
                $stat = $this->fileInfo->getStat($fileName);
                $mime = $this->fileInfo->getMimeType($fileName);

                // phpcs:ignore Magento2.Functions.DiscouragedFunction
                $bannerData[$attributeCode][0]['name'] = basename($fileName);

                $bannerData[$attributeCode][0]['url'] = $this->getUrl($fileName);

                $bannerData[$attributeCode][0]['size'] = isset($stat) ? $stat['size'] : 0;
                $bannerData[$attributeCode][0]['type'] = $mime;
            }
        }

        return $bannerData;
    }

    /**
     * @param $image
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @SuppressWarnings(PHPMD)
     */
    public function getUrl($image): string // @codingStandardsIgnoreLine
    {
        $url = '';
        if ($image) {
            if (is_string($image)) {
                $store        = $this->storeManager->getStore();
                $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                if ($this->fileInfo->isBeginsWithMediaDirectoryPath($image)) {
                    $relativePath = $this->fileInfo->getRelativePathToMediaDirectory($image);
                    $url          = rtrim($mediaBaseUrl, '/') . '/' . ltrim($relativePath, '/');
                } elseif (substr($image, 0, 1) !== '/') {
                    $url = rtrim($mediaBaseUrl, '/') . '/' . ltrim(FileInfo::ENTITY_MEDIA_PATH, '/') . '/' . $image;
                } else {
                    $url = $image;
                }
            } else {
                throw new LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    public function getMeta()
    {
        $meta = parent::getMeta();

        foreach ($this->uiDataModifiersPool->getModifiersInstances() as $modifier) {
            $meta = $modifier->modifyMeta($meta);
        }

        return $meta;
    }

    /**
     * @param $bannerId
     *
     * @return BannerInterface|null
     */
    private function getBanner($bannerId)
    {
        try {
            $model = $this->bannerRepository->get($bannerId);
        } catch (NoSuchEntityException $e) {
            $model = null;
        }

        return $model;
    }

    /**
     * @param BannerInterface $model
     * @param array           $bannerData
     *
     * @return array
     * @throws NoSuchEntityException
     */
    protected function modifyData(BannerInterface $model, $bannerData)
    {

        foreach ($this->uiDataModifiersPool->getModifiersInstances() as $modifier) {
            $bannerData = $modifier->modifyData($bannerData);
        }

        return $bannerData;
    }
}
