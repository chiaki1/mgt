<?php

namespace Chiaki\HomeBanner\Model\DataProvider;

use Chiaki\HomeBanner\Model\BannerRegistry;
use Chiaki\HomeBanner\Model\ResourceModel\Tag\CollectionFactory;
use Chiaki\HomeBanner\Model\Tag;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\AbstractDataProvider;

class TagDataProvider extends AbstractDataProvider
{

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var BannerRegistry
     */
    private $bannerRegistry;

    /**
     * TagDataProvider constructor.
     *
     * @param string                 $name
     * @param string                 $primaryFieldName
     * @param string                 $requestFieldName
     * @param DataPersistorInterface $dataPersistor
     * @param CollectionFactory      $collectionFactory
     * @param BannerRegistry         $bannerRegistry
     * @param array                  $meta
     * @param array                  $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        DataPersistorInterface $dataPersistor,
        CollectionFactory $collectionFactory,
        BannerRegistry $bannerRegistry,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection     = $collectionFactory->create();
        $this->dataPersistor  = $dataPersistor;
        $this->bannerRegistry = $bannerRegistry;
    }

    /**
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData()
    {
        $data    = parent::getData();
        $current = $this->bannerRegistry->registry('current_banner_tag');

        if ($current && $current->getId()) {
            $data[$current->getId()] = $current->getData();
        }

        if ($savedData = $this->dataPersistor->get(Tag::PERSISTENT_NAME)) {
            $savedTagId        = isset($savedData['tag_id']) ? $savedData['tag_id'] : null;
            $data[$savedTagId] = isset($data[$savedTagId])
                ? array_merge($data[$savedTagId], $savedData)
                : $savedData;
            $this->dataPersistor->clear(Tag::PERSISTENT_NAME);
        }

        return $data;
    }
}
