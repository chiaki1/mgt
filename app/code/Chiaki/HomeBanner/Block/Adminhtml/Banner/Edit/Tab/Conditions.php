<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\HomeBanner\Block\Adminhtml\Banner\Edit\Tab;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class Conditions
 *
 * @package Chiaki\HomeBanner\Block\Adminhtml\Banner\Edit\Tab
 */
class Conditions extends Template
{
    /**
     *
     * @var string
     */
    protected $_template = 'Magento_PageBuilder::form/element/conditions.phtml';

    /**
     * @var Json
     */
    private $serializer;

    /**
     * Conditions constructor.
     *
     * @param Context                             $context
     * @param Json $serializer
     * @param array                                        $data
     */
    public function __construct(
        Context $context,
        Json $serializer,
        array $data = []
    ) {
        $this->serializer = $serializer;
        parent::__construct($context, $data);
    }

    /**
     * Returns an array of arguments to pass to the condition tree UIComponent
     *
     * @return array
     */
    private function getConfig(): array
    {
        $formNamespace = $this->getData('formNamespace');
        $attribute     = $this->getData('attribute');
        $jsObjectName  = $formNamespace . '_' . $attribute;

        return [
            'formNamespace'     => $formNamespace,
            'componentUrl'      => $this->getUrl(
                'homebanner/form/element_productconditions',
                [
                    'form_namespace' => $formNamespace,
                    'prefix'         => $attribute,
                    'js_object_name' => $jsObjectName,
                ]
            ),
            'jsObjectName'      => $jsObjectName,
            'childComponentUrl' => $this->getUrl(
                'homebanner/form/element_productconditions_child',
                [
                    'form_namespace' => $formNamespace,
                    'prefix'         => $attribute,
                    'js_object_name' => $jsObjectName,
                ]
            ),
            'attribute'         => $attribute,
        ];
    }

    /**
     * Creates a JSON string containing the configuration for the needed JS components in the mage-init format
     *
     * @return string
     */
    public function getConfigJson(): string
    {
        return $this->serializer->serialize([
                                                '[data-role=pagebuilder-conditions-form-placeholder-' . $this->getData('attribute') . ']' => [
                                                    'Magento_PageBuilder/js/form/element/conditions-loader' => $this->getConfig(),
                                                ]
                                            ]
        );
    }
}
