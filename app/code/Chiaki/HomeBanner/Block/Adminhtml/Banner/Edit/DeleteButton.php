<?php

namespace Chiaki\HomeBanner\Block\Adminhtml\Banner\Edit;

use Chiaki\HomeBanner\Controller\Adminhtml\Banner\Edit;

class DeleteButton extends \Chiaki\HomeBanner\Block\Adminhtml\DeleteButton
{
    /**
     * @return int
     */
    public function getItemId()
    {
        return (int)$this->getRegistry()->registry(Edit::CURRENT_HOME_BANNER)->getBannerId();
    }
}
