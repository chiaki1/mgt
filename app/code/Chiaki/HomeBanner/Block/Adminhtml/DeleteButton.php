<?php

namespace Chiaki\HomeBanner\Block\Adminhtml;

use Chiaki\HomeBanner\Model\BannerRegistry;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class DeleteButton implements ButtonProviderInterface
{
    /**
     * Url Builder
     *
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var BannerRegistry
     */
    private $bannerRegistry;

    /**
     * DeleteButton constructor.
     *
     * @param UrlInterface   $urlBuilder
     * @param BannerRegistry $bannerRegistry
     */
    public function __construct(
        UrlInterface $urlBuilder,
        BannerRegistry $bannerRegistry
    ) {
        $this->urlBuilder     = $urlBuilder;
        $this->bannerRegistry = $bannerRegistry;
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        $data = [];
        $id   = $this->getItemId();
        if ($id) {
            $deleteUrl = $this->urlBuilder->getUrl('*/*/delete', ['id' => $id]);
            $data      = [
                'label'      => __('Delete'),
                'class'      => 'delete',
                'on_click'   => 'deleteConfirm(\'' . $this->getConfirmText()
                                . '\', \'' . $deleteUrl . '\')',
                'sort_order' => 20,
            ];
        }

        return $data;
    }

    /**
     * @return BannerRegistry
     */
    public function getRegistry()
    {
        return $this->bannerRegistry;
    }

    /**
     * @return Phrase
     */
    public function getConfirmText()
    {
        return __('Are you sure you want to delete this?');
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return 0;
    }
}
