<?php

namespace Chiaki\HomeBanner\Block\Adminhtml\Tags\Edit;

use Chiaki\HomeBanner\Controller\Adminhtml\Tags\Edit;

class DeleteButton extends \Chiaki\HomeBanner\Block\Adminhtml\DeleteButton
{
    /**
     * @return int
     */
    public function getItemId()
    {
        return (int)$this->getRegistry()->registry(Edit::CURRENT_BANNER_TAG)->getTagId();
    }
}
