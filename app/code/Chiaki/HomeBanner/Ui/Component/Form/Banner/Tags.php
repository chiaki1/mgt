<?php

namespace Chiaki\HomeBanner\Ui\Component\Form\Banner;

use Chiaki\HomeBanner\Api\TagRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Form\Field;

class Tags extends Field
{
    /**
     * @var TagRepositoryInterface
     */
    private $tagRepository;

    /**
     * Tags constructor.
     *
     * @param ContextInterface       $context
     * @param UiComponentFactory     $uiComponentFactory
     * @param TagRepositoryInterface $tagRepository
     * @param array                  $components
     * @param array                  $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        TagRepositoryInterface $tagRepository,
        $components,
        array $data = []
    ) {
        $this->tagRepository = $tagRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare component configuration
     *
     * @return void
     * @throws LocalizedException
     */
    public function prepare()
    {
        $tagsArray = [];
        foreach ($this->tagRepository->getAllTags() as $tag) {
            $tagName = $tag->getName();
            if ($tagName) {
                $tagsArray[] = $tagName;
            }
        }
        $config         = $this->getData('config');
        $config['tags'] = $tagsArray;
        $this->setData('config', $config);
        parent::prepare();
    }
}
