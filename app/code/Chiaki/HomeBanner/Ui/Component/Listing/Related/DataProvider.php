<?php

namespace Chiaki\HomeBanner\Ui\Component\Listing\Related;

use Magento\Framework\Api\Search\SearchResultInterface;

/**
 * Class
 */
class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{
    /**
     * @param SearchResultInterface $searchResult
     *
     * @return array
     */
    protected function searchResultToOutput(SearchResultInterface $searchResult)
    {
        $arrItems          = [];
        $arrItems['items'] = [];
        $this->applyFilters($searchResult);

        foreach ($searchResult->getItems() as $item) {
            $data                = $item->getData();
            $arrItems['items'][] = $data;
        }

        $arrItems['totalRecords'] = $searchResult->getSize();

        return $arrItems;
    }

    protected function applyFilters(SearchResultInterface $searchResult)
    {
        $tag = (int)$this->request->getParam('tag_id');
        if ($tag) {
            $searchResult->addTagFilter($tag);
        }
    }
}
