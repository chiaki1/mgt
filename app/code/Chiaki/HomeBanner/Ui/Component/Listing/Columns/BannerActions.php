<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class BannerActions extends Column
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * BannerActions constructor.
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name                = $this->getData('name');
                $item[$name]['edit'] = [
                    'href'  => $this->urlBuilder->getUrl(
                        'homebanner/banner/edit',
                        ['id' => $item['banner_id']]
                    ),
                    'label' => __('Edit')
                ];

                $item[$name]['delete'] = [
                    'href'    => $this->urlBuilder->getUrl(
                        'homebanner/banner/delete',
                        ['id' => $item['banner_id']]
                    ),
                    'label'   => __('Delete'),
                    'confirm' => [
                        'title'         => __('Delete "${ $.$data.title }"'),
                        'message'       => __('Are you sure you want to duplicate a "${ $.$data.title }" record?'),
                        '__disableTmpl' => false
                    ]
                ];
            }
        }

        return $dataSource;
    }
}
