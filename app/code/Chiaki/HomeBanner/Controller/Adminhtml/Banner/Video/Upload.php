<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner\Video;

use Chiaki\HomeBanner\Model\VideoUpload;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Upload
 */
class Upload extends Action implements HttpPostActionInterface
{
    /**
     * @var VideoUpload
     */
    protected $videoUploader;

    /**
     * Upload constructor.
     *
     * @param Context     $context
     * @param VideoUpload $videoUploader
     */
    public function __construct(
        Context $context,
        VideoUpload $videoUploader
    ) {
        parent::__construct($context);
        $this->videoUploader = $videoUploader;
    }

    /**
     * Upload file controller action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $imageId = $this->_request->getParam('param_name', 'banner_video');

        try {
            $result = $this->videoUploader->saveFileToTmpDir($imageId);
        } catch (Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
