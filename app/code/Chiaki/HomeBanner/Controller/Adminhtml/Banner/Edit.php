<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Controller\Adminhtml\Banner;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class Edit extends Banner
{
    const CURRENT_HOME_BANNER = 'current_banners';

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $id    = $this->getRequest()->getParam('id');
        $model = $this->getBannerRepository()->getBanner();
        if ($id) {
            try {
                $model = $this->getBannerRepository()->get($id);
            } catch (Exception $e) {
                $this->getMessageManager()->addErrorMessage($e->getMessage());
                $this->_redirect('*/*');

                return;
            }
        }

        // set entered data if was error when we do save
        $data = $this->_getSession()->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->getRegistry()->register(self::CURRENT_HOME_BANNER, $model);
        $this->initAction();

        $title = $model->getId() ? __('Edit Banner `%1`', $model->getTitle()) : __("Add New Banner");

        $this->_view->getPage()->getConfig()->getTitle()->prepend($title);
        $this->_view->renderLayout();
    }

    /**
     * @return $this
     */
    private function initAction()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu('Chiaki_HomeBanner::manage_banner')->_addBreadcrumb(
            __('Manage Banner'),
            __('Manage Banner')
        );

        return $this;
    }
}
