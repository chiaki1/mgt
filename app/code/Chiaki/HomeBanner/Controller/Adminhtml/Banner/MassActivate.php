<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Model\Source\BannerStatus;
use Exception;
use Magento\Framework\Controller\Result\Redirect;

/**
 * Class
 */
class MassActivate extends AbstractMassAction
{
    /**
     * @param $banner
     *
     * @return Redirect
     */
    protected function itemAction($banner)
    {
        try {
            $banner->setStatus(BannerStatus::STATUS_ENABLED);
            $this->getRepository()->save($banner);
        } catch (Exception $e) {
            $this->getMessageManager()->addErrorMessage($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
