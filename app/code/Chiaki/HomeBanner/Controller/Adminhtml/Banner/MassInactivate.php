<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Model\Source\BannerStatus;
use Exception;
use Magento\Framework\Controller\Result\Redirect;

/**
 * Class MassInactivate
 */
class MassInactivate extends AbstractMassAction
{
    /**
     * @param BannerInterface $banner
     *
     * @return Redirect
     */
    protected function itemAction($banner)
    {
        try {
            $banner->setStatus(BannerStatus::STATUS_DISABLED);
            $this->getRepository()->save($banner);
        } catch (Exception $e) {
            $this->getMessageManager()->addErrorMessage($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
