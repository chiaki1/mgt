<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Chiaki\HomeBanner\Model\Banner;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Zend_Filter_Input;

class Save extends \Chiaki\HomeBanner\Controller\Adminhtml\Banner
{

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            $id = (int)$this->getRequest()->getParam('banner_id');

            try {
                $inputFilter = new Zend_Filter_Input([], [], $data);
                $data        = $inputFilter->getUnescaped();

                if ($id) {
                    $model = $this->getBannerRepository()->getById($id);
                } else {
                    $model = $this->getBannerRepository()->getBanner();
                }

                $parameters = $this->getRequest()->getParam('parameters');
                if ($parameters && isset($parameters['data_filters']) && count($parameters['data_filters']) > 1) {
                    $newData = [];
                    foreach ($dataFilters = $parameters['data_filters'] as $key => $dataFilter) {
                        if ($dataFilter['type'] == "Chiaki\HomeBanner\Model\Rule\Condition\Combine") {
                            unset($dataFilters[$key]);
                            continue;
                        }
                        $newData[$dataFilter['attribute']][] = $dataFilter;
                    }
                    foreach ($newData as $attributeCode => $datum) {
                        if ($attributeCode == "category_ids" && count($datum) > 1) {
                            $this->getMessageManager()->addErrorMessage(
                                __('Category is only allowed to set 1 condition.')
                            );
                            $this->_getSession()->setPageData($data);
                            $this->_redirect('*/*/edit', ['id' => $id]);

                            return;
                        } elseif ($attributeCode == "price") {
                            if (count($datum) > 2) {
                                $this->getMessageManager()->addErrorMessage(
                                    __('Price is only allowed to set 2 condition from and to.')
                                );
                                $this->_getSession()->setPageData($data);
                                $this->_redirect('*/*/edit', ['id' => $id]);

                                return;
                            } elseif (count($datum) == 2) {
                                if ($datum[0]['operator'] == $datum[1]['operator']) {
                                    $this->getMessageManager()->addErrorMessage(
                                        __('Price is only allowed to set 2 condition from and to.')
                                    );
                                    $this->_getSession()->setPageData($data);
                                    $this->_redirect('*/*/edit', ['id' => $id]);

                                    return;
                                } elseif (in_array($datum[0]['operator'], ['==', '()']) || in_array($datum[1]['operator'], ['==', '()'])) {
                                    $this->getMessageManager()->addErrorMessage(
                                        __('Price is only allowed to set 2 condition from and to. Please not set operator "is" or "is one of".')
                                    );
                                    $this->_getSession()->setPageData($data);
                                    $this->_redirect('*/*/edit', ['id' => $id]);

                                    return;
                                }
                            }
                        } elseif ($attributeCode != "category_ids" && $attributeCode != "price" && count($datum) > 1) {
                            $this->getMessageManager()->addErrorMessage(
                                __('%1 is only allowed to set 1 condition.', $attributeCode)
                            );
                            $this->_getSession()->setPageData($data);
                            $this->_redirect('*/*/edit', ['id' => $id]);

                            return;
                        }
                    }
                }

                $data = $this->prepareData($data);
                $model->addData($data);

                $this->_getSession()->setPageData($data);
                $this->getBannerRepository()->save($model);

                $this->getMessageManager()->addSuccessMessage(__('You saved the item.'));
                $this->_getSession()->setPageData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $model->getBannerId()]);

                    return;
                }

                $this->_redirect('*/*/');

                return;
            } catch (NoSuchEntityException $e) {
                $this->getMessageManager()->addErrorMessage($e->getMessage());
                $this->getDataPersistor()->set(Banner::PERSISTENT_NAME, $data);
                $this->_redirect('*/*/edit', ['id' => $id]);

                return;
            } catch (Exception $e) {
                $this->getMessageManager()->addErrorMessage(
                    __($e->getMessage())
                );
                $this->getLogger()->critical($e);
                $this->_getSession()->setPageData($data);
                $this->_redirect('*/*/edit', ['id' => $id]);

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function prepareData(array $data)
    {
        return $data;
    }

    protected function getRepository(): BannerRepositoryInterface
    {
        return $this->getBannerRepository();
    }
}
