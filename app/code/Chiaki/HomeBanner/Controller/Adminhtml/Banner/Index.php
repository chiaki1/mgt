<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Controller\Adminhtml\Banner;
use Magento\Backend\Model\View\Result\Page;

/**
 * Class Index
 */
class Index extends Banner
{
    /**
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->getPageFactory()->create();
        $resultPage->setActiveMenu('Chiaki_HomeBanner::manage_banner');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Banner'));
        $resultPage->addBreadcrumb(__('Manage Banner'), __('Manage Banner'));

        return $resultPage;
    }
}
