<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Exception;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Phrase;

/**
 * Class MassDelete
 */
class MassDelete extends AbstractMassAction
{
    /**
     * @param $banner
     *
     * @return Redirect
     */
    protected function itemAction($banner)
    {
        try {
            $this->getRepository()->deleteById($banner->getBannerId());
        } catch (Exception $e) {
            $this->getMessageManager()->addErrorMessage($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }

    /**
     * @return Phrase
     */
    protected function getErrorMessage()
    {
        return __('We can\'t delete item right now. Please review the log and try again.');
    }

    /**
     * @param int $collectionSize
     *
     * @return Phrase
     */
    protected function getSuccessMessage($collectionSize = 0)
    {
        return $collectionSize
            ? __('A total of %1 record(s) have been deleted.', $collectionSize)
            : __('No records have been deleted.');
    }
}
