<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Chiaki\HomeBanner\Model\Repository\BannerRepository;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\Collection;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractMassAction
 */
abstract class AbstractMassAction extends \Chiaki\HomeBanner\Controller\Adminhtml\AbstractMassAction
{

    /**
     * @var BannerRepository
     */
    private $repository;

    /**
     * AbstractMassAction constructor.
     *
     * @param Action\Context            $context
     * @param Filter                    $filter
     * @param LoggerInterface           $logger
     * @param BannerRepositoryInterface $repository
     */
    public function __construct(
        Action\Context $context,
        Filter $filter,
        LoggerInterface $logger,
        BannerRepositoryInterface $repository
    ) {
        parent::__construct($context, $filter, $logger);
        $this->repository = $repository;
    }

    /**
     * @return BannerRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->repository->getBannerCollection();
    }
}
