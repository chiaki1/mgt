<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Banner;

use Chiaki\HomeBanner\Controller\Adminhtml\Banner;

/**
 * Class NewAction
 */
class NewAction extends Banner
{
    public function execute()
    {
        $this->_forward('edit');
    }
}
