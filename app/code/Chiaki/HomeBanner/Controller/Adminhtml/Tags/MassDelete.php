<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Api\Data\TagInterface;
use Exception;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Phrase;

/**
 * Class MassDelete
 */
class MassDelete extends AbstractMassAction
{
    /**
     * @param TagInterface $tag
     *
     * @return Redirect
     */
    protected function itemAction($tag)
    {
        try {
            $this->getRepository()->deleteById($tag->getTagId());
        } catch (Exception $e) {
            $this->getMessageManager()->addErrorMessage($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }

    /**
     * @return Phrase
     */
    protected function getErrorMessage()
    {
        return __('We can\'t delete item right now. Please review the log and try again.');
    }

    /**
     * @param int $collectionSize
     *
     * @return Phrase
     */
    protected function getSuccessMessage($collectionSize = 0)
    {
        return $collectionSize
            ? __('A total of %1 record(s) have been deleted.', $collectionSize)
            : __('No records have been deleted.');
    }
}
