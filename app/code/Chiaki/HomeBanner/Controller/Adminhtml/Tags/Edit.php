<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Controller\Adminhtml\Tags;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class Edit extends Tags
{
    const CURRENT_BANNER_TAG = 'current_banner_tag';

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $id    = (int)$this->getRequest()->getParam('id');
        $model = $this->getTagRepository()->getTagModel();

        if ($id) {
            try {
                $model = $this->getTagRepository()->getById($id);
            } catch (Exception $e) {
                $this->getMessageManager()->addErrorMessage($e->getMessage());
                $this->_redirect('*/*');

                return;
            }
        }

        // set entered data if was error when we do save
        $data = $this->_getSession()->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->getRegistry()->register(self::CURRENT_BANNER_TAG, $model);
        $this->initAction();
        if ($model->getId()) {
            $title = __('Edit Tag `%1`', $model->getName());
        } else {
            $title = __("Add New Tag");
        }
        $this->_view->getPage()->getConfig()->getTitle()->prepend($title);
        $this->_view->renderLayout();
    }

    /**
     * @return $this
     */
    private function initAction()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu('Chiaki_HomeBanner::manage_tags')->_addBreadcrumb(
            __('Manage Tags'),
            __('Manage Tags')
        );

        return $this;
    }
}
