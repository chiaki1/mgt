<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Controller\Adminhtml\Tags;
use Exception;
use Magento\Backend\Model\View\Result\Redirect;

/**
 * Class Delete
 */
class Delete extends Tags
{
    /**
     * @return Redirect
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $id = (int)$this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->getTagRepository()->deleteById($id);
                $this->getMessageManager()->addSuccessMessage(__('You deleted tag.'));

                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->getMessageManager()->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->getMessageManager()->addErrorMessage(__('We can\'t find a tag to delete.'));

        return $resultRedirect->setPath('*/*/');
    }
}
