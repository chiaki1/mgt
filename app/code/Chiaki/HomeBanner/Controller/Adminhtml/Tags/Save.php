<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Api\TagRepositoryInterface;
use Chiaki\HomeBanner\Controller\Adminhtml\Tags;
use Chiaki\HomeBanner\Model\Tag;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Zend_Filter_Input;

class Save extends Tags
{

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            $data = $this->getRequest()->getPostValue();
            $id   = (int)$this->getRequest()->getParam('tag_id');
            try {
                $inputFilter = new Zend_Filter_Input([], [], $data);
                $data        = $inputFilter->getUnescaped();
                if ($id) {
                    $model = $this->getTagRepository()->getById($id);
                    $data  = $this->retrieveItemContent($data, $model);
                } else {
                    $model = $this->getTagRepository()->getTagModel();
                }

                $model->addData($data);

                $this->_getSession()->setPageData($model->getData());
                $this->getTagRepository()->save($model);
                $this->getMessageManager()->addSuccessMessage(__('You saved the item.'));
                $this->_getSession()->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', [
                                                   'id' => $model->getId()
                                               ]
                    );

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (NoSuchEntityException $e) {
                $this->getMessageManager()->addErrorMessage($e->getMessage());
                $this->getDataPersistor()->set(Tag::PERSISTENT_NAME, $data);
                $this->_redirect('*/*/edit', [
                                               'id' => $model->getId()
                                           ]
                );

                return;
            } catch (Exception $e) {
                $this->getMessageManager()->addErrorMessage(
                    __('Something went wrong while saving the item data. Please review the error log.')
                );
                $this->getLogger()->critical($e);
                $this->_getSession()->setPageData($data);
                $this->_redirect('*/*/edit', ['id' => $id]);

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    protected function getRepository(): TagRepositoryInterface
    {
        return $this->getTagRepository();
    }
}
