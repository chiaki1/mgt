<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\Layout;
use Magento\Framework\View\Result\LayoutFactory;

/**
 * Class
 */
class TaggedGrid extends Action
{
    /**
     * @var LayoutFactory
     */
    private $resultLayoutFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;

    /**
     * TaggedGrid constructor.
     *
     * @param Action\Context            $context
     * @param LayoutFactory             $resultLayoutFactory
     * @param Registry                  $registry
     * @param BannerRepositoryInterface $bannerRepository
     */
    public function __construct(
        Action\Context $context,
        LayoutFactory $resultLayoutFactory,
        Registry $registry,
        BannerRepositoryInterface $bannerRepository
    ) {
        parent::__construct($context);

        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->coreRegistry        = $registry;
        $this->bannerRepository    = $bannerRepository;
    }

    /**
     * @return ResultInterface|Layout
     */
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $tagId        = (int)$this->_request->getParam('id');
        if ($tagId) {
            $bannerCollection = $this->bannerRepository->getTaggedBanners($tagId);
            $this->coreRegistry->register('current_banners', $bannerCollection);
        }

        return $resultLayout;
    }
}
