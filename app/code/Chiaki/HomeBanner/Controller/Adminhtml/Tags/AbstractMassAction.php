<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Model\Repository\TagRepository;
use Chiaki\HomeBanner\Model\ResourceModel\Tag\Collection;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractMassAction
 */
abstract class AbstractMassAction extends \Chiaki\HomeBanner\Controller\Adminhtml\AbstractMassAction
{

    /**
     * @var TagRepository
     */
    private $repository;

    /**
     * AbstractMassAction constructor.
     *
     * @param Action\Context  $context
     * @param Filter          $filter
     * @param LoggerInterface $logger
     * @param TagRepository   $repository
     */
    public function __construct(
        Action\Context $context,
        Filter $filter,
        LoggerInterface $logger,
        TagRepository $repository
    ) {
        parent::__construct($context, $filter, $logger);
        $this->repository = $repository;
    }

    /**
     * @return TagRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->repository->getTagCollection();
    }
}
