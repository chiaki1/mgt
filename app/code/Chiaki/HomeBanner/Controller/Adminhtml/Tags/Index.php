<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Controller\Adminhtml\Tags;
use Magento\Backend\Model\View\Result\Page;

/**
 * Class Index
 */
class Index extends Tags
{
    /**
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->getPageFactory()->create();
        $resultPage->setActiveMenu('Chiaki_HomeBanner::manage_tags');
        $resultPage->getConfig()->getTitle()->prepend(__('Tags'));
        $resultPage->addBreadcrumb(__('Tags'), __('Tags'));

        return $resultPage;
    }
}
