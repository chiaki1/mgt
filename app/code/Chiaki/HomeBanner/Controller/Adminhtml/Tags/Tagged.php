<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\LayoutFactory;

/**
 * Class
 */
class Tagged extends Action
{
    /**
     * @var LayoutFactory
     */
    private $resultLayoutFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;

    public function __construct(
        Action\Context $context,
        LayoutFactory $resultLayoutFactory,
        Registry $registry,
        BannerRepositoryInterface $bannerRepository
    ) {
        parent::__construct($context);

        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->coreRegistry        = $registry;
        $this->bannerRepository    = $bannerRepository;
    }

    public function execute()
    {
        $tagId = (int)$this->_request->getParam('id');
        if ($tagId) {
            $bannerCollection = $this->bannerRepository->getTaggedBanners($tagId);
            $this->coreRegistry->register('current_banners', $bannerCollection);
        }

        return $this->resultLayoutFactory->create();
    }
}
