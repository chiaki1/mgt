<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml\Tags;

use Chiaki\HomeBanner\Controller\Adminhtml\Banner;

/**
 * Class
 */
class NewAction extends Banner
{
    public function execute()
    {
        $this->_forward('edit');
    }
}
