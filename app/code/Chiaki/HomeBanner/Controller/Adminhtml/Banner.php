<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml;

use Chiaki\HomeBanner\Api\BannerRepositoryInterface;
use Chiaki\HomeBanner\Model\bannerRegistry;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

abstract class Banner extends Action
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var BannerRepositoryInterface
     */
    private $bannerRepository;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var BannerRegistry
     */
    private $bannerRegistry;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        BannerRepositoryInterface $bannerRepository,
        DataPersistorInterface $dataPersistor,
        BannerRegistry $bannerRegistry,
        TimezoneInterface $timezone,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->bannerRepository  = $bannerRepository;
        $this->dataPersistor     = $dataPersistor;
        $this->timezone          = $timezone;
        $this->logger            = $logger;
        $this->bannerRegistry    = $bannerRegistry;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return DataPersistorInterface
     */
    public function getDataPersistor()
    {
        return $this->dataPersistor;
    }

    /**
     * @return BannerRegistry
     */
    public function getRegistry()
    {
        return $this->bannerRegistry;
    }

    /**
     * @return BannerRepositoryInterface
     */
    public function getBannerRepository()
    {
        return $this->bannerRepository;
    }

    /**
     * @return PageFactory
     */
    public function getPageFactory()
    {
        return $this->resultPageFactory;
    }

    /**
     * @return TimezoneInterface
     */
    public function getTimezone()
    {
        return $this->timezone;
    }
}
