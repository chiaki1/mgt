<?php

namespace Chiaki\HomeBanner\Controller\Adminhtml;

use Chiaki\HomeBanner\Api\TagRepositoryInterface;
use Chiaki\HomeBanner\Model\BannerRegistry;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;

abstract class Tags extends Action
{
    /**
     * @var TagRepositoryInterface
     */
    private $tagRepository;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var BannerRegistry
     */
    private $bannerRegistry;

    /**
     * Tags constructor.
     *
     * @param Context                $context
     * @param PageFactory            $resultPageFactory
     * @param TagRepositoryInterface $tagRepository
     * @param DataPersistorInterface $dataPersistor
     * @param LoggerInterface        $logger
     * @param BannerRegistry         $bannerRegistry
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        TagRepositoryInterface $tagRepository,
        DataPersistorInterface $dataPersistor,
        LoggerInterface $logger,
        BannerRegistry $bannerRegistry
    ) {
        parent::__construct($context);
        $this->tagRepository     = $tagRepository;
        $this->dataPersistor     = $dataPersistor;
        $this->logger            = $logger;
        $this->resultPageFactory = $resultPageFactory;
        $this->bannerRegistry    = $bannerRegistry;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return DataPersistorInterface
     */
    public function getDataPersistor()
    {
        return $this->dataPersistor;
    }

    /**
     * @return BannerRegistry
     */
    public function getRegistry()
    {
        return $this->bannerRegistry;
    }

    /**
     * @return TagRepositoryInterface
     */
    public function getTagRepository()
    {
        return $this->tagRepository;
    }

    /**
     * @return PageFactory
     */
    public function getPageFactory()
    {
        return $this->resultPageFactory;
    }
}
