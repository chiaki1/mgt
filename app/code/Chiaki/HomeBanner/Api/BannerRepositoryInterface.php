<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Api;

use Chiaki\HomeBanner\Api\Data\BannerInterface;
use Chiaki\HomeBanner\Model\ResourceModel\Banner\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface BannerRepositoryInterface
{

    /**
     * Save Banner
     *
     * @param BannerInterface $banner
     *
     * @return BannerInterface
     * @throws LocalizedException
     */
    public function save(
        BannerInterface $banner
    );

    /**
     * Retrieve Banner
     *
     * @param string $bannerId
     *
     * @return BannerInterface
     * @throws LocalizedException
     */
    public function get($bannerId);

    /**
     * Retrieve Banner
     *
     * @param string $bannerId
     *
     * @return BannerInterface
     * @throws LocalizedException
     */
    public function getById($bannerId);

    /**
     * Delete Banner
     *
     * @param BannerInterface $banner
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        BannerInterface $banner
    );

    /**
     * Delete Banner by ID
     *
     * @param string $bannerId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($bannerId);

    /**
     * @return BannerInterface
     */
    public function getBanner();

    /**
     * @param int $tagId
     *
     * @return Collection
     */
    public function getTaggedBanners($tagId);
}

