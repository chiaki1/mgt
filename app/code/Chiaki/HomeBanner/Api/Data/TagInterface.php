<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Api\Data;

interface TagInterface
{

    const NAME   = 'name';
    const TAG_ID = 'tag_id';

    /**
     * Get tag_id
     *
     * @return string|null
     */
    public function getTagId();

    /**
     * Set tag_id
     *
     * @param string $tagId
     *
     * @return TagInterface
     */
    public function setTagId($tagId);

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TagInterface
     */
    public function setName($name);
}

