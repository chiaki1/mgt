<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Api\Data;

interface BannerInterface
{

    const DATA_FILTERS    = 'data_filters';
    const BANNER_TYPE     = 'banner_type';
    const CREATED_AT      = 'created_at';
    const BANNER_ID       = 'banner_id';
    const STATUS          = 'status';
    const TITLE           = 'title';
    const BANNER_VIDEO    = 'banner_video';
    const BANNER_IMAGE    = 'banner_image';
    const BANNER_POSITION = 'banner_position';
    const TAGS            = 'tags';
    const TAG_IDS         = 'tag_ids';

    /**
     * Get banner_id
     *
     * @return string|null
     */
    public function getBannerId();

    /**
     * Set banner_id
     *
     * @param string $bannerId
     *
     * @return BannerInterface
     */
    public function setBannerId($bannerId);

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param string $status
     *
     * @return BannerInterface
     */
    public function setStatus($status);

    /**
     * Get banner_type
     *
     * @return string|null
     */
    public function getBannerType();

    /**
     * Set banner_type
     *
     * @param string $bannerType
     *
     * @return BannerInterface
     */
    public function setBannerType($bannerType);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BannerInterface
     */
    public function setTitle($title);

    /**
     * Get banner_position
     *
     * @return string|null
     */
    public function getBannerPosition();

    /**
     * Set banner_position
     *
     * @param string $bannerPosition
     *
     * @return BannerInterface
     */
    public function setBannerPosition($bannerPosition);

    /**
     * Get banner_image
     *
     * @return string|null
     */
    public function getBannerImage();

    /**
     * Set banner_image
     *
     * @param string $bannerImage
     *
     * @return BannerInterface
     */
    public function setBannerImage($bannerImage);

    /**
     * Get banner_video
     *
     * @return string|null
     */
    public function getBannerVideo();

    /**
     * Set banner_video
     *
     * @param string $bannerVideo
     *
     * @return BannerInterface
     */
    public function setBannerVideo($bannerVideo);

    /**
     * Get data_filters
     *
     * @return string|null
     */
    public function getDataFilters();

    /**
     * Set data_filters
     *
     * @param string $dataFilters
     *
     * @return BannerInterface
     */
    public function setDataFilters($dataFilters);

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     *
     * @param string $createdAt
     *
     * @return BannerInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * @return array
     */
    public function getTagIds();

    /**
     * @return string
     */
    public function getTags();
}

