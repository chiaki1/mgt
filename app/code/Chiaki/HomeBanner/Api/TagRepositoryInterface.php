<?php

declare(strict_types=1);

namespace Chiaki\HomeBanner\Api;

use Chiaki\HomeBanner\Api\Data\TagInterface;
use Chiaki\HomeBanner\Model\ResourceModel\Tag\Collection;
use Chiaki\HomeBanner\Model\Tag;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface TagRepositoryInterface
{

    /**
     * Save Tag
     *
     * @param TagInterface $tag
     *
     * @return TagInterface
     * @throws LocalizedException
     */
    public function save(
        TagInterface $tag
    );

    /**
     * Retrieve Tag
     *
     * @param string $tagId
     *
     * @return TagInterface
     * @throws LocalizedException
     */
    public function get($tagId);

    /**
     * Delete Tag
     *
     * @param TagInterface $tag
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        TagInterface $tag
    );

    /**
     * Delete Tag by ID
     *
     * @param string $tagId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($tagId);

    /**
     * Lists
     *
     * @param string[] $tags
     *
     * @return Collection
     */
    public function getList($tags);

    /**
     * @return Tag
     */
    public function getTagModel();

    /**
     * @return Collection
     */
    public function getTagCollection();

    /**
     * @param $bannerId
     *
     * @return Collection
     */
    public function getTagsByBanner($bannerId);

    /**
     * @param array $tagsIds
     *
     * @return Collection
     */
    public function getTagsByIds($tagsIds = []);

    /**
     * @return DataObject[]
     */
    public function getAllTags();
}

