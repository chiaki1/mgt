<?php


namespace Chiaki\Address\Plugin;


class WhenSaveCustomerAddress
{
    const CUSTOM_ADDRESS_FIELDS= [
        'iz_address_province',
        'iz_address_district',
        'iz_address_ward'
    ];

    public function afterExportCustomerAddress(\Magento\Quote\Model\Quote\Address $subject, $result){
        /*
         * attach custom address fields to customer address
         * Because mgt get data from extension_attribute but i don't want to use this way.
         * So will attach directly to customer address data
         * */

        foreach (WhenSaveCustomerAddress::CUSTOM_ADDRESS_FIELDS as $CUSTOM_ADDRESS_FIELD) {
            if($subject->getData($CUSTOM_ADDRESS_FIELD)){
                /** @var \Magento\Framework\Api\AttributeValue $customAttr */
                $customAttr = $result->getCustomAttribute($CUSTOM_ADDRESS_FIELD);
                $customAttr->setValue($subject->getData($CUSTOM_ADDRESS_FIELD));
            }
        }

        return $result;
    }

    protected function attachToCustomerAddress(){

    }
}
