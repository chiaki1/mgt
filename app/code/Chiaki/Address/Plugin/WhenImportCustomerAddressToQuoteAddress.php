<?php


namespace Chiaki\Address\Plugin;


use Magento\Customer\Api\Data\AddressInterface;

class WhenImportCustomerAddressToQuoteAddress
{
    public function aroundImportCustomerAddressData(\Magento\Quote\Model\Quote\Address $subject, callable $proceed, AddressInterface $address)
    {
        $result = $proceed($address);
        foreach (\Chiaki\Address\Plugin\WhenSaveCustomerAddress::CUSTOM_ADDRESS_FIELDS as $CUSTOM_ADDRESS_FIELD) {
            /** @var \Magento\Framework\Api\AttributeValue $customAttr */
            $customAttr = $address->getCustomAttribute($CUSTOM_ADDRESS_FIELD);

            if($customAttr && $customAttr->getValue()){
                $subject->setData($CUSTOM_ADDRESS_FIELD, $customAttr->getValue());
            }
        }

        return $result;
    }
}
