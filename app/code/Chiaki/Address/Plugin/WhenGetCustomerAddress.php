<?php


namespace Chiaki\Address\Plugin;


class WhenGetCustomerAddress
{
    public function afterExecute(\Magento\CustomerGraphQl\Model\Customer\Address\GetCustomerAddress $subject, $customerAddress)
    {
        foreach (\Chiaki\Address\Plugin\WhenSaveCustomerAddress::CUSTOM_ADDRESS_FIELDS as $CUSTOM_ADDRESS_FIELD) {
            /** @var \Magento\Framework\Api\AttributeValue $customAttr */
            $customAttr = $customerAddress->getCustomAttribute($CUSTOM_ADDRESS_FIELD);

            if($customAttr && $customAttr->getValue()){
                $customerAddress->setData($CUSTOM_ADDRESS_FIELD, $customAttr->getValue());
            }
        }

        return $customerAddress;
    }
}
