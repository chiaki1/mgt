<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Console\Command;

use Chiaki\Address\Helper\SyncTikiAddress;
use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SyncVnAddressFields extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        /** @var SyncTikiAddress $syncTikiHelper */
        $syncTikiHelper =  ObjectManager::getInstance()->create(SyncTikiAddress::class);
        $syncTikiHelper->sync($output);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("chiaki:sync-address");
        $this->setDescription("Sync VietNam address fields data");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

