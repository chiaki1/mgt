#### VietNam address bao gồm các fields:

- **fistname**: Sử dụng default của magento
- **middlename**: Sử dụng default của magento
- **lastname**: Sử dụng default của magento

- **country_id**: Lấy default của magento. Thực ra sẽ không hiển thị country lên fe bởi vì luôn fix là **vn**. Có 1 chỗ có thể sau sẽ tìm hiểu đó là làm sao để magento hiển thị tên country. Thì đó là nó dùng class **RegionBundle** của php. Xem trong này: \Magento\Directory\Model\ResourceModel\Country\Collection::getSort
- **iz_address_province**: Là tỉnh hoặc thành phố.
  https://tiki.vn/api/v2/directory/regions?country_id=vn

- **iz_address_district**: Đây là custom field. Lưu quận huyện.
  https://tiki.vn/api/v2/directory/districts?region_id=297

- **iz_address_ward**: Đây là custom field. Lưu phường xã.
  https://tiki.vn/api/v2/directory/wards?district_id=10

- **street**: Sử dụng default của magento. Lưu địa chỉ
- **telephone**: Sử dụng default của magento
