<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model;

use Chiaki\Address\Api\Data\IzAddressWardInterfaceFactory;
use Chiaki\Address\Api\Data\IzAddressWardSearchResultsInterfaceFactory;
use Chiaki\Address\Api\IzAddressWardRepositoryInterface;
use Chiaki\Address\Model\ResourceModel\IzAddressWard as ResourceIzAddressWard;
use Chiaki\Address\Model\ResourceModel\IzAddressWard\CollectionFactory as IzAddressWardCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class IzAddressWardRepository implements IzAddressWardRepositoryInterface
{

    protected $izAddressWardFactory;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $izAddressWardCollectionFactory;

    private $storeManager;

    protected $dataIzAddressWardFactory;


    /**
     * @param ResourceIzAddressWard $resource
     * @param IzAddressWardFactory $izAddressWardFactory
     * @param IzAddressWardInterfaceFactory $dataIzAddressWardFactory
     * @param IzAddressWardCollectionFactory $izAddressWardCollectionFactory
     * @param IzAddressWardSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceIzAddressWard $resource,
        IzAddressWardFactory $izAddressWardFactory,
        IzAddressWardInterfaceFactory $dataIzAddressWardFactory,
        IzAddressWardCollectionFactory $izAddressWardCollectionFactory,
        IzAddressWardSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->izAddressWardFactory = $izAddressWardFactory;
        $this->izAddressWardCollectionFactory = $izAddressWardCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataIzAddressWardFactory = $dataIzAddressWardFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Chiaki\Address\Api\Data\IzAddressWardInterface $izAddressWard
    ) {
        /* if (empty($izAddressWard->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $izAddressWard->setStoreId($storeId);
        } */

        $izAddressWardData = $this->extensibleDataObjectConverter->toNestedArray(
            $izAddressWard,
            [],
            \Chiaki\Address\Api\Data\IzAddressWardInterface::class
        );

        $izAddressWardModel = $this->izAddressWardFactory->create()->setData($izAddressWardData);

        try {
            $this->resource->save($izAddressWardModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the izAddressWard: %1',
                $exception->getMessage()
            ));
        }
        return $izAddressWardModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($izAddressWardId)
    {
        $izAddressWard = $this->izAddressWardFactory->create();
        $this->resource->load($izAddressWard, $izAddressWardId);
        if (!$izAddressWard->getId()) {
            throw new NoSuchEntityException(__('IzAddressWard with id "%1" does not exist.', $izAddressWardId));
        }
        return $izAddressWard->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->izAddressWardCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Chiaki\Address\Api\Data\IzAddressWardInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Chiaki\Address\Api\Data\IzAddressWardInterface $izAddressWard
    ) {
        try {
            $izAddressWardModel = $this->izAddressWardFactory->create();
            $this->resource->load($izAddressWardModel, $izAddressWard->getIzaddresswardId());
            $this->resource->delete($izAddressWardModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the IzAddressWard: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($izAddressWardId)
    {
        return $this->delete($this->get($izAddressWardId));
    }
}

