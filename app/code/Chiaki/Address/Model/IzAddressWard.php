<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model;

use Chiaki\Address\Api\Data\IzAddressWardInterface;
use Chiaki\Address\Api\Data\IzAddressWardInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class IzAddressWard extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'iz_address_izaddressward';
    protected $izaddresswardDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param IzAddressWardInterfaceFactory $izaddresswardDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Chiaki\Address\Model\ResourceModel\IzAddressWard $resource
     * @param \Chiaki\Address\Model\ResourceModel\IzAddressWard\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        IzAddressWardInterfaceFactory $izaddresswardDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Chiaki\Address\Model\ResourceModel\IzAddressWard $resource,
        \Chiaki\Address\Model\ResourceModel\IzAddressWard\Collection $resourceCollection,
        array $data = []
    ) {
        $this->izaddresswardDataFactory = $izaddresswardDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve izaddressward model with izaddressward data
     * @return IzAddressWardInterface
     */
    public function getDataModel()
    {
        $izaddresswardData = $this->getData();

        $izaddresswardDataObject = $this->izaddresswardDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $izaddresswardDataObject,
            $izaddresswardData,
            IzAddressWardInterface::class
        );

        return $izaddresswardDataObject;
    }
}

