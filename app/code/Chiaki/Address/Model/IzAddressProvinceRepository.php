<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model;

use Chiaki\Address\Api\Data\IzAddressProvinceInterfaceFactory;
use Chiaki\Address\Api\Data\IzAddressProvinceSearchResultsInterfaceFactory;
use Chiaki\Address\Api\IzAddressProvinceRepositoryInterface;
use Chiaki\Address\Model\ResourceModel\IzAddressProvince as ResourceIzAddressProvince;
use Chiaki\Address\Model\ResourceModel\IzAddressProvince\CollectionFactory as IzAddressProvinceCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class IzAddressProvinceRepository implements IzAddressProvinceRepositoryInterface
{

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $izAddressProvinceFactory;

    protected $izAddressProvinceCollectionFactory;

    private $storeManager;

    protected $dataIzAddressProvinceFactory;


    /**
     * @param ResourceIzAddressProvince $resource
     * @param IzAddressProvinceFactory $izAddressProvinceFactory
     * @param IzAddressProvinceInterfaceFactory $dataIzAddressProvinceFactory
     * @param IzAddressProvinceCollectionFactory $izAddressProvinceCollectionFactory
     * @param IzAddressProvinceSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceIzAddressProvince $resource,
        IzAddressProvinceFactory $izAddressProvinceFactory,
        IzAddressProvinceInterfaceFactory $dataIzAddressProvinceFactory,
        IzAddressProvinceCollectionFactory $izAddressProvinceCollectionFactory,
        IzAddressProvinceSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->izAddressProvinceFactory = $izAddressProvinceFactory;
        $this->izAddressProvinceCollectionFactory = $izAddressProvinceCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataIzAddressProvinceFactory = $dataIzAddressProvinceFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Chiaki\Address\Api\Data\IzAddressProvinceInterface $izAddressProvince
    ) {
        /* if (empty($izAddressProvince->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $izAddressProvince->setStoreId($storeId);
        } */

        $izAddressProvinceData = $this->extensibleDataObjectConverter->toNestedArray(
            $izAddressProvince,
            [],
            \Chiaki\Address\Api\Data\IzAddressProvinceInterface::class
        );

        $izAddressProvinceModel = $this->izAddressProvinceFactory->create()->setData($izAddressProvinceData);

        try {
            $this->resource->save($izAddressProvinceModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the izAddressProvince: %1',
                $exception->getMessage()
            ));
        }
        return $izAddressProvinceModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($izAddressProvinceId)
    {
        $izAddressProvince = $this->izAddressProvinceFactory->create();
        $this->resource->load($izAddressProvince, $izAddressProvinceId);
        if (!$izAddressProvince->getId()) {
            throw new NoSuchEntityException(__('IzAddressProvince with id "%1" does not exist.', $izAddressProvinceId));
        }
        return $izAddressProvince->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->izAddressProvinceCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Chiaki\Address\Api\Data\IzAddressProvinceInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Chiaki\Address\Api\Data\IzAddressProvinceInterface $izAddressProvince
    ) {
        try {
            $izAddressProvinceModel = $this->izAddressProvinceFactory->create();
            $this->resource->load($izAddressProvinceModel, $izAddressProvince->getIzaddressprovinceId());
            $this->resource->delete($izAddressProvinceModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the IzAddressProvince: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($izAddressProvinceId)
    {
        return $this->delete($this->get($izAddressProvinceId));
    }
}

