<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model\Data;

use Chiaki\Address\Api\Data\IzAddressWardInterface;

class IzAddressWard extends \Magento\Framework\Api\AbstractExtensibleObject implements IzAddressWardInterface
{

    /**
     * Get izaddressward_id
     * @return string|null
     */
    public function getIzaddresswardId()
    {
        return $this->_get(self::IZADDRESSWARD_ID);
    }

    /**
     * Set izaddressward_id
     * @param string $izaddresswardId
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setIzaddresswardId($izaddresswardId)
    {
        return $this->setData(self::IZADDRESSWARD_ID, $izaddresswardId);
    }

    /**
     * Get district_id
     * @return string|null
     */
    public function getDistrictId()
    {
        return $this->_get(self::DISTRICT_ID);
    }

    /**
     * Set district_id
     * @param string $districtId
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setDistrictId($districtId)
    {
        return $this->setData(self::DISTRICT_ID, $districtId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\Address\Api\Data\IzAddressWardExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Chiaki\Address\Api\Data\IzAddressWardExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\Address\Api\Data\IzAddressWardExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
}

