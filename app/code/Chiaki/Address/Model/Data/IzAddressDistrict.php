<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model\Data;

use Chiaki\Address\Api\Data\IzAddressDistrictInterface;

class IzAddressDistrict extends \Magento\Framework\Api\AbstractExtensibleObject implements IzAddressDistrictInterface
{

    /**
     * Get izaddressdistrict_id
     * @return string|null
     */
    public function getIzaddressdistrictId()
    {
        return $this->_get(self::IZADDRESSDISTRICT_ID);
    }

    /**
     * Set izaddressdistrict_id
     * @param string $izaddressdistrictId
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setIzaddressdistrictId($izaddressdistrictId)
    {
        return $this->setData(self::IZADDRESSDISTRICT_ID, $izaddressdistrictId);
    }

    /**
     * Get province_id
     * @return string|null
     */
    public function getProvinceId()
    {
        return $this->_get(self::PROVINCE_ID);
    }

    /**
     * Set province_id
     * @param string $provinceId
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setProvinceId($provinceId)
    {
        return $this->setData(self::PROVINCE_ID, $provinceId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Chiaki\Address\Api\Data\IzAddressDistrictExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\Address\Api\Data\IzAddressDistrictExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
}

