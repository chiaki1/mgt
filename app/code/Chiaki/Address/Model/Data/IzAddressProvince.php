<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model\Data;

use Chiaki\Address\Api\Data\IzAddressProvinceInterface;

class IzAddressProvince extends \Magento\Framework\Api\AbstractExtensibleObject implements IzAddressProvinceInterface
{

    /**
     * Get izaddressprovince_id
     * @return string|null
     */
    public function getIzaddressprovinceId()
    {
        return $this->_get(self::IZADDRESSPROVINCE_ID);
    }

    /**
     * Set izaddressprovince_id
     * @param string $izaddressprovinceId
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setIzaddressprovinceId($izaddressprovinceId)
    {
        return $this->setData(self::IZADDRESSPROVINCE_ID, $izaddressprovinceId);
    }

    /**
     * Get country_id
     * @return string|null
     */
    public function getCountryId()
    {
        return $this->_get(self::COUNTRY_ID);
    }

    /**
     * Set country_id
     * @param string $countryId
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setCountryId($countryId)
    {
        return $this->setData(self::COUNTRY_ID, $countryId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Chiaki\Address\Api\Data\IzAddressProvinceExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\Address\Api\Data\IzAddressProvinceExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
}

