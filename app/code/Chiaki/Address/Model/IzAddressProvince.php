<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model;

use Chiaki\Address\Api\Data\IzAddressProvinceInterface;
use Chiaki\Address\Api\Data\IzAddressProvinceInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class IzAddressProvince extends \Magento\Framework\Model\AbstractModel
{

    protected $izaddressprovinceDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'iz_address_izaddressprovince';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param IzAddressProvinceInterfaceFactory $izaddressprovinceDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Chiaki\Address\Model\ResourceModel\IzAddressProvince $resource
     * @param \Chiaki\Address\Model\ResourceModel\IzAddressProvince\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        IzAddressProvinceInterfaceFactory $izaddressprovinceDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Chiaki\Address\Model\ResourceModel\IzAddressProvince $resource,
        \Chiaki\Address\Model\ResourceModel\IzAddressProvince\Collection $resourceCollection,
        array $data = []
    ) {
        $this->izaddressprovinceDataFactory = $izaddressprovinceDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve izaddressprovince model with izaddressprovince data
     * @return IzAddressProvinceInterface
     */
    public function getDataModel()
    {
        $izaddressprovinceData = $this->getData();

        $izaddressprovinceDataObject = $this->izaddressprovinceDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $izaddressprovinceDataObject,
            $izaddressprovinceData,
            IzAddressProvinceInterface::class
        );

        return $izaddressprovinceDataObject;
    }
}

