<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model;

use Chiaki\Address\Api\Data\IzAddressDistrictInterface;
use Chiaki\Address\Api\Data\IzAddressDistrictInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class IzAddressDistrict extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $izaddressdistrictDataFactory;

    protected $_eventPrefix = 'iz_address_izaddressdistrict';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param IzAddressDistrictInterfaceFactory $izaddressdistrictDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Chiaki\Address\Model\ResourceModel\IzAddressDistrict $resource
     * @param \Chiaki\Address\Model\ResourceModel\IzAddressDistrict\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        IzAddressDistrictInterfaceFactory $izaddressdistrictDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Chiaki\Address\Model\ResourceModel\IzAddressDistrict $resource,
        \Chiaki\Address\Model\ResourceModel\IzAddressDistrict\Collection $resourceCollection,
        array $data = []
    ) {
        $this->izaddressdistrictDataFactory = $izaddressdistrictDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve izaddressdistrict model with izaddressdistrict data
     * @return IzAddressDistrictInterface
     */
    public function getDataModel()
    {
        $izaddressdistrictData = $this->getData();

        $izaddressdistrictDataObject = $this->izaddressdistrictDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $izaddressdistrictDataObject,
            $izaddressdistrictData,
            IzAddressDistrictInterface::class
        );

        return $izaddressdistrictDataObject;
    }
}

