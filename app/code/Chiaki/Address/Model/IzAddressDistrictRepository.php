<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model;

use Chiaki\Address\Api\Data\IzAddressDistrictInterfaceFactory;
use Chiaki\Address\Api\Data\IzAddressDistrictSearchResultsInterfaceFactory;
use Chiaki\Address\Api\IzAddressDistrictRepositoryInterface;
use Chiaki\Address\Model\ResourceModel\IzAddressDistrict as ResourceIzAddressDistrict;
use Chiaki\Address\Model\ResourceModel\IzAddressDistrict\CollectionFactory as IzAddressDistrictCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class IzAddressDistrictRepository implements IzAddressDistrictRepositoryInterface
{

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $izAddressDistrictFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    private $storeManager;

    protected $dataIzAddressDistrictFactory;

    protected $izAddressDistrictCollectionFactory;


    /**
     * @param ResourceIzAddressDistrict $resource
     * @param IzAddressDistrictFactory $izAddressDistrictFactory
     * @param IzAddressDistrictInterfaceFactory $dataIzAddressDistrictFactory
     * @param IzAddressDistrictCollectionFactory $izAddressDistrictCollectionFactory
     * @param IzAddressDistrictSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceIzAddressDistrict $resource,
        IzAddressDistrictFactory $izAddressDistrictFactory,
        IzAddressDistrictInterfaceFactory $dataIzAddressDistrictFactory,
        IzAddressDistrictCollectionFactory $izAddressDistrictCollectionFactory,
        IzAddressDistrictSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->izAddressDistrictFactory = $izAddressDistrictFactory;
        $this->izAddressDistrictCollectionFactory = $izAddressDistrictCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataIzAddressDistrictFactory = $dataIzAddressDistrictFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Chiaki\Address\Api\Data\IzAddressDistrictInterface $izAddressDistrict
    ) {
        /* if (empty($izAddressDistrict->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $izAddressDistrict->setStoreId($storeId);
        } */

        $izAddressDistrictData = $this->extensibleDataObjectConverter->toNestedArray(
            $izAddressDistrict,
            [],
            \Chiaki\Address\Api\Data\IzAddressDistrictInterface::class
        );

        $izAddressDistrictModel = $this->izAddressDistrictFactory->create()->setData($izAddressDistrictData);

        try {
            $this->resource->save($izAddressDistrictModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the izAddressDistrict: %1',
                $exception->getMessage()
            ));
        }
        return $izAddressDistrictModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($izAddressDistrictId)
    {
        $izAddressDistrict = $this->izAddressDistrictFactory->create();
        $this->resource->load($izAddressDistrict, $izAddressDistrictId);
        if (!$izAddressDistrict->getId()) {
            throw new NoSuchEntityException(__('IzAddressDistrict with id "%1" does not exist.', $izAddressDistrictId));
        }
        return $izAddressDistrict->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->izAddressDistrictCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Chiaki\Address\Api\Data\IzAddressDistrictInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Chiaki\Address\Api\Data\IzAddressDistrictInterface $izAddressDistrict
    ) {
        try {
            $izAddressDistrictModel = $this->izAddressDistrictFactory->create();
            $this->resource->load($izAddressDistrictModel, $izAddressDistrict->getIzaddressdistrictId());
            $this->resource->delete($izAddressDistrictModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the IzAddressDistrict: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($izAddressDistrictId)
    {
        return $this->delete($this->get($izAddressDistrictId));
    }
}

