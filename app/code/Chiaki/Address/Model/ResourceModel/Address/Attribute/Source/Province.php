<?php


namespace Chiaki\Address\Model\ResourceModel\Address\Attribute\Source;


use Magento\Framework\App\ObjectManager;

class Province extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @inheritdoc
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            /** @var \Chiaki\Address\Model\ResourceModel\IzAddressProvince\Collection $provinceCollection */
            $provinceCollection = ObjectManager::getInstance()->create(\Chiaki\Address\Model\ResourceModel\IzAddressProvince\Collection::class);
            $options = [];
            foreach ($provinceCollection as $item) {
                $options[] = [
                    'label' => $item['name'],
                    'value' => $item['name']
                ];
            }
            $this->_options = $options;
        }
        return $this->_options;
    }
}
