<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Model\ResourceModel\IzAddressDistrict;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'izaddressdistrict_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\Address\Model\IzAddressDistrict::class,
            \Chiaki\Address\Model\ResourceModel\IzAddressDistrict::class
        );
    }
}

