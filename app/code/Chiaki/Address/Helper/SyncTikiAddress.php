<?php


namespace Chiaki\Address\Helper;


use Chiaki\Address\Model\IzAddressDistrict;
use Chiaki\Address\Model\IzAddressWard;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;
use GuzzleHttp\Client;
use Symfony\Component\Console\Output\OutputInterface;

class SyncTikiAddress
{
    /**
     * @var ResourceConnection
     */
    private $resource;
    /**
     * @var Client
     */
    private $httpClient;
    /**
     * @var OutputInterface|null
     */
    private $output;

    public function __construct(
        ResourceConnection $resource,
        Client $client = null
    )
    {
        if ($client === null) {
            $client = new Client();
        }
        $this->resource = $resource;
        $this->httpClient = $client;
    }

    public function sync(OutputInterface $output = null)
    {
        try {
            $this->output = $output;
            $regions = $this->getRegionsFromTiki();
            if (empty($regions)) {
                $this->mess("Could not get regions data from tiki");
                return;
            } else {
                $this->mess("We got " . count($regions) . " provinces");

                $this->mess("Start clear old address data");
                $this->resource->getConnection()->truncateTable($this->resource->getTableName('iz_address_izaddressprovince'));
                $this->resource->getConnection()->truncateTable($this->resource->getTableName('iz_address_izaddressdistrict'));
                $this->resource->getConnection()->truncateTable($this->resource->getTableName('iz_address_izaddressward'));
                $this->mess("Clear old address data successfully");

                $this->mess("start import to database");
                foreach ($regions as $region) {
                    /** @var \Chiaki\Address\Model\IzAddressProvince $provinceModel */
                    $provinceModel = ObjectManager::getInstance()->create(\Chiaki\Address\Model\IzAddressProvince::class);
                    $provinceModel->addData(['country_id' => 'vn', 'name' => $region['name'], 'code' => $region['id'],
                    ])->save();
                    $districts = $this->getDistrictsFromTiki($region['id']);
                    if (empty($districts)) {
                        $this->mess('Could not get districts for ' . $region['name']);
                        continue;
                    }
                    $this->mess("We got " . count($districts) . " districts for ". $region['name']);
                    foreach ($districts as $district) {
                        /** @var IzAddressDistrict $districtModel */
                        $districtModel = ObjectManager::getInstance()->create(IzAddressDistrict::class);
                        $districtModel->addData(['province_id' => $provinceModel->getId(), 'code' => $district['id'], 'name' => $district['name']])
                            ->save();

                        $wards = $this->getWardFromTiki($district['id']);

                        if (empty($wards)) {
                            $this->mess('Could not get $wards for ' . $district['name']);
                            continue;
                        }
                        $this->mess("We got " . count($wards) . " wards for ". $district['name']);
                        foreach ($wards as $ward) {
                            /** @var IzAddressWard $wardModel */
                            $wardModel = ObjectManager::getInstance()->create(IzAddressWard::class);
                            $wardModel->addData(['district_id' => $districtModel->getId(), 'name' => $ward['name'], 'code' => $ward['id']])
                                ->save();
                        }
                    }
                }
                $this->mess('import successfully');
            }
        } catch (\Exception $e) {
            $this->mess('Could not sync');
            $this->mess($e->getMessage());
        }

    }

    protected function mess($mess)
    {
        if ($this->output) {
            $this->output->writeln($mess);
        }
    }

    protected function syncRegions()
    {
        try {
            $table = $this->resource->getTableName('directory_country_region');
        } catch (\Exception $e) {
        }
    }

    protected function getRegionsFromTiki()
    {
        $response = $this->httpClient->request('GET', "https://tiki.vn/api/v2/directory/regions?country_id=vn");

        try {
            return json_decode($response->getBody()->getContents(), true)['data'];
        } catch (\Exception $e) {
            return [];
        }
    }

    protected function getDistrictsFromTiki($regionId)
    {
        $response = $this->httpClient->request('GET', "https://tiki.vn/api/v2/directory/districts?region_id=" . $regionId);

        try {
            return json_decode($response->getBody()->getContents(), true)['data'];
        } catch (\Exception $e) {
            return [];
        }
    }

    protected function getWardFromTiki($districtId)
    {
        $response = $this->httpClient->request('GET', "https://tiki.vn/api/v2/directory/wards?district_id=" . $districtId);

        try {
            return json_decode($response->getBody()->getContents(), true)['data'];
        } catch (\Exception $e) {
            return [];
        }

    }
}
