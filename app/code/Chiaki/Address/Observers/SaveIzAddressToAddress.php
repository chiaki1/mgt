<?php


namespace Chiaki\Address\Observers;


use Magento\Framework\Event\Observer;

class SaveIzAddressToAddress implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        try {
            if ($quote->getBillingAddress()) {
                $order->getBillingAddress()->setData('iz_address_province',$quote->getBillingAddress()->getData('iz_address_province'));
                $order->getBillingAddress()->setData('iz_address_district',$quote->getBillingAddress()->getData('iz_address_district'));
                $order->getBillingAddress()->setData('iz_address_ward',$quote->getBillingAddress()->getData('iz_address_ward'));
            }

            if (!$quote->isVirtual()) {
                $order->getShippingAddress()->setData('iz_address_province',$quote->getShippingAddress()->getData('iz_address_province'));
                $order->getShippingAddress()->setData('iz_address_district',$quote->getShippingAddress()->getData('iz_address_district'));
                $order->getShippingAddress()->setData('iz_address_ward',$quote->getShippingAddress()->getData('iz_address_ward'));
            }
        } catch (\Exception $e) {
            // add logger
        }

    }
}
