<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Controller\Adminhtml\IzAddressDistrict;

class Edit extends \Chiaki\Address\Controller\Adminhtml\IzAddressDistrict
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('izaddressdistrict_id');
        $model = $this->_objectManager->create(\Chiaki\Address\Model\IzAddressDistrict::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Izaddressdistrict no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('iz_address_izaddressdistrict', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Izaddressdistrict') : __('New Izaddressdistrict'),
            $id ? __('Edit Izaddressdistrict') : __('New Izaddressdistrict')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Izaddressdistricts'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Izaddressdistrict %1', $model->getId()) : __('New Izaddressdistrict'));
        return $resultPage;
    }
}

