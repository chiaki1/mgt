<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Controller\Adminhtml\IzAddressProvince;

class Delete extends \Chiaki\Address\Controller\Adminhtml\IzAddressProvince
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('izaddressprovince_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Chiaki\Address\Model\IzAddressProvince::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Izaddressprovince.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['izaddressprovince_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Izaddressprovince to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

