<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface IzAddressDistrictRepositoryInterface
{

    /**
     * Save IzAddressDistrict
     * @param \Chiaki\Address\Api\Data\IzAddressDistrictInterface $izAddressDistrict
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Chiaki\Address\Api\Data\IzAddressDistrictInterface $izAddressDistrict
    );

    /**
     * Retrieve IzAddressDistrict
     * @param string $izaddressdistrictId
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($izaddressdistrictId);

    /**
     * Retrieve IzAddressDistrict matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete IzAddressDistrict
     * @param \Chiaki\Address\Api\Data\IzAddressDistrictInterface $izAddressDistrict
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Chiaki\Address\Api\Data\IzAddressDistrictInterface $izAddressDistrict
    );

    /**
     * Delete IzAddressDistrict by ID
     * @param string $izaddressdistrictId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($izaddressdistrictId);
}

