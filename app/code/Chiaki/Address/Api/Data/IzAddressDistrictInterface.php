<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api\Data;

interface IzAddressDistrictInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PROVINCE_ID = 'province_id';
    const CODE = 'code';
    const IZADDRESSDISTRICT_ID = 'izaddressdistrict_id';
    const NAME = 'name';

    /**
     * Get izaddressdistrict_id
     * @return string|null
     */
    public function getIzaddressdistrictId();

    /**
     * Set izaddressdistrict_id
     * @param string $izaddressdistrictId
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setIzaddressdistrictId($izaddressdistrictId);

    /**
     * Get province_id
     * @return string|null
     */
    public function getProvinceId();

    /**
     * Set province_id
     * @param string $provinceId
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setProvinceId($provinceId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Chiaki\Address\Api\Data\IzAddressDistrictExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\Address\Api\Data\IzAddressDistrictExtensionInterface $extensionAttributes
    );

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setCode($code);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface
     */
    public function setName($name);
}

