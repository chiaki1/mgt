<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api\Data;

interface IzAddressDistrictSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get IzAddressDistrict list.
     * @return \Chiaki\Address\Api\Data\IzAddressDistrictInterface[]
     */
    public function getItems();

    /**
     * Set province_id list.
     * @param \Chiaki\Address\Api\Data\IzAddressDistrictInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

