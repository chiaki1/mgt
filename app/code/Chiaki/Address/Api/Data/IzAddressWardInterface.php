<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api\Data;

interface IzAddressWardInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const IZADDRESSWARD_ID = 'izaddressward_id';
    const CODE = 'code';
    const DISTRICT_ID = 'district_id';
    const NAME = 'name';

    /**
     * Get izaddressward_id
     * @return string|null
     */
    public function getIzaddresswardId();

    /**
     * Set izaddressward_id
     * @param string $izaddresswardId
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setIzaddresswardId($izaddresswardId);

    /**
     * Get district_id
     * @return string|null
     */
    public function getDistrictId();

    /**
     * Set district_id
     * @param string $districtId
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setDistrictId($districtId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\Address\Api\Data\IzAddressWardExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Chiaki\Address\Api\Data\IzAddressWardExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\Address\Api\Data\IzAddressWardExtensionInterface $extensionAttributes
    );

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setCode($code);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     */
    public function setName($name);
}

