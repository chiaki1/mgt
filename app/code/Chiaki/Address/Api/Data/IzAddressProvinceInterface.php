<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api\Data;

interface IzAddressProvinceInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const COUNTRY_ID = 'country_id';
    const CODE = 'code';
    const IZADDRESSPROVINCE_ID = 'izaddressprovince_id';
    const NAME = 'name';

    /**
     * Get izaddressprovince_id
     * @return string|null
     */
    public function getIzaddressprovinceId();

    /**
     * Set izaddressprovince_id
     * @param string $izaddressprovinceId
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setIzaddressprovinceId($izaddressprovinceId);

    /**
     * Get country_id
     * @return string|null
     */
    public function getCountryId();

    /**
     * Set country_id
     * @param string $countryId
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setCountryId($countryId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Chiaki\Address\Api\Data\IzAddressProvinceExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Chiaki\Address\Api\Data\IzAddressProvinceExtensionInterface $extensionAttributes
    );

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setCode($code);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     */
    public function setName($name);
}

