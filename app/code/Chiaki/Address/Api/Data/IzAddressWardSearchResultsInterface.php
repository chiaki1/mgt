<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api\Data;

interface IzAddressWardSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get IzAddressWard list.
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface[]
     */
    public function getItems();

    /**
     * Set district_id list.
     * @param \Chiaki\Address\Api\Data\IzAddressWardInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

