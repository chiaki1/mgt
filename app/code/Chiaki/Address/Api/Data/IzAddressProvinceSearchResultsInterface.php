<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api\Data;

interface IzAddressProvinceSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get IzAddressProvince list.
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface[]
     */
    public function getItems();

    /**
     * Set country_id list.
     * @param \Chiaki\Address\Api\Data\IzAddressProvinceInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

