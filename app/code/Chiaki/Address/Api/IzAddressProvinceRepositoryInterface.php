<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface IzAddressProvinceRepositoryInterface
{

    /**
     * Save IzAddressProvince
     * @param \Chiaki\Address\Api\Data\IzAddressProvinceInterface $izAddressProvince
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Chiaki\Address\Api\Data\IzAddressProvinceInterface $izAddressProvince
    );

    /**
     * Retrieve IzAddressProvince
     * @param string $izaddressprovinceId
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($izaddressprovinceId);

    /**
     * Retrieve IzAddressProvince matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Chiaki\Address\Api\Data\IzAddressProvinceSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete IzAddressProvince
     * @param \Chiaki\Address\Api\Data\IzAddressProvinceInterface $izAddressProvince
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Chiaki\Address\Api\Data\IzAddressProvinceInterface $izAddressProvince
    );

    /**
     * Delete IzAddressProvince by ID
     * @param string $izaddressprovinceId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($izaddressprovinceId);
}

