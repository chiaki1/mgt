<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Address\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface IzAddressWardRepositoryInterface
{

    /**
     * Save IzAddressWard
     * @param \Chiaki\Address\Api\Data\IzAddressWardInterface $izAddressWard
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Chiaki\Address\Api\Data\IzAddressWardInterface $izAddressWard
    );

    /**
     * Retrieve IzAddressWard
     * @param string $izaddresswardId
     * @return \Chiaki\Address\Api\Data\IzAddressWardInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($izaddresswardId);

    /**
     * Retrieve IzAddressWard matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Chiaki\Address\Api\Data\IzAddressWardSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete IzAddressWard
     * @param \Chiaki\Address\Api\Data\IzAddressWardInterface $izAddressWard
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Chiaki\Address\Api\Data\IzAddressWardInterface $izAddressWard
    );

    /**
     * Delete IzAddressWard by ID
     * @param string $izaddresswardId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($izaddresswardId);
}

