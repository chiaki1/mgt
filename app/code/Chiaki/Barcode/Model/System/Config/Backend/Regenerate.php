<?php

namespace Chiaki\Barcode\Model\System\Config\Backend;

use Magento\Framework\App\Config\Value;

/**
 * Class Regenerate
 *
 * @package Chiaki\Barcode\Model\System\Config\Backend
 */
class Regenerate extends Value
{
    /**
     * @return Value
     */
    public function beforeSave()
    {
        $value = $this->getValue();
        $this->setValue(0);
        if (is_array($value) && isset($value['regenerate'])) {
            $this->setValue(1);
        }

        return parent::beforeSave();
    }
}
