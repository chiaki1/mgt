<?php

namespace Chiaki\Barcode\Model\System\Config\Backend;

use Chiaki\Barcode\Helper\Data as HelperData;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Class PaperMargin
 *
 * @package Chiaki\Barcode\Model\System\Config\Backend
 */
class PaperMargin extends Value
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * PaperMargin constructor.
     *
     * @param Context               $context
     * @param Registry              $registry
     * @param ScopeConfigInterface  $config
     * @param TypeListInterface     $cacheTypeList
     * @param HelperData            $helperData
     * @param AbstractResource|null $resource
     * @param AbstractDb|null       $resourceCollection
     * @param array                 $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        HelperData $helperData,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_helperData = $helperData;

        parent::__construct($context, $registry, $config, $cacheTypeList, $resource, $resourceCollection, $data);
    }

    /**
     * @return Value
     * @throws ValidatorException
     */
    public function beforeSave()
    {
        $savedValue = $this->getValue();
        if (substr_count($savedValue, ';') + 1 !== 4) {
            throw new ValidatorException(__('Incorrect Paper Margin Format'));
        }

        $paperMargin = $this->_helperData->getPaperMargin($savedValue, $this->getScopeId(), true);
        foreach ($paperMargin as $key => $input) {
            if (!is_numeric($input) || (float)$input < 0) {
                throw new ValidatorException(__('Paper Margin %1 Must Be A Positive Float Number', ucfirst($key)));
            }
        }

        return parent::beforeSave();
    }
}
