<?php

namespace Chiaki\Barcode\Model\System\Config\Source;

/**
 * Class PaperSize
 *
 * @package Chiaki\Barcode\Model\System\Config\Source
 */
class PaperSize extends OptionArray
{
    const A4     = 'A4';
    const A5     = 'A5';
    const A6     = 'A6';
    const LETTER = 'Letter';
    const DEFAULT = 'Default';

    /**
     * @return array
     */
    public function getOptionHash()
    {
        return [
            self::A4     => __('A4'),
            self::A5     => __('A5'),
            self::A6     => __('A6'),
            self::LETTER => __('Letter'),
            self::DEFAULT => __('Default'),
        ];
    }
}
