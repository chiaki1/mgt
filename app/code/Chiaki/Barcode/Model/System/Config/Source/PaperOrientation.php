<?php

namespace Chiaki\Barcode\Model\System\Config\Source;

/**
 * Class PaperOrientation
 *
 * @package Chiaki\Barcode\Model\System\Config\Source
 */
class PaperOrientation extends OptionArray
{
    const PORTRAIT  = 'P';
    const LANDSCAPE = 'L';

    /**
     * @return array
     */
    public function getOptionHash()
    {
        return [
            self::PORTRAIT  => __('Portrait'),
            self::LANDSCAPE => __('Landscape'),
        ];
    }
}
