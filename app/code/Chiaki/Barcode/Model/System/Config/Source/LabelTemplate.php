<?php

namespace Chiaki\Barcode\Model\System\Config\Source;

/**
 * Class LabelTemplate
 *
 * @package Chiaki\Barcode\Model\System\Config\Source
 */
class LabelTemplate extends OptionArray
{
    const QR_CODE   = 'qrCode';
    const CLASSIC_1 = 'classic1';
    const CLASSIC_2 = 'classic2';
    const STANDARD  = 'standard';

    /**
     * @return array
     */
    public function getOptionHash()
    {
        return [
            self::STANDARD  => __('Standard'),
            self::CLASSIC_1 => __('Classic 1'),
            self::CLASSIC_2 => __('Classic 2'),
            self::QR_CODE   => __('Standard with QR code'),
        ];
    }
}
