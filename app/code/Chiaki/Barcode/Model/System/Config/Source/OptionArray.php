<?php

namespace Chiaki\Barcode\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class OptionArray
 *
 * @package Chiaki\Barcode\Model\System\Config\Source
 */
abstract class OptionArray implements ArrayInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->getOptionHash() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }

    /**
     * @return array
     */
    abstract public function getOptionHash();
}
