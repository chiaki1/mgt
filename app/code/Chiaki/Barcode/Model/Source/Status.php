<?php

namespace Chiaki\Barcode\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 *
 * @package Chiaki\Barcode\Model\Source
 */
class Status implements OptionSourceInterface
{

    const ACTIVE = '1';
    const INACTIVE = '2';

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = [
            self::ACTIVE => __('Active'),
            self::INACTIVE => __('Inactive')
        ];
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
