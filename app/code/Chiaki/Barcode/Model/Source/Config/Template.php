<?php

namespace Chiaki\Barcode\Model\Source\Config;

use Chiaki\Barcode\Model\Source\Template as TemplateSource;

/**
 * Class Template
 *
 * @package Chiaki\Barcode\Model\Source\Config
 */
class Template extends TemplateSource
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $options[] = [
            'label' => __('-- Select Template --'),
            'value' => "0"
        ];
        $parentOptions = parent::toOptionArray();
        $options = array_merge($options, $parentOptions);
        return $options;
    }
}
