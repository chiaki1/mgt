<?php

namespace Chiaki\Barcode\Model\Source;

use Magento\CatalogSearch\Model\Advanced;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Attributes
 *
 * @package Chiaki\Barcode\Model\Source
 */
class Attributes implements OptionSourceInterface
{
    protected $_catalogSearchAdvanced;

    /**
     * @param Advanced $catalogSearchAdvanced
     */
    public function __construct(
        Advanced $catalogSearchAdvanced
    ) {
        $this->_catalogSearchAdvanced = $catalogSearchAdvanced;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->getAttributeOptions();
        $options          = [];
        $options[]        = array('value' => 'sku', 'label' => 'SKU');
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }

    public function getAttributeOptions()
    {
        $availableOptions = array();
        $attributes       = $this->_catalogSearchAdvanced->getAttributes();
        foreach ($attributes as $attribute) {
            if (($attribute->getAttributeCode() === 'sku') ||
                ($attribute->getAttributeCode() === 'description') ||
                ($attribute->getAttributeCode() === 'short_description'))
                continue;
            $availableOptions[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }
        return $availableOptions;
    }
}
