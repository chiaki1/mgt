<?php

namespace Chiaki\Barcode\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class MeasurementUnit
 *
 * @package Chiaki\Barcode\Model\Source
 */
class MeasurementUnit implements OptionSourceInterface
{

    const MM = 'mm';
    const CM = 'cn';
    const IN = 'in';
    const PX = 'px';
    const PERCENTAGE = '%';

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = [
            self::MM => __('mm'),
            self::CM => __('cm'),
            self::IN => __('in'),
            self::PX => __('px'),
            self::PERCENTAGE => __('%')
        ];
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
