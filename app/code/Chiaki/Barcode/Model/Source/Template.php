<?php

namespace Chiaki\Barcode\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Chiaki\Barcode\Model\ResourceModel\Template\Collection as BarcodeTemplateCollection;

/**
 * Class Template
 *
 * @package Chiaki\Barcode\Model\Source
 */
class Template implements OptionSourceInterface
{
    /**
     * @var BarcodeTemplateCollection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $options;

    /**
     * TemplateType constructor.
     * @param BarcodeTemplateCollection $collection
     */
    public function __construct(
        BarcodeTemplateCollection $collection
    ) {
        $this->collection = $collection;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if(!$this->options) {
            $this->options = [];
            $collection = $this->collection;
            $collection->addFieldToFilter('status', Status::ACTIVE);
            if ($collection->getSize() > 0) {
                foreach ($collection as $template) {
                    $this->options[] = [
                        'label' => $template->getName(),
                        'value' => $template->getTemplateId(),
                    ];
                }
            }
        }
        return $this->options;
    }

    public function toArray()
    {
        $result = [];
        $options = $this->toOptionArray();
        foreach ($options as $option) {
            $result[$option['value']] = $option['label'];
        }
        return $result;
    }
}
