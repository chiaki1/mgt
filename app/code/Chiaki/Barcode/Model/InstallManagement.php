<?php

namespace Chiaki\Barcode\Model;

use Chiaki\Barcode\Api\InstallManagementInterface;

class InstallManagement implements InstallManagementInterface
{

    /**
     *
     * @var \Magestore\BarcodeSuccess\Model\ResourceModel\InstallManagement
     */
    protected $_resource;

    /**
     * InstallManagement constructor.
     * @param ResourceModel\InstallManagement $installManagementResource
     */
    public function __construct(
        \Chiaki\Barcode\Model\ResourceModel\InstallManagement $installManagementResource
    )
    {
        $this->_resource = $installManagementResource;
    }

    /**
     * @inheritdoc
     */
    public function createBarcodeTemplate()
    {
        $this->getResource()->createBarcodeTemplate();
        return $this;
    }

    /**
     *
     * @return \Magestore\BarcodeSuccess\Model\ResourceModel\InstallManagement
     */
    public function getResource()
    {
        return $this->_resource;
    }
}
