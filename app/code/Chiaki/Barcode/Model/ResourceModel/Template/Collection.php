<?php

namespace Chiaki\Barcode\Model\ResourceModel\Template;

/**
 * Class Collection
 *
 * @package Chiaki\Barcode\Model\ResourceModel\Template
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'template_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Chiaki\Barcode\Model\Template', 'Chiaki\Barcode\Model\ResourceModel\Template');
    }
}
