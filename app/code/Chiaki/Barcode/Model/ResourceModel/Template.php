<?php

namespace Chiaki\Barcode\Model\ResourceModel;

class Template  extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('barcode_template', 'template_id');
    }
}
