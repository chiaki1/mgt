<?php

namespace Chiaki\Barcode\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;

/**
 *
 *
 * @category Magestore
 * @package  Magestore_BarcodeSuccess
 * @module   BarcodeSuccess
 * @author   Magestore Developer
 */
abstract class AbstractResource extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Chiaki\Barcode\Model\Source\TemplateType
     */
    protected $barcodeTemplates;

    /**
     *
     * @var \Chiaki\Barcode\Model\Db\QueryProcessor
     */
    protected $_queryProcessor;

    /**
     * AbstractResource constructor.
     *
     * @param \Chiaki\Barcode\Model\Db\QueryProcessor    $queryProcessor
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Chiaki\Barcode\Model\Source\TemplateType         $templates
     * @param null                                              $connectionName
     */
    public function __construct(
    \Chiaki\Barcode\Model\Db\QueryProcessor $queryProcessor,
    \Magento\Framework\Model\ResourceModel\Db\Context $context,
    \Chiaki\Barcode\Model\Source\TemplateType $templates,
    $connectionName = null
    )
    {
        $this->_queryProcessor = $queryProcessor;
        $this->barcodeTemplates = $templates;
        parent::__construct($context, $connectionName);
    }

    /**
     * insert data to table.
     *
     * @param $table
     * @param array $data
     *
     * @throws LocalizedException
     */
    public function insertData($table, array $data = [])
    {
        if (empty($data)) {
            return;
        }

        $connection = $this->getConnection();
        $connection->beginTransaction();
        try {
            $connection->insertMultiple($table, $data);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new LocalizedException(__($e->getMessage()), $e);
        }
    }

    /**
     * delete data from table.
     *
     * @param $table
     * @param array $where
     *
     * @throws LocalizedException
     */
    public function deleteData($table, array $where = [])
    {
        if (empty($where)) {
            return;
        }

        $connection = $this->getConnection();
        $connection->beginTransaction();
        try {
            $connection->delete($table, $where);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new LocalizedException(__($e->getMessage()), $e);
        }
    }

    /**
     * update data for table.
     *
     * @param $table
     * @param $bind
     * @param $where
     *
     * @throws LocalizedException
     */
    public function updateData($table, $bind, $where = [])
    {
        if (empty($where)) {
            return;
        }

        $connection = $this->getConnection();
        $connection->beginTransaction();
        try {
            $connection->update($table, $bind, $where);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new LocalizedException(__($e->getMessage()), $e);
        }
    }
}
