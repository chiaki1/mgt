<?php

namespace Chiaki\Barcode\Model\ResourceModel;

use Chiaki\Barcode\Api\Db\QueryProcessorInterface;

class InstallManagement extends AbstractResource
{
    protected function _construct()
    {
        /* do nothing */
    }

    /**
     * Create default barcode template
     *
     * @return \Chiaki\Barcode\Model\ResourceModel\InstallManagement
     */
    public function createBarcodeTemplate()
    {
        /* start query process */
        $this->_queryProcessor->start();

        $defaultData = $this->barcodeTemplates->getDefaultData();

        /* add query to Processor */
        $this->_queryProcessor->addQuery([
            'type' => QueryProcessorInterface::QUERY_TYPE_INSERT,
            'values' => $defaultData,
            'table' => $this->getTable('barcode_template')
        ]);

        /* process queries in Processor */
        $this->_queryProcessor->process();

        return $this;
    }

}
