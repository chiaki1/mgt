<?php

namespace Chiaki\Barcode\Model\Locator;

/**
 * Interface LocatorInterface
 */
interface LocatorInterface
{
    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function add($key, $value);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function get($key);

    /**
     * @param $key
     * @return mixed
     */
    public function remove($key);

}
