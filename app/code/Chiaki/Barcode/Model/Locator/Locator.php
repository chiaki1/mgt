<?php

namespace Chiaki\Barcode\Model\Locator;

use Magento\Framework\Registry;
use Magento\Backend\Model\Session;
/**
 * Class RegistryLocator
 */
class Locator implements \Chiaki\Barcode\Model\Locator\LocatorInterface
{
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Locator constructor.
     *
     * @param Registry $registry
     * @param Session  $session
     */
    public function __construct(
        Registry $registry,
        Session $session
    ) {
        $this->registry = $registry;
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function add($key, $value){
        $this->session->setData($key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function get($key){
        return $this->session->getData($key);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key){
        $this->session->setData($key, null);
    }

}
