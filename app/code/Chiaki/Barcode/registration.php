<?php

use Magento\Framework\Component\ComponentRegistrar;

require_once(BP.'/app/library/vendor/autoload.php');

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Chiaki_Barcode',
    __DIR__
);
