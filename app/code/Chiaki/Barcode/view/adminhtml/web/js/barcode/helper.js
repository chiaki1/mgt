define([
    'Magento_Ui/js/modal/alert'
], function (alert) {
    'use strict';

    return {
        customTemplate: 'custom',
        templateDetailUrl: {
            OL160: 'https://www.onlinelabels.com/templates/ol160-template-microsoft-word',
            OL161: 'https://www.onlinelabels.com/templates/ol161-template',
            OL171: 'https://www.onlinelabels.com/templates/ol171-template'
        },
        config: {
            paper: {
                size: '#barcode_barcode_paper_paper_size',
                width: '#barcode_barcode_paper_width',
                height: '#barcode_barcode_paper_height',
                padding: '#barcode_barcode_paper_padding',
                margin: '#barcode_barcode_paper_margin',
                vertical: '#barcode_barcode_paper_vertical',
                horizontal: '#barcode_barcode_paper_horizontal',
                orient: '#barcode_barcode_paper_orientation'
            },
            label: {
                template: '#barcode_barcode_label_template',
                width: '#barcode_barcode_label_width',
                height: '#barcode_barcode_label_height',
                size: '#barcode_barcode_label_size',
                html: '#barcode_barcode_label_content',
                css: '#barcode_barcode_label_css'
            }
        },

        massAction: {
            barcode: {
                qty: '#barcode_qty',
                custom: '#barcode_custom_print_barcode',
                type: '#barcode_barcode_type',
                paper: '#barcode_paper_template',
                label: '#barcode_barcode_label_template'
            },
            dependCustom: {
                type: '.field-barcode_type',
                label: '.field-barcode_label_template',
                paper: '.field-paper_template',
                previewButton: '.field-preview_button'
            },
            useConfig: {
                type: '#barcode_use_config_barcode_type',
                label: '#barcode_use_config_barcode_label_template',
                paper: '#barcode_use_config_paper_template'
            }
        },

        productEdit: {
            type: 'select[name="product[barcode_data][barcode_type]"]',
            paper: 'select[name="product[barcode_data][paper_template]"]',
            label: 'select[name="product[barcode_data][barcode_label_template]"]'
        },

        /**
         * Display Base 64 PDF String
         * @param base64String
         * @returns {Blob}
         */
        displayPdf: function (base64String) {
            var windowSpecs = 'dependent=1,locationbar=0,scrollbars=1,menubar=1,resizable=1,' +
                'screenX=50,screenY=50,width=1000,height=1000';
            var htmlContent = '<iframe width=100% height=100% type="application/pdf"'
                + ' src="data:application/pdf;base64,' + base64String + '"></iframe>';
            var ProductBarcode = window.open('#', "_blank", windowSpecs);

            ProductBarcode.document.write(htmlContent);
            ProductBarcode.document.close();
            ProductBarcode.focus();
        },

        /**
         * Download BASE 64 PDF String after display
         * @param base64String
         * @param filename
         */
        downloadPdf: function (base64String, filename) {
            var barcodeAnchor = document.createElement('a');

            barcodeAnchor.href = 'data:application/pdf;base64,' + base64String;
            barcodeAnchor.download = filename + '.pdf';
            barcodeAnchor.click();
        },

        alertError: function (title, content) {
            alert({
                title: title,
                content: content
            });
        },
        processResponse: function(response){
            var self = this;
            if(response.success && response.html){
                self.print(response.html);
            }
            if(response.error && response.messages){
                Alert('Error',response.messages);
            }
        },
        print: function(html){
            var print_window = window.open('', 'print', 'status=1,width=500,height=500');
            if(print_window){
                print_window.document.open();
                print_window.document.write(html);
                print_window.document.close();
                print_window.addEventListener('load',function(){
                    print_window.print();
                });
            }else{
                Alert('Message','Your browser has blocked the automatic popup, please change your browser settings or print the receipt manually');
            }
        }
    };
});
