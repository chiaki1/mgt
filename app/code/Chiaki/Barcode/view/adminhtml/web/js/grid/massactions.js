define([
    'jquery',
    'mage/translate'
], function ($, $t) {
    'use strict';

    return function (TreeMassActions) {
        return TreeMassActions.extend({
            togglePrintBarcodeModal: function () {
                var barcodeModal = $('#barcode-print-modal');

                barcodeModal.modal({
                    type: 'slide',
                    title: $t('Print Barcode Label'),
                    innerScroll: true,
                    modalClass: 'barcode-print-action-box',
                    buttons: []
                });
                barcodeModal.trigger('openModal');
            },

            toggleImportBarcodeModal: function () {
                var barcodeModal = $('#barcode-import-modal');

                barcodeModal.modal({
                    type: 'slide',
                    title: $t('Print Import Barcode Label'),
                    innerScroll: true,
                    modalClass: 'barcode-print-action-box',
                    buttons: []
                });
                barcodeModal.trigger('openModal');
            }
        });
    };
});
