define([
    'Magento_Ui/js/form/element/single-checkbox'
], function (checkbox) {
    'use strict';

    return checkbox.extend({
        defaults: {
            valueFromConfig: '',
            linkedValue: ''
        },

        /**
         * @returns {Element}
         */
        initObservable: function () {
            return this
                ._super()
                .observe(['defaultConfig', 'linkedValue']);
        },

        /**
         * @inheritdoc
         */
        onCheckedChanged: function (newChecked) {
            if (newChecked) {
                this.linkedValue(this.defaultConfig());
            }

            this._super(newChecked);
        }
    });
});
