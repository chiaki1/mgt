define([
    'jquery',
    'Chiaki_Barcode/js/barcode/helper',
    'mage/translate'
], function ($, barcodeHelper, $t) {
    'use strict';

    $.widget('Barcode.print', {
        options: {
            ajaxUrl: '',
            printBtn: '#barcode_print_btn_print_sample'
        },

        _create: function () {
            var self = this;

            $(this.options.printBtn).on('click', function () {
                $.ajax({
                    url: self.options.ajaxUrl,
                    type: 'POST',
                    data: {
                        form_key: window.FORM_KEY
                    },
                    dataType: 'json',
                    showLoader: true,
                    success: function (response) {
                        if (response.error) {
                            barcodeHelper.alertError($t('Error'), response.message);
                        } else {
                            /*barcodeHelper.displayPdf(response.data);
                            barcodeHelper.downloadPdf(response.data, 'BarcodeLabelSample');*/
                            barcodeHelper.processResponse(response);
                        }
                    },
                    error: function (error) {
                        barcodeHelper.alertError($t('Request Error'), error.status + ' ' + error.statusText);
                    }
                });
            });
        }
    });

    return $.Barcode.print;
});
