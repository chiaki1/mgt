define([
    'jquery',
    "Magento_Ui/js/modal/alert",
    'mage/translate',
    'Chiaki_Barcode/js/barcode/helper'
], function ($, alert, $t, barcodeHelper) {
    'use strict';

    $.widget('Barcode.importBarcode', {
        options: {
            gridUrl: '',
            printUrl: '',
            checkBtn: $('#barcode_check_btn'),
            importForm: $('#barcode_import_modal_form'),
            csv: $('#barcode_import'),
            printBtn: $('#mass_print_barcode_csv'),
            csvResult: $('#barcode-modal-csv-result'),
            importMessage: $('#barcode-import-message'),
            printDiv: $('#barcode-print-btn-div')
        },

        _create: function () {
            var self = this,
                formData = new FormData();

            this.options.checkBtn.on('click', function () {
                if (self.options.importForm.valid()) {
                    formData.append('csv', self.options.csv.prop('files')[0]);
                    self._validateCsvFile(formData);
                }
            });

            this.options.printBtn.on('click', function () {
                var barcodeData = $('input[name="products"]').val();

                self._massPrint(barcodeData);
            });
        },

        _validateCsvFile: function (formData) {
            var self = this;

            $.ajax({
                url: self.options.importForm.attr('action'),
                type: 'POST',
                dataType: 'json',
                showLoader: true,
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    if (response.error) {
                        barcodeHelper.alertError($t('Error'), response.message);
                    }
                    if (response.validated) {
                        self.options.importMessage.find('#message-content').text(response.message);
                        self.options.importMessage.show();
                        self._renderGrid(response.gridData);
                        setTimeout(function () {
                            self.options.importMessage.hide();
                        }, 10000);
                    }
                },
                error: function (error) {
                    barcodeHelper.alertError($t('Request Error'), error.status + ' ' + error.statusText);
                }
            });
        },

        _renderGrid: function (gridData) {
            var self = this;

            $.ajax({
                url: self.options.gridUrl,
                showLoader: true,
                data: {
                    products: gridData,
                    firstLoad: 1
                },
                success: function (response) {
                    self.options.csvResult.html(response);
                    self.options.csvResult.trigger('contentUpdated');
                    self.options.printDiv.show();
                },
                error: function (error) {
                    barcodeHelper.alertError($t('Request Error'), error.status + ' ' + error.statusText);
                }
            });
        },

        _massPrint: function (barcodeData) {
            var self = this;

            $.ajax({
                type: 'POST',
                url: self.options.printUrl,
                showLoader: true,
                dataType: 'json',
                data: {
                    products: barcodeData
                },
                success: function (response) {
                    if (response.error) {
                        barcodeHelper.alertError('Error', response.message);
                    } else {
                        /*barcodeHelper.displayPdf(response.data);
                        barcodeHelper.downloadPdf(response.data, 'ImportBarcodeLabel');*/
                        barcodeHelper.processResponse(response);
                    }
                },
                error: function (error) {
                    barcodeHelper.alertError('Request Error', error.status + ' ' + error.statusText);
                }
            });
        }

    });

    return $.Barcode.importBarcode;
});
