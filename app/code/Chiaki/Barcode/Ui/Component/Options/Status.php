<?php

namespace Chiaki\Barcode\Ui\Component\Options;

class Status extends AbstractOption
{
    public function toOptionHash() {
        return array(
            \Chiaki\Barcode\Model\Source\Status::ACTIVE => __('Active'),
            \Chiaki\Barcode\Model\Source\Status::INACTIVE => __('Inactive')
        );
    }
}
