<?php

namespace Chiaki\Barcode\Ui\Component\Options;

class TemplateType extends AbstractOption
{
    public function toOptionHash() {
        return array(
            \Chiaki\Barcode\Model\Source\TemplateType::STANDARD => __('Standard'),
            \Chiaki\Barcode\Model\Source\TemplateType::A4 => __('A4'),
            \Chiaki\Barcode\Model\Source\TemplateType::JEWELRY => __('Jewelry')
        );
    }
}
