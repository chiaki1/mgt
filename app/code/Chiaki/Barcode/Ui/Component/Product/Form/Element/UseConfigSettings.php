<?php

namespace Chiaki\Barcode\Ui\Component\Product\Form\Element;

use Magento\Framework\Data\ValueSourceInterface;
use Magento\Ui\Component\Form\Element\Checkbox;

/**
 * Class UseConfigSettings
 *
 * @package Chiaki\Barcode\Ui\Component\Product\Form\Element
 */
class UseConfigSettings extends Checkbox
{
    /**
     * Prepare component default configuration data
     *
     * @return void
     */
    public function prepare()
    {
        $config = $this->getData('config');
        if ($config['defaultConfig'] instanceof ValueSourceInterface
            && isset($config['keyInConfig'], $config['defaultConfig'])) {
            $config['defaultConfig'] = $config['defaultConfig']->getValue($config['keyInConfig']);
        }

        $this->setData('config', (array)$config);
        parent::prepare();
    }
}
