<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Barcode\Setup\Patch\Data;

use Chiaki\Barcode\Api\InstallManagementInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Patch is mechanism, that allows to do atomic upgrade data changes
 */
class AddBarcodeTemplate implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @var InstallManagementInterface
     */
    private $installManagement;

    /**
     * AddBarcodeTemplate constructor.
     *
     * @param ModuleDataSetupInterface   $moduleDataSetup
     * @param InstallManagementInterface $installManagement
     */
    public function __construct(ModuleDataSetupInterface $moduleDataSetup, InstallManagementInterface $installManagement
    ) {
        $this->moduleDataSetup   = $moduleDataSetup;
        $this->installManagement = $installManagement;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->installManagement->createBarcodeTemplate();
        $this->moduleDataSetup->endSetup();
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
