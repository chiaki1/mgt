<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Template;

/**
 * Class View
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Template
 */
class View extends \Chiaki\Barcode\Controller\Adminhtml\AbstractTemplate
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = parent::execute();
        $id = $this->getRequest()->getParam('id');
        if($id){
            $this->locator->add('current_barcode_template',$id);
            $resultPage->getConfig()->getTitle()->prepend(__('Edit Barcode Template'));
        }else{
            $this->locator->remove('current_barcode_template');
            $resultPage->getConfig()->getTitle()->prepend(__('Add a New Barcode Template'));
        }
        return $resultPage;
    }
}
