<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Template;

/**
 * Class Save
 *
 * @package MChiaki\Barcode\Controller\Adminhtml\Template
 */
class Save extends \Chiaki\Barcode\Controller\Adminhtml\AbstractTemplate
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $path           = '*/*/';
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $params = $this->getRequest()->getParams();
            if (isset($params['template_id']) && empty($params['template_id'])) {
                unset($params['template_id']);
            }
            if (isset($params['product_attribute_show_on_barcode'])) {
                if (is_array( $params['product_attribute_show_on_barcode'])) {
                    $params['product_attribute_show_on_barcode'] = implode(',', $params['product_attribute_show_on_barcode']);
                }
            } else {
                $params['product_attribute_show_on_barcode'] = '';
            }
            $model = $this->templateFactory->create();
            $model->addData($params);
            $this->resource->save($model);
            $this->messageManager->addSuccessMessage(__('The template has been saved successfully'));
        } catch (\Exception $ex) {
            $this->messageManager->addErrorMessage($ex->getMessage());
        }
        return $resultRedirect->setPath($path);
    }
}
