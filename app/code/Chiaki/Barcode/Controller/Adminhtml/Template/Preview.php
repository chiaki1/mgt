<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Template;

/**
 * Class Preview
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Template
 */
class Preview extends \Chiaki\Barcode\Controller\Adminhtml\AbstractTemplate
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Json|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $defaultBarcode = $this->helper->generateBarcode();
        $resultJson     = $this->resultJsonFactory->create();
        $data           = $this->getRequest()->getParam('data');
        $barcode        = $this->getRequest()->getParam('barcode', $defaultBarcode);
        $qty            = $this->getRequest()->getParam('qty');
        if (!empty($data)) {
            $barcodes = [
                ['barcode' => $barcode, 'qty' => $qty]
            ];
            $html     = "";
            $block    = $this->createBlock('Chiaki\Barcode\Block\Barcode\Container\Template', '', 'Chiaki_Barcode::barcode/print/template.phtml');
            $block->setData('barcodes', $barcodes);
            if (isset($data['is_print_preview']) && isset($data['type'])) {
                $template = $this->templateFactory->create();
                $this->resource->load($template, $data['type']);
                $data = $template->getData();
            }
            $block->setData('template_data', $data);
            $html .= $block->toHtml();
            $resultJson->setData([
                                     'html'    => $html,
                                     'success' => true
                                 ]
            );
        } else {
            $resultJson->setData([
                                     'messages' => __('Cannot find any data to preview'),
                                     'error'    => true
                                 ]
            );
        }
        return $resultJson;
    }

    /**
     * Determine if authorized to perform group actions.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Chiaki_Barcode::barcode_template');
    }
}
