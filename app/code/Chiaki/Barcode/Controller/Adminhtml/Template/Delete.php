<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Template;

/**
 * Class Delete
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Template
 */
class Delete extends \Chiaki\Barcode\Controller\Adminhtml\AbstractTemplate
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $path = '*/*/';
        $resultRedirect = $this->resultRedirectFactory->create();
        try{
            $id = $this->getRequest()->getParam('id');
            if(isset($id) && !empty($id)){
                $model = $this->templateFactory->create()->load($id);
                $this->resource->delete($model);
            }
            $this->messageManager->addSuccessMessage(__('The template has been deleted successfully'));
        }catch (\Exception $ex){
            $this->messageManager->addErrorMessage($ex->getMessage());
        }
        return $resultRedirect->setPath($path);
    }
}
