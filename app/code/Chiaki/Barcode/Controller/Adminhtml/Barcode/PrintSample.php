<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Barcode;

use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class PrintSample
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\System
 */
class PrintSample extends AbstractPrint
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws Exception
     */
    public function execute()
    {
        $paperData = $this->getPaperData();
        $maxItems  = $this->getPageMaxItem($paperData);
        $products  = $this->_helperData->getPrintProducts($maxItems, $this->getStoreId());

        try {
            $pdfString = $this->printBarcode(
                1,
                $products,
                $this->getBarcodeData(),
                $paperData,
                $this->getLabelData()
            );

            if ($pdfString) {
                return $this->_json->setData(['data' => base64_encode($pdfString)]);
            }

            return $this->errorMessage(self::ERROR_EMPTY_BARCODE);
        } catch (Exception $e) {
            return $this->errorMessage($e->getMessage());
        }
    }
}
