<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Barcode;

use Chiaki\Barcode\Helper\Template as TemplateHelper;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class LoadTemplate
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Barcode
 */
class LoadTemplate extends AbstractPrint
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $paper = $this->getRequest()->getParam('paper');
        $label = $this->getRequest()->getParam('label', 'standard');
        if ($paper === null) {
            return $this->errorMessage(self::MISSING_PARAMETERS);
        }

        $paperData    = $this->_paperTemplate->getTemplateBaseSpecs($paper);
        $labelBarcode = $this->_paperTemplate->getBarcodeBaseSpecs($paper, $label);

        try {
            $templateHtml = $this->_helperData->getDefaultTemplate($label, TemplateHelper::HTML, $paper);
            $templateCss  = $this->_helperData->getDefaultTemplate($label, TemplateHelper::CSS, $paper);

            return $this->_json->setData([
                                             'paper' => $paperData,
                                             'label' => [
                                                 'html'     => $templateHtml,
                                                 'css'      => $templateCss,
                                                 'template' => 'standard',
                                                 'width'    => $labelBarcode['width'],
                                                 'height'   => $labelBarcode['height'],
                                                 'size'     => $labelBarcode['size'],
                                             ],
                                         ]
            );
        } catch (Exception $e) {
            return $this->errorMessage($e->getMessage());
        }
    }
}
