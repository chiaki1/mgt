<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Barcode;

use Chiaki\Barcode\Helper\Data as HelperData;
use Chiaki\Barcode\Model\ResourceModel\Barcode\Generate as BarcodeGenerate;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Run
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Barcode
 */
class Run extends Action
{
    const REDIRECT_URL = 'adminhtml/system_config/edit/section/barcode';

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var BarcodeGenerate
     */
    protected $_barcodeGenerate;

    /**
     * Generate constructor.
     *
     * @param Context         $context
     * @param HelperData      $helperData
     * @param BarcodeGenerate $barcodeGenerate
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        BarcodeGenerate $barcodeGenerate
    ) {
        $this->_helperData      = $helperData;
        $this->_barcodeGenerate = $barcodeGenerate;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        try {
            $productIds = $this->_helperData->getProductIdsForBarcode();
            $this->_barcodeGenerate->generateBarcodeAttribute($productIds);
            $this->messageManager->addSuccessMessage(
                __('%1 Barcode Number(s) Generated Successfully.', count($productIds))
            );
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Fail To Generate Barcode Number.'));
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        $this->_redirect(self::REDIRECT_URL);
    }
}
