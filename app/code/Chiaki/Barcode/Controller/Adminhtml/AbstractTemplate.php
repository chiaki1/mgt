<?php

namespace Chiaki\Barcode\Controller\Adminhtml;

use Chiaki\Barcode\Helper\Data;
use Chiaki\Barcode\Model\Locator\LocatorInterface;
use Chiaki\Barcode\Model\ResourceModel\Template as TemplateResource;
use Chiaki\Barcode\Model\TemplateFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class AbstractTemplate
 *
 * @package Chiaki\Barcode\Controller\Adminhtml
 */
abstract class AbstractTemplate extends \Magento\Backend\App\Action
{

    const ERROR_EMPTY_BARCODE      = 'Attribute Selected For Barcode Is Empty';
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Chiaki_Barcode::barcode_template';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Chiaki\Barcode\Helper\Data
     */
    protected $helper;

    /**
     * @var TemplateResource
     */
    protected $resource;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var TemplateFactory
     */
    protected $templateFactory;

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Json
     */
    protected $_json;

    /**
     * AbstractTemplate constructor.
     *
     * @param Context               $context
     * @param PageFactory           $resultPageFactory
     * @param Data                  $data
     * @param TemplateResource      $resource
     * @param JsonFactory           $resultJsonFactory
     * @param TemplateFactory       $templateFactory
     * @param LocatorInterface      $locator
     * @param StoreManagerInterface $storeManager
     * @param Json                  $json
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Data $data,
        TemplateResource $resource,
        JsonFactory $resultJsonFactory,
        TemplateFactory $templateFactory,
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        Json $json
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->helper            = $data;
        $this->resource          = $resource;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->templateFactory   = $templateFactory;
        $this->locator           = $locator;
        $this->_storeManager     = $storeManager;
        $this->_json             = $json;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Chiaki_Barcode::barcode_template');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Barcode Label Templates'));
        return $resultPage;
    }

    /**
     * @param        $class
     * @param string $name
     * @param string $template
     *
     * @return \Magento\Framework\View\Element\BlockInterface|string
     */
    public function createBlock($class, $name = '', $template = "")
    {
        $block = "";
        try {
            $block = $this->_view->getLayout()->createBlock($class, $name);
            if ($block && $template != "") {
                $block->setTemplate($template);
            }
        } catch (\Exception $e) {
            return $block;
        }
        return $block;
    }

    /**
     * @return int
     * @throws NoSuchEntityException
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @param string $message
     *
     * @return Json
     */
    public function errorMessage($message)
    {
        return $this->_json->setData([
                                         'error'   => true,
                                         'message' => __($message),
                                     ]
        );
    }

}
