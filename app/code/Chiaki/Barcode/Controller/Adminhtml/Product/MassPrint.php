<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Product;

use Chiaki\Barcode\Controller\Adminhtml\Barcode\AbstractPrint;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class MassPrint
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Product
 */
class MassPrint extends AbstractPrint
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $data   = $this->processParameters($params);
        if ($this->hasError) {
            return $this->errorMessage($this->errorMessage);
        }

        $productIds = $params['productIds'];
        $products   = $this->_helperData->getPrintProducts($productIds, $data['qtyPerItem'], $this->getStoreId());
        $templateId = $data['type'];

        try {
            $html = "";

            $block = $this->createBlock('Chiaki\Barcode\Block\Barcode\Container\Template', '', 'Chiaki_Barcode::barcode/print/template.phtml');
            $block->setData('barcodes', $products);
            if (isset($templateId)) {
                $template = $this->_templateFactory->create()->load($templateId);
                $data     = $template->getData();
                $block->setData('template_data', $data);
            } else {
                return $this->errorMessage(self::ERROR_CONFIG_TEMPLATE);
            }
            $html .= $block->toHtml();

            if ($html) {
                return $this->_json->setData([
                                                 'html'    => $html,
                                                 'success' => true
                                             ]
                );
            }

            return $this->errorMessage(self::ERROR_EMPTY_BARCODE);
        } catch (Exception $e) {
            return $this->errorMessage($e->getMessage());
        }
    }

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function processParameters($params)
    {
        if (isset($params['barcode'], $params['productIds'])) {
            $barcode            = $params['barcode'];
            $data['qtyPerItem'] = (int)$barcode['qty'];
            $data['type']       = isset($barcode['barcode_type'])
                ? $barcode['barcode_type']
                : $this->_helperData->getConfigGeneral('default_barcode_template');

            return $data;
        }

        return $this->raiseError(self::MISSING_PARAMETERS);
    }
}
