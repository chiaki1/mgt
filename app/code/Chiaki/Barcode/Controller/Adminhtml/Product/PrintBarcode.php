<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Product;

use Chiaki\Barcode\Controller\Adminhtml\Barcode\AbstractPrint;
use Exception;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class PrintBarcode
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Product
 */
class PrintBarcode extends AbstractPrint
{
    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute()
    {
        $params            = $this->getRequest()->getParams();
        $data              = $this->processParameters($params);
        $products          = $this->_helperData->getPrintProducts([$data['id']], $data['qty'], $this->getStoreId());
        $defaultTemplateId = $this->_helperData->getConfigGeneral('default_barcode_template');
        $templateId        = $this->getRequest()->getParam('type', $defaultTemplateId);

        try {
            $html = "";

            $block = $this->createBlock('Chiaki\Barcode\Block\Barcode\Container\Template', '', 'Chiaki_Barcode::barcode/print/template.phtml');
            $block->setData('barcodes', $products);
            if (isset($templateId)) {
                $template = $this->_templateFactory->create()->load($templateId);
                $data     = $template->getData();
                $block->setData('template_data', $data);
            } else {
                return $this->errorMessage(self::ERROR_CONFIG_TEMPLATE);
            }
            $html .= $block->toHtml();

            if ($html) {
                return $this->_json->setData([
                                                 'html'    => $html,
                                                 'success' => true
                                             ]
                );
            }

            return $this->errorMessage(self::ERROR_EMPTY_BARCODE);
        } catch (Exception $e) {
            return $this->errorMessage($e->getMessage());
        }
    }

    /**
     * @param array $p
     *
     * @return mixed
     */
    public function processParameters($p)
    {
        if (isset($p['id'], $p['qty'], $p['type'])) {
            return [
                'id'   => (int)$p['id'],
                'qty'  => (int)$p['qty'],
                'type' => $p['type']
            ];
        }

        return $this->raiseError(self::MISSING_PARAMETERS);
    }
}
