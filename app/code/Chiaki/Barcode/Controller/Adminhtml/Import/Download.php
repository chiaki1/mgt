<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Import;

use Chiaki\Barcode\Helper\Data as HelperData;
use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;

/**
 * Class Download
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Import
 */
class Download extends Action
{
    const FILE_NAME = 'barcode_sample.csv';

    /**
     * @var Filesystem
     */
    protected $_fileSystem;

    /**
     * @var FileFactory
     */
    protected $_fileFactory;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * Download constructor.
     *
     * @param Context     $context
     * @param Filesystem  $filesystem
     * @param FileFactory $fileFactory
     * @param HelperData  $helperData
     */
    public function __construct(
        Context $context,
        Filesystem $filesystem,
        FileFactory $fileFactory,
        HelperData $helperData
    ) {
        $this->_fileSystem  = $filesystem;
        $this->_fileFactory = $fileFactory;
        $this->_helperData  = $helperData;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     * @throws FileSystemException
     * @throws Exception
     */
    public function execute()
    {
        $tempName = time();
        $this->_fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR)->create('chiaki/barcode');
        $file         = DirectoryList::VAR_DIR . '/chiaki/barcode/' . $tempName . '.csv';
        $firstProduct = $this->_helperData->getPrintProducts(1, 10)[0];

        $stream = $this->_fileSystem->getDirectoryWrite(DirectoryList::VAR_DIR)
                                    ->openFile($file, 'w+');
        $stream->lock();
        $data = [
            ['sku', 'qty'],
            [$firstProduct['sku'], '10']
        ];
        foreach ($data as $row) {
            $stream->writeCsv($row);
        }
        $stream->unlock();
        $stream->close();

        return $this->_fileFactory->create(
            self::FILE_NAME,
            [
                'type'  => 'filename',
                'value' => $file,
                'rm'    => true
            ],
            DirectoryList::VAR_DIR
        );
    }
}
