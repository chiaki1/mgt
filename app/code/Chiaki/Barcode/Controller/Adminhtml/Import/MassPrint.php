<?php

namespace Chiaki\Barcode\Controller\Adminhtml\Import;

use Chiaki\Barcode\Controller\Adminhtml\Barcode\AbstractPrint;
use Chiaki\Barcode\Helper\Data as HelperData;
use Chiaki\Barcode\Helper\Pdf as PdfHelper;
use Chiaki\Barcode\Helper\Template as TemplateHelper;
use Chiaki\Barcode\Model\System\Config\Source\PaperTemplate;
use Chiaki\Barcode\Model\TemplateFactory;
use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Helper\Js as JsHelper;
use Magento\Catalog\Model\Session;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PrintAction
 *
 * @package Chiaki\Barcode\Controller\Adminhtml\Import
 */
class MassPrint extends AbstractPrint
{
    /**
     * @var JsHelper
     */
    protected $_jsHelper;

    /**
     * @var Session
     */
    protected $_catalogSession;

    /**
     * MassPrint constructor.
     *
     * @param Context               $context
     * @param PdfHelper             $pdfHelper
     * @param TemplateHelper        $templateHelper
     * @param Json                  $json
     * @param HelperData            $helperData
     * @param StoreManagerInterface $storeManager
     * @param PaperTemplate         $paperTemplate
     * @param TemplateFactory       $templateFactory
     * @param JsHelper              $jsHelper
     * @param Session               $catalogSession
     */
    public function __construct(
        Context $context,
        PdfHelper $pdfHelper,
        TemplateHelper $templateHelper,
        Json $json,
        HelperData $helperData,
        StoreManagerInterface $storeManager,
        PaperTemplate $paperTemplate,
        TemplateFactory $templateFactory,
        JsHelper $jsHelper,
        Session $catalogSession
    ) {
        $this->_jsHelper       = $jsHelper;
        $this->_catalogSession = $catalogSession;
        parent::__construct($context, $pdfHelper, $templateHelper, $json, $helperData, $storeManager, $paperTemplate, $templateFactory);
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws Exception
     */
    public function execute()
    {
        $params       = $this->getRequest()->getParams();
        $importCol    = $this->_catalogSession->getBarcodeProducts();
        $customQtyCol = [];
        if (!empty($params['products'])) {
            $newQty = $this->_jsHelper->decodeGridSerializedInput($params['products']);
            foreach ($newQty as $id => $product) {
                $customQtyCol[$id] = $product['qty'];
            }
        }

        $finalQty   = array_replace($importCol, $customQtyCol);
        $productIds = array_keys($finalQty);
        $products   = $this->_helperData->getPrintProducts($productIds, $finalQty, $this->getStoreId());

        try {
            $html = "";

            $templateId = $this->_helperData->getConfigGeneral('default_barcode_template');
            $block      = $this->createBlock('Chiaki\Barcode\Block\Barcode\Container\Template', '', 'Chiaki_Barcode::barcode/print/template.phtml');
            $block->setData('barcodes', $products);
            if ($templateId) {
                $template = $this->_templateFactory->create()->load($templateId);
                $data     = $template->getData();
                $block->setData('template_data', $data);
            } else {
                return $this->errorMessage(self::ERROR_CONFIG_TEMPLATE);
            }
            $html .= $block->toHtml();

            if ($html) {
                return $this->_json->setData([
                                                 'html'    => $html,
                                                 'success' => true
                                             ]
                );
            }

            return $this->errorMessage(self::ERROR_EMPTY_BARCODE);
        } catch (Exception $e) {
            return $this->errorMessage($e->getMessage());
        }
    }
}
