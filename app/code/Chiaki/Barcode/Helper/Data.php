<?php

namespace Chiaki\Barcode\Helper;

use Chiaki\Barcode\Model\System\Config\Source\PaperTemplate;
use Exception;
use Magento\Backend\App\Config;
use Magento\Backend\Model\UrlInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Action as ProductAction;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\State;
use Magento\Framework\Math\Random;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Asset\Repository as AssetRepo;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 *
 * @package Chiaki\Barcode\Helper
 */
class Data extends AbstractHelper
{
    const CONFIG_MODULE_PATH = 'barcode';
    const ATTRIBUTE_CODE     = 'barcode';
    const MODULE_NAME        = 'Chiaki_Barcode';

    /**
     * @var Random
     */
    protected $_random;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var PriceHelper
     */
    protected $_priceHelper;

    /**
     * @var ProductAction
     */
    protected $_productAction;

    /**
     * @var PaperTemplate
     */
    protected $_paperTemplate;

    /**
     * @var AssetRepo
     */
    protected $_assetRepo;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Config
     */
    protected $backendConfig;

    /**
     *
     * @var UrlInterface
     */
    protected $_backendUrlBuilder;

    /**
     * @var Json
     */
    protected $_serializer;

    protected $isArea = [];

    /**
     * Data constructor.
     *
     * @param Context                  $context
     * @param ObjectManagerInterface   $objectManager
     * @param StoreManagerInterface    $storeManager
     * @param ProductCollectionFactory $productCollection
     * @param PriceHelper              $priceHelper
     * @param Random                   $random
     * @param ProductAction            $productAction
     * @param PaperTemplate            $paperTemplate
     * @param AssetRepo                $assetRepo
     * @param UrlInterface             $backendUrlBuilder
     * @param Json                     $serializer
     */
    public function __construct(
        Context                  $context,
        ObjectManagerInterface   $objectManager,
        StoreManagerInterface    $storeManager,
        ProductCollectionFactory $productCollection,
        PriceHelper              $priceHelper,
        Random                   $random,
        ProductAction            $productAction,
        PaperTemplate            $paperTemplate,
        AssetRepo                $assetRepo,
        UrlInterface             $backendUrlBuilder,
        Json                     $serializer
    ) {
        $this->_random                   = $random;
        $this->_productCollectionFactory = $productCollection;
        $this->_priceHelper              = $priceHelper;
        $this->_productAction            = $productAction;
        $this->_paperTemplate            = $paperTemplate;
        $this->_assetRepo                = $assetRepo;
        $this->objectManager             = $objectManager;
        $this->storeManager              = $storeManager;
        $this->_backendUrlBuilder        = $backendUrlBuilder;
        $this->_serializer               = $serializer;
        parent::__construct($context);
    }

    /**
     * /////////////////////////////////////////////////////////
     * GENERAL CONFIGURATION
     * ////////////////////////////////////////////////////////
     */

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getBarcodeType($storeId = null)
    {
        return $this->getModuleConfig('barcode/type', $storeId);
    }

    /**
     * @param $rawValue
     *
     * @return string
     */
    public function toMillimetre($rawValue)
    {
        return ((float)$rawValue) . 'mm';
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelLogo($storeId = null)
    {
        return $this->getBarcodeLabelConfig('logo', $storeId);
    }

    /**
     * @param string $code
     * @param null   $storeId
     *
     * @return array|mixed
     */
    public function getBarcodeLabelConfig($code, $storeId)
    {
        return $this->getModuleConfig('barcode/label/' . $code, $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getBarcodeWidth($storeId = null)
    {
        return $this->getBarcodeLabelConfig('width', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getBarcodeHeight($storeId = null)
    {
        return $this->getBarcodeLabelConfig('height', $storeId);
    }

    /**
     * /////////////////////////////////////////////////////////
     * BARCODE LABEL CONTENT
     * ////////////////////////////////////////////////////////
     */

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getQrCodeSize($storeId = null)
    {
        return $this->getBarcodeLabelConfig('size', $storeId);
    }

    /**
     * @param null $paper
     * @param null $label
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelContent(
        $paper = null,
        $label = null,
        $storeId = null
    ) {
        $template   = $this->checkTemplate($paper, $label);
        $configHtml = $this->getBarcodeLabelConfig('content', $storeId);

        if ($configHtml !== null && $template['label'] === $this->getLabelTemplate()) {
            return $this->getBarcodeLabelConfig('content', $storeId);
        }

        return $this->getDefaultTemplate($template['label'], 'html', $template['paper']);
    }

    /**
     * @param mixed $paper
     * @param mixed $label
     *
     * @return array
     */
    public function checkTemplate($paper, $label)
    {
        return [
            'paper' => $paper === null ? $this->getPaperTemplate() : $paper,
            'label' => $label === null ? $this->getLabelTemplate() : $label,
        ];
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getPaperTemplate($storeId = null)
    {
        return $this->getBarcodePaperConfig('paper_template', $storeId);
    }

    /**
     * @param string $code
     * @param null   $storeId
     *
     * @return array|mixed
     */
    public function getBarcodePaperConfig($code, $storeId)
    {
        return $this->getModuleConfig('barcode/paper/' . $code, $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelTemplate($storeId = null)
    {
        return $this->getBarcodeLabelConfig('template', $storeId);
    }

    /**
     * @param string $label
     * @param string $type
     * @param null   $paper
     *
     * @return bool|string
     */
    public function getDefaultTemplate($label, $type, $paper = null)
    {
        $path         = $paper !== null
            ? self::MODULE_NAME . "::templates/{$paper}/{$label}.{$type}"
            : self::MODULE_NAME . "::templates/{$label}.{$type}";
        $templateFile = $this->_assetRepo->createAsset($path);

        return $templateFile->getContent();
    }

    /**
     * @param null $paper
     * @param null $label
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelCss($paper = null, $label = null, $storeId = null)
    {
        $template  = $this->checkTemplate($paper, $label);
        $configCss = $this->getBarcodeLabelConfig('css', $storeId);

        if ($configCss !== null && $template['label'] === $this->getLabelTemplate()) {
            return $this->getBarcodeLabelConfig('css', $storeId);
        }

        return $this->getDefaultTemplate($template['label'], 'css', $template['paper']);
    }

    /**
     * @param string $paperTemplate
     *
     * @return array
     */
    public function getBasePaperSpecs($paperTemplate)
    {
        $paper                   = $this->_paperTemplate->getTemplateBaseSpecs($paperTemplate);
        $paper['vertical_pitch'] = $paper['height'] + $paper['vertical'];
        $paper['template']       = $paperTemplate;

        $margin           = $this->getPaperMargin($paper['margin']);
        $margin['bottom'] -= $paper['vertical'];
        $margin['left']   -= $paper['horizontal'];
        $paper['margin']  = $margin;

        return $paper;
    }

    /**
     * @param null $inputMargin
     * @param null $storeId
     * @param bool $previewPaper
     *
     * @return array
     */
    public function getPaperMargin($inputMargin = null, $storeId = null, $previewPaper = false)
    {
        $marginData           = [];
        $configMargin         = $this->getBarcodePaperConfig('margin', $storeId);
        $margin               = $inputMargin !== null ? explode(';', $inputMargin) : explode(';', $configMargin);
        $marginData['top']    = $previewPaper ? $margin['0'] : (float)$margin['0'];
        $marginData['right']  = $previewPaper ? $margin['1'] : (float)$margin['1'];
        $marginData['bottom'] = $previewPaper ? $margin['2'] : (float)$margin['2'];
        $marginData['left']   = $previewPaper ? $margin['3'] : (float)$margin['3'];

        return $marginData;
    }

    /**
     * /////////////////////////////////////////////////////////
     * BARCODE PRINTING PAPER
     * ////////////////////////////////////////////////////////
     */

    /**
     * @param null $storeId
     *
     * @return array
     */
    public function getCustomPaper($storeId = null)
    {
        return [
            'width'          => (float)$this->getLabelWidth($storeId),
            'height'         => (float)$this->getLabelHeight($storeId),
            'padding'        => (float)$this->getLabelPadding($storeId),
            'margin'         => $this->getPaperMargin($storeId),
            'vertical'       => (float)$this->getVerticalSpacing($storeId),
            'horizontal'     => (float)$this->getHorizontalSpacing($storeId),
            'vertical_pitch' => (float)$this->getLabelHeight($storeId) + (float)$this->getVerticalSpacing($storeId),
            'size'           => $this->getPaperSize($storeId),
            'orient'         => $this->getPaperOrientation($storeId),
            'template'       => PaperTemplate::CUSTOM,
        ];
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelWidth($storeId = null)
    {
        return $this->getBarcodePaperConfig('width', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelHeight($storeId = null)
    {
        return $this->getBarcodePaperConfig('height', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getLabelPadding($storeId = null)
    {
        return $this->getBarcodePaperConfig('padding', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getVerticalSpacing($storeId = null)
    {
        return $this->getBarcodePaperConfig('vertical', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getHorizontalSpacing($storeId = null)
    {
        return $this->getBarcodePaperConfig('horizontal', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getPaperSize($storeId = null)
    {
        return $this->getBarcodePaperConfig('paper_size', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return array|mixed
     */
    public function getPaperOrientation($storeId = null)
    {
        return $this->getBarcodePaperConfig('orientation', $storeId);
    }

    /**
     * @param mixed $productIds
     * @param mixed $qty
     * @param int   $storeId
     *
     * @return array
     */
    public function getPrintProducts($productIds, $qty, $storeId = 0)
    {
        $data = [];

        if (!empty($productIds)) {
            $qty = is_array($qty) ? array_sum($qty) / count($productIds) : $qty;
        }

        $productCollection = $this->_productCollectionFactory->create()
                                                             ->addAttributeToSelect('*');

        if (is_array($productIds)) {
            $products = $productCollection->addAttributeToFilter('entity_id', $productIds);
        }

        if (is_int($productIds)) {
            $products = $productCollection->setPageSize($productIds);
        }

        /** @var ProductCollection $products */
        foreach ($products as $product) {
            $data[] = $this->prepareProductData($product, $qty, $storeId);
        }

        return $data;
    }

    /**
     * @param Product $product
     * @param mixed   $qty
     * @param int     $storeId
     *
     * @return array
     */
    public function prepareProductData($product, $qty, $storeId)
    {
        $barcodeAttr  = $this->getAttributeBarcode($storeId);
        $barcodeValue = $product->getDataByKey($barcodeAttr);

        if ($barcodeAttr === self::ATTRIBUTE_CODE && $barcodeValue === null) {
            $barcodeValue = $this->generateBarcode();
            $this->_productAction->updateAttributes(
                [$product->getId()],
                [self::ATTRIBUTE_CODE => $barcodeValue],
                $storeId
            );
        }

        return [
            'id'      => $product->getId(),
            'name'    => $product->getName(),
            'price'   => $this->_priceHelper->currency($product->getFinalPrice()),
            'barcode' => $barcodeValue,
            'sku'     => $product->getSku(),
            'qty'     => $qty
        ];
    }

    /**
     * @param $storeId
     *
     * @return mixed
     */
    public function getAttributeBarcode($storeId = null)
    {
        return $this->getConfigGeneral('attribute_barcode', $storeId);
    }

    /**
     * @param null $storeId
     *
     * @return string|string[]|null
     */
    public function generateBarcode($storeId = null)
    {
        $pattern       = $this->getBarcodeNumberTemplate($storeId);
        $patternString = '/\[([1-9]|1[\d]|2[0-5])([AN]{1,2})\]/';
        $code          = preg_replace_callback(
            $patternString,
            function ($param) {
                $pool = strpos($param[2], 'A') === false ? '' : Random::CHARS_UPPERS;
                $pool .= strpos($param[2], 'N') === false ? '' : Random::CHARS_DIGITS;

                return $this->_random->getRandomString($param[1], $pool);
            },
            $pattern
        );

        return $code;
    }

    /**
     * /////////////////////////////////////////////////////////
     * PRODUCT DATA
     * ////////////////////////////////////////////////////////
     */

    /**
     * @param $storeId
     *
     * @return mixed
     */
    public function getBarcodeNumberTemplate($storeId = null)
    {
        return $this->getConfigGeneral('barcode_template', $storeId);
    }

    /**
     * @return array
     */
    public function getProductIdsForBarcode()
    {
        $isRegenerate      = $this->isRegenerate();
        $productCollection = $this->_productCollectionFactory->create();
        if ($isRegenerate) {
            return $productCollection->getAllIds();
        }

        return $productCollection->addAttributeToFilter(self::ATTRIBUTE_CODE, ['null' => true], 'left')->getAllIds();
    }

    /**
     * @return mixed
     */
    public function isRegenerate()
    {
        return $this->getConfigGeneral('btn_generate');
    }

    /**
     * @param string $field
     * @param null   $storeId
     *
     * @return mixed
     */
    public function getModuleConfig($field = '', $storeId = null)
    {
        $field = ($field !== '') ? '/' . $field : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . $field, $storeId);
    }

    /**
     * @param string $code
     * @param null   $storeId
     *
     * @return mixed
     */
    public function getConfigGeneral($code = '', $storeId = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/general' . $code, $storeId);
    }

    /**
     * @param        $field
     * @param null   $scopeValue
     * @param string $scopeType
     *
     * @return array|mixed
     */
    public function getConfigValue($field, $scopeValue = null, $scopeType = ScopeInterface::SCOPE_STORE)
    {
        if (!$this->isArea() && is_null($scopeValue)) {
            /** @var Config $backendConfig */
            if (!$this->backendConfig) {
                $this->backendConfig = $this->objectManager->get('Magento\Backend\App\ConfigInterface');
            }

            return $this->backendConfig->getValue($field);
        }

        return $this->scopeConfig->getValue($field, $scopeType, $scopeValue);
    }

    /**
     * Is Admin Store
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->isArea(Area::AREA_ADMINHTML);
    }

    /**
     * @param string $area
     *
     * @return mixed
     */
    public function isArea($area = Area::AREA_FRONTEND)
    {
        if (!isset($this->isArea[$area])) {
            /** @var State $state */
            $state = $this->objectManager->get('Magento\Framework\App\State');

            try {
                $this->isArea[$area] = ($state->getAreaCode() == $area);
            } catch (Exception $e) {
                $this->isArea[$area] = false;
            }
        }

        return $this->isArea[$area];
    }

    /**
     * @param null $storeId
     *
     * @return bool
     */
    public function isEnabled($storeId = null)
    {
        return $this->getConfigGeneral('enabled', $storeId);
    }

    /**
     * @param       $path
     * @param array $params
     *
     * @return mixed
     */
    public function getBackendUrl($path, $params = [])
    {
        return $this->_backendUrlBuilder->getUrl($path, $params);
    }

    public function getDefaultSkuPreview()
    {
        return $this->getConfigGeneral('sku_preview_template');
    }

    public function getDefaultProductId()
    {
        if ($this->getDefaultSkuPreview()) {
            $product = $this->_productCollectionFactory->create()->addFieldToFilter('sku', $this->getDefaultSkuPreview())->getFirstItem();
            if (!$product) {
                $product = $this->_productCollectionFactory->create()->addFieldToFilter('type_id', 'simple')->getLastItem();
            }
        } else {
            $product = $this->_productCollectionFactory->create()->addFieldToFilter('type_id', 'simple')->getLastItem();
        }

        if ($product) {
            return $product->getData('entity_id');
        }
        return 1;
    }

    /**
     * @return array
     */
    public function getSortOrder()
    {
        $value = $this->getConfigGeneral('sort_order');
        if ($value) {
            $value = $this->_serializer->unserialize($value);
        }
        if (!$value) {
            $value = [];
        }

        return $value;
    }
}
