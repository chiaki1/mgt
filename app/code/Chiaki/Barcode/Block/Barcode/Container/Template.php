<?php

namespace Chiaki\Barcode\Block\Barcode\Container;

use Chiaki\Barcode\Model\Source\MeasurementUnit;
use Chiaki\Barcode\Model\Source\TemplateType;

class Template extends \Chiaki\Barcode\Block\Barcode\Container
{

    public function getBarcodes()
    {
        $barcodes = [];
        $datas    = $this->getData('barcodes');
        if ($datas) {
            $template = $this->getTemplateData();
            foreach ($datas as $data) {
                if (empty($data['qty'])) {
                    $data['qty'] = $template['label_per_row'];
                }
                for ($i = 1; $i <= $data['qty']; $i++) {
                    $barcodes[] = $this->getBarcodeSource($data);
                }
            }
        }
        return $barcodes;
    }

    /**
     * @return string
     */
    public function getBarcodeSource($data)
    {
        $source = "";
        if ($data) {
            $template        = $this->getTemplateData();
            $type            = $template['symbology'];
            $barcodeOptions  = array('text'     => $data['barcode'],
                                     'fontSize' => $template['font_size'],
                                     'barHeight' => $template['barcode_height'] ?? 50,
            );
            $rendererOptions = array(
                //'width' => '198',
                'height'    => '0',
                'imageType' => 'png'
            );
            $source          = \Zend\Barcode\Barcode::factory(
                $type, 'image', $barcodeOptions, $rendererOptions
            );

            if (isset($template['product_attribute_show_on_barcode'])) {
                $attributeDatas = $this->getAttributeSoucre($data, $template['product_attribute_show_on_barcode']);
            } else {
                $attributeDatas = array();
            }
        }
        $result['attribute_data'] = $attributeDatas;
        $result['barcode_source'] = $source;
        return $result;
    }

    public function getAttributeSoucre($data, $attributes)
    {
        if (!isset($data['id'])) {
            $product_id = $this->helper->getDefaultProductId();
        } else {
            $product_id = $data['id'];
        }
        $attributeArray = array();
        if ($product_id && $attributes && $attributes != '') {
            if (is_array($attributes)) {
                $array = $attributes;
            } else {
                $array = explode(',', $attributes);
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            /** @var \Magento\Catalog\Model\Product $product */
            $prod = $objectManager->get('Magento\Catalog\Model\Product')->load($product_id);
            $sortAttribute = $this->helper->getSortOrder();
            if (!empty($sortAttribute)) {
                foreach ($sortAttribute as $code => $attributeName) {
                    foreach ($array as $key) {
                        if ($code == $key) {
                            if ($key && ($text = ($prod->getData($key)))) {
                                if (($key === 'sku') || ($key === 'name')) {
                                    $attributeArray[] = (is_numeric($text) ? (int)$text : $text);
                                } elseif (($key === 'price')) {
                                    $price            = $text;
                                    $attributeArray[] = '<strong>' . $this->priceHelper->currency(round($price, 2),true,false) . '</strong>';
                                } else {
                                    $attr = $prod->getResource()->getAttribute($key);
                                    if ($attr->usesSource()) {
                                        $optionText = $attr->getSource()->getOptionText($text);
                                        $attributeArray[] = '[' . $key . '] ' . $optionText;
                                    } else {
                                        $attributeArray[] = '[' . $key . '] ' . (is_numeric($text) ? (int)$text : $text);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($array as $key) {
                    if ($key && ($text = ($prod->getData($key)))) {
                        if (($key === 'sku') || ($key === 'name')) {
                            $attributeArray[] = (is_numeric($text) ? (int)$text : $text);
                        } elseif (($key === 'price')) {
                            $price            = $text;
                            $attributeArray[] = '<strong>' . $this->priceHelper->currency(round($price, 2),true,false) . '</strong>';
                        } else {
                            $attr = $prod->getResource()->getAttribute($key);
                            if ($attr->usesSource()) {
                                $optionText = $attr->getSource()->getOptionText($text);
                                $attributeArray[] = '[' . $key . '] ' . $optionText;
                            } else {
                                $attributeArray[] = '[' . $key . '] ' . (is_numeric($text) ? (int)$text : $text);
                            }
                        }
                    }
                }
            }
        }
        return $attributeArray;

    }

    /**
     * @return string
     */
    public function getTemplateData()
    {
        $data = [];
        if ($this->getData('template_data')) {
            $data = $this->getData('template_data');
        }
        if (empty($data['font_size'])) {
            $data['font_size'] = '24';
        }
        if (empty($data['label_per_row'])) {
            $data['label_per_row'] = '1';
        }
        if (empty($data['measurement_unit'])) {
            $data['measurement_unit'] = MeasurementUnit::MM;
        }
        if (empty($data['paper_height'])) {
            $data['paper_height'] = '30';
        }
        if (empty($data['paper_width'])) {
            $data['paper_width'] = '100';
        }
        if (empty($data['label_height'])) {
            $data['label_height'] = '30';
        }
        if (empty($data['label_width'])) {
            $data['label_width'] = '100';
        }
        if (empty($data['left_margin'])) {
            $data['left_margin'] = '0';
        }
        if (empty($data['right_margin'])) {
            $data['right_margin'] = '0';
        }
        if (empty($data['bottom_margin'])) {
            $data['bottom_margin'] = '0';
        }
        if (empty($data['top_margin'])) {
            $data['top_margin'] = '0';
        }
        if (empty($data['top_padding'])) {
            $data['top_padding'] = '0';
        }
        return $data;
    }

    /**
     * @return bool
     */
    public function isJewelry()
    {
        $template = $this->getTemplateData();
        return ($template['type'] == TemplateType::JEWELRY) ? true : false;
    }
}
