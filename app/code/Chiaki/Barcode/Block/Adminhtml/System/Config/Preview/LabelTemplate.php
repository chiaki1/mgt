<?php

namespace Chiaki\Barcode\Block\Adminhtml\System\Config\Preview;

use Chiaki\Barcode\Helper\Data as HelperData;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class LabelTemplate
 *
 * @package Chiaki\Barcode\Block\Adminhtml\System\Config\Preview
 */
class LabelTemplate extends Field
{
    /**
     * @var string
     */
    protected $_template = 'system/config/preview/label.phtml';

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * LabelTemplate constructor.
     *
     * @param Context    $context
     * @param HelperData $helperData
     * @param array      $data
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        array $data = []
    ) {
        $this->_helperData = $helperData;
        parent::__construct($context, $data);
    }

    /**
     * Unset scope
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope();

        return parent::render($element);
    }

    /**
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
