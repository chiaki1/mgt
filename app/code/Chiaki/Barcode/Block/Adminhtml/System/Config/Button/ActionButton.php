<?php

namespace Chiaki\Barcode\Block\Adminhtml\System\Config\Button;

use Chiaki\Barcode\Helper\Data as HelperData;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class ActionButton
 *
 * @package Chiaki\Barcode\Block\Adminhtml\System\Config\Button
 */
class ActionButton extends Field
{
    /**
     * @var string
     */
    protected $_template = 'system/config/button/actionbutton.phtml';

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * ActionButton constructor.
     *
     * @param Context    $context
     * @param HelperData $helperData
     * @param array      $data
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        array $data = []
    ) {
        $this->_helperData = $helperData;

        parent::__construct($context, $data);
    }

    /**
     * Unset scope
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope();

        return parent::render($element);
    }

    /**
     * @return bool
     */
    public function isRegenerate()
    {
        return $this->_helperData->isRegenerate() === '1';
    }

    /**
     * Get the button and scripts contents
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $this->addData($this->getButtonData($element));

        return $this->_toHtml();
    }

    /**
     * @param AbstractElement $element
     *
     * @return array
     */
    public function getButtonData($element)
    {
        $systemData = $element->getOriginalData();

        return [
            'button_label' => __($systemData['button_label']),
            'button_url'   => $this->getUrl($systemData['button_url']),
            'html_id'      => $element->getHtmlId(),
            'button_type'  => ($systemData['button_label'] === 'RUN') ? 'basic' : 'primary',
        ];
    }
}
