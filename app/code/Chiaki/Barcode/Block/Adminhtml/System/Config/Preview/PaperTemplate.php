<?php

namespace Chiaki\Barcode\Block\Adminhtml\System\Config\Preview;

/**
 * Class PaperTemplate
 *
 * @package Chiaki\Barcode\Block\Adminhtml\System\Config\Preview
 */
class PaperTemplate extends LabelTemplate
{
    /**
     * @var string
     */
    protected $_template = 'system/config/preview/paper.phtml';
}
