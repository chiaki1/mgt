<?php

namespace Chiaki\Barcode\Block\Adminhtml;

use Chiaki\Barcode\Model\Source\Attributes;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Chiaki\Barcode\Helper\Data;

class Position extends Field
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Attributes
     */
    private $attributes;

    public function __construct(
        Data $helper,
        Attributes $attributes,
        Context $context
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->attributes = $attributes;
    }

    protected function _construct()
    {
        $this->setTemplate('Chiaki_Barcode::/position.phtml');
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $this->setElement($element);

        return $this->_toHtml();
    }

    /**
     * @return array
     */
    public function getPositions()
    {
        $positions =  (array) $this->helper->getSortOrder();
        if ($positions === []) {
            $positions = $this->getOptionalArray();
        }

        return $positions;
    }

    /**
     * @param $index
     * @return string
     */
    public function getNamePrefix($index)
    {
        return $this->getElement()->getName() . '[' . $index . ']';
    }

    private function getOptionalArray()
    {
        $positions = [];
        $methods = $this->attributes->toOptionArray();
        foreach ($methods as $method) {
            $positions[$method['value']] = $method['label'];
        }

        return $positions;
    }
}
