<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode;

use Magento\Backend\Block\Widget\Form\Container;

/**
 * Class AbstractContainer
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode
 */
abstract class AbstractContainer extends Container
{
    /**
     * @return string
     */
    abstract public function getFormId();

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->buttonList->remove('back');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');

        $this->_blockGroup = 'Chiaki_Barcode';
    }
}
