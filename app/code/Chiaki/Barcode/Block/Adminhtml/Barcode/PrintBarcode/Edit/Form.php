<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode\PrintBarcode\Edit;

use Chiaki\Barcode\Helper\Data as HelperData;
use Chiaki\Barcode\Model\Source\Template;
use Chiaki\Barcode\Model\System\Config\Source\BarcodeType;
use Chiaki\Barcode\Model\System\Config\Source\LabelTemplate;
use Chiaki\Barcode\Model\System\Config\Source\PaperSize;
use Chiaki\Barcode\Model\System\Config\Source\PaperTemplate;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;

/**
 * Class Form
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode\PrintBarcode\Edit
 */
class Form extends Generic
{
    /**
     * @var Yesno
     */
    protected $_yesNo;

    /**
     * @var BarcodeType
     */
    protected $_barcodeType;

    /**
     * @var LabelTemplate
     */
    protected $_labelTemplate;

    /**
     * @var PaperTemplate
     */
    protected $_paperTemplate;

    /**
     * @var PaperSize
     */
    protected $_paperSize;

    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var Template
     */
    protected $_barcodeTemplate;

    /**
     * Form constructor.
     *
     * @param Context       $context
     * @param Registry      $registry
     * @param FormFactory   $formFactory
     * @param Yesno         $yesNo
     * @param BarcodeType   $barcodeType
     * @param LabelTemplate $labelTemplate
     * @param PaperTemplate $PaperTemplate
     * @param PaperSize     $paperSize
     * @param HelperData    $helperData
     * @param Template      $barcodeTemplate
     * @param array         $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Yesno $yesNo,
        BarcodeType $barcodeType,
        LabelTemplate $labelTemplate,
        PaperTemplate $PaperTemplate,
        PaperSize $paperSize,
        HelperData $helperData,
        Template $barcodeTemplate,
        array $data = []
    ) {
        $this->_yesNo           = $yesNo;
        $this->_barcodeType     = $barcodeType;
        $this->_labelTemplate   = $labelTemplate;
        $this->_paperTemplate   = $PaperTemplate;
        $this->_paperSize       = $paperSize;
        $this->_helperData      = $helperData;
        $this->_barcodeTemplate = $barcodeTemplate;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return Generic
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create([
                                                'data' => [
                                                    'id'     => 'barcode_print_modal_form',
                                                    'action' => $this->getUrl(
                                                        'barcode/product/massprint',
                                                        ['form_key' => $this->getFormKey()]
                                                    ),
                                                    'method' => 'post',
                                                ],
                                            ]
        );

        $form->setHtmlIdPrefix('barcode_');
        $form->setFieldNameSuffix('barcode');

        $qtyFieldset = $form->addFieldset('barcode_qty_fieldset', ['class' => 'fieldset-wide']);
        $qtyFieldset->addField(
            'qty',
            'text',
            [
                'name'     => 'qty',
                'label'    => __('Quantity'),
                'title'    => __('Quantity'),
                'class'    => 'validate-number validate-digits',
                'required' => true,
                'value'    => 50,
                'note'     => __('Print quantity is recommended to be less than 200 barcode labels.')
            ]
        );

        $qtyFieldset->addField(
            'barcode_type',
            'select',
            [
                'name'     => 'barcode_type',
                'label'    => __('Barcode Template Type'),
                'title'    => __('Barcode Template Type'),
                'disabled' => false,
                'values'   => $this->_barcodeTemplate->toOptionArray(),
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
