<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode\Element;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class AbstractButton
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode\Element
 */
abstract class AbstractButton extends AbstractElement
{
    const PREVIEW_BUTTON = 'Preview Paper Template';
    const CHECK_BUTTON   = 'Check and Show';

    /**
     * @return string
     */
    public function getElementHtml()
    {
        $buttonName = $this->getButtonName();
        $buttonId   = $buttonName === self::CHECK_BUTTON ? 'barcode_check_btn' : 'barcode_preview_paper_btn';

        $buttonHtml = '<div class="pp-buttons-container">';
        $buttonHtml .= '<button type="button" ';
        $buttonHtml .= "id='{$buttonId}' ";
        $buttonHtml .= 'class="action-default save primary">';
        $buttonHtml .= $buttonName === self::CHECK_BUTTON ? __('Check and Show') : __('Preview Paper Template');
        $buttonHtml .= '</button></div>';

        return $buttonHtml;
    }

    /**
     * @return string
     */
    abstract public function getButtonName();
}
