<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode\Element;

/**
 * Class CheckButton
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode\Element
 */
class CheckButton extends AbstractButton
{
    /**
     * @return string
     */
    public function getButtonName()
    {
        return self::CHECK_BUTTON;
    }
}
