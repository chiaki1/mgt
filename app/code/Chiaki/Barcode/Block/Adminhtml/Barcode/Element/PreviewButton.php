<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode\Element;

/**
 * Class PreviewPaper
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode\Element
 */
class PreviewButton extends AbstractButton
{
    /**
     * @return string
     */
    public function getButtonName()
    {
        return self::PREVIEW_BUTTON;
    }
}
