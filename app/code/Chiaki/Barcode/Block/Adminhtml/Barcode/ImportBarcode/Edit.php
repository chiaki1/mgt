<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode\ImportBarcode;

use Chiaki\Barcode\Block\Adminhtml\Barcode\AbstractContainer;

/**
 * Class Edit
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode\ImportBarcode
 */
class Edit extends AbstractContainer
{
    /**
     * @return string
     */
    public function getFormId()
    {
        return 'barcode_import_modal_form';
    }

    /**
     * Internal constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_controller = 'adminhtml_barcode_importBarcode';
    }
}
