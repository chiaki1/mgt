<?php

namespace Chiaki\Barcode\Block\Adminhtml\Barcode\ImportBarcode\Edit;

use Chiaki\Barcode\Block\Adminhtml\Barcode\Element\CheckButton;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Form
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Barcode\ImportBarcode\Edit
 */
class Form extends Generic
{
    /**
     * @return Generic|void
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id'      => 'barcode_import_modal_form',
                    'action'  => $this->getUrl(
                        'barcode/import/validate',
                        ['form_key' => $this->getFormKey()]
                    ),
                    'method'  => 'post',
                    'enctype' => 'multipart/form-data',
                ],
            ]
        );

        $form->setHtmlIdPrefix('barcode_');
        $form->setFieldNameSuffix('barcode');

        $importFieldSet = $form->addFieldset('barcode_import_fieldset', ['class' => 'fieldset-wide']);
        $importFieldSet->addField(
            'import',
            'file',
            [
                'name'              => 'import',
                'label'             => __('Import'),
                'title'             => __('Import'),
                'class'             => 'required-entry',
                'required'          => true,
                'note'              => $this->getInputNote(),
                'allowedExtensions' => ['csv'],
            ]
        )->setAfterElementHtml($this->getDownloadSampleFileHtml());

        $importFieldSet->addType('checkButton', CheckButton::class);
        $importFieldSet->addField('check_button', 'checkButton', [
            'name'  => '',
            'label' => ''
        ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * @return string
     */
    public function getInputNote()
    {
        $html = '<span>' . __('Allow csv file type.') . '</span><br>';
        $html .= '<a href="http://docs.mageplaza.com/barcode/" target="_blank" rel="noopener noreferrer">';
        $html .= __('Learn more ');
        $html .= '</a>';

        return $html;
    }

    /**
     * @return string
     */
    public function getDownloadSampleFileHtml()
    {
        $downloadUrl = $this->getUrl('barcode/import/download');

        return "<a href='{$downloadUrl}'>" . __('Download sample file') . '</a>';
    }
}
