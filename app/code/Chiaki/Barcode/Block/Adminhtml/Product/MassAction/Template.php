<?php

namespace Chiaki\Barcode\Block\Adminhtml\Product\MassAction;

use Chiaki\Barcode\Block\Adminhtml\Product\Form\PaperPreview;
use Magento\Backend\Block\Template as BlockTemplate;
use Magento\Backend\Block\Template\Context;

/**
 * Class Template
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Product\MassAction
 */
class Template extends BlockTemplate
{
    /**
     * @var PaperPreview
     */
    protected $_paperPreview;

    /**
     * @var string
     */
    protected $_template = 'Chiaki_Barcode::product/massaction/form.phtml';

    /**
     * Template constructor.
     *
     * @param Context      $context
     * @param PaperPreview $paperPreview
     * @param array        $data
     */
    public function __construct(
        Context $context,
        PaperPreview $paperPreview,
        array $data = []
    ) {
        $this->_paperPreview = $paperPreview;

        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->_paperPreview->getAjaxUrl();
    }
}
