<?php

namespace Chiaki\Barcode\Block\Adminhtml\Product\Import;

use Magento\Backend\Block\Template as BlockTemplate;

/**
 * Class Template
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Product\Import
 */
class Template extends BlockTemplate
{
    /**
     * @var string
     */
    protected $_template = 'Chiaki_Barcode::product/import/form.phtml';

    /**
     * @param $finalController
     *
     * @return string
     */
    public function getAjaxUrl($finalController)
    {
        return $this->getUrl("barcode/import/{$finalController}", ['form_key' => $this->getFormKey()]);
    }
}
