<?php

namespace Chiaki\Barcode\Block\Adminhtml\Product\Import;

use Magento\Backend\Block\Widget\Container;

/**
 * Class Button
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Product\Import
 */
class Button extends Container
{
    /**
     * @return Container
     */
    protected function _prepareLayout()
    {
        $buttonData = [
            'id'             => 'barcode_import_btn',
            'label'          => __('Print Barcode'),
            'class'          => 'add action-secondary',
            'button_class'   => 'primary',
            'data_attribute' => [
                'mage-init' => [
                    'Magento_Ui/js/form/button-adapter' => [
                        'actions' => [
                            [
                                'targetName' => 'product_listing.product_listing.listing_top.listing_massaction',
                                'actionName' => 'toggleImportBarcodeModal'
                            ],
                        ]
                    ]
                ]
            ],
        ];
        $this->buttonList->add('barcode_import_btn', $buttonData);

        return parent::_prepareLayout();
    }
}
