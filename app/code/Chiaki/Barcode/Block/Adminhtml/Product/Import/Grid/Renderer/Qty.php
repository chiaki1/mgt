<?php

namespace Chiaki\Barcode\Block\Adminhtml\Product\Import\Grid\Renderer;

use Chiaki\Barcode\Block\Adminhtml\Product\Import\Grid\Grid as ImportGrid;
use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\Input as InputRenderer;
use Magento\Framework\DataObject;

/**
 * Class Qty
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Product\Import\Grid\Renderer
 */
class Qty extends InputRenderer
{
    /**
     * @var ImportGrid
     */
    protected $_importGrid;

    /**
     * Qty constructor.
     *
     * @param Context    $context
     * @param ImportGrid $importGrid
     * @param array      $data
     */
    public function __construct(
        Context $context,
        ImportGrid $importGrid,
        array $data = []
    ) {
        $this->_importGrid = $importGrid;

        parent::__construct($context, $data);
    }

    /**
     * @param DataObject $row
     *
     * @return string
     */
    public function render(DataObject $row)
    {
        $products  = $this->_importGrid->getBarcodeProductCollection();
        $productId = $row->getData('entity_id');
        $qty       = isset($products[$productId]) ? $products[$productId] : 1;

        $html = '<input type="text" ';
        $html .= 'name="' . $this->getColumn()->getId() . '" ';
        $html .= 'value="' . $qty . '" ';
        $html .= 'class="input-text admin__control-text"/>';

        return $html;
    }
}
