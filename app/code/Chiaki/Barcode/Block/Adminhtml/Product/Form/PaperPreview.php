<?php

namespace Chiaki\Barcode\Block\Adminhtml\Product\Form;

use Magento\Backend\Block\Template;

/**
 * Class PaperPreview
 *
 * @package Chiaki\Barcode\Block\Adminhtml\Product\Form
 */
class PaperPreview extends Template
{
    /**
     * @var string
     */
    protected $_template = 'product/form/paper.phtml';

    /**
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('barcode/barcode/previewpaper');
    }
}
