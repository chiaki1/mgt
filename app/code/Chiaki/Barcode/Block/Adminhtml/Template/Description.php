<?php

namespace Chiaki\Barcode\Block\Adminhtml\Template;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;

class Description extends Template
{
    /**
     * Description constructor.
     *
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->setTemplate('Chiaki_Barcode::template/description.phtml');
    }

    /**
     *
     * @return string
     */
    public function getDescriptionImage()
    {
        return $this->getViewFileUrl('Chiaki_Barcode::image/description.png');
    }
}
