<?php

namespace Chiaki\Barcode\Block\Adminhtml\Template\Edit\Button;

/**
 * Class Save
 */
class Delete extends Generic
{
    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        $id = $this->getParam('id', false);
        if (!$id) {
            return [];
        }
        return [
            'label' => __('Delete'),
            'class' => 'save primary',
            'url'   => $this->getUrl('*/*/delete', ['id' => $id])
        ];
    }
}
