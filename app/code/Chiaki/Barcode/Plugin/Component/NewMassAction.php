<?php

namespace Chiaki\Barcode\Plugin\Component;

use Magento\Catalog\Ui\Component\Product\MassAction as ProductMassAction;

/**
 * Class NewMassAction
 *
 * @package Chiaki\Barcode\Plugin\Component
 */
class NewMassAction extends MassAction
{
    /**
     * @param ProductMassAction $subject
     */
    public function afterPrepare(ProductMassAction $subject)
    {
        if ($this->_helperData->isEnabled() && $this->_request->getFullActionName() === 'catalog_product_index') {
            $this->addBarcodeMassAction($subject);
        }
    }
}
