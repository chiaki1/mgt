<?php

namespace Chiaki\Barcode\Plugin\Component;

use Chiaki\Barcode\Helper\Data as HelperData;
use Magento\Framework\App\RequestInterface;
use Magento\Ui\Component\AbstractComponent;

/**
 * Class MassAction
 *
 * @package Chiaki\Barcode\Plugin\Component
 */
abstract class MassAction
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * MassAction constructor.
     *
     * @param HelperData       $_helperData
     * @param RequestInterface $request
     */
    public function __construct(
        HelperData $_helperData,
        RequestInterface $request
    ) {
        $this->_helperData = $_helperData;
        $this->_request    = $request;
    }

    /**
     * @param AbstractComponent $subject
     */
    public function addBarcodeMassAction($subject)
    {
        $config = $subject->getData('config');
        if (isset($config['actions'])) {
            $config['actions'][] = [
                'component' => 'uiComponent',
                'type'      => 'barcode_print',
                'label'     => 'Print Barcode Label',
                'callback'  => [
                    'provider' => 'product_listing.product_listing.listing_top.listing_massaction',
                    'target'   => 'togglePrintBarcodeModal',
                ]
            ];
        }

        $subject->setData('config', $config);
    }
}
