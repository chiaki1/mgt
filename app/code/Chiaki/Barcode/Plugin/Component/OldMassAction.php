<?php

namespace Chiaki\Barcode\Plugin\Component;

use Magento\Ui\Component\MassAction as ComponentMassAction;

/**
 * Class OldMassAction
 *
 * @package Chiaki\Barcode\Plugin\Component
 */
class OldMassAction extends MassAction
{
    /**
     * @param ComponentMassAction $subject
     */
    public function afterPrepare(ComponentMassAction $subject)
    {
        if ($this->_helperData->isEnabled() && $this->_request->getFullActionName() === 'catalog_product_index') {
            $this->addBarcodeMassAction($subject);
        }
    }
}
