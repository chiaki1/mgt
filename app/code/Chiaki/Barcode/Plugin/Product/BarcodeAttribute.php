<?php

namespace Chiaki\Barcode\Plugin\Product;

use Chiaki\Barcode\Helper\Data as HelperData;
use Magento\Catalog\Ui\DataProvider\Product\Form\ProductDataProvider;

/**
 * Class BarcodeAttribute
 *
 * @package Chiaki\Barcode\Plugin\Product
 */
class BarcodeAttribute
{
    /**
     * @var HelperData
     */
    protected $_helperData;

    /**
     * MassAction constructor.
     *
     * @param HelperData $_helperData
     */
    public function __construct(HelperData $_helperData)
    {
        $this->_helperData = $_helperData;
    }

    /**
     * @param ProductDataProvider $subject
     * @param                     $result
     *
     * @return mixed
     */
    public function afterGetMeta(ProductDataProvider $subject, $result)
    {
        if (!$this->_helperData->isEnabled()) {
            unset($result['product-details']['children']['container_barcode']);
        }

        return $result;
    }
}
