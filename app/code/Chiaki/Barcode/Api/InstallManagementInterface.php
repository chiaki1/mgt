<?php

namespace Chiaki\Barcode\Api;


interface InstallManagementInterface
{

    /**
     * Create default barcode template
     *
     * @return \Chiaki\Barcode\Api\InstallManagementInterface
     */
    public function createBarcodeTemplate();
}
