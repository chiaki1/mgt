<?php


namespace Chiaki\CmsGraphql\Model\Resolver;


use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\Data\BlockInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\Store;

class Block implements ResolverInterface
{
    /**
     * @var \Magento\PageBuilder\Model\Filter\Template
     */
    private $template;
    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;

    public function __construct(
        BlockRepositoryInterface $blockRepository,
        \Magento\PageBuilder\Model\Filter\Template $template
    )
    {
        $this->template = $template;
        $this->blockRepository = $blockRepository;
    }

    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null): array
    {
        $storeId = (int)$context->getExtensionAttributes()->getStore()->getId();
        if (!isset($args['userId'])) {
            throw new GraphQlInputException(__('"Page userId should be specified'));
        }
        $blockIdentifier = $this->getBlockIdentifier($args);
        return $this->getBlocksData($blockIdentifier, $args['userId'], $storeId);
    }

    /**
     * Get block identifiers
     *
     * @param array $args
     * @return string
     * @throws GraphQlInputException
     */
    private function getBlockIdentifier(array $args): string
    {
        if (!isset($args['identifier']) || !is_string($args['identifier'])) {
            throw new GraphQlInputException(__('"identifier" of CMS blocks should be specified'));
        }

        return $args['identifier'];
    }

    /**
     * Get blocks data
     *
     * @param string $identifier
     * @param int $storeId
     * @return array
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getBlocksData(string $identifier, $userId, int $storeId): array
    {

        /** @var SearchCriteriaBuilder $searchCriteria */
        $searchCriteria = ObjectManager::getInstance()->create(SearchCriteriaBuilder::class);

        /** @var \Magento\Framework\Api\SortOrder $sortOrder */
        $sortOrder = ObjectManager::getInstance()->create(\Magento\Framework\Api\SortOrder::class);
        $sortOrder->setField('user_id')->setDirection('ASC');

        $searchCriteria = $searchCriteria->addFilter('identifier', $identifier)
            ->addFilter('user_id', [$userId, 'default'], 'in')
            ->addFilter(Store::STORE_ID, [$storeId, Store::DEFAULT_STORE_ID], 'in')
            ->addFilter(BlockInterface::IS_ACTIVE, true)
            ->setSortOrders([$sortOrder])->create();

        $blockResults = $this->blockRepository->getList($searchCriteria)->getItems();

        if (empty($blockResults)) {
            throw new NoSuchEntityException(
                __('The CMS block with the "%1" ID doesn\'t exist.', $identifier)
            );
        }

        $block = current($blockResults);
        $renderedContent = $this->template->filter($block->getContent());

        return [
            'user_id' => $block->getData('user_id'),
            BlockInterface::BLOCK_ID => $block->getId(),
            BlockInterface::IDENTIFIER => $block->getIdentifier(),
            BlockInterface::TITLE => $block->getTitle(),
            BlockInterface::CONTENT => $renderedContent,
        ];
    }
}
