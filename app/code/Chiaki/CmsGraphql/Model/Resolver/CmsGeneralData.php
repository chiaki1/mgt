<?php
/**
 *
 * @author Khoi Le - mr.vjcspy@gmail.com
 * @time 8/25/20 3:11 PM
 *
 */

namespace Chiaki\CmsGraphql\Model\Resolver;


use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\StoreManagerInterface;

class CmsGeneralData implements ResolverInterface
{

    /**
     * @var \Magento\Theme\Block\Html\Header\Logo
     */
    private $logo;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        \Magento\Theme\Block\Html\Header\Logo $logo,
        StoreManagerInterface $storeManager
    )
    {
        $this->logo = $logo;
        $this->storeManager = $storeManager;
    }

    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        try {
            $this->storeManager->setCurrentStore($args['store_id']);
        } catch (\Exception $exception) {
            throw new GraphQlInputException(__($exception->getMessage()));
        }

        return [
            "store_id" => $args['store_id'],
            "logo_url" => $this->logo->getLogoSrc(),
            "logo_alt" => $this->logo->getLogoAlt(),
            "logo_width" => $this->logo->getLogoWidth(),
            "logo_height" => $this->logo->getLogoHeight(),
        ];
    }
}
