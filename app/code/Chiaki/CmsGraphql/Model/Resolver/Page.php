<?php


namespace Chiaki\CmsGraphql\Model\Resolver;


use Magento\Cms\Api\Data\PageInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Page implements ResolverInterface
{
    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;
    /**
     * @var \Magento\PageBuilder\Model\Filter\Template
     */
    private $template;

    public function __construct(
        PageRepositoryInterface $pageRepository,
        \Magento\PageBuilder\Model\Filter\Template $template
    )
    {
        $this->pageRepository = $pageRepository;
        $this->template = $template;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    )
    {
        if (!isset($args['urlKey'])) {
            throw new GraphQlInputException(__('"Page urlKey should be specified'));
        }

        if (!isset($args['userId'])) {
            throw new GraphQlInputException(__('"Page userId should be specified'));
        }

        try {
            /** @var SearchCriteriaInterface $searchCriteria */
            $searchCriteria = ObjectManager::getInstance()->create(SearchCriteriaInterface::class);
            $searchCriteria->setPageSize(1);
            $searchCriteria->setCurrentPage(1);

            /** @var \Magento\Framework\Api\Search\FilterGroup $filterGroupUrlKey */
            $filterGroupUrlKey = ObjectManager::getInstance()->create(\Magento\Framework\Api\Search\FilterGroup::class);
            /** @var \Magento\Framework\Api\Search\FilterGroup $filterGroupUser */
            $filterGroupUser = ObjectManager::getInstance()->create(\Magento\Framework\Api\Search\FilterGroup::class);
            /** @var \Magento\Framework\Api\Filter $urlKeyFilter */
            $urlKeyFilter = ObjectManager::getInstance()->create(\Magento\Framework\Api\Filter::class);
            $urlKeyFilter->setField('identifier')->setValue($args['urlKey']);
            /** @var \Magento\Framework\Api\Filter $userFilter */
            $userFilter = ObjectManager::getInstance()->create(\Magento\Framework\Api\Filter::class);
            $userFilter->setField('user_id')->setValue($args['userId']);

            $filterGroupUrlKey->setFilters([$urlKeyFilter]);
            $filterGroupUser->setFilters([$userFilter]);
            $searchCriteria->setFilterGroups([$filterGroupUrlKey,$filterGroupUser]);

            $pages = $this->pageRepository->getList($searchCriteria);
            if (is_array($pages->getItems()) && count($pages->getItems()) === 1) {
                return $this->convertPageData(array_values($pages->getItems())[0]);
            } else {
                return null;
            }

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
    }

    /**
     * Convert page data
     *
     * @param PageInterface $page
     * @return array
     * @throws NoSuchEntityException
     */
    private function convertPageData(PageInterface $page)
    {
        if (false === $page->isActive()) {
            throw new NoSuchEntityException();
        }

        return [
            'url_key' => $page->getIdentifier(),
            'user_id' => $page->getUserId(),
            PageInterface::TITLE => $page->getTitle(),
            PageInterface::CONTENT => $this->template->filter($page->getContent()),
            PageInterface::CONTENT_HEADING => $page->getContentHeading(),
            PageInterface::PAGE_LAYOUT => $page->getPageLayout(),
            PageInterface::META_TITLE => $page->getMetaTitle(),
            PageInterface::META_DESCRIPTION => $page->getMetaDescription(),
            PageInterface::META_KEYWORDS => $page->getMetaKeywords(),
            PageInterface::PAGE_ID => $page->getId(),
            PageInterface::IDENTIFIER => $page->getIdentifier(),
        ];
    }
}
