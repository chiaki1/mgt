<?php


namespace Chiaki\CmsGraphql\Model\Resolver;


use Chiaki\Cms\Model\ImageProcessor;
use Chiaki\Cms\Model\ResourceModel\Gallery\CollectionFactory;
use Chiaki\Cms\Model\TypeFactory;
use Magento\Cms\Api\Data\PageInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class Pages implements ResolverInterface
{
    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;
    /**
     * @var \Magento\PageBuilder\Model\Filter\Template
     */
    private $template;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var ImageProcessor
     */
    private $imageProcessor;
    /**
     * @var TypeFactory
     */
    private $typeFactory;

    /**
     * Pages constructor.
     * @param PageRepositoryInterface $pageRepository
     * @param \Magento\PageBuilder\Model\Filter\Template $template
     * @param CollectionFactory $collectionFactory
     * @param ImageProcessor $imageProcessor
     * @param TypeFactory $typeFactory
     */
    public function __construct(
        PageRepositoryInterface $pageRepository,
        \Magento\PageBuilder\Model\Filter\Template $template,
        CollectionFactory $collectionFactory,
        ImageProcessor $imageProcessor,
        TypeFactory $typeFactory
    )
    {
        $this->pageRepository = $pageRepository;
        $this->template = $template;
        $this->collectionFactory = $collectionFactory;
        $this->imageProcessor = $imageProcessor;
        $this->typeFactory = $typeFactory;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    )
    {
        if (!isset($args['userId'])) {
            throw new GraphQlInputException(__('"Page userId should be specified'));
        }

        try {
            /** @var SearchCriteriaInterface $searchCriteria */
            $searchCriteria = ObjectManager::getInstance()->create(SearchCriteriaInterface::class);
            $searchCriteria->setPageSize(50);
            $searchCriteria->setCurrentPage(1);

            if ($args['userId'] !== 'default') {
                /** @var \Magento\Framework\Api\Search\FilterGroup $filterGroupUser */
                $filterGroupUser = ObjectManager::getInstance()->create(\Magento\Framework\Api\Search\FilterGroup::class);
                /** @var \Magento\Framework\Api\Filter $urlKeyFilter */
                /** @var \Magento\Framework\Api\Filter $userFilter */
                $userFilter = ObjectManager::getInstance()->create(\Magento\Framework\Api\Filter::class);
                $userFilter->setField('user_id')->setValue($args['userId']);

                $filterGroupUser->setFilters([$userFilter]);
                $searchCriteria->setFilterGroups([$filterGroupUser]);
            }
            $pages = $this->pageRepository->getList($searchCriteria);

            return array_map(function ($item) {
                return $this->convertPageData($item);
            }, $pages->getItems());

        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
    }

    /**
     * Convert page data
     *
     * @param PageInterface $page
     * @return array
     * @throws NoSuchEntityException
     */
    private function convertPageData(PageInterface $page)
    {
        if (false === $page->isActive()) {
            throw new NoSuchEntityException();
        }

        return [
            'url_key' => $page->getIdentifier(),
            'user_id' => $page->getUserId(),
            PageInterface::TITLE => $page->getTitle(),
            PageInterface::CONTENT => $this->template->filter($page->getContent()),
            PageInterface::CONTENT_HEADING => $page->getContentHeading(),
            PageInterface::PAGE_LAYOUT => $page->getPageLayout(),
            PageInterface::META_TITLE => $page->getMetaTitle(),
            PageInterface::META_DESCRIPTION => $page->getMetaDescription(),
            PageInterface::META_KEYWORDS => $page->getMetaKeywords(),
            PageInterface::PAGE_ID => $page->getId(),
            PageInterface::IDENTIFIER => $page->getIdentifier(),
            'gallery_images' => $this->getGalleryImages($page->getId())
        ];
    }

    /**
     * @param $pageId
     *
     * @return array
     */
    public function getGalleryImages($pageId)
    {
        $galleryImages = $this->collectionFactory->create()->getImagesByPage($pageId);
        $gallery_image = [];
        if (!empty($galleryImages)) {
            foreach ($galleryImages as $image) {
                $imgName = $image->getData('image_name');
                $gallery_image[] = $this->imageProcessor->getImageUrl(
                    [ImageProcessor::CHIAKI_CMS_GALLERY_MEDIA_PATH, $pageId, $imgName]
                );
            }
        }
        return $gallery_image;
    }
}
