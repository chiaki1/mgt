<?php

namespace Chiaki\CmsGraphql\Model\Resolver\DataProvider;

use Chiaki\Cms\Model\ImageProcessor;
use Chiaki\Cms\Model\ResourceModel\Gallery\CollectionFactory;
use Chiaki\Cms\Model\TypeFactory;
use Magento\Cms\Api\Data\PageInterface;
use Magento\Cms\Api\GetPageByIdentifierInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Widget\Model\Template\FilterEmulate;

class Page extends \Magento\CmsGraphQl\Model\Resolver\DataProvider\Page
{
    /**
     * @var GetPageByIdentifierInterface
     */
    private $pageByIdentifier;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * @var FilterEmulate
     */
    private $widgetFilter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ImageProcessor
     */
    private $imageProcessor;

    /**
     * @var TypeFactory
     */
    private $typeFactory;

    public function __construct(
        PageRepositoryInterface $pageRepository,
        FilterEmulate $widgetFilter,
        GetPageByIdentifierInterface $getPageByIdentifier,
        CollectionFactory $collectionFactory,
        ImageProcessor $imageProcessor,
        TypeFactory $typeFactory
    ) {
        parent::__construct($pageRepository, $widgetFilter, $getPageByIdentifier);
        $this->pageRepository = $pageRepository;
        $this->widgetFilter = $widgetFilter;
        $this->pageByIdentifier = $getPageByIdentifier;
        $this->collectionFactory = $collectionFactory;
        $this->imageProcessor = $imageProcessor;
        $this->typeFactory = $typeFactory;
    }

    /**
     * Returns page data by page_id
     *
     * @param int $pageId
     * @return array
     * @throws NoSuchEntityException
     */
    public function getDataByPageId(int $pageId): array
    {
        $page = $this->pageRepository->getById($pageId);

        return $this->convertPageData($page);
    }

    /**
     * Returns page data by page identifier
     *
     * @param string $pageIdentifier
     * @param int $storeId
     * @return array
     * @throws NoSuchEntityException
     */
    public function getDataByPageIdentifier(string $pageIdentifier, int $storeId): array
    {
        $page = $this->pageByIdentifier->execute($pageIdentifier, $storeId);

        return $this->convertPageData($page);
    }

    /**
     * Convert page data
     *
     * @param PageInterface $page
     * @return array
     * @throws NoSuchEntityException
     */
    private function convertPageData(PageInterface $page)
    {
        if (false === $page->isActive()) {
            throw new NoSuchEntityException();
        }

        $renderedContent = $this->widgetFilter->filter($page->getContent());

        $pageData = [
            'url_key' => $page->getIdentifier(),
            PageInterface::TITLE => $page->getTitle(),
            PageInterface::CONTENT => $renderedContent,
            PageInterface::CONTENT_HEADING => $page->getContentHeading(),
            PageInterface::PAGE_LAYOUT => $page->getPageLayout(),
            PageInterface::META_TITLE => $page->getMetaTitle(),
            PageInterface::META_DESCRIPTION => $page->getMetaDescription(),
            PageInterface::META_KEYWORDS => $page->getMetaKeywords(),
            PageInterface::PAGE_ID => $page->getId(),
            PageInterface::IDENTIFIER => $page->getIdentifier(),
            'page_type' => $this->getPageType($page->getPageType()),
            'gallery_images' => $this->getGalleryImages($page->getId()),
            'start_date' => $page->getStartDate(),
            'end_date' => $page->getEndDate()
        ];
        return $pageData;
    }

    /**
     * @param $pageTypeId
     *
     * @return string
     */
    public function getPageType($pageTypeId){
        $page_type = '';
        $type      = $this->typeFactory->create()->load($pageTypeId);
        if ($type->getId()) {
            $page_type = $type->getCode();
        }
        return $page_type;
    }

    /**
     * @param $pageId
     *
     * @return array
     */
    public function getGalleryImages($pageId)
    {
        $galleryImages = $this->collectionFactory->create()->getImagesByPage($pageId);
        $gallery_image = [];
        if (!empty($galleryImages)) {
            foreach ($galleryImages as $image) {
                $imgName = $image->getData('image_name');
                $gallery_image[] = $this->imageProcessor->getImageUrl(
                    [ImageProcessor::CHIAKI_CMS_GALLERY_MEDIA_PATH, $pageId, $imgName]
                );
            }
        }
        return $gallery_image;
    }
}
