<?php
/**
 *
 * @author Khoi Le - mr.vjcspy@gmail.com
 * @time 8/25/20 3:00 PM
 *
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Chiaki_CmsGraphql', __DIR__);
