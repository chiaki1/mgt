<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Helper;

use Chiaki\ReceivablePayment\Model\Payment\Receivable;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class OrderManagement
 */
class OrderHelper
{
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getOrderItemsData(int $orderId): ?array
    {
        $order = $this->orderRepository->get($orderId);
        $orderItems = $order->getItems();
        $orderItemsData = [];
        foreach ($orderItems as $orderItem) {
            $rowTotal = $orderItem->getRowTotal() - $orderItem->getDiscountAmount() - $orderItem->getTaxAmount();
            $price = round($rowTotal / $orderItem->getQtyOrdered(), 4);

            $orderItemsData[$orderItem->getItemId()] = [
                'order_item' => $orderItem->getItemId(),
                'price_per_once' => $price
            ];
        }
        return $orderItemsData;
    }

    /**
     * @param int $orderId
     * @return int|null
     */
    public function getTotalQty(int $orderId): ?float
    {
        $order = $this->orderRepository->get($orderId);
        $orderItems = $order->getItems();
        $totalQty = 0;
        foreach ($orderItems as $orderItem) {
            $totalQty += $orderItem->getQtyOrdered();
        }
        return $totalQty;
    }

    /**
     * @param int $orderId
     * @return OrderInterface
     */
    public function getOrder(int $orderId): OrderInterface
    {
        return $this->orderRepository->get($orderId);
    }

    /**
     * @param int $orderId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isAcceptReceivablePayment(int $orderId): bool
    {
        $order = $this->orderRepository->get($orderId);
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $paymentCode = $method->getCode();
        if ($paymentCode == Receivable::PAYMENT_METHOD_RECEIVABLE_CODE) {
            return true;
        }
        return false;
    }
}
