<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Helper;

use Magento\Sales\Model\ResourceModel\Order\Shipment\Collection;

/**
 * Class ShipmentManagement
 */
class ShipmentHelper
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory
     */
    private $shipmentCollectionFactory;

    /**
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollectionFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollectionFactory
    ) {
        $this->shipmentCollectionFactory = $shipmentCollectionFactory;
    }

    /**
     * @param int $oderId
     * @param array $excludeIds
     * @return array
     */
    public function getByOrderId(int $oderId, array $excludeIds = []): ?array
    {
        /** @var Collection $collection */
        $collection = $this->shipmentCollectionFactory->create();
        $collection
            ->addFieldToFilter('order_id', ['eq' => $oderId]);

        if (count($excludeIds) > 0) {
            $entityIds = [];
            foreach ($excludeIds as $excludeId) {
                $entityIds[] = explode(',', $excludeId);
            }
            $collection
                ->addFieldToFilter('entity_id', ['nin' => $entityIds]);
        }
        return $collection->getItems();
    }
}
