<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Helper;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Service\InvoiceService;

/**
 * Class InvoiceHelper
 */
class InvoiceHelper
{
    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    private $transactionFactory;

    public function __construct(
        InvoiceService $invoiceService,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\DB\TransactionFactory $transactionFactory
    ) {
        $this->invoiceService = $invoiceService;
        $this->orderFactory = $orderFactory;
        $this->transactionFactory = $transactionFactory;
    }

    /**
     * @param int $orderId
     * @param array $invoiceItems
     * @throws LocalizedException
     * @throws \Exception
     */
    public function createInvoice(int $orderId, array $invoiceItems)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create();
        $order = $order->load($orderId);

        if (!$order->getId()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
        }

        if (!$order->canInvoice()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The order does not allow an invoice to be created.')
            );
        }
        $invoiceItemPrepare = $this->prepareInvoiceItems($order, $invoiceItems);
        $invoice = $this->invoiceService->prepareInvoice($order, $invoiceItemPrepare);

        if (!$invoice) {
            throw new LocalizedException(__("The invoice can't be saved at this time. Please try again later."));
        }

        if (!$invoice->getTotalQty()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("The invoice can't be created without products. Add products and try again.")
            );
        }

        $invoice->register();
        $invoice->getOrder()->setCustomerNoteNotify(true);
        $invoice->getOrder()->setIsInProcess(true);

        /** @var \Magento\Framework\DB\Transaction  $transactionSave */
        $transactionSave = $this->transactionFactory->create();
        $transactionSave->addObject(
            $invoice
        )->addObject(
            $invoice->getOrder()
        );
        $transactionSave->save();
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param array $invoiceItems
     * @return array
     */
    private function prepareInvoiceItems(\Magento\Sales\Model\Order $order, array $invoiceItems): array
    {
        $orderItems = $order->getItems();
        foreach ($orderItems as $orderItem) {
            if ($orderItem->getQtyOrdered() - $orderItem->getQtyInvoiced() > 0 && !isset($invoiceItems[$orderItem->getItemId()])) {
                $invoiceItems[$orderItem->getItemId()] = '0';
            }
        }
        return $invoiceItems;
    }
}
