<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Ui\Component\Listing\Columns;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Escaper;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class ReceivableActions
 */
class ReceivableActions extends Column
{
    const ACL_RECEIVABLE_DELETE = 'Magento_Sales::sales_receivable_delete';

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var AuthorizationInterface
     */
    private $authorization;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Escaper $escaper
     * @param UrlInterface $urlBuilder
     * @param AuthorizationInterface $authorization
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Escaper $escaper,
        UrlInterface $urlBuilder,
        AuthorizationInterface $authorization,
        array $components = [],
        array $data = []
    ) {
        $this->escaper = $escaper;
        $this->urlBuilder = $urlBuilder;
        $this->authorization = $authorization;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                $indexField = 'entity_id';
                if (isset($item[$indexField])) {
                    $templateName = $this->escaper->escapeHtml($item['entity_id']);
                    if ($this->authorization->isAllowed(self::ACL_RECEIVABLE_DELETE)) {
                        $item[$name]['delete'] = [
                            'label' => __('Delete'),
                            'href' => $this->urlBuilder->getUrl(
                                'receivable/receivable/delete',
                                [
                                    'entity_id' => $item[$indexField]
                                ]
                            ),
                            'confirm' => [
                                'title' => __('Delete %1?', $templateName),
                                'message' => __(
                                    'Are you sure you want to permanently delete template %1?',
                                    $templateName
                                ),
                                '__disableTmpl' => true,
                            ],
                            'post' => true,
                            '__disableTmpl' => true,
                        ];
                    }
                }
            }
        }

        return $dataSource;
    }
}
