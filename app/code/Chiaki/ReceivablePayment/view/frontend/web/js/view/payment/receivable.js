/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/* @api */
define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ], function (Component, rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'receivable',
                component: 'Chiaki_ReceivablePayment/js/view/payment/method-renderer/receivable-method'
            }
        );
        return Component.extend({});
    });
