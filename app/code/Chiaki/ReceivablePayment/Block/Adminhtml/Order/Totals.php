<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Block\Adminhtml\Order;

use Chiaki\ReceivablePayment\Helper\OrderHelper;
use Chiaki\ReceivablePayment\Helper\ShipmentHelper;
use Chiaki\ReceivablePayment\Model\ReceivableRepository;

/**
 * Class Totals
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Totals
{
    /**
     * @var ShipmentHelper
     */
    private $shipmentHelper;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var ReceivableRepository
     */
    private $receivableRepository;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        ShipmentHelper $shipmentHelper,
        OrderHelper $orderHelper,
        ReceivableRepository $receivableRepository,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);
        $this->shipmentHelper = $shipmentHelper;
        $this->orderHelper = $orderHelper;
        $this->receivableRepository = $receivableRepository;
    }

    /**
     * Initialize order totals array
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _initTotals()
    {
        parent::_initTotals();
        $this->initReceivableTotal();
        $this->_totals['paid'] = new \Magento\Framework\DataObject(
            [
                'code' => 'paid',
                'strong' => true,
                'value' => $this->getSource()->getTotalPaid(),
                'base_value' => $this->getSource()->getBaseTotalPaid(),
                'label' => __('Total Paid'),
                'area' => 'footer',
            ]
        );
        $this->_totals['refunded'] = new \Magento\Framework\DataObject(
            [
                'code' => 'refunded',
                'strong' => true,
                'value' => $this->getSource()->getTotalRefunded(),
                'base_value' => $this->getSource()->getBaseTotalRefunded(),
                'label' => __('Total Refunded'),
                'area' => 'footer',
            ]
        );
        $code = 'due';
        $label = 'Total Due';
        $value = $this->getSource()->getTotalDue();
        $baseValue = $this->getSource()->getBaseTotalDue();
        if ($this->getSource()->getTotalCanceled() > 0 && $this->getSource()->getBaseTotalCanceled() > 0) {
            $code = 'canceled';
            $label = 'Total Canceled';
            $value = $this->getSource()->getTotalCanceled();
            $baseValue = $this->getSource()->getBaseTotalCanceled();
        }
        $this->_totals[$code] = new \Magento\Framework\DataObject(
            [
                'code' => 'due',
                'strong' => true,
                'value' => $value,
                'base_value' => $baseValue,
                'label' => __($label),
                'area' => 'footer',
            ]
        );
        return $this;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function initReceivableTotal()
    {
        $orderId = (int) $this->getSource()->getId();
        if ($this->orderHelper->isAcceptReceivablePayment($orderId)) {
            $this->_totals['shipment'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'shipment',
                    'strong' => true,
                    'value' => $this->getTotalShipment(),
                    'base_value' => $this->getTotalShipment(),
                    'label' => __('Total Shipment'),
                    'area' => 'footer',
                ]
            );
            $this->_totals['receivable'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'receivable',
                    'strong' => true,
                    'value' => $this->getTotalReceivable(),
                    'base_value' => $this->getTotalReceivable(),
                    'label' => __('Total Receivable'),
                    'area' => 'footer',
                ]
            );
            $this->_totals['payable'] = new \Magento\Framework\DataObject(
                [
                    'code' => 'payable',
                    'strong' => true,
                    'value' => $this->getTotalAmountPayable(),
                    'base_value' => $this->getTotalAmountPayable(),
                    'label' => __('Total Amount Payable'),
                    'area' => 'footer',
                ]
            );
        }
    }

    /**
     * @return float|int
     */
    private function getTotalShipment()
    {
        $orderId = (int) $this->getSource()->getId();
        $orderItems = $this->orderHelper->getOrderItemsData($orderId);
        $shipments = $this->shipmentHelper->getByOrderId($orderId);

        $shipmentTotalAmount = 0;
        $shipmentQty = 0;
        foreach ($shipments as $shipment) {
            $shippingItems = $shipment->getItems();
            foreach ($shippingItems as $shippingItem) {
                $shipmentQty += $shippingItem->getQty();
                $shipmentTotalAmount += $orderItems[$shippingItem->getOrderItemId()]['price_per_once'] * $shippingItem->getQty();
            }
        }

        $totalQty = $this->orderHelper->getTotalQty($orderId);
        $shippingAmount = $shipmentQty * $this->getSource()->getShippingAmount() / $totalQty;
        $shippingAmount = round($shippingAmount, 4);

        return $shipmentTotalAmount + $shippingAmount;
    }

    /**
     * @return int|float
     */
    private function getTotalReceivable()
    {
        $orderId = (int) $this->getSource()->getId();
        $receivables = $this->receivableRepository->getByOrderId($orderId);
        $totalReceivable = 0;
        foreach ($receivables as $receivable) {
            $totalReceivable += $receivable->getAmount();
        }
        return $totalReceivable;
    }

    /**
     * @return float|int|null
     */
    private function getTotalAmountPayable()
    {
        $totalAmountPayable = 0;
        $grandTotal = $this->getSource()->getGrandTotal();
        $receivable = $this->getTotalReceivable();
        if ($grandTotal >= $receivable) {
            $totalAmountPayable = $grandTotal - $receivable;
        }
        return $totalAmountPayable;
    }
}
