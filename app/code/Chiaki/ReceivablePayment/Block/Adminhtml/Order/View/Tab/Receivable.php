<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Block\Adminhtml\Order\View\Tab;

use Chiaki\ReceivablePayment\Helper\OrderHelper;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\View\Element\Text\ListText;

/**
 * Order Invoices grid
 *
 */
class Receivable extends ListText implements TabInterface
{
    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param OrderHelper $orderHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        OrderHelper $orderHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->registry = $registry;
        $this->orderHelper = $orderHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Receivable');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Order Receivable');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        $order = $this->registry->registry('current_order');
        $orderId = (int) $order->getId();
        if ($this->orderHelper->isAcceptReceivablePayment($orderId)) {
            return false;
        }
        return true;
    }
}
