<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Block\Adminhtml\Order\View;

use Chiaki\ReceivablePayment\Helper\OrderHelper;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Rma\Helper\Data;
use Magento\Sales\Block\Adminhtml\Order\View;
use Magento\Sales\Model\ConfigInterface;

/**
 * Additional buttons on order view page
 */
class ReceivableButton extends View
{
    const CREATE_RMA_BUTTON_DEFAULT_SORT_ORDER = 36;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ConfigInterface $salesConfig
     * @param \Magento\Sales\Helper\Reorder $reorderHelper
     * @param DataPersistorInterface $dataPersistor
     * @param OrderHelper $orderHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        ConfigInterface $salesConfig,
        \Magento\Sales\Helper\Reorder $reorderHelper,
        OrderHelper $orderHelper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $salesConfig, $reorderHelper, $data);
        $this->orderHelper = $orderHelper;
    }

    /**
     * Add button to Shopping Cart Management etc.
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addButtons()
    {
        if ($this->isCreateReceivableButtonRequired()) {
            $parentBlock = $this->getParentBlock();
            $buttonUrl = $this->_urlBuilder->getUrl(
                'receivable/receivable/index',
                ['order_id' => $parentBlock->getOrderId()]
            );

            $this->getToolbar()->addChild(
                'create_receivable',
                \Magento\Backend\Block\Widget\Button::class,
                ['label' => __('Receivable'), 'onclick' => 'setLocation(\'' . $buttonUrl . '\')']
            );
        }
        return $this;
    }

    /**
     *
     * @return boolean
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function isCreateReceivableButtonRequired(): bool
    {
        $orderId = (int) $this->getOrderId();
        if ($this->orderHelper->isAcceptReceivablePayment($orderId)) {
            return true;
        }
        return false;
    }
}
