<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Controller\Adminhtml\Receivable;

use Chiaki\ReceivablePayment\Api\Data\ReceivableInterfaceFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 */
class Index implements HttpGetActionInterface
{
    const ACL_RECEIVABLE_CREATE = 'Magento_Sales::sales_receivable_create';

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var AuthorizationInterface
     */
    private $authorization;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @param RequestInterface $request
     * @param PageFactory $resultPageFactory
     * @param DataPersistorInterface $dataPersistor
     * @param AuthorizationInterface $authorization
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        RequestInterface $request,
        PageFactory $resultPageFactory,
        DataPersistorInterface $dataPersistor,
        AuthorizationInterface $authorization,
        ResultFactory    $resultFactory
    ) {
        $this->request = $request;
        $this->resultPageFactory = $resultPageFactory;
        $this->dataPersistor = $dataPersistor;
        $this->authorization = $authorization;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $orderId = (int) $this->request->getParam('order_id', null);

        if (!$this->authorization->isAllowed(self::ACL_RECEIVABLE_CREATE)) {
            $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $redirect->setUrl('/admin/sales/order/view/order_id/' . $orderId);
        }
        $this->dataPersistor->set('receivable', [
            'order_id' => $orderId
        ]);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Receivable Form'));
        return $resultPage;
    }
}
