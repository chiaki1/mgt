<?php

namespace Chiaki\ReceivablePayment\Controller\Adminhtml\Receivable;

use Chiaki\ReceivablePayment\Helper\InvoiceHelper;
use Chiaki\ReceivablePayment\Helper\OrderHelper;
use Chiaki\ReceivablePayment\Helper\ShipmentHelper;
use Chiaki\ReceivablePayment\Model\Receivable;
use Chiaki\ReceivablePayment\Model\ReceivableRepository;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Chiaki\ReceivablePayment\Api\Data\ReceivableInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class Save
 */
class Save implements HttpPostActionInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var ReceivableRepository
     */
    private $receivableRepository;

    /**
     * @var ReceivableInterfaceFactory
     */
    private $receivableFactory;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var ShipmentHelper
     */
    private $shipmentHelper;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var InvoiceHelper
     */
    private $invoiceHelper;

    /**
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param DataPersistorInterface $dataPersistor
     * @param ManagerInterface $messageManager
     * @param ReceivableRepository $receivableRepository
     * @param ReceivableInterfaceFactory $receivableFactory
     * @param ShipmentHelper $shipmentHelper
     * @param OrderHelper $orderHelper
     * @param InvoiceHelper $invoiceHelper
     */
    public function __construct(
        RequestInterface           $request,
        ResultFactory              $resultFactory,
        DataPersistorInterface     $dataPersistor,
        ManagerInterface           $messageManager,
        ReceivableRepository       $receivableRepository,
        ReceivableInterfaceFactory $receivableFactory,
        ShipmentHelper             $shipmentHelper,
        OrderHelper                $orderHelper,
        InvoiceHelper              $invoiceHelper
    ) {
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->dataPersistor = $dataPersistor;
        $this->messageManager = $messageManager;
        $this->receivableRepository = $receivableRepository;
        $this->receivableFactory = $receivableFactory;
        $this->shipmentHelper = $shipmentHelper;
        $this->orderHelper = $orderHelper;
        $this->invoiceHelper = $invoiceHelper;
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $data = $this->request->getPostValue();
        $amountEnter = (float)$data['amount'] ?? 0;
        $note = $data['note'];

        $receivableTemp = $this->dataPersistor->get('receivable');
        $orderId = $receivableTemp['order_id'] ?? 0;
        if (!$orderId) {
            $this->messageManager->addErrorMessage(__('The order_id is require.'));
            $redirect->setUrl('/admin/sales/order/');
            return $redirect;
        }

        $order = $this->getCurrentOrder();
        $orderId = (int) $order->getEntityId();

        if (!$this->orderHelper->isAcceptReceivablePayment($orderId)) {
            $this->messageManager->addErrorMessage(__('This function use only for receivable payment.'));
            $redirect->setUrl('/admin/sales/order/view/order_id/' . $orderId);
            return $redirect;
        }

        if (!$this->validateAmount($amountEnter, $orderId)) {
            $this->messageManager->addErrorMessage(__('The amount enter must less than or equal to shipment amount.'));
            $redirect->setUrl('/admin/receivable/receivable/index/order_id/' . $orderId);
            return $redirect;
        }

        /** @var Receivable $receivable */
        $receivable = $this->receivableFactory->create();
        $receivable->setOrderId($orderId);
        $receivable->setAmount($amountEnter);
        $receivable->setNote($note);
        $this->receivableRepository->save($receivable);

        $receivableHaveNotInvoices = $this->receivableRepository->getReceivableHaveNotInvoice($orderId);

        $shipmentReceivableApplies = $this->receivableRepository->getShipmentIdApplied($orderId);
        $shipmentIdApplies = [];
        foreach ($shipmentReceivableApplies as $shipmentReceivableApply) {
            $shipmentIdApplies[] = $shipmentReceivableApply['shipment_id'];
        }

        $orderItems = $this->orderHelper->getOrderItemsData($orderId);
        $totalQty = $this->orderHelper->getTotalQty($orderId);
        $order = $this->orderHelper->getOrder($orderId);
        $shippingAmount = $order->getShippingAmount() ?? 0;

        $shipments = $this->shipmentHelper->getByOrderId($orderId, $shipmentIdApplies);
        $shipmentTotalAmounts = [];
        foreach ($shipments as $shipment) {
            $totalAmountMustReceive = 0;
            $shippingItems = $shipment->getItems();
            $orderItemsFormatters = [];
            $shippingAmountFee = 0;
            foreach ($shippingItems as $shippingItem) {
                $shippingAmountFee = $shippingItem->getQty() * $shippingAmount / $totalQty;
                $shippingAmountFee = round($shippingAmountFee, 4);
                $totalAmountMustReceive += $orderItems[$shippingItem->getOrderItemId()]['price_per_once'] * $shippingItem->getQty();
                $totalAmountMustReceive += $shippingAmountFee;
                $orderItemsFormatters[$shippingItem->getOrderItemId()] = $shippingItem->getQty();
            }
            $shipmentTotalAmounts[$shipment->getId()] = [
                'shipment_id' => $shipment->getId(),
                'total_amount' => $totalAmountMustReceive,
                'applied' => false,
                'items' => $orderItemsFormatters
            ];
        }

        $receivableAmount = 0;
        foreach ($receivableHaveNotInvoices as $receivableHaveNotInvoice) {
            $receivableAmount += $receivableHaveNotInvoice['amount'];
            $receivableIds[] = $receivableHaveNotInvoice['entity_id'];
            foreach ($shipmentTotalAmounts as $key => $shipmentTotalAmount) {
                if ($shipmentTotalAmount['applied'] == false &&
                    round($shipmentTotalAmount['total_amount'], 4) == round($receivableAmount, 4)
                ) {
                    $shipmentTotalAmounts[$key]['applied'] = true;
                    $this->receivableRepository->updateByIds($receivableIds, $shipmentTotalAmount['shipment_id']);
                    $receivableAmount -= $shipmentTotalAmount['total_amount'];
                }
            }
        }

        foreach ($shipmentTotalAmounts as $shipmentTotalAmount) {
            if ($shipmentTotalAmount['applied'] == true) {
                $this->invoiceHelper->createInvoice($orderId, $shipmentTotalAmount['items']);
            }
        }

        $redirect->setUrl('/admin/sales/order/view/order_id/' . $orderId);
        return $redirect;
    }

    /**
     * @param $amountEnter
     * @param $orderId
     * @return bool
     */
    private function validateAmount($amountEnter, $orderId): bool
    {
        $receivableHaveNotInvoices = $this->receivableRepository->getReceivableHaveNotInvoice($orderId);
        $receivableAmount = 0;
        foreach ($receivableHaveNotInvoices as $receivableHaveNotInvoice) {
            $receivableAmount += $receivableHaveNotInvoice['amount'];
        }
        $receivableAmount += $amountEnter;
        $totalShipment = $this->getTotalShipment($orderId);
        if (round($receivableAmount, 4) <= round($totalShipment, 4)) {
            return true;
        }
        return false;
    }

    /**
     * @return float|int
     */
    private function getTotalShipment($orderId)
    {
        $orderItems = $this->orderHelper->getOrderItemsData($orderId);
        $shipments = $this->shipmentHelper->getByOrderId($orderId);

        $shipmentTotalAmount = 0;
        $shipmentQty = 0;
        foreach ($shipments as $shipment) {
            $shippingItems = $shipment->getItems();
            foreach ($shippingItems as $shippingItem) {
                $shipmentQty += $shippingItem->getQty();
                $shipmentTotalAmount += $orderItems[$shippingItem->getOrderItemId()]['price_per_once'] * $shippingItem->getQty();
            }
        }

        $totalQty = $this->orderHelper->getTotalQty($orderId);
        $shippingAmount = $shipmentQty * $this->getCurrentOrder()->getShippingAmount() / $totalQty;
        $shippingAmount = round($shippingAmount, 4);

        return $shipmentTotalAmount + $shippingAmount;
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    private function getCurrentOrder(): \Magento\Sales\Api\Data\OrderInterface
    {
        $receivableTemp = $this->dataPersistor->get('receivable');
        $orderId = $receivableTemp['order_id'];
        return $this->orderHelper->getOrder($orderId);
    }
}
