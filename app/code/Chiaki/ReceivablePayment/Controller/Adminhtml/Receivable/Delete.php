<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Controller\Adminhtml\Receivable;

use Chiaki\ReceivablePayment\Model\ReceivableRepository;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class Delete
 */
class Delete implements HttpPostActionInterface
{
    const ACL_RECEIVABLE_DELETE = 'Magento_Sales::sales_receivable_delete';

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var ReceivableRepository
     */
    private $receivableRepository;

    /**
     * @var AuthorizationInterface
     */
    private $authorization;

    /**
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param ManagerInterface $messageManager
     * @param ReceivableRepository $receivableRepository
     * @param AuthorizationInterface $authorization
     */
    public function __construct(
        RequestInterface $request,
        ResultFactory    $resultFactory,
        ManagerInterface $messageManager,
        ReceivableRepository $receivableRepository,
        AuthorizationInterface $authorization
    ) {
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->messageManager = $messageManager;
        $this->receivableRepository = $receivableRepository;
        $this->authorization = $authorization;
    }

    /**
     * Execute action based on request and return result
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $receivableId = (int) $this->request->getParam('entity_id', null);
        $receivable = $this->receivableRepository->getById($receivableId);
        $orderId = $receivable->getOrderId();
        $shippingId = $receivable->getShippingId();

        if (!$this->authorization->isAllowed(self::ACL_RECEIVABLE_DELETE)) {
            $this->messageManager->addErrorMessage(__('Please check your permission again.'));
            $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $redirect->setUrl('/admin/sales/order/view/order_id/' . $orderId);
        }

        if ($shippingId) {
            $this->messageManager->addErrorMessage(__('The receivable_id %1 was applied to shipment. We cannot delete.', $receivableId));
            $redirect->setUrl('/admin/sales/order/view/order_id/' . $orderId);
            return $redirect;
        }
        $this->receivableRepository->deleteById($receivableId);
        $redirect->setUrl('/admin/sales/order/view/order_id/' . $orderId);
        return $redirect;
    }
}
