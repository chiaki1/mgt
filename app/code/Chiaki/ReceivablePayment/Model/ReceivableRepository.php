<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Model;

use Chiaki\ReceivablePayment\Api\Data\ReceivableInterface;
use Chiaki\ReceivablePayment\Api\ReceivableRepositoryInterface;
use Chiaki\ReceivablePayment\Model\ResourceModel\Receivable as ResourceModelObject;
use Chiaki\ReceivablePayment\Model\ReceivableFactory as ModelObjectFactory;
use Chiaki\ReceivablePayment\Model\ResourceModel\Receivable\Collection;
use Chiaki\ReceivablePayment\Model\ResourceModel\Receivable\CollectionFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ReceivableRepository
 */
class ReceivableRepository implements ReceivableRepositoryInterface
{
    /**
     * @var ResourceModelObject
     */
    private $resourceModelObject;

    /**
     * @var ReceivableFactory
     */
    private $modelObjectFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param ResourceModelObject $resourceModelObject
     * @param ReceivableFactory $modelObjectFactory
     * @param CollectionFactory $collection
     */
    public function __construct(
        ResourceModelObject $resourceModelObject,
        ModelObjectFactory $modelObjectFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->resourceModelObject = $resourceModelObject;
        $this->modelObjectFactory = $modelObjectFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param ReceivableInterface $receivable
     * @return ReceivableInterface
     * @throws CouldNotSaveException
     */
    public function save(ReceivableInterface $receivable): ReceivableInterface
    {
        try {
            $this->resourceModelObject->save($receivable);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $receivable;
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getByOrderId(int $orderId): array
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        return $collection
            ->addFieldToFilter('order_id', ['eq' => $orderId])
            ->getItems();
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getReceivableHaveNotInvoice(int $orderId): ?array
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();

        $collection
            ->addFieldToFilter('order_id', ['eq' => $orderId])
            ->addFieldToFilter('shipping_id', ['null' => true]);
        return $collection->getData();
    }

    /**
     * @param int $orderId
     * @return array
     */
    public function getShipmentIdApplied(int $orderId): array
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection
            ->addFieldToSelect('order_id')
            ->addExpressionFieldToSelect('shipment_id', 'DISTINCT(shipping_id)', '*')
            ->addFieldToFilter('order_id', ['eq' => $orderId])
            ->addFieldToFilter('shipping_id', ['notnull' => true])
            ->removeFieldFromSelect('order_id');
        return $collection->getData();
    }

    /**
     * @param array $ids
     * @param string $shippingId
     * @return void
     */
    public function updateByIds(array $ids, string $shippingId)
    {
        /** @var Collection $collection */
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('entity_id', ['in' => $ids]);
        foreach ($collection->getItems() as $item) {
            $shippingIdNew = $shippingId;
            if ($item->getShippingId()) {
                $shippingIdNew = $item->getShippingId() . "," . $shippingId;
            }
            $item->setShippingId($shippingIdNew);
            $item->save();
        }
    }

    /**
     * @param int $entityId
     * @return Receivable
     * @throws NoSuchEntityException
     */
    public function getById(int $entityId): Receivable
    {
        /** @var Receivable $objectModel */
        $objectModel = $this->modelObjectFactory->create();
        $objectModel->load($entityId);
        if (!$objectModel->getId()) {
            throw new NoSuchEntityException(__('The Receivable with the "%1" ID doesn\'t exist.', $entityId));
        }
        return $objectModel;
    }

    /**
     * @param ReceivableInterface $receivable
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(ReceivableInterface $receivable): bool
    {
        try {
            $this->resourceModelObject->delete($receivable);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(
                __('Could not delete the receivable: %1', $e->getMessage())
            );
        }
        return true;
    }

    /**
     * @param int $entityId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $entityId): bool
    {
        return $this->delete($this->getById($entityId));
    }
}
