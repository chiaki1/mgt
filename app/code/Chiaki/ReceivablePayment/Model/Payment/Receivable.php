<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Model\Payment;

use Magento\Payment\Model\Method\AbstractMethod;

/**
 * Class Receivable
 */
class Receivable extends AbstractMethod
{
    /**
     * Const
     */
    const PAYMENT_METHOD_RECEIVABLE_CODE = 'receivable';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD_RECEIVABLE_CODE;

    /**
     * @var bool
     */
    protected $_isOffline = true;
}
