<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Receivable
 */
class Receivable extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'sales_receivable_resource_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('sales_receivable', 'entity_id');
        $this->_useIsObjectNew = true;
    }
}
