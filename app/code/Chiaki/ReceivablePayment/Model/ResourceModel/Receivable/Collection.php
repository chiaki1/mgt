<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Model\ResourceModel\Receivable;

use Chiaki\ReceivablePayment\Model\Receivable as Model;
use Chiaki\ReceivablePayment\Model\ResourceModel\Receivable as ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'sales_receivable_collection';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
