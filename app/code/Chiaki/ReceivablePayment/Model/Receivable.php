<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Model;

use Chiaki\ReceivablePayment\Api\Data\ReceivableInterface;
use Chiaki\ReceivablePayment\Model\ResourceModel\Receivable as ResourceModel;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Receivable
 */
class Receivable extends AbstractModel implements ReceivableInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'sales_receivable_model';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID) === null ? null
            : (int)$this->getData(self::ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEntityId($entityId)
    {
        $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * @inheritDoc
     */
    public function getOrderId(): ?int
    {
        return $this->getData(self::ORDER_ID) === null ? null
            : (int)$this->getData(self::ORDER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderId(?int $orderId): void
    {
        $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @inheritDoc
     */
    public function getShippingId(): ?string
    {
        return $this->getData(self::SHIPPING_ID);
    }

    /**
     * @inheritDoc
     */
    public function setShippingId(?string $shippingId): void
    {
        $this->setData(self::SHIPPING_ID, $shippingId);
    }

    /**
     * @inheritDoc
     */
    public function getAmount(): ?float
    {
        return $this->getData(self::AMOUNT) === null ? null
            : (float)$this->getData(self::AMOUNT);
    }

    /**
     * @inheritDoc
     */
    public function setAmount(?float $amount): void
    {
        $this->setData(self::AMOUNT, $amount);
    }

    /**
     * @inheritDoc
     */
    public function getNote(): ?string
    {
        return $this->getData(self::NOTE);
    }

    /**
     * @inheritDoc
     */
    public function setNote(?string $note): void
    {
        $this->setData(self::NOTE, $note);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?string
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(?string $createdAt): void
    {
        $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): ?string
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt(?string $updatedAt): void
    {
        $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
