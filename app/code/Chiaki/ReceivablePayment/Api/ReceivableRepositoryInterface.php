<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Api;

use Chiaki\ReceivablePayment\Api\Data\ReceivableInterface;
use Chiaki\ReceivablePayment\Model\Receivable;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ReceivableRepositoryInterface
 */
interface ReceivableRepositoryInterface
{
    /**
     * @param ReceivableInterface $receivable
     * @return ReceivableInterface
     */
    public function save(ReceivableInterface $receivable): ReceivableInterface;

    /**
     * @param int $orderId
     * @return array
     */
    public function getByOrderId(int $orderId): array;

    /**
     * @param int $orderId
     * @return array
     */
    public function getReceivableHaveNotInvoice(int $orderId): ?array;

    /**
     * @param int $orderId
     * @return array
     */
    public function getShipmentIdApplied(int $orderId): array;

    /**
     * @param array $ids
     * @param string $shippingId
     * @return void
     */
    public function updateByIds(array $ids, string $shippingId);


    /**
     * @param int $entityId
     * @return Receivable
     * @throws NoSuchEntityException
     */
    public function getById(int $entityId): Receivable;

    /**
     * @param ReceivableInterface $receivable
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(ReceivableInterface $receivable): bool;

    /**
     * @param int $entityId
     * @return bool
     * @throws NoSuchEntityException
     * @throws \Exception
     */
    public function deleteById(int $entityId): bool;
}
