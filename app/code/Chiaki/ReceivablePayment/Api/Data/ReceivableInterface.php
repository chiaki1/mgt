<?php

declare(strict_types=1);

namespace Chiaki\ReceivablePayment\Api\Data;

/**
 * Class ReceivableInterface
 */
interface ReceivableInterface
{
    /**
     * String constants for property names
     */
    const ENTITY_ID = "entity_id";
    const ORDER_ID = "order_id";
    const SHIPPING_ID = "shipping_id";
    const AMOUNT = "amount";
    const NOTE = "note";
    const CREATED_AT = "created_at";
    const UPDATED_AT = "updated_at";

    /**
     * Getter for EntityId.
     *
     * @return int|null
     */
    public function getEntityId();

    /**
     * Setter for EntityId.
     *
     * @param int|null $entityId
     *
     * @return void
     */
    public function setEntityId($entityId);

    /**
     * Getter for OrderId.
     *
     * @return int|null
     */
    public function getOrderId(): ?int;

    /**
     * Setter for OrderId.
     *
     * @param int|null $orderId
     *
     * @return void
     */
    public function setOrderId(?int $orderId): void;

    /**
     * Getter for ShippingId.
     *
     * @return string|null
     */
    public function getShippingId(): ?string;

    /**
     * Setter for ShippingId.
     *
     * @param string|null $shippingId
     *
     * @return void
     */
    public function setShippingId(?string $shippingId): void;

    /**
     * Getter for Amount.
     *
     * @return float|null
     */
    public function getAmount(): ?float;

    /**
     * Setter for Amount.
     *
     * @param float|null $amount
     *
     * @return void
     */
    public function setAmount(?float $amount): void;

    /**
     * Getter for Note.
     *
     * @return string|null
     */
    public function getNote(): ?string;

    /**
     * Setter for Note.
     *
     * @param string|null $note
     *
     * @return void
     */
    public function setNote(?string $note): void;

    /**
     * Getter for CreatedAt.
     *
     * @return string|null
     */
    public function getCreatedAt(): ?string;

    /**
     * Setter for CreatedAt.
     *
     * @param string|null $createdAt
     *
     * @return void
     */
    public function setCreatedAt(?string $createdAt): void;

    /**
     * Getter for UpdatedAt.
     *
     * @return string|null
     */
    public function getUpdatedAt(): ?string;

    /**
     * Setter for UpdatedAt.
     *
     * @param string|null $updatedAt
     *
     * @return void
     */
    public function setUpdatedAt(?string $updatedAt): void;
}
