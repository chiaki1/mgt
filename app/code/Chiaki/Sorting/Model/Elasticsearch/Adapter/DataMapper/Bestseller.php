<?php

namespace Chiaki\Sorting\Model\Elasticsearch\Adapter\DataMapper;

use Chiaki\Sorting\Model\Elasticsearch\Adapter\IndexedDataMapper;

class Bestseller extends IndexedDataMapper
{
    const FIELD_NAME = 'bestsellers';

    /**
     * @inheritdoc
     */
    public function getIndexerCode()
    {
        return 'chiaki_sorting_bestseller';
    }
}
