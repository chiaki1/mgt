<?php

namespace Chiaki\Sorting\Model\Indexer;

use Chiaki\Sorting\Helper\Data;
use Chiaki\Sorting\Model\Indexer\Bestsellers\BestsellersProcessor;
use Magento\Indexer\Model\Indexer;
use Magento\Indexer\Model\IndexerFactory;

class Summary
{
    /**
     * @var array
     */
    private $indexerIds = [
        BestsellersProcessor::INDEXER_ID
    ];

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var \Chiaki\Sorting\Model\MethodProvider
     */
    private $methodProvider;

    /**
     * @var IndexerFactory
     */
    private $indexerFactory;

    public function __construct(
        \Chiaki\Sorting\Helper\Data $helper,
        \Chiaki\Sorting\Model\MethodProvider $methodProvider,
        IndexerFactory $indexerFactory
    ) {
        $this->helper = $helper;
        $this->methodProvider = $methodProvider;
        $this->indexerFactory = $indexerFactory;
    }

    /**
     * @return void
     */
    public function reindexAll()
    {
        foreach ($this->indexerIds as $indexerId) {
            // do full reindex if method not disabled
            $this->loadIndexer($indexerId)->reindexAll();
        }
    }

    /**
     * @param int $indexerId
     *
     * @return Indexer
     */
    private function loadIndexer($indexerId)
    {
        return $this->indexerFactory->create()
            ->load($indexerId);
    }
}
