<?php

namespace Chiaki\Sorting\Model\Indexer\Bestsellers;

use Chiaki\Sorting\Model\Indexer\AbstractSortingProcessor;

class BestsellersProcessor extends AbstractSortingProcessor
{
    /**
     * Indexer id
     */
    const INDEXER_ID = 'chiaki_sorting_bestseller';
}
