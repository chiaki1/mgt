<?php

namespace Chiaki\Sorting\Helper;

use Magento\CatalogSearch\Model\ResourceModel\EngineInterface;
use Magento\Framework\Registry;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CONFIG_SORT_ORDER = 'general/sort_order';

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var \Amasty\Base\Model\MagentoVersion
     */
    private $magentoVersion;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    private $storeManager;

    public function __construct(
        Registry                              $registry,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManager     $storeManager,
        \Amasty\Base\Model\MagentoVersion     $magentoVersion
    ) {
        parent::__construct($context);
        $this->registry       = $registry;
        $this->storeManager   = $storeManager;
        $this->magentoVersion = $magentoVersion;
    }

    /**
     * Get config value for Store
     *
     * @param string                                          $path
     * @param null|string|bool|int|\Magento\Store\Model\Store $store
     *
     * @return mixed
     */
    public function getScopeValue($path, $store = null)
    {
        return $this->scopeConfig->getValue(
            'sorting/' . $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Is Sorting Method Disabled
     *
     * @param string   $methodCode
     * @param int|null $storeId
     *
     * @return bool
     */
    public function isMethodDisabled($methodCode, $storeId = null)
    {
        $result = false;
        if (!$this->registry->registry('sorting_all_attributes')) {
            $disabledMethods = $this->getScopeValue('general/disable_methods', $storeId);
            if ($disabledMethods && !empty($disabledMethods)) {
                $disabledMethods = explode(',', $disabledMethods);
                foreach ($disabledMethods as $disabledCode) {
                    if (trim($disabledCode) == $methodCode) {
                        $result = true;
                        break;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param bool $skipStoreCheck
     *
     * @return bool
     */
    public function isElasticSort(bool $skipStoreCheck = false)
    {
        return version_compare($this->magentoVersion->get(), '2.3.2', '>=')
               && strpos($this->scopeConfig->getValue(EngineInterface::CONFIG_ENGINE_PATH), 'elast') !== false
               && ($skipStoreCheck || $this->storeManager->getStore()->getId());
    }

    /**
     * @return array
     */
    public function getChiakiAttributesCodes()
    {
        $result = [
            'created_at',
            $this->getScopeValue('bestsellers/best_attr'),
        ];

        return array_filter($result);
    }
}
