<?php

namespace Chiaki\Sorting\Plugin\Catalog\Product;

use Magento\Framework\DB\Select;
use Magento\Framework\Search\Adapter\Mysql\TemporaryStorage;

class Collection
{
    const PROCESS_FLAG = 'chiakisort_process';

    /**
     * @var \Chiaki\Sorting\Helper\Data
     */
    private $helper;

    /**
     * @var \Chiaki\Sorting\Model\MethodProvider
     */
    private $methodProvider;

    /**
     * @var \Chiaki\Sorting\Model\SortingAdapterFactory
     */
    private $adapterFactory;

    /**
     * @var array
     */
    private $skipAttributes = [];

    public function __construct(
        \Chiaki\Sorting\Helper\Data                 $helper,
        \Chiaki\Sorting\Model\MethodProvider        $methodProvider,
        \Chiaki\Sorting\Model\SortingAdapterFactory $adapterFactory
    ) {
        $this->helper         = $helper;
        $this->methodProvider = $methodProvider;
        $this->adapterFactory = $adapterFactory;
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
     * @param string                                                  $attribute
     * @param string                                                  $dir 'ASC'|'DESC'
     *
     * @return array
     */
    public function beforeSetOrder($subject, $attribute, $dir = Select::SQL_DESC)
    {
        $subject->setFlag(self::PROCESS_FLAG, true);
        $flagName = $this->getFlagName($attribute);
        if ($subject->getFlag($flagName)) {
            if ($this->helper->isElasticSort()) {
                $this->skipAttributes[] = $flagName;
            } else {
                // attribute already used in sorting; disable double sorting by renaming attribute into not existing
                $attribute .= '_ignore';
            }
        } else {
            $method = $this->methodProvider->getMethodByCode($attribute);
            if ($method) {
                $method->apply($subject, $dir);
                $attribute = $method->getAlias();
            }
            if (!$this->helper->isElasticSort()) {
                if ($attribute == 'relevance' && !$subject->getFlag($this->getFlagName('chiaki_relevance'))) {
                    $this->addRelevanceSorting($subject, $dir);
                    $attribute = 'chiaki_relevance';
                }
                if ($attribute == 'price') {
                    $subject->addOrder($attribute, $dir);
                    $attribute .= '_ignore';
                }
            }
            $subject->setFlag($flagName, true);
        }

        $subject->setFlag(self::PROCESS_FLAG, false);

        return [$attribute, $dir];
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $subject
     * @param callable                                                $proceed
     * @param string                                                  $attribute
     * @param string                                                  $dir
     *
     * @return mixed
     */
    public function aroundSetOrder($subject, callable $proceed, $attribute, $dir = Select::SQL_DESC)
    {
        $flagName = $this->getFlagName($attribute);
        if (!in_array($flagName, $this->skipAttributes)) {
            $proceed($attribute, $dir);
        }

        return $subject;
    }

    private function getFlagName($attribute)
    {
        if ($attribute == 'price_asc' || $attribute == 'price_desc') {
            $attribute = 'price';
        }
        if (is_string($attribute)) {
            return 'sorted_by_' . $attribute;
        }

        return 'chiaki_sorting';
    }

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     */
    private function addRelevanceSorting($collection)
    {
        $collection->getSelect()->columns(['chiaki_relevance' => new \Zend_Db_Expr(
            'search_result.' . TemporaryStorage::FIELD_SCORE
        )]);
        $collection->addExpressionAttributeToSelect('chiaki_relevance', 'chiaki_relevance', []);

        // remove last item from columns because e.chiaki_relevance from addExpressionAttributeToSelect not exist
        $columns = $collection->getSelect()->getPart(\Zend_Db_Select::COLUMNS);
        array_pop($columns);
        $collection->getSelect()->setPart(\Zend_Db_Select::COLUMNS, $columns);
        $collection->setFlag($this->getFlagName('chiaki_relevance'), true);
    }

    /**
     * @param        $subject
     * @param        $attribute
     * @param string $dir
     *
     * @return array
     */
    public function beforeAddOrder($subject, $attribute, $dir = Select::SQL_DESC)
    {
        if (!$subject->getFlag(self::PROCESS_FLAG)) {
            $result = $this->beforeSetOrder($subject, $attribute, $dir);
        }

        return $result ?? [$attribute, $dir];
    }

    /**
     * @param          $subject
     * @param callable $proceed
     * @param          $attribute
     * @param string   $dir
     *
     * @return mixed
     */
    public function aroundAddOrder($subject, callable $proceed, $attribute, $dir = Select::SQL_DESC)
    {
        return $this->aroundSetOrder($subject, $proceed, $attribute, $dir);
    }
}
