<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\CustomerGraphql\Model\Resolver;

use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Answer\CollectionFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Orders data resolver
 */
class CustomerSizes implements ResolverInterface
{

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * CustomerSizes constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (false === $context->getExtensionAttributes()->getIsCustomer()) {
            throw new GraphQlAuthorizationException(__('The current customer isn\'t authorized.'));
        }
        $userId     = $context->getUserId();
        $sizes      = $this->collectionFactory->create()->addFieldToFilter('customer_id', $userId);
        $sizesArray = [];
        foreach ($sizes as $size) {
            $sizesArray[] = [
                'answer_id'  => $size->getData('answer_id'),
                'name'       => $size->getData('name'),
                'active'     => $size->getData('active'),
                'shirt_size' => $size->getData('shirt_size'),
                'pants_size' => $size->getData('pants_size')
            ];
        }

        return [
            'items' => $sizesArray,
        ];
    }
}
