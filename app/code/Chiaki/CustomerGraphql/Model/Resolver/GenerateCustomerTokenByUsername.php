<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\CustomerGraphql\Model\Resolver;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthenticationException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Integration\Api\CustomerTokenServiceInterface;

/**
 * Customers Token resolver, used for GraphQL request processing.
 */
class GenerateCustomerTokenByUsername implements ResolverInterface
{
    /**
     * @var CustomerTokenServiceInterface
     */
    private $customerTokenService;

    /**
     * @var CollectionFactory
     */
    private $customerCollectionFactory;

    /**
     * @param CustomerTokenServiceInterface $customerTokenService
     * @param CollectionFactory             $customerCollectionFactory
     */
    public function __construct(
        CustomerTokenServiceInterface $customerTokenService,
        CollectionFactory             $customerCollectionFactory
    ) {
        $this->customerTokenService      = $customerTokenService;
        $this->customerCollectionFactory = $customerCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field       $field,
                    $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    ) {
        if (empty($args['username'])) {
            throw new GraphQlInputException(__('Specify the "username" value.'));
        }

        if (empty($args['password'])) {
            throw new GraphQlInputException(__('Specify the "password" value.'));
        }

        try {
            $collection = $this->customerCollectionFactory->create();
            $customer   = $collection->addAttributeToSelect('*')
                                     ->addAttributeToFilter('username', $args['username'])
                                     ->getFirstItem();
            if (!$customer->getId()) {
                throw new GraphQlAuthenticationException(__("Customer with username %1 does not exist", $args['username']));
            }

            $token = $this->customerTokenService->createCustomerAccessToken($customer->getEmail(), $args['password']);
            return ['token' => $token];
        } catch (AuthenticationException $e) {
            throw new GraphQlAuthenticationException(__($e->getMessage()), $e);
        }
    }
}
