<?php

namespace Chiaki\CustomerGraphql\Plugin;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\AddressRegistry;
use Magento\Customer\Model\EmailNotificationInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface as PsrLogger;
use Chiaki\Customer\Model\EmailNotification;

class AccountManagement
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var EmailNotificationInterface
     */
    private $emailNotification;

    /**
     * @var EmailNotification
     */
    private $emailNotificationCustom;

    /**
     * @var PsrLogger
     */
    private $logger;

    /**
     * @var AddressRegistry
     */
    private $addressRegistry;

    /**
     * @param StoreManagerInterface       $storeManager
     * @param CustomerRepositoryInterface $customerRepository
     * @param Random                      $mathRandom
     * @param PsrLogger                   $logger
     * @param AddressRegistry             $addressRegistry
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CustomerRepositoryInterface $customerRepository,
        Random $mathRandom,
        PsrLogger $logger,
        AddressRegistry $addressRegistry
    ) {
        $this->storeManager = $storeManager;
        $this->customerRepository = $customerRepository;
        $this->mathRandom = $mathRandom;
        $this->logger = $logger;
        $this->addressRegistry = $addressRegistry;
    }

    /**
     * @param \Magento\Customer\Model\AccountManagement $subject
     * @param callable          $proceed
     * @param string            $email
     * @param string            $template
     * @param int|null          $websiteId
     *
     * @return bool
     */
    public function aroundInitiatePasswordReset(
        \Magento\Customer\Model\AccountManagement $subject, callable $proceed, $email, $template, $websiteId = null
    ): bool {
        if ($websiteId === null) {
            $websiteId = $this->storeManager->getStore()->getWebsiteId();
        }
        // load customer by email
        $customer = $this->customerRepository->get($email, $websiteId);

        // No need to validate customer address while saving customer reset password token
        $this->disableAddressValidation($customer);

        $newPasswordToken = $this->mathRandom->getUniqueHash();
        $subject->changeResetPasswordLinkToken($customer, $newPasswordToken);

        try {
            switch ($template) {
                case \Magento\Customer\Model\AccountManagement::EMAIL_REMINDER:
                    $this->getEmailNotification()->passwordReminder($customer);
                    break;
                case \Magento\Customer\Model\AccountManagement::EMAIL_RESET:
                    $this->getEmailNotification()->passwordResetConfirmation($customer);
                    break;
                case 'email_reset_app':
                    $this->getEmailNotificationCustom()->passwordResetConfirmationApp($customer);
                    break;
                case 'email_reset_web':
                    $this->getEmailNotificationCustom()->passwordResetConfirmationWeb($customer);
                    break;
                default:
                    $this->handleUnknownTemplate($template);
                    break;
            }
            return true;
        } catch (MailException $e) {
            // If we are not able to send a reset password email, this should be ignored
            $this->logger->critical($e);
        }
        return false;
    }

    /**
     * Disable Customer Address Validation
     *
     * @param CustomerInterface $customer
     * @throws NoSuchEntityException
     */
    private function disableAddressValidation($customer)
    {
        foreach ($customer->getAddresses() as $address) {
            $addressModel = $this->addressRegistry->retrieve($address->getId());
            $addressModel->setShouldIgnoreValidation(true);
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotificationInterface
     * @deprecated 100.1.0
     */
    private function getEmailNotification()
    {
        if (!($this->emailNotification instanceof EmailNotificationInterface)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                EmailNotificationInterface::class
            );
        } else {
            return $this->emailNotification;
        }
    }

    /**
     * Get email notification
     *
     * @return EmailNotification
     * @deprecated 100.1.0
     */
    private function getEmailNotificationCustom()
    {
        if (!($this->emailNotificationCustom instanceof EmailNotification)) {
            return \Magento\Framework\App\ObjectManager::getInstance()->get(
                EmailNotification::class
            );
        } else {
            return $this->emailNotificationCustom;
        }
    }

    /**
     * Handle not supported template
     *
     * @param string $template
     * @throws InputException
     */
    private function handleUnknownTemplate($template)
    {
        throw new InputException(
            __(
                'Invalid value of "%value" provided for the %fieldName field. '
                . 'Possible values: %template1 or %template2 or %template3 or %template4.',
                [
                    'value' => $template,
                    'fieldName' => 'template',
                    'template1' => \Magento\Customer\Model\AccountManagement::EMAIL_REMINDER,
                    'template2' => \Magento\Customer\Model\AccountManagement::EMAIL_RESET,
                    'template3' => 'email_reset_app',
                    'template4' => 'email_reset_web'
                ]
            )
        );
    }
}
