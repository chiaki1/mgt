<?php

declare(strict_types=1);

namespace Chiaki\Review\Controller\Adminhtml\Rating;

use Chiaki\Review\Model\Rating;
use Exception;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

class Delete extends \Chiaki\Review\Controller\Adminhtml\Rating
{

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('rating_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(Rating::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Rating.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['rating_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Rating to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

