<?php

namespace Chiaki\Review\Block\Adminhtml\Order\View\Tab;

use Chiaki\Review\Model\ResourceModel\Review\Collection;
use Chiaki\Review\Model\ResourceModel\Review\CollectionFactory;
use Chiaki\Review\Model\Source\Rating;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;

class ReviewTab extends Template implements TabInterface
{
    protected $_template = 'order/view/tab/review_tab.phtml';

    /**
     * @var Registry
     */
    private $_coreRegistry;

    /**
     * @var CollectionFactory
     */
    protected $_reviewCollectionFactory;

    /**
     * @var Rating
     */
    protected $_rating;

    /**
     * View constructor.
     *
     * @param Context           $context
     * @param Registry          $registry
     * @param CollectionFactory $reviewCollectionFactory
     * @param Rating            $rating
     * @param array             $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        CollectionFactory $reviewCollectionFactory,
        Rating $rating,
        array $data = []
    ) {
        $this->_coreRegistry            = $registry;
        $this->_reviewCollectionFactory = $reviewCollectionFactory;
        $this->_rating                  = $rating;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve order model instance
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    /**
     * Retrieve order model instance
     *
     * @return int
     *Get current id order
     */
    public function getOrderId()
    {
        return $this->getOrder()->getEntityId();
    }

    /**
     * Retrieve order increment id
     *
     * @return string
     */
    public function getOrderIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Customer Review');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Customer Review');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return (bool)count($this->getReview());
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return !count($this->getReview());
    }

    /**
     * @return Collection
     */
    public function getReview()
    {
        return $this->_reviewCollectionFactory->create()->addFieldToFilter('order_id', $this->getOrderId());
    }

    public function getReviewModel()
    {
        return $this->getReview()->getFirstItem();
    }

    /**
     * @param $rating
     *
     * @return mixed
     */
    public function getRatingLabel($rating)
    {
        $ratingOption = $this->_rating->toArray();
        return $ratingOption[$rating];
    }

    public function getImageUrl($imageName)
    {
        return $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'order/review/' . $this->getReviewModel()->getOrderId() . '/' . $imageName;
    }
}
