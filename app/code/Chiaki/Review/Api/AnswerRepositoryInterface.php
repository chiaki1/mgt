<?php

declare(strict_types=1);

namespace Chiaki\Review\Api;

use Chiaki\Review\Api\Data\AnswerInterface;
use Chiaki\Review\Model\Answer;
use Chiaki\Review\Model\ResourceModel\Answer\Collection;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface AnswerRepositoryInterface
{

    /**
     * Save Answer
     *
     * @param AnswerInterface $answer
     *
     * @return AnswerInterface
     * @throws LocalizedException
     */
    public function save(
        AnswerInterface $answer
    );

    /**
     * Retrieve Answer
     *
     * @param string $answerId
     *
     * @return AnswerInterface
     * @throws LocalizedException
     */
    public function get($answerId);

    /**
     * Lists
     *
     * @param string[] $answers
     *
     * @return Collection
     */
    public function getList($answers);

    /**
     * Delete Answer
     *
     * @param AnswerInterface $answer
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        AnswerInterface $answer
    );

    /**
     * Delete Answer by ID
     *
     * @param string $answerId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($answerId);

    /**
     * @return Answer
     */
    public function getAnswerModel();

    /**
     * @return Collection
     */
    public function getAnswerCollection();

    /**
     * @param $ratingId
     *
     * @return Collection
     */
    public function getAnswersByRating($ratingId);

    /**
     * @param array $answersIds
     *
     * @return Collection
     */
    public function getAnswersByIds($answersIds = []);

    /**
     * @return DataObject[]
     */
    public function getAllAnswers();
}

