<?php

declare(strict_types=1);

namespace Chiaki\Review\Api;

use Chiaki\Review\Api\Data\MediaInterface;
use Chiaki\Review\Api\Data\MediaSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface MediaRepositoryInterface
{

    /**
     * Save Media
     *
     * @param MediaInterface $media
     *
     * @return MediaInterface
     * @throws LocalizedException
     */
    public function save(
        MediaInterface $media
    );

    /**
     * Retrieve Media
     *
     * @param string $mediaId
     *
     * @return MediaInterface
     * @throws LocalizedException
     */
    public function get($mediaId);

    /**
     * Retrieve Media matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return MediaSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Media
     *
     * @param MediaInterface $media
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        MediaInterface $media
    );

    /**
     * Delete Media by ID
     *
     * @param string $mediaId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($mediaId);
}

