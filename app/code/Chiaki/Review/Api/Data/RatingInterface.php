<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

interface RatingInterface
{

    const RATING_ID  = 'rating_id';
    const VALUE      = 'value';
    const CONTENT    = 'content';
    const ANSWERS    = 'answers';
    const ANSWER_IDS = 'answer_ids';

    /**
     * Get rating_id
     *
     * @return string|null
     */
    public function getRatingId();

    /**
     * Set rating_id
     *
     * @param string $ratingId
     *
     * @return RatingInterface
     */
    public function setRatingId($ratingId);

    /**
     * Get value
     *
     * @return string|null
     */
    public function getValue();

    /**
     * Set value
     *
     * @param string $value
     *
     * @return RatingInterface
     */
    public function setValue($value);

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     *
     * @param string $content
     *
     * @return RatingInterface
     */
    public function setContent($content);

    /**
     * Get answer
     *
     * @return string|null
     */
    public function getAnswers();

    /**
     * Get answer_ids
     *
     * @return string|null
     */
    public function getAnswerIds();
}

