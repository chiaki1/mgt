<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface RatingSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Rating list.
     *
     * @return RatingInterface[]
     */
    public function getItems();

    /**
     * Set value list.
     *
     * @param RatingInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

