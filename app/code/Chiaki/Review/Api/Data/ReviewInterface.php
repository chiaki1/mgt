<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

interface ReviewInterface
{

    const RATING    = 'rating';
    const REVIEW_ID = 'review_id';
    const COMMENT   = 'comment';
    const ANSWER    = 'answer';
    const ORDER_ID  = 'order_id';

    /**
     * Get review_id
     *
     * @return string|null
     */
    public function getReviewId();

    /**
     * Set review_id
     *
     * @param string $reviewId
     *
     * @return ReviewInterface
     */
    public function setReviewId($reviewId);

    /**
     * Get order_id
     *
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     *
     * @param string $orderId
     *
     * @return ReviewInterface
     */
    public function setOrderId($orderId);

    /**
     * Get comment
     *
     * @return string|null
     */
    public function getComment();

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ReviewInterface
     */
    public function setComment($comment);

    /**
     * Get rating
     *
     * @return string|null
     */
    public function getRating();

    /**
     * Set rating
     *
     * @param string $rating
     *
     * @return ReviewInterface
     */
    public function setRating($rating);

    /**
     * Get answer
     *
     * @return string|null
     */
    public function getAnswer();

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return ReviewInterface
     */
    public function setAnswer($answer);
}

