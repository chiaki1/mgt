<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

interface MediaInterface
{

    const REVIEW_ID  = 'review_id';
    const MEDIA_ID   = 'media_id';
    const IMAGE_NAME = 'image_name';

    /**
     * Get media_id
     *
     * @return string|null
     */
    public function getMediaId();

    /**
     * Set media_id
     *
     * @param string $mediaId
     *
     * @return MediaInterface
     */
    public function setMediaId($mediaId);

    /**
     * Get review_id
     *
     * @return string|null
     */
    public function getReviewId();

    /**
     * Set review_id
     *
     * @param string $reviewId
     *
     * @return MediaInterface
     */
    public function setReviewId($reviewId);

    /**
     * Get image_name
     *
     * @return string|null
     */
    public function getImageName();

    /**
     * Set image_name
     *
     * @param string $imageName
     *
     * @return MediaInterface
     */
    public function setImageName($imageName);
}

