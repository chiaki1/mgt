<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

interface AnswerInterface
{

    const CONTENT   = 'content';
    const ANSWER_ID = 'answer_id';

    /**
     * Get answer_id
     *
     * @return string|null
     */
    public function getAnswerId();

    /**
     * Set answer_id
     *
     * @param string $answerId
     *
     * @return AnswerInterface
     */
    public function setAnswerId($answerId);

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     *
     * @param string $content
     *
     * @return AnswerInterface
     */
    public function setContent($content);
}

