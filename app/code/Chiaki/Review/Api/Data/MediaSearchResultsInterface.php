<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface MediaSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Media list.
     *
     * @return MediaInterface[]
     */
    public function getItems();

    /**
     * Set review_id list.
     *
     * @param MediaInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

