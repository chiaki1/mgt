<?php

declare(strict_types=1);

namespace Chiaki\Review\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ReviewSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Review list.
     *
     * @return ReviewInterface[]
     */
    public function getItems();

    /**
     * Set order_id list.
     *
     * @param ReviewInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

