<?php

declare(strict_types=1);

namespace Chiaki\Review\Api;

use Chiaki\Review\Api\Data\RatingInterface;
use Chiaki\Review\Api\Data\RatingSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface RatingRepositoryInterface
{

    /**
     * Save Rating
     *
     * @param RatingInterface $rating
     *
     * @return RatingInterface
     * @throws LocalizedException
     */
    public function save(
        RatingInterface $rating
    );

    /**
     * Retrieve Rating
     *
     * @param string $ratingId
     *
     * @return RatingInterface
     * @throws LocalizedException
     */
    public function get($ratingId);

    /**
     * Retrieve Rating matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return RatingSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Rating
     *
     * @param RatingInterface $rating
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        RatingInterface $rating
    );

    /**
     * Delete Rating by ID
     *
     * @param string $ratingId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($ratingId);
}

