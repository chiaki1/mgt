<?php

namespace Chiaki\Review\Ui\Component\Form\Rating;

use Chiaki\Review\Api\AnswerRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Form\Field;

class Answers extends Field
{
    /**
     * @var AnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * Tags constructor.
     *
     * @param ContextInterface          $context
     * @param UiComponentFactory        $uiComponentFactory
     * @param AnswerRepositoryInterface $answerRepository
     * @param array                     $components
     * @param array                     $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AnswerRepositoryInterface $answerRepository,
        $components,
        array $data = []
    ) {
        $this->answerRepository = $answerRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare component configuration
     *
     * @return void
     * @throws LocalizedException
     */
    public function prepare()
    {
        $answersArray = [];
        foreach ($this->answerRepository->getAllAnswers() as $answer) {
            $answerContent = $answer->getContent();
            if ($answerContent) {
                $answersArray[] = $answerContent;
            }
        }
        $config         = $this->getData('config');
        $config['tags'] = $answersArray;
        $this->setData('config', $config);
        parent::prepare();
    }
}
