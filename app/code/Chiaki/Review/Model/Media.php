<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\Data\MediaInterface;
use Chiaki\Review\Api\Data\MediaInterfaceFactory;
use Chiaki\Review\Model\ResourceModel\Media\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Media extends AbstractModel implements MediaInterface
{

    protected $mediaDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'chiaki_review_media';

    /**
     * @param Context $context
     * @param Registry      $registry
     * @param MediaInterfaceFactory            $mediaDataFactory
     * @param DataObjectHelper                 $dataObjectHelper
     * @param ResourceModel\Media              $resource
     * @param Collection                       $resourceCollection
     * @param array                            $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        MediaInterfaceFactory $mediaDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Media $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->mediaDataFactory = $mediaDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve media model with media data
     *
     * @return MediaInterface
     */
    public function getDataModel()
    {
        $mediaData = $this->getData();

        $mediaDataObject = $this->mediaDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $mediaDataObject,
            $mediaData,
            MediaInterface::class
        );

        return $mediaDataObject;
    }

    /**
     * Get media_id
     *
     * @return string|null
     */
    public function getMediaId()
    {
        return $this->_getData(self::MEDIA_ID);
    }

    /**
     * Set media_id
     *
     * @param string $mediaId
     *
     * @return MediaInterface
     */
    public function setMediaId($mediaId)
    {
        return $this->setData(self::MEDIA_ID, $mediaId);
    }

    /**
     * Get review_id
     *
     * @return string|null
     */
    public function getReviewId()
    {
        return $this->_getData(self::REVIEW_ID);
    }

    /**
     * Set review_id
     *
     * @param string $reviewId
     *
     * @return MediaInterface
     */
    public function setReviewId($reviewId)
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }

    /**
     * Get image_name
     *
     * @return string|null
     */
    public function getImageName()
    {
        return $this->_getData(self::IMAGE_NAME);
    }

    /**
     * Set image_name
     *
     * @param string $imageName
     *
     * @return MediaInterface
     */
    public function setImageName($imageName)
    {
        return $this->setData(self::IMAGE_NAME, $imageName);
    }
}

