<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\AnswerRepositoryInterface;
use Chiaki\Review\Api\Data\AnswerInterface;
use Chiaki\Review\Api\Data\AnswerInterfaceFactory;
use Chiaki\Review\Api\Data\AnswerSearchResultsInterfaceFactory;
use Chiaki\Review\Model\ResourceModel\Answer as AnswerResource;
use Chiaki\Review\Model\ResourceModel\Answer as ResourceAnswer;
use Chiaki\Review\Model\ResourceModel\Answer\Collection;
use Chiaki\Review\Model\ResourceModel\Answer\CollectionFactory as AnswerCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class AnswerRepository implements AnswerRepositoryInterface
{

    protected $resource;

    protected $answerFactory;

    protected $answerCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataAnswerFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * Model data storage
     *
     * @var array
     */
    private $answers;

    protected $answerResource;

    /**
     * @param ResourceAnswer                      $resource
     * @param AnswerFactory                       $answerFactory
     * @param AnswerInterfaceFactory              $dataAnswerFactory
     * @param AnswerCollectionFactory             $answerCollectionFactory
     * @param AnswerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                    $dataObjectHelper
     * @param DataObjectProcessor                 $dataObjectProcessor
     * @param StoreManagerInterface               $storeManager
     * @param CollectionProcessorInterface        $collectionProcessor
     * @param JoinProcessorInterface              $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter       $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceAnswer $resource,
        AnswerFactory $answerFactory,
        AnswerInterfaceFactory $dataAnswerFactory,
        AnswerCollectionFactory $answerCollectionFactory,
        AnswerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        AnswerResource $answerResource
    ) {
        $this->resource                         = $resource;
        $this->answerFactory                    = $answerFactory;
        $this->answerCollectionFactory          = $answerCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataAnswerFactory                = $dataAnswerFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
        $this->answerResource                   = $answerResource;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        AnswerInterface $answer
    ) {
        try {
            if ($answer->getAnswerId()) {
                $answer = $this->getById($answer->getAnswerId())->addData($answer->getData());
            } else {
                $answer->setAnswerId(null);
            }
            $this->answerResource->save($answer);
            unset($this->answers[$answer->getAnswerId()]);
        } catch (Exception $e) {
            if ($answer->getAnswerId()) {
                throw new CouldNotSaveException(
                    __(
                        'Unable to save answer with ID %1. Error: %2',
                        [$answer->getAnswerId(), $e->getMessage()]
                    )
                );
            }
            throw new CouldNotSaveException(__('Unable to save new answer. Error: %1', $e->getMessage()));
        }

        return $answer;
    }

    /**
     * @param int $answerId
     *
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($answerId)
    {
        if (!isset($this->answers[$answerId])) {
            /** @var Answer $tag */
            $answer = $this->answerFactory->create();
            $this->answerResource->load($answer, $answerId);
            if (!$answer->getAnswerId()) {
                throw new NoSuchEntityException(__('Answer with specified ID "%1" not found.', $answerId));
            }
            $this->answers[$answerId] = $answer;
        }

        return $this->answers[$answerId];
    }

    /**
     * {@inheritdoc}
     */
    public function get($answerId)
    {
        $answer = $this->answerFactory->create();
        $this->resource->load($answer, $answerId);
        if (!$answer->getId()) {
            throw new NoSuchEntityException(__('Answer with id "%1" does not exist.', $answerId));
        }
        return $answer->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList($answers)
    {
        return $this->answerCollectionFactory->create()
                                             ->addFieldToFilter(AnswerInterface::CONTENT, ['in' => $answers]);
    }

    /**
     * @param AnswerInterface $answer
     *
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(AnswerInterface $answer)
    {
        try {
            $this->answerResource->delete($answer);
            unset($this->answers[$answer->getAnswerId()]);
        } catch (Exception $e) {
            if ($answer->getAnswerId()) {
                throw new CouldNotDeleteException(
                    __(
                        'Unable to remove answer with ID %1. Error: %2',
                        [$answer->getAnswerId(), $e->getMessage()]
                    )
                );
            }
            throw new CouldNotDeleteException(__('Unable to remove answer. Error: %1', $e->getMessage()));
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($answerId)
    {
        return $this->delete($this->get($answerId));
    }

    /**
     * @return Answer
     */
    public function getAnswerModel()
    {
        return $this->answerFactory->create();
    }

    /**
     * @return Collection
     */
    public function getAnswerCollection()
    {
        return $this->answerCollectionFactory->create();
    }

    /**
     * @param $ratingId
     *
     * @return Collection
     */
    public function getAnswersByRating($ratingId)
    {
        $answers = $this->answerCollectionFactory->create();
        $answers->addRatingFilter($ratingId);

        return $answers;
    }

    /**
     * @param array|string $answersIds
     *
     * @return Collection
     */
    public function getAnswersByIds($answersIds = [])
    {
        if (!is_array($answersIds)) {
            $answersIds = explode(',', $answersIds);
        }
        $answers = $this->answerCollectionFactory->create();
        $answers->addIdFilter($answersIds);

        return $answers;
    }

    /**
     * @return DataObject[]
     */
    public function getAllAnswers()
    {
        return $this->answerCollectionFactory->create()->getItems();
    }
}

