<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel\Media;

use Chiaki\Review\Model\ResourceModel\Media;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'media_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\Review\Model\Media::class,
            Media::class
        );
    }
}

