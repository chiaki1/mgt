<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel\Rating;

use Chiaki\Review\Model\ResourceModel\Rating;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'rating_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\Review\Model\Rating::class,
            Rating::class
        );
    }

    /**
     * Load data
     *
     * @param boolean $printQuery
     * @param boolean $logQuery
     *
     * @return $this
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if (!$this->isLoaded()) {
            parent::load($printQuery, $logQuery);
            $this->addLinkedTables();
        }

        return $this;
    }

    /**
     * @param $answerIds
     *
     * @return $this
     */
    public function addAnswerFilter($answerIds)
    {
        if (!is_array($answerIds)) {
            $answerIds = [$answerIds];
        }
        $this->getSelect()
             ->joinLeft(
                 ['answers' => $this->getTable('chiaki_review_rating_answer')],
                 'main_table.rating_id = answers.rating_id',
                 []
             )
             ->where('answers.answer_id IN (?)', $answerIds)
             ->group('main_table.rating_id');

        return $this;
    }

    /**
     * @return void
     */
    private function addLinkedTables()
    {
        $this->addLinkedData('answer_id', 'answers');
    }

    /**
     * @param $linkedTable
     * @param $linkedField
     * @param $fieldName
     */
    private function addLinkedData($linkedField, $fieldName)
    {
        $connection = $this->getConnection();

        $ratingId = $this->getColumnValues('rating_id');
        $linked   = [];
        if (!empty($ratingId)) {
            $inCond = $connection->prepareSqlCondition('rating_id', ['in' => $ratingId]);
            $select = $connection->select()
                                 ->from(['rating_answers' => $this->getTable('chiaki_review_rating_answer')])
                                 ->joinLeft(
                                     ['answers' => $this->getTable('chiaki_review_answer')],
                                     'rating_answers.answer_id = answers.answer_id',
                                     ['content']
                                 )
                                 ->where($inCond);
            $result = $connection->fetchAll($select);
            foreach ($result as $row) {
                if (!isset($linked[$row['rating_id']])) {
                    $linked[$row['rating_id']] = [];
                }
                $linked[$row['rating_id']][] = $row['content'];
            }
        }
        foreach ($this as $item) {
            if (isset($linked[$item->getId()])) {
                $item->setData($fieldName, $linked[$item->getId()]);
            } else {
                $item->setData($fieldName, []);
            }
        }
    }
}

