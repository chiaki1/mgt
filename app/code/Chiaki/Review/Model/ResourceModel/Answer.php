<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Answer extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('chiaki_review_answer', 'answer_id');
    }
}

