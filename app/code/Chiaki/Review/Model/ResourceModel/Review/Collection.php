<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel\Review;

use Chiaki\Review\Model\ResourceModel\Review;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'review_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\Review\Model\Review::class,
            Review::class
        );
    }

    /**
     * Load data
     *
     * @param boolean $printQuery
     * @param boolean $logQuery
     *
     * @return $this
     */
    public function load($printQuery = false, $logQuery = false)
    {
        if (!$this->isLoaded()) {
            parent::load($printQuery, $logQuery);
            $this->addLinkedTables();
        }

        return $this;
    }

    /**
     * @return void
     */
    private function addLinkedTables()
    {
        $this->addLinkedData('media');
    }

    /**
     * @param $fieldName
     */
    private function addLinkedData($fieldName)
    {
        $connection = $this->getConnection();

        $reviewId = $this->getColumnValues('review_id');
        $linked   = [];
        if (!empty($reviewId)) {
            $inCond = $connection->prepareSqlCondition('review_id', ['in' => $reviewId]);
            $select = $connection->select()
                                 ->from([$this->getTable('chiaki_review_media')])
                                 ->where($inCond);
            $result = $connection->fetchAll($select);
            foreach ($result as $row) {
                if (!isset($linked[$row['review_id']])) {
                    $linked[$row['review_id']] = [];
                }
                $linked[$row['review_id']][] = $row['image_name'];
            }
        }
        foreach ($this as $item) {
            if (isset($linked[$item->getId()])) {
                $item->setData($fieldName, $linked[$item->getId()]);
            } else {
                $item->setData($fieldName, []);
            }
        }
    }
}

