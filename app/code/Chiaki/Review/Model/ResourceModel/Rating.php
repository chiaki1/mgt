<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel;

use Chiaki\Review\Api\AnswerRepositoryInterface;
use Chiaki\Review\Api\Data\AnswerInterface;
use Chiaki\Review\Api\Data\RatingInterface;
use Chiaki\Review\Model\Rating as RatingModel;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Rating extends AbstractDb
{

    const TABLE_NAME = 'chiaki_review_rating';

    const CHIAKI_REVIEW_ANSWER_TABLE = 'chiaki_review_answer';

    const CHIAKI_REVIEW_RATING_ANSWER_TABLE = 'chiaki_review_rating_answer';

    /**
     * @var AnswerRepositoryInterface
     */
    private $answerRepository;

    /**
     * @var AnswerInterface
     */
    private $answerModel;

    /**
     * Rating constructor.
     *
     * @param Context                   $context
     * @param AnswerRepositoryInterface $answerRepository
     * @param null                      $connectionName
     */
    public function __construct(
        Context $context,
        AnswerRepositoryInterface $answerRepository,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->answerRepository = $answerRepository;
        $this->answerModel      = $answerRepository->getAnswerModel();
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('chiaki_review_rating', 'rating_id');
    }

    /**
     * @param RatingInterface $object
     *
     * @return Rating|AbstractDb
     */
    protected function _afterLoad(AbstractModel $object)
    {
        parent::_afterLoad($object);

        return $this->loadAnswers($object);
    }

    /**
     * @param RatingInterface $ratingModel
     *
     * @return $this
     */
    private function loadAnswers(RatingInterface $ratingModel)
    {
        $ratingId = $ratingModel->getId();
        if ($ratingId) {
            $select = $this->getConnection()->select()
                           ->from(
                               ['rating_answers' => $this->getTable(Rating::CHIAKI_REVIEW_ANSWER_TABLE)],
                               ['rating_answers.answer_id', 'rating_answers.content']
                           )
                           ->joinLeft(
                               ['answers' => $this->getTable(self:: CHIAKI_REVIEW_RATING_ANSWER_TABLE)],
                               'answers.answer_id = rating_answers.answer_id',
                               []
                           )
                           ->where('answers.rating_id = :rating_id');

            $answers = $this->getConnection()->fetchPairs($select, [':rating_id' => $ratingId]);
            $ratingModel->setData(RatingInterface::ANSWERS, implode(',', array_values($answers)));
            $ratingModel->setData(RatingInterface::ANSWER_IDS, implode(',', array_keys($answers)));
        }

        return $this;
    }

    /**
     * @param RatingModel $object
     *
     * @return AbstractDb
     * @throws FileSystemException
     * @throws LocalizedException
     */
    protected function _afterSave(AbstractModel $object)
    {
        $connection = $this->getConnection();
        $this->saveAnswers($object, $connection);

        return parent::_afterSave($object);
    }

    /**
     * @param RatingInterface  $rating
     * @param AdapterInterface $connection
     *
     * @return $this
     */
    private function saveAnswers(RatingInterface $rating, AdapterInterface $connection)
    {
        $answers   = $rating->getData('answers');
        $condition = [RatingInterface::RATING_ID . ' = ?' => $rating->getRatingId()];
        $connection->delete($this->getTable(self::CHIAKI_REVIEW_RATING_ANSWER_TABLE), $condition);
        if (!empty($answers)) {
            $answersArray = explode(',', $answers);
            $answersList  = $this->answerRepository->getList($answersArray);
            $existAnswers = [];
            foreach ($answersList as $answer) {
                $existAnswers[$answer->getId()] = $answer->getContent();
            }

            foreach ($answersArray as $answer) {
                if (!$answer) {
                    continue;
                }
                //insert exist tag or create new tag
                $this->answerModel->setData([])->isObjectNew(false);
                if (in_array($answer, $existAnswers)) {
                    $answerInsert = ['answer_id' => array_search($answer, $existAnswers), 'rating_id' => $rating->getRatingId()];
                } else {
                    $newAnswer = [
                        AnswerInterface::CONTENT => $answer,
                    ];
                    $this->answerModel->setData($newAnswer);
                    $this->answerRepository->save($this->answerModel);
                    $id           = $this->answerModel->getId();
                    $answerInsert = ['answer_id' => $id, 'rating_id' => $rating->getRatingId()];
                }

                $connection->insert($this->getTable(self::CHIAKI_REVIEW_RATING_ANSWER_TABLE), $answerInsert);
            }
        }

        return $this;
    }
}

