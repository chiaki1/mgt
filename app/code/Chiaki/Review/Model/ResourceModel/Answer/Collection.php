<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel\Answer;

use Chiaki\Review\Model\ResourceModel\Answer;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'answer_id';

    protected $ratingDataJoined;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\Review\Model\Answer::class,
            Answer::class
        );
    }

    /**
     * @param $ratingIds
     *
     * @return $this
     */
    public function addRatingFilter($ratingIds)
    {
        $ratingIds = is_array($ratingIds) ? $ratingIds : [$ratingIds];

        $this->joinRatingData();
        $this->getSelect()->where('rating_answer.rating_id IN (?)', $ratingIds);

        return $this;
    }

    private function joinRatingData()
    {
        if ($this->ratingDataJoined) {
            return $this;
        }

        $this->ratingDataJoined = true;

        $ratingAnswerTable = $this->getTable('chiaki_review_rating_answer');
        $this->getSelect()->join(['rating_answer' => $ratingAnswerTable], "rating_answer.answer_id = main_table.answer_id", []);

        return $this;
    }

    /**
     * @param array $answerIds
     *
     * @return $this
     */
    public function addIdFilter($answerIds = [])
    {
        if (!is_array($answerIds)) {
            $answerIds = [$answerIds];
        }
        $this->addFieldToFilter('answer_id', ['in' => $answerIds]);

        return $this;
    }
}

