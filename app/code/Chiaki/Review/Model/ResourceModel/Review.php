<?php

declare(strict_types=1);

namespace Chiaki\Review\Model\ResourceModel;

use Chiaki\Review\Api\Data\MediaInterface;
use Chiaki\Review\Api\Data\ReviewInterface;
use Chiaki\Review\Model\Review as ReviewModel;
use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Review extends AbstractDb
{

    protected $filesystem;

    protected $mediaPath;

    protected $io;

    public function __construct(
        Context $context,
        Filesystem $filesystem,
        File $io,
        $connectionName = null
    ) {
        $this->filesystem = $filesystem;
        $this->io         = $io;
        $this->mediaPath  = $filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath() . 'order/review/';
        parent::__construct($context, $connectionName);
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('chiaki_review_review', 'review_id');
    }

    /**
     * @param ReviewModel $object
     *
     * @return AbstractDb
     * @throws FileSystemException
     * @throws LocalizedException
     */
    protected function _afterSave(AbstractModel $object)
    {
        $connection = $this->getConnection();
        $this->saveMedia($object, $connection);

        return parent::_afterSave($object);
    }

    /**
     * @param ReviewInterface  $review
     * @param AdapterInterface $connection
     *
     * @return $this
     * @throws LocalizedException
     */
    private function saveMedia(ReviewInterface $review, AdapterInterface $connection)
    {
        try {
            $media = $review->getData('media');
            if (!empty($media)) {
                foreach ($media as $_media) {
                    $name = $_media['image_name'];
                    $data = $_media['base64data'];
                    list(, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $data = base64_decode($data);
                    $this->io->mkdir($this->mediaPath . $review->getData('order_id'), 0775);
                    $this->io->write($this->mediaPath . $review->getData('order_id') . '/' . $name, $data);
                    $newImage = [
                        MediaInterface::IMAGE_NAME => $name,
                        MediaInterface::REVIEW_ID  => $review->getReviewId()
                    ];
                    $connection->insert($this->getTable('chiaki_review_media'), $newImage);
                }
            }
        } catch (Exception $e) {
            throw new LocalizedException(__($e->getMessage()));
        }
        return $this;
    }
}

