<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\Data\RatingInterface;
use Chiaki\Review\Api\Data\RatingInterfaceFactory;
use Chiaki\Review\Model\ResourceModel\Rating\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Rating extends AbstractModel implements RatingInterface
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'chiaki_review_rating';
    protected $ratingDataFactory;

    /**
     * @param Context $context
     * @param Registry      $registry
     * @param RatingInterfaceFactory           $ratingDataFactory
     * @param DataObjectHelper                 $dataObjectHelper
     * @param ResourceModel\Rating             $resource
     * @param Collection                       $resourceCollection
     * @param array                            $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RatingInterfaceFactory $ratingDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Rating $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->ratingDataFactory = $ratingDataFactory;
        $this->dataObjectHelper  = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve rating model with rating data
     *
     * @return RatingInterface
     */
    public function getDataModel()
    {
        $ratingData = $this->getData();

        $ratingDataObject = $this->ratingDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $ratingDataObject,
            $ratingData,
            RatingInterface::class
        );

        return $ratingDataObject;
    }

    /**
     * Get rating_id
     *
     * @return string|null
     */
    public function getRatingId()
    {
        return $this->_getData(self::RATING_ID);
    }

    /**
     * Set rating_id
     *
     * @param string $ratingId
     *
     * @return RatingInterface
     */
    public function setRatingId($ratingId)
    {
        return $this->setData(self::RATING_ID, $ratingId);
    }

    /**
     * Get value
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->_getData(self::VALUE);
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return RatingInterface
     */
    public function setValue($value)
    {
        return $this->setData(self::VALUE, $value);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->_getData(self::CONTENT);
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return RatingInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get answer
     *
     * @return string|null
     */
    public function getAnswers()
    {
        return $this->_getData(self::ANSWERS);
    }

    /**
     * Get answer_ids
     *
     * @return string|null
     */
    public function getAnswerIds()
    {
        return $this->_getData(self::ANSWER_IDS);
    }
}

