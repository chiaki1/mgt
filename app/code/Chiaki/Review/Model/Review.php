<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\Data\ReviewInterface;
use Chiaki\Review\Api\Data\ReviewInterfaceFactory;
use Chiaki\Review\Model\ResourceModel\Review\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Review extends AbstractModel implements ReviewInterface
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'chiaki_review_review';
    protected $reviewDataFactory;

    /**
     * @param Context $context
     * @param Registry      $registry
     * @param ReviewInterfaceFactory           $reviewDataFactory
     * @param DataObjectHelper                 $dataObjectHelper
     * @param ResourceModel\Review             $resource
     * @param Collection                       $resourceCollection
     * @param array                            $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ReviewInterfaceFactory $reviewDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Review $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->reviewDataFactory = $reviewDataFactory;
        $this->dataObjectHelper  = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve review model with review data
     *
     * @return ReviewInterface
     */
    public function getDataModel()
    {
        $reviewData = $this->getData();

        $reviewDataObject = $this->reviewDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $reviewDataObject,
            $reviewData,
            ReviewInterface::class
        );

        return $reviewDataObject;
    }

    /**
     * Get review_id
     *
     * @return string|null
     */
    public function getReviewId()
    {
        return $this->_getData(self::REVIEW_ID);
    }

    /**
     * Set review_id
     *
     * @param string $reviewId
     *
     * @return ReviewInterface
     */
    public function setReviewId($reviewId)
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }

    /**
     * Get order_id
     *
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_getData(self::ORDER_ID);
    }

    /**
     * Set order_id
     *
     * @param string $orderId
     *
     * @return ReviewInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get comment
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->_getData(self::COMMENT);
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ReviewInterface
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * Get rating
     *
     * @return string|null
     */
    public function getRating()
    {
        return $this->_getData(self::RATING);
    }

    /**
     * Set rating
     *
     * @param string $rating
     *
     * @return ReviewInterface
     */
    public function setRating($rating)
    {
        return $this->setData(self::RATING, $rating);
    }

    /**
     * Get answer
     *
     * @return string|null
     */
    public function getAnswer()
    {
        return $this->_getData(self::ANSWER);
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return ReviewInterface
     */
    public function setAnswer($answer)
    {
        return $this->setData(self::ANSWER, $answer);
    }
}

