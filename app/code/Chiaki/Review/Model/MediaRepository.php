<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\Data\MediaInterface;
use Chiaki\Review\Api\Data\MediaInterfaceFactory;
use Chiaki\Review\Api\Data\MediaSearchResultsInterfaceFactory;
use Chiaki\Review\Api\MediaRepositoryInterface;
use Chiaki\Review\Model\ResourceModel\Media as ResourceMedia;
use Chiaki\Review\Model\ResourceModel\Media\CollectionFactory as MediaCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class MediaRepository implements MediaRepositoryInterface
{

    protected $dataMediaFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $mediaCollectionFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $mediaFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    /**
     * @param ResourceMedia                      $resource
     * @param MediaFactory                       $mediaFactory
     * @param MediaInterfaceFactory              $dataMediaFactory
     * @param MediaCollectionFactory             $mediaCollectionFactory
     * @param MediaSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                   $dataObjectHelper
     * @param DataObjectProcessor                $dataObjectProcessor
     * @param StoreManagerInterface              $storeManager
     * @param CollectionProcessorInterface       $collectionProcessor
     * @param JoinProcessorInterface             $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter      $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMedia $resource,
        MediaFactory $mediaFactory,
        MediaInterfaceFactory $dataMediaFactory,
        MediaCollectionFactory $mediaCollectionFactory,
        MediaSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->mediaFactory                     = $mediaFactory;
        $this->mediaCollectionFactory           = $mediaCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataMediaFactory                 = $dataMediaFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        MediaInterface $media
    ) {
        /* if (empty($media->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $media->setStoreId($storeId);
        } */

        $mediaData = $this->extensibleDataObjectConverter->toNestedArray(
            $media,
            [],
            MediaInterface::class
        );

        $mediaModel = $this->mediaFactory->create()->setData($mediaData);

        try {
            $this->resource->save($mediaModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the media: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $mediaModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($mediaId)
    {
        $media = $this->mediaFactory->create();
        $this->resource->load($media, $mediaId);
        if (!$media->getId()) {
            throw new NoSuchEntityException(__('Media with id "%1" does not exist.', $mediaId));
        }
        return $media->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->mediaCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            MediaInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        MediaInterface $media
    ) {
        try {
            $mediaModel = $this->mediaFactory->create();
            $this->resource->load($mediaModel, $media->getMediaId());
            $this->resource->delete($mediaModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Media: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($mediaId)
    {
        return $this->delete($this->get($mediaId));
    }
}

