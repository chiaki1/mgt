<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\Data\AnswerInterface;
use Chiaki\Review\Api\Data\AnswerInterfaceFactory;
use Chiaki\Review\Model\ResourceModel\Answer\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Answer extends AbstractModel implements AnswerInterface
{

    protected $answerDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'chiaki_review_answer';

    /**
     * @param Context $context
     * @param Registry      $registry
     * @param AnswerInterfaceFactory           $answerDataFactory
     * @param DataObjectHelper                 $dataObjectHelper
     * @param ResourceModel\Answer             $resource
     * @param Collection                       $resourceCollection
     * @param array                            $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AnswerInterfaceFactory $answerDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Answer $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->answerDataFactory = $answerDataFactory;
        $this->dataObjectHelper  = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve answer model with answer data
     *
     * @return AnswerInterface
     */
    public function getDataModel()
    {
        $answerData = $this->getData();

        $answerDataObject = $this->answerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $answerDataObject,
            $answerData,
            AnswerInterface::class
        );

        return $answerDataObject;
    }

    /**
     * Get answer_id
     *
     * @return string|null
     */
    public function getAnswerId()
    {
        return $this->_getData(self::ANSWER_ID);
    }

    /**
     * Set answer_id
     *
     * @param string $answerId
     *
     * @return AnswerInterface
     */
    public function setAnswerId($answerId)
    {
        return $this->setData(self::ANSWER_ID, $answerId);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->_getData(self::CONTENT);
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return AnswerInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }
}

