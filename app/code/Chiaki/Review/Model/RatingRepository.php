<?php

declare(strict_types=1);

namespace Chiaki\Review\Model;

use Chiaki\Review\Api\Data\RatingInterface;
use Chiaki\Review\Api\Data\RatingInterfaceFactory;
use Chiaki\Review\Api\Data\RatingSearchResultsInterfaceFactory;
use Chiaki\Review\Api\RatingRepositoryInterface;
use Chiaki\Review\Model\ResourceModel\Rating as ResourceRating;
use Chiaki\Review\Model\ResourceModel\Rating\CollectionFactory as RatingCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class RatingRepository implements RatingRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    private $storeManager;

    protected $ratingCollectionFactory;

    protected $dataObjectHelper;

    protected $dataRatingFactory;

    protected $ratingFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    /**
     * @param ResourceRating                      $resource
     * @param RatingFactory                       $ratingFactory
     * @param RatingInterfaceFactory              $dataRatingFactory
     * @param RatingCollectionFactory             $ratingCollectionFactory
     * @param RatingSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                    $dataObjectHelper
     * @param DataObjectProcessor                 $dataObjectProcessor
     * @param StoreManagerInterface               $storeManager
     * @param CollectionProcessorInterface        $collectionProcessor
     * @param JoinProcessorInterface              $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter       $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceRating $resource,
        RatingFactory $ratingFactory,
        RatingInterfaceFactory $dataRatingFactory,
        RatingCollectionFactory $ratingCollectionFactory,
        RatingSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->ratingFactory                    = $ratingFactory;
        $this->ratingCollectionFactory          = $ratingCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataRatingFactory                = $dataRatingFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        RatingInterface $rating
    ) {
        /* if (empty($rating->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $rating->setStoreId($storeId);
        } */

        $ratingData = $this->extensibleDataObjectConverter->toNestedArray(
            $rating,
            [],
            RatingInterface::class
        );

        $ratingModel = $this->ratingFactory->create()->setData($ratingData);

        try {
            $this->resource->save($ratingModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the rating: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $ratingModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($ratingId)
    {
        $rating = $this->ratingFactory->create();
        $this->resource->load($rating, $ratingId);
        if (!$rating->getId()) {
            throw new NoSuchEntityException(__('Rating with id "%1" does not exist.', $ratingId));
        }
        return $rating->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->ratingCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            RatingInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        RatingInterface $rating
    ) {
        try {
            $ratingModel = $this->ratingFactory->create();
            $this->resource->load($ratingModel, $rating->getRatingId());
            $this->resource->delete($ratingModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Rating: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($ratingId)
    {
        return $this->delete($this->get($ratingId));
    }
}

