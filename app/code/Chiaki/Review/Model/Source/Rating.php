<?php

namespace Chiaki\Review\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Rating
 *
 * @package Chiaki\Review\Model\Source
 */
class Rating implements ArrayInterface
{
    const RATING_1_STAR = '1';
    const RATING_2_STAR = '2';
    const RATING_3_STAR = '3';
    const RATING_4_STAR = '4';
    const RATING_5_STAR = '5';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::RATING_1_STAR, 'label' => __('One Star')],
            ['value' => self::RATING_2_STAR, 'label' => __('Two Star')],
            ['value' => self::RATING_3_STAR, 'label' => __('Three Star')],
            ['value' => self::RATING_4_STAR, 'label' => __('Four Star')],
            ['value' => self::RATING_5_STAR, 'label' => __('Five Star')],
        ];
    }

    public function toArray()
    {
        return [
            self::RATING_1_STAR => __('One Star'),
            self::RATING_2_STAR => __('Two Star'),
            self::RATING_3_STAR => __('Three Star'),
            self::RATING_4_STAR => __('Four Star'),
            self::RATING_5_STAR => __('Five Star')
        ];
    }
}
