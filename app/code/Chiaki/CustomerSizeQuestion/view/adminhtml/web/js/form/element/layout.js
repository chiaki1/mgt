define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
], function (_, uiRegistry, select) {
    'use strict';
    return select.extend({

        initialize: function () {
            this._super();
            var value = this.initialValue;
            if (value === '2column_image') {
                uiRegistry.promise('customersize_question_form.customersize_question_form.general.gallery').done(function (component) {
                    component.show();
                });
                uiRegistry.promise('customersize_question_form.customersize_question_form.general.options').done(function (component) {
                    component.visible(false);
                });
            } else {
                uiRegistry.promise('customersize_question_form.customersize_question_form.general.gallery').done(function (component) {
                    component.hide();
                });
                uiRegistry.promise('customersize_question_form.customersize_question_form.general.options').done(function (component) {
                    component.visible(true);
                });
            }
            return this;

        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {

            var gallery = uiRegistry.get('customersize_question_form.customersize_question_form.general.gallery');
            var options = uiRegistry.get('customersize_question_form.customersize_question_form.general.options');

            if (value === '2column_image') {
                gallery.show();
                options.visible(false);
            } else {
                gallery.hide();
                options.visible(true);
            }
            return this._super();
        },
    });
});
