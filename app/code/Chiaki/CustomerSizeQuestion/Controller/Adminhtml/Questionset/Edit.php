<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Controller\Adminhtml\Questionset;

use Chiaki\CustomerSizeQuestion\Model\QuestionSet;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Chiaki\CustomerSizeQuestion\Controller\Adminhtml\QuestionSet
{

    protected $resultPageFactory;

    /**
     * @param Context     $context
     * @param Registry    $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id    = $this->getRequest()->getParam('question_set_id');
        $model = $this->_objectManager->create(QuestionSet::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Question Set no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('customer_size_question_set', $model);

        // 3. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Question Set') : __('New Question Set'),
            $id ? __('Edit Question Set') : __('New Question Set')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Questions Set'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Question Set %1', $model->getId()) : __('New Question Set'));
        return $resultPage;
    }
}

