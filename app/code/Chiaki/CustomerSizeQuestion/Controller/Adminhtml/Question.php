<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class Question extends Action
{

    protected $_coreRegistry;

    /**
     * @param Context  $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     *
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Chiaki_CustomerSizeQuestion::manage_question')
                   ->addBreadcrumb(__('Chiaki'), __('Chiaki'))
                   ->addBreadcrumb(__('Question'), __('Question'));
        return $resultPage;
    }
}

