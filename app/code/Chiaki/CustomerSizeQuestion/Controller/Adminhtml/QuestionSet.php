<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class QuestionSet extends Action
{

    protected $_coreRegistry;

    /**
     * @param Context  $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     *
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Chiaki_CustomerSizeQuestion::manage_question_set')
                   ->addBreadcrumb(__('Chiaki'), __('Chiaki'))
                   ->addBreadcrumb(__('Question Set'), __('Question Set'));
        return $resultPage;
    }

    /**
     * Init Slider
     *
     * @return \Chiaki\CustomerSizeQuestion\Model\QuestionSet
     */
    protected function initSet()
    {
        $question_set_id = (int)$this->getRequest()->getParam('question_set_id');
        $model           = $this->_objectManager->create(\Chiaki\CustomerSizeQuestion\Model\QuestionSet::class);
        if ($question_set_id) {
            $model->load($question_set_id);
        }
        $this->_coreRegistry->register('customer_size_question_set', $model);

        return $model;
    }
}

