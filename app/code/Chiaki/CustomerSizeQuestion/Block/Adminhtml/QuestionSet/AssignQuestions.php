<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Chiaki\CustomerSizeQuestion\Block\Adminhtml\QuestionSet;

use Chiaki\CustomerSizeQuestion\Block\Adminhtml\QuestionSet\Tab\Question;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\BlockInterface;

class AssignQuestions extends Template
{
    /**
     * Block template
     *
     * @var string
     */
    protected $_template = 'Chiaki_CustomerSizeQuestion::set/question/edit/assign_questions.phtml';

    /**
     * @var Question
     */
    protected $blockGrid;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * AssignProducts constructor.
     *
     * @param Context  $context
     * @param Registry              $registry
     * @param EncoderInterface $jsonEncoder
     * @param array                                    $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        EncoderInterface $jsonEncoder,
        array $data = []
    ) {
        $this->registry    = $registry;
        $this->jsonEncoder = $jsonEncoder;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve instance of grid block
     *
     * @return BlockInterface
     * @throws LocalizedException
     */
    public function getBlockGrid()
    {
        if (null === $this->blockGrid) {
            $this->blockGrid = $this->getLayout()->createBlock(
                Question::class,
                'questionset.question.grid'
            );
        }
        return $this->blockGrid;
    }

    /**
     * Return HTML of grid block
     *
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getBlockGrid()->toHtml();
    }

    /**
     * @return string
     */
    public function getQuestionsJson()
    {
        $questions = $this->getQuestionSet()->getQuestionsPosition();
        if (!empty($questions)) {
            return $this->jsonEncoder->encode($questions);
        }
        return '{}';
    }

    /**
     * Retrieve current category instance
     *
     * @return array|null
     */
    public function getQuestionSet()
    {
        return $this->registry->registry('customer_size_question_set');
    }
}
