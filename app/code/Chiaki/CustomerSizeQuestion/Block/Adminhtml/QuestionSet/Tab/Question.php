<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Product in category grid
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */

namespace Chiaki\CustomerSizeQuestion\Block\Adminhtml\QuestionSet\Tab;

use Chiaki\CustomerSizeQuestion\Model\QuestionFactory;
use Chiaki\CustomerSizeQuestion\Model\QuestionSetFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\Collection;
use Chiaki\CustomerSizeQuestion\Model\Source\Status;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;

class Question extends Extended
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var QuestionFactory
     */
    protected $_questionFactory;

    /**
     * @var Status
     */
    private $status;

    protected $questionSetFactory;

    /**
     * @param Context $context
     * @param Data            $backendHelper
     * @param ProductFactory   $productFactory
     * @param Registry             $coreRegistry
     * @param array                                   $data
     * @param Status|null                             $status
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        QuestionFactory $questionFactory,
        Registry $coreRegistry,
        QuestionSetFactory $questionSetFactory,
        array $data = [],
        Status $status = null
    ) {
        $this->_questionFactory   = $questionFactory;
        $this->_coreRegistry      = $coreRegistry;
        $this->status             = $status ?: ObjectManager::getInstance()->get(Status::class);
        $this->questionSetFactory = $questionSetFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('questionset_questions');
        $this->setDefaultSort('question_id');
        $this->setUseAjax(true);
    }

    public function getQuestionSet()
    {
        return $this->_coreRegistry->registry('customer_size_question_set');
    }

    /**
     * @param Column $column
     *
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in questionset flag
        if ($column->getId() == 'in_questionset') {
            $questionIds = $this->_getSelectedQuestions();
            if (empty($questionIds)) {
                $questionIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('main_table.question_id', ['in' => $questionIds]);
            } elseif (!empty($questionIds)) {
                $this->getCollection()->addFieldToFilter('main_table.question_id', ['nin' => $questionIds]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {
        if ($this->getQuestionSet()->getId()) {
            $this->setDefaultFilter(['in_questionset' => 1]);
            $constraint = 'question_set_questions.question_set_id=' . $this->getQuestionSet()->getId();
        } else {
            $constraint = 'question_set_questions.question_set_id=0';
        }
        /** @var Collection $collection */
        $collection = $this->_questionFactory->create()->getCollection();
        $collection->getSelect()->joinLeft(
            ['question_set_questions' => $collection->getTable("customer_size_question_set_questions")],
            'question_set_questions.question_id = main_table.question_id AND ' . $constraint,
            ['position']
        );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_questionset',
            [
                'type'             => 'checkbox',
                'name'             => 'in_questionset',
                'values'           => $this->_getSelectedQuestions(),
                'index'            => 'question_id',
                'header_css_class' => 'col-select col-massaction',
                'column_css_class' => 'col-select col-massaction'
            ]
        );
        $this->addColumn(
            'question_id',
            [
                'header'           => __('ID'),
                'sortable'         => true,
                'index'            => 'question_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn('title', ['header' => __('Title'), 'index' => 'title']);

        $this->addColumn(
            'status',
            [
                'header'  => __('Status'),
                'index'   => 'status',
                'type'    => 'options',
                'options' => $this->status->getOptionArray()
            ]
        );

        $this->addColumn(
            'position',
            [
                'header'   => __('Position'),
                'type'     => 'number',
                'index'    => 'position',
                'editable' => true
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('customersize/*/grid', ['_current' => true]);
    }

    /**
     * @return array
     */
    protected function _getSelectedQuestions()
    {
        $questions = $this->getRequest()->getPost('selected_questions');
        if ($questions === null) {
            $questions = $this->getQuestionSet()->getQuestionsPosition();
            if ($questions) {
                return array_keys($questions);
            }
        }
        return $questions;
    }
}
