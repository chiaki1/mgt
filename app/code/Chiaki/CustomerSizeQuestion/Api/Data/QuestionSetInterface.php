<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

interface QuestionSetInterface
{

    const QUESTION_SET_ID = 'question_set_id';
    const TITLE           = 'title';
    const STATUS          = 'status';
    const FROM_DATE       = 'from_date';
    const TO_DATE         = 'to_date';
    const CREATED_AT      = 'created_at';
    const QUESTIONS       = 'questions';

    /**
     * Get question_set_id
     *
     * @return string|null
     */
    public function getQuestionSetId();

    /**
     * Set question_set_id
     *
     * @param string $questionSetId
     *
     * @return QuestionSetInterface
     */
    public function setQuestionSetId($questionSetId);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     *
     * @return QuestionSetInterface
     */
    public function setTitle($title);

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param $status
     *
     * @return QuestionSetInterface
     */
    public function setStatus($status);

    /**
     * Get from_date
     *
     * @return string|null
     */
    public function getFromDate();

    /**
     * Set from_date
     *
     * @param $fromDate
     *
     * @return QuestionSetInterface
     */
    public function setFromDate($fromDate);

    /**
     * Get to_date
     *
     * @return string|null
     */
    public function getToDate();

    /**
     * Set to_date
     *
     * @param $toDate
     *
     * @return QuestionSetInterface
     */
    public function setToDate($toDate);

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     *
     * @param $createdAt
     *
     * @return QuestionSetInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get questions
     *
     * @return array
     */
    public function getQuestions();
}

