<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface QuestionSetSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Question Set list.
     *
     * @return QuestionSetInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     *
     * @param QuestionSetInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

