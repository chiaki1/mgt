<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

interface QuestionInterface
{

    const QUESTION_ID          = 'question_id';
    const TITLE                = 'title';
    const CAN_SKIP             = 'can_skip';
    const LAYOUT               = 'layout';
    const CAN_USE_CUSTOM_VALUE = 'can_use_custom_value';
    const STATUS               = 'status';
    const OPTIONS              = 'options';

    /**
     * Get question_id
     *
     * @return string|null
     */
    public function getQuestionId();

    /**
     * Set question_id
     *
     * @param string $questionId
     *
     * @return QuestionInterface
     */
    public function setQuestionId($questionId);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     *
     * @return QuestionInterface
     */
    public function setTitle($title);

    /**
     * Get layout
     *
     * @return string|null
     */
    public function getLayout();

    /**
     * Set layout
     *
     * @param string $layout
     *
     * @return QuestionInterface
     */
    public function setLayout($layout);

    /**
     * Get can_skip
     *
     * @return string|null
     */
    public function getCanSkip();

    /**
     * Set can_skip
     *
     * @param string $canSkip
     *
     * @return QuestionInterface
     */
    public function setCanSkip($canSkip);

    /**
     * Get can_use_custom_value
     *
     * @return string|null
     */
    public function getCanUseCustomValue();

    /**
     * Set can_use_custom_value
     *
     * @param string $canUseCustomValue
     *
     * @return QuestionInterface
     */
    public function setCanUseCustomValue($canUseCustomValue);

    /**
     * Get status
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param $status
     *
     * @return QuestionInterface
     */
    public function setStatus($status);

    /**
     * Get options
     *
     * @return string|null
     */
    public function getOptions();

    /**
     * Set options
     *
     * @param $options
     *
     * @return QuestionInterface
     */
    public function setOptions($options);
}

