<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface GallerySearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Gallery list.
     *
     * @return GalleryInterface[]
     */
    public function getItems();

    /**
     * Set value list.
     *
     * @param GalleryInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

