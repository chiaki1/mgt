<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface QuestionSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Question list.
     *
     * @return QuestionInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     *
     * @param QuestionInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}

