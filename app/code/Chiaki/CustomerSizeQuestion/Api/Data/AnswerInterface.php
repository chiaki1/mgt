<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

interface AnswerInterface
{

    const CUSTOMER_ID = 'customer_id';
    const VALUE       = 'value';
    const ANSWER_ID   = 'answer_id';
    const NAME        = 'name';

    /**
     * Get answer_id
     *
     * @return string|null
     */
    public function getAnswerId();

    /**
     * Set answer_id
     *
     * @param string $answerId
     *
     * @return AnswerInterface
     */
    public function setAnswerId($answerId);

    /**
     * Get value
     *
     * @return string|null
     */
    public function getValue();

    /**
     * Set value
     *
     * @param string $value
     *
     * @return AnswerInterface
     */
    public function setValue($value);

    /**
     * Get customer_id
     *
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     *
     * @param string $customerId
     *
     * @return AnswerInterface
     */
    public function setCustomerId($customerId);

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AnswerInterface
     */
    public function setName($name);
}
