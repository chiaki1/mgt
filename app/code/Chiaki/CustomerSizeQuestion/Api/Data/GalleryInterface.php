<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

interface GalleryInterface
{

    const IMAGE_NAME  = 'image_name';
    const GALLERY_ID  = 'gallery_id';
    const QUESTION_ID = 'question_id';

    /**
     * Get gallery_id
     *
     * @return string|null
     */
    public function getGalleryId();

    /**
     * Set gallery_id
     *
     * @param string $galleryId
     *
     * @return GalleryInterface
     */
    public function setGalleryId($galleryId);

    /**
     * Get image_name
     *
     * @return string|null
     */
    public function getImageName();

    /**
     * Set value
     *
     * @param string $image_name
     *
     * @return GalleryInterface
     */
    public function setImageName($image_name);

    /**
     * Get question_id
     *
     * @return int
     */
    public function getQuestionId();

    /**
     * Set question_id
     *
     * @param string $question_id
     *
     * @return GalleryInterface
     */
    public function setQuestionId($question_id);
}

