<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface AnswerSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Answer list.
     *
     * @return AnswerInterface[]
     */
    public function getItems();

    /**
     * Set value list.
     *
     * @param AnswerInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}
