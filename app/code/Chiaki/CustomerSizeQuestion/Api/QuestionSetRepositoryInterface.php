<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface QuestionSetRepositoryInterface
{

    /**
     * Save QuestionSet
     *
     * @param QuestionSetInterface $questionSet
     *
     * @return QuestionSetInterface
     * @throws LocalizedException
     */
    public function save(
        QuestionSetInterface $questionSet
    );

    /**
     * Retrieve Question Set
     *
     * @param string $questionSetId
     *
     * @return QuestionSetInterface
     * @throws LocalizedException
     */
    public function get($questionSetId);

    /**
     * Retrieve Question matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return QuestionSetSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Question Set
     *
     * @param QuestionSetInterface $questionSet
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        QuestionSetInterface $questionSet
    );

    /**
     * Delete Question Set by ID
     *
     * @param string $questionSetId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($questionSetId);
}

