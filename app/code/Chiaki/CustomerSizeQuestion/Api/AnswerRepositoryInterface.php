<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api;

use Chiaki\CustomerSizeQuestion\Api\Data\AnswerInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\AnswerSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface AnswerRepositoryInterface
{

    /**
     * Save Answer
     *
     * @param AnswerInterface $answer
     *
     * @return AnswerInterface
     * @throws LocalizedException
     */
    public function save(
        AnswerInterface $answer
    );

    /**
     * Retrieve Answer
     *
     * @param string $answerId
     *
     * @return AnswerInterface
     * @throws LocalizedException
     */
    public function get($answerId);

    /**
     * Retrieve Answer matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return AnswerSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Answer
     *
     * @param AnswerInterface $answer
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        AnswerInterface $answer
    );

    /**
     * Delete Answer by ID
     *
     * @param string $answerId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($answerId);
}
