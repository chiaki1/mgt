<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface QuestionRepositoryInterface
{

    /**
     * Save Question
     *
     * @param QuestionInterface $question
     *
     * @return QuestionInterface
     * @throws LocalizedException
     */
    public function save(
        QuestionInterface $question
    );

    /**
     * Retrieve Question
     *
     * @param string $questionId
     *
     * @return QuestionInterface
     * @throws LocalizedException
     */
    public function get($questionId);

    /**
     * Retrieve Question matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return QuestionSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Question
     *
     * @param QuestionInterface $question
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        QuestionInterface $question
    );

    /**
     * Delete Question by ID
     *
     * @param string $questionId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($questionId);
}

