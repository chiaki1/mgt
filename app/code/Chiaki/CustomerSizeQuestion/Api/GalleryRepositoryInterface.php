<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Api;

use Chiaki\CustomerSizeQuestion\Api\Data\GalleryInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\GallerySearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface GalleryRepositoryInterface
{

    /**
     * Save Gallery
     *
     * @param GalleryInterface $gallery
     *
     * @return GalleryInterface
     * @throws LocalizedException
     */
    public function save(
        GalleryInterface $gallery
    );

    /**
     * Retrieve Gallery
     *
     * @param string $galleryId
     *
     * @return GalleryInterface
     * @throws LocalizedException
     */
    public function get($galleryId);

    /**
     * Retrieve Gallery matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return GallerySearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Gallery
     *
     * @param GalleryInterface $gallery
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        GalleryInterface $gallery
    );

    /**
     * Delete Gallery by ID
     *
     * @param string $galleryId
     *
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($galleryId);
}

