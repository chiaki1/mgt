<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\Collection;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Store\Model\StoreManagerInterface;

class Question extends AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'customer_size_question';
    protected $questionDataFactory;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * @param Context                  $context
     * @param Registry                 $registry
     * @param QuestionInterfaceFactory $questionDataFactory
     * @param DataObjectHelper         $dataObjectHelper
     * @param ResourceModel\Question   $resource
     * @param Collection               $resourceCollection
     * @param array                    $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        QuestionInterfaceFactory $questionDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Question $resource,
        Collection $resourceCollection,
        SerializerJson $serializer,
        ImageUploader $imageUploader = null,
        StoreManagerInterface $storeManager = null,
        array $data = []
    ) {
        $this->questionDataFactory = $questionDataFactory;
        $this->dataObjectHelper    = $dataObjectHelper;
        $this->imageUploader       = $imageUploader ?: ObjectManager::getInstance()->get(ImageUploader::class);
        $this->serializer          = $serializer;
        $this->storeManager        = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve question model with question data
     *
     * @return QuestionInterface
     */
    public function getDataModel()
    {
        $questionData = $this->getData();

        $questionDataObject = $this->questionDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $questionDataObject,
            $questionData,
            QuestionInterface::class
        );

        return $questionDataObject;
    }

    /**
     * Avoiding saving potential upload data to DB.
     *
     * Will set empty image attribute value if image was not uploaded.
     *
     * @return $this
     * @since 101.0.8
     */
    public function beforeSave()
    {

        $options = $this->getData('options');
        if ($options) {
            $options = $this->serializer->serialize($options);
            $this->setOptions($options);
        }

        return parent::beforeSave();
    }
}

