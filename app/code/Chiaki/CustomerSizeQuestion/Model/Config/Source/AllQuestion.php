<?php

namespace Chiaki\CustomerSizeQuestion\Model\Config\Source;

use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class AllQuestion implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * AllQuestion constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options    = [];
        $options[]  = [
            'value' => '',
            'label' => __("Please select a question.")
        ];
        $collection = $this->_collectionFactory->create();
        foreach ($collection as $item) {
            $options[] = [
                'value' => $item->getId(),
                'label' => $item->getTitle()
            ];
        }
        return $options;
    }
}
