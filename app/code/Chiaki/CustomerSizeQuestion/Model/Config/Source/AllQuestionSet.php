<?php

namespace Chiaki\CustomerSizeQuestion\Model\Config\Source;

use Chiaki\CustomerSizeQuestion\Model\ResourceModel\QuestionSet\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class AllQuestionSet implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * AllQuestion constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options    = [];
        $collection = $this->_collectionFactory->create();
        foreach ($collection as $item) {
            $options[] = [
                'value' => $item->getId(),
                'label' => $item->getTitle()
            ];
        }
        return $options;
    }
}
