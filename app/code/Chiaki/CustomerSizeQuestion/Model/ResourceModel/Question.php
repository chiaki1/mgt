<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\ResourceModel;

use Chiaki\CustomerSizeQuestion\Model\GalleryFactory;
use Chiaki\CustomerSizeQuestion\Model\ImageProcessor;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Gallery\Collection as GalleryCollection;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Store\Model\StoreManagerInterface;

class Question extends AbstractDb
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var File
     */
    protected $ioFile;

    /**
     * @var ImageProcessor
     */
    protected $imageProcessor;

    /**
     * @var GalleryCollection
     */
    protected $galleryCollection;

    /**
     * @var Gallery
     */
    protected $galleryResource;

    /**
     * @var GalleryFactory
     */
    protected $galleryFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        Context $context,
        Filesystem $filesystem,
        File $ioFile,
        ImageProcessor $imageProcessor,
        GalleryCollection $galleryCollection,
        GalleryFactory $galleryFactory,
        Gallery $galleryResource,
        StoreManagerInterface $storeManager,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->filesystem        = $filesystem;
        $this->ioFile            = $ioFile;
        $this->imageProcessor    = $imageProcessor;
        $this->galleryCollection = $galleryCollection;
        $this->galleryFactory    = $galleryFactory;
        $this->galleryResource   = $galleryResource;
        $this->storeManager      = $storeManager;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_size_question', 'question_id');
    }

    protected function _beforeDelete(AbstractModel $object)
    {
        $allImages = $this->galleryCollection->getImagesByQuestion($object->getId());

        foreach ($allImages as $image) {
            $this->galleryResource->delete($image);
        }
    }

    protected function _afterSave(AbstractModel $object)
    {
        if (!($object->getData('inlineEdit') || $object->getData('massAction'))) {
            $this->saveGallery($object->getData(), $object->isObjectNew());
        }
    }

    private function saveGallery($data, $isObjectNew = false)
    {
        $questionId = $data['question_id'];
        $allImages  = $this->galleryCollection->getImagesByQuestion($questionId);

        if (!isset($data['gallery_image'])) {
            foreach ($allImages as $image) {
                $this->galleryResource->delete($image);
            }
            return;
        }
        $galleryImages    = $data['gallery_image'];
        $imagesOfQuestion = [];

        foreach ($allImages as $image) {
            $imagesOfQuestion[$image->getData('image_name')] = $image;
        }

        foreach ($galleryImages as $galleryImage) {
            if (array_key_exists($galleryImage['name'], $imagesOfQuestion)) {
                unset($imagesOfQuestion[$galleryImage['name']]);
            }
            if (isset($galleryImage['tmp_name']) && isset($galleryImage['name'])) {
                $newImage = $this->galleryFactory->create();
                $newImage->addData(
                    [
                        'question_id'     => $questionId,
                        'image_name'      => $galleryImage['name'],
                        'question_is_new' => $isObjectNew
                    ]
                );
                $this->galleryResource->save($newImage);
            }
        }

        foreach ($imagesOfQuestion as $imageToDelete) {
            $this->galleryResource->delete($imageToDelete);
        }
    }
}

