<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\ResourceModel\Gallery;

use Chiaki\CustomerSizeQuestion\Model\Gallery;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Psr\Log\LoggerInterface;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'gallery_id';

    protected $factory;

    public function __construct(
        EntityFactoryInterface $entityFactory, LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        CollectionFactory $factory,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->factory = $factory;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Gallery::class,
            \Chiaki\CustomerSizeQuestion\Model\ResourceModel\Gallery::class
        );
    }

    public function getImagesByQuestion($questionId)
    {
        /** @var Collection $imagesCollection */
        $imagesCollection = $this->factory->create();

        /** @var Gallery[] $images */
        $images = $imagesCollection->addFieldToFilter('question_id', $questionId)->getItems();

        return $images;
    }
}

