<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\ResourceModel\QuestionSet;

use Chiaki\CustomerSizeQuestion\Model\ResourceModel\QuestionSet;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'question_set_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Chiaki\CustomerSizeQuestion\Model\QuestionSet::class,
            QuestionSet::class
        );
    }
}
