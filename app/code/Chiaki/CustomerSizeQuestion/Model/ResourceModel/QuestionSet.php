<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\ResourceModel;

use Magento\Framework\DataObject;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Zend_Db_Expr;

class QuestionSet extends AbstractDb
{

    protected $_questionSetQuestionsTable;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_size_question_set', 'question_set_id');
    }

    /**
     * Get positions of associated to question set questions
     *
     * @param \Chiaki\CustomerSizeQuestion\Model\QuestionSet $questionSet
     *
     * @return array
     */
    public function getQuestionsPosition($questionSet)
    {
        $select = $this->getConnection()->select()->from(
            $this->getQuestionSetQuestionsTable(),
            ['question_id', 'position']
        )->where(
            "{$this->getTable('customer_size_question_set_questions')}.question_set_id = ?",
            $questionSet->getId()
        );
        $bind   = ['question_set_id' => (int)$questionSet->getId()];

        return $this->getConnection()->fetchPairs($select, $bind);
    }

    /**
     * Category product table name getter
     *
     * @return string
     */
    public function getQuestionSetQuestionsTable()
    {
        if (!$this->_questionSetQuestionsTable) {
            $this->_questionSetQuestionsTable = $this->getTable('customer_size_question_set_questions');
        }
        return $this->_questionSetQuestionsTable;
    }

    /**
     * @inheirtDoc 
     */
    protected function _afterSave($object)
    {

        $this->_saveSetQuestions($object);
        return parent::_afterSave($object);
    }

    /**
     * @param \Chiaki\CustomerSizeQuestion\Model\QuestionSet $questionSet
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _saveSetQuestions($questionSet)
    {
        $questionSet->setIsChangedQuestionList(false);
        $id = $questionSet->getId();

        $questions = $questionSet->getQuestionsetQuestions();
        if (!is_array($questions)) {
            $questions = json_decode($questions, true);
        }

        /**
         * Example re-save category
         */
        if ($questions === null) {
            return $this;
        }

        /**
         * old category-product relationships
         */
        $oldQuestions = $questionSet->getQuestionsPosition();

        $insert = array_diff_key($questions, $oldQuestions);
        $delete = array_diff_key($oldQuestions, $questions);

        $update = array_intersect_key($questions, $oldQuestions);
        $update = array_diff_assoc($update, $oldQuestions);

        $connection = $this->getConnection();

        if (!empty($delete)) {
            $cond = ['question_id IN(?)' => array_keys($delete), 'question_set_id=?' => $id];
            $connection->delete($this->getQuestionSetQuestionsTable(), $cond);
        }

        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $questionId => $position) {
                $data[] = [
                    'question_set_id' => (int)$id,
                    'question_id'     => (int)$questionId,
                    'position'        => (int)$position,
                ];
            }
            $connection->insertMultiple($this->getQuestionSetQuestionsTable(), $data);
        }

        if (!empty($update)) {
            $newPositions = [];
            foreach ($update as $questionId => $position) {
                $delta = $position - $oldQuestions[$questionId];
                if (!isset($newPositions[$delta])) {
                    $newPositions[$delta] = [];
                }
                $newPositions[$delta][] = $questionId;
            }

            foreach ($newPositions as $delta => $questionIds) {
                $bind  = ['position' => new Zend_Db_Expr("position + ({$delta})")];
                $where = ['question_set_id = ?' => (int)$id, 'question_id IN (?)' => $questionIds];
                $connection->update($this->getQuestionSetQuestionsTable(), $bind, $where);
            }
        }

        if (!empty($insert) || !empty($delete)) {
            $questionIds = array_unique(array_merge(array_keys($insert), array_keys($delete)));

            $questionSet->setChangedQuesrionIds($questionIds);
        }

        if (!empty($insert) || !empty($update) || !empty($delete)) {
            $questionSet->setIsChangedQuestionList(true);

            $questionIds = array_keys($insert + $delete + $update);
            $questionSet->setAffectedQuestionIds($questionIds);
        }
        return $this;
    }
}
