<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Answer extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_answer_question', 'answer_id');
    }

    /**
     * @param \Chiaki\CustomerSizeQuestion\Model\Answer $answer
     */
    public function activeAnswer($answer)
    {
        $this->getConnection()->update(
            $this->getTable('customer_answer_question'),
            ['active' => 0],
            ['customer_id = ?' => $answer->getData('customer_id')]
        );
        $this->getConnection()->update(
            $this->getTable('customer_answer_question'),
            ['active' => 1],
            ['answer_id = ?' => $answer->getData('answer_id')]
        );
    }
}
