<?php

namespace Chiaki\CustomerSizeQuestion\Model\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class
 */
class Layout implements ArrayInterface
{
    const LAYOUT_ONE_COLUMN_BUTTON  = '1column_button';
    const LAYOUT_TWO_COLUMN_BUTTON  = '2column_button';
    const LAYOUT_FOUR_COLUMN_BUTTON = '4column_button';
    const LAYOUT_TWO_COLUMN_IMAGE   = '2column_image';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::LAYOUT_ONE_COLUMN_BUTTON, 'label' => __('One Column Button')],
            ['value' => self::LAYOUT_TWO_COLUMN_BUTTON, 'label' => __('Two Column Button')],
            ['value' => self::LAYOUT_FOUR_COLUMN_BUTTON, 'label' => __('Four Column Button')],
            ['value' => self::LAYOUT_TWO_COLUMN_IMAGE, 'label' => __('Two Column Image')],
        ];
    }
}
