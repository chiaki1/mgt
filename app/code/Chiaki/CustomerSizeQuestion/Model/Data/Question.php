<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\Data;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Question extends AbstractSimpleObject implements QuestionInterface
{

    /**
     * Get question_id
     *
     * @return string|null
     */
    public function getQuestionId()
    {
        return $this->_get(self::QUESTION_ID);
    }

    /**
     * Set question_id
     *
     * @param string $questionId
     *
     * @return QuestionInterface
     */
    public function setQuestionId($questionId)
    {
        return $this->setData(self::QUESTION_ID, $questionId);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return QuestionInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get layout
     *
     * @return string|null
     */
    public function getLayout()
    {
        return $this->_get(self::LAYOUT);
    }

    /**
     * Set layout
     *
     * @param string $layout
     *
     * @return QuestionInterface
     */
    public function setLayout($layout)
    {
        return $this->setData(self::LAYOUT, $layout);
    }

    /**
     * Get can_skip
     *
     * @return string|null
     */
    public function getCanSkip()
    {
        return $this->_get(self::CAN_SKIP);
    }

    /**
     * Set can_skip
     *
     * @param string $canSkip
     *
     * @return QuestionInterface
     */
    public function setCanSkip($canSkip)
    {
        return $this->setData(self::CAN_SKIP, $canSkip);
    }

    /**
     * Get can_use_custom_value
     *
     * @return string|null
     */
    public function getCanUseCustomValue()
    {
        return $this->_get(self::CAN_USE_CUSTOM_VALUE);
    }

    /**
     * Set can_use_custom_value
     *
     * @param string $canUseCustomValue
     *
     * @return QuestionInterface
     */
    public function setCanUseCustomValue($canUseCustomValue)
    {
        return $this->setData(self::CAN_USE_CUSTOM_VALUE, $canUseCustomValue);
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     *
     * @param $status
     *
     * @return QuestionInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get options
     *
     * @return string|null
     */
    public function getOptions()
    {
        return $this->_get(self::OPTIONS);
    }

    /**
     * Set options
     *
     * @param $options
     *
     * @return QuestionInterface
     */
    public function setOptions($options)
    {
        return $this->setData(self::OPTIONS, $options);
    }
}
