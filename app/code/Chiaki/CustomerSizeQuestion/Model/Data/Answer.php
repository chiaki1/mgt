<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\Data;

use Chiaki\CustomerSizeQuestion\Api\Data\AnswerInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Answer extends AbstractSimpleObject implements AnswerInterface
{

    /**
     * Get answer_id
     *
     * @return string|null
     */
    public function getAnswerId()
    {
        return $this->_get(self::ANSWER_ID);
    }

    /**
     * Set answer_id
     *
     * @param string $answerId
     *
     * @return AnswerInterface
     */
    public function setAnswerId($answerId)
    {
        return $this->setData(self::ANSWER_ID, $answerId);
    }

    /**
     * Get value
     *
     * @return string|null
     */
    public function getValue()
    {
        return $this->_get(self::VALUE);
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return AnswerInterface
     */
    public function setValue($value)
    {
        return $this->setData(self::VALUE, $value);
    }

    /**
     * Get customer_id
     *
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     *
     * @param string $customerId
     *
     * @return AnswerInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AnswerInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
}
