<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\Data;

use Chiaki\CustomerSizeQuestion\Api\Data\GalleryInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Gallery extends AbstractSimpleObject implements GalleryInterface
{

    /**
     * Get gallery_id
     *
     * @return string|null
     */
    public function getGalleryId()
    {
        return $this->_get(self::GALLERY_ID);
    }

    /**
     * Set gallery_id
     *
     * @param string $galleryId
     *
     * @return GalleryInterface
     */
    public function setGalleryId($galleryId)
    {
        return $this->setData(self::GALLERY_ID, $galleryId);
    }

    /**
     * Get image_name
     *
     * @return string|null
     */
    public function getImageName()
    {
        return $this->_get(self::IMAGE_NAME);
    }

    /**
     * Set image_name
     *
     * @param string $image_name
     *
     * @return GalleryInterface
     */
    public function setImageName($image_name)
    {
        return $this->setData(self::IMAGE_NAME, $image_name);
    }

    /**
     * Get question_id
     *
     * @return int
     */
    public function getQuestionId()
    {
        return $this->_get(self::QUESTION_ID);
    }

    /**
     * Set question_id
     *
     * @param string $question_id
     *
     * @return GalleryInterface
     */
    public function setQuestionId($question_id)
    {
        return $this->setData(self::QUESTION_ID, $question_id);
    }
}

