<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\Data;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class QuestionSet extends AbstractSimpleObject implements QuestionSetInterface
{

    /**
     * Get question_set_id
     *
     * @return string|null
     */
    public function getQuestionSetId()
    {
        return $this->_get(self::QUESTION_SET_ID);
    }

    /**
     * Set question_set_id
     *
     * @param string $questionSetId
     *
     * @return QuestionSetInterface
     */
    public function setQuestionSetId($questionSetId)
    {
        return $this->setData(self::QUESTION_SET_ID, $questionSetId);
    }

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return QuestionSetInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     *
     * @param $status
     *
     * @return QuestionSetInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get from_date
     *
     * @return string|null
     */
    public function getFromDate()
    {
        return $this->_get(self::FROM_DATE);
    }

    /**
     * Set from_date
     *
     * @param $fromDate
     *
     * @return QuestionSetInterface
     */
    public function setFromDate($fromDate)
    {
        return $this->setData(self::FROM_DATE, $fromDate);
    }

    /**
     * Get to_date
     *
     * @return string|null
     */
    public function getToDate()
    {
        return $this->_get(self::TO_DATE);
    }

    /**
     * Set to_date
     *
     * @param $toDate
     *
     * @return QuestionSetInterface
     */
    public function setToDate($toDate)
    {
        return $this->setData(self::TO_DATE, $toDate);
    }

    /**
     * Get questions
     *
     * @return array
     */
    public function getQuestions()
    {
        return $this->_get(self::QUESTIONS);
    }

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     *
     * @param $createdAt
     *
     * @return QuestionSetInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
