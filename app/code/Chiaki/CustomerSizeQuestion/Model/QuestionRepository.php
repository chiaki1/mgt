<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSearchResultsInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Api\QuestionRepositoryInterface;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question as ResourceQuestion;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\CollectionFactory as QuestionCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class QuestionRepository implements QuestionRepositoryInterface
{

    protected $questionFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $dataQuestionFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $questionCollectionFactory;

    /**
     * @param ResourceQuestion                      $resource
     * @param QuestionFactory                       $questionFactory
     * @param QuestionInterfaceFactory              $dataQuestionFactory
     * @param QuestionCollectionFactory             $questionCollectionFactory
     * @param QuestionSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                      $dataObjectHelper
     * @param DataObjectProcessor                   $dataObjectProcessor
     * @param StoreManagerInterface                 $storeManager
     * @param CollectionProcessorInterface          $collectionProcessor
     * @param JoinProcessorInterface                $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter         $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceQuestion $resource,
        QuestionFactory $questionFactory,
        QuestionInterfaceFactory $dataQuestionFactory,
        QuestionCollectionFactory $questionCollectionFactory,
        QuestionSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->questionFactory                  = $questionFactory;
        $this->questionCollectionFactory        = $questionCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataQuestionFactory              = $dataQuestionFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        QuestionInterface $question
    ) {
        /* if (empty($question->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $question->setStoreId($storeId);
        } */

        $questionData = $this->extensibleDataObjectConverter->toNestedArray(
            $question,
            [],
            QuestionInterface::class
        );

        $questionModel = $this->questionFactory->create()->setData($questionData);

        try {
            $this->resource->save($questionModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the question: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $questionModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($questionId)
    {
        $question = $this->questionFactory->create();
        $this->resource->load($question, $questionId);
        if (!$question->getId()) {
            throw new NoSuchEntityException(__('Question with id "%1" does not exist.', $questionId));
        }
        return $question->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->questionCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            QuestionInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        QuestionInterface $question
    ) {
        try {
            $questionModel = $this->questionFactory->create();
            $this->resource->load($questionModel, $question->getQuestionId());
            $this->resource->delete($questionModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Question: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($questionId)
    {
        return $this->delete($this->get($questionId));
    }
}

