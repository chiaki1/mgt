<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\QuestionSet\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class QuestionSet extends AbstractModel
{

    protected $_eventPrefix = 'customer_size_question_set';
    protected $questionSetDataFactory;

    protected $dataObjectHelper;

    /**
     * @param Context                     $context
     * @param Registry                    $registry
     * @param QuestionSetInterfaceFactory $questionSetDataFactory
     * @param DataObjectHelper            $dataObjectHelper
     * @param ResourceModel\QuestionSet   $resource
     * @param Collection                  $resourceCollection
     * @param array                       $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        QuestionSetInterfaceFactory $questionSetDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\QuestionSet $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->questionSetDataFactory = $questionSetDataFactory;
        $this->dataObjectHelper       = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve question set model with question set data
     *
     * @return QuestionSetInterface
     */
    public function getDataModel()
    {
        $answerData = $this->getData();

        $answerDataObject = $this->questionSetDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $answerDataObject,
            $answerData,
            QuestionSetInterface::class
        );

        return $answerDataObject;
    }

    /**
     * Retrieve array of question id's for set
     *
     * The array returned has the following format:
     * array($questionId => $position)
     *
     * @return array
     */
    public function getQuestionsPosition()
    {
        if (!$this->getId()) {
            return [];
        }

        $array = $this->getData('questions_position');
        if ($array === null) {
            $array = $this->getResource()->getQuestionsPosition($this);
            $this->setData('questions_position', $array);
        }
        return $array;
    }
}
