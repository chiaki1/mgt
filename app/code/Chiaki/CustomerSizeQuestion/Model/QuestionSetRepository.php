<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Api\Data\QuestionSetSearchResultsInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Api\QuestionSetRepositoryInterface;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\QuestionSet as ResourceQuestionSet;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\QuestionSet\CollectionFactory as QuestionSetCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class QuestionSetRepository implements QuestionSetRepositoryInterface
{

    protected $questionSetFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $dataQuestionSetFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $questionSetCollectionFactory;

    /**
     * @param ResourceQuestionSet                      $resource
     * @param QuestionSetFactory                       $questionSetFactory
     * @param QuestionSetInterfaceFactory              $dataQuestionSetFactory
     * @param QuestionSetCollectionFactory             $questionSetCollectionFactory
     * @param QuestionSetSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                         $dataObjectHelper
     * @param DataObjectProcessor                      $dataObjectProcessor
     * @param StoreManagerInterface                    $storeManager
     * @param CollectionProcessorInterface             $collectionProcessor
     * @param JoinProcessorInterface                   $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter            $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceQuestionSet $resource,
        QuestionSetFactory $questionSetFactory,
        QuestionSetInterfaceFactory $dataQuestionSetFactory,
        QuestionSetCollectionFactory $questionSetCollectionFactory,
        QuestionSetSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->questionSetFactory               = $questionSetFactory;
        $this->questionSetCollectionFactory     = $questionSetCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataQuestionSetFactory           = $dataQuestionSetFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        QuestionSetInterface $questionSet
    ) {

        $questionSetData = $this->extensibleDataObjectConverter->toNestedArray(
            $questionSet,
            [],
            QuestionSetInterface::class
        );

        $questionSetModel = $this->questionSetFactory->create()->setData($questionSetData);

        try {
            $this->resource->save($questionSetModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the question set: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $questionSetModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($questionSetId)
    {
        $questionSet = $this->questionSetFactory->create();
        $this->resource->load($questionSet, $questionSetId);
        if (!$questionSet->getId()) {
            throw new NoSuchEntityException(__('Question Set with id "%1" does not exist.', $questionSetId));
        }
        return $questionSet->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->questionSetCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            QuestionSetInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        QuestionSetInterface $questionSet
    ) {
        try {
            $questionSetModel = $this->questionSetFactory->create();
            $this->resource->load($questionSetModel, $questionSet->getQuestionSetId());
            $this->resource->delete($questionSetModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Question Set: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($questionSetId)
    {
        return $this->delete($this->get($questionSetId));
    }
}

