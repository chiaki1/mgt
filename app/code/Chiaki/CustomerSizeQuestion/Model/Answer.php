<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\Data\AnswerInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\AnswerInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Answer\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Answer extends AbstractModel
{

    protected $_eventPrefix = 'customer_answer_question';
    protected $answerDataFactory;

    protected $dataObjectHelper;

    /**
     * @param Context                $context
     * @param Registry               $registry
     * @param AnswerInterfaceFactory $answerDataFactory
     * @param DataObjectHelper       $dataObjectHelper
     * @param ResourceModel\Answer   $resource
     * @param Collection             $resourceCollection
     * @param array                  $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AnswerInterfaceFactory $answerDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Answer $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->answerDataFactory = $answerDataFactory;
        $this->dataObjectHelper  = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve answer model with answer data
     *
     * @return AnswerInterface
     */
    public function getDataModel()
    {
        $answerData = $this->getData();

        $answerDataObject = $this->answerDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $answerDataObject,
            $answerData,
            AnswerInterface::class
        );

        return $answerDataObject;
    }

    public function activeAnswer()
    {
        $this->getResource()->activeAnswer($this);
    }
}
