<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\AnswerRepositoryInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\AnswerInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\AnswerInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Api\Data\AnswerSearchResultsInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Answer as ResourceAnswer;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Answer\CollectionFactory as AnswerCollectionFactory;
use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class AnswerRepository implements AnswerRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $dataAnswerFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $answerFactory;

    protected $answerCollectionFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    /**
     * @param ResourceAnswer                      $resource
     * @param AnswerFactory                       $answerFactory
     * @param AnswerInterfaceFactory              $dataAnswerFactory
     * @param AnswerCollectionFactory             $answerCollectionFactory
     * @param AnswerSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper                    $dataObjectHelper
     * @param DataObjectProcessor                 $dataObjectProcessor
     * @param StoreManagerInterface               $storeManager
     * @param CollectionProcessorInterface        $collectionProcessor
     * @param JoinProcessorInterface              $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter       $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceAnswer $resource,
        AnswerFactory $answerFactory,
        AnswerInterfaceFactory $dataAnswerFactory,
        AnswerCollectionFactory $answerCollectionFactory,
        AnswerSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource                         = $resource;
        $this->answerFactory                    = $answerFactory;
        $this->answerCollectionFactory          = $answerCollectionFactory;
        $this->searchResultsFactory             = $searchResultsFactory;
        $this->dataObjectHelper                 = $dataObjectHelper;
        $this->dataAnswerFactory                = $dataAnswerFactory;
        $this->dataObjectProcessor              = $dataObjectProcessor;
        $this->storeManager                     = $storeManager;
        $this->collectionProcessor              = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter    = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        AnswerInterface $answer
    ) {

        $answerData = $this->extensibleDataObjectConverter->toNestedArray(
            $answer,
            [],
            AnswerInterface::class
        );

        $answerModel = $this->answerFactory->create()->setData($answerData);

        try {
            $this->resource->save($answerModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                                                'Could not save the answer: %1',
                                                $exception->getMessage()
                                            )
            );
        }
        return $answerModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($answerId)
    {
        $answer = $this->answerFactory->create();
        $this->resource->load($answer, $answerId);
        if (!$answer->getId()) {
            throw new NoSuchEntityException(__('Answer with id "%1" does not exist.', $answerId));
        }
        return $answer->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->answerCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            AnswerInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        AnswerInterface $answer
    ) {
        try {
            $answerModel = $this->answerFactory->create();
            $this->resource->load($answerModel, $answer->getAnswerId());
            $this->resource->delete($answerModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                                                  'Could not delete the Answer: %1',
                                                  $exception->getMessage()
                                              )
            );
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($answerId)
    {
        return $this->delete($this->get($answerId));
    }
}
