<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model;

use Chiaki\CustomerSizeQuestion\Api\Data\GalleryInterface;
use Chiaki\CustomerSizeQuestion\Api\Data\GalleryInterfaceFactory;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Gallery\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Gallery extends AbstractModel
{

    protected $galleryDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'chiaki_customersizequestion_gallery';

    /**
     * @param Context                 $context
     * @param Registry                $registry
     * @param GalleryInterfaceFactory $galleryDataFactory
     * @param DataObjectHelper        $dataObjectHelper
     * @param ResourceModel\Gallery   $resource
     * @param Collection              $resourceCollection
     * @param array                   $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        GalleryInterfaceFactory $galleryDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Gallery $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->galleryDataFactory = $galleryDataFactory;
        $this->dataObjectHelper   = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve gallery model with gallery data
     *
     * @return GalleryInterface
     */
    public function getDataModel()
    {
        $galleryData = $this->getData();

        $galleryDataObject = $this->galleryDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $galleryDataObject,
            $galleryData,
            GalleryInterface::class
        );

        return $galleryDataObject;
    }
}

