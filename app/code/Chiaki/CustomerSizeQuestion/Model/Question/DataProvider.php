<?php

declare(strict_types=1);

namespace Chiaki\CustomerSizeQuestion\Model\Question;

use Chiaki\CustomerSizeQuestion\Model\ImageProcessor;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Gallery\Collection as GalleryCollection;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\Collection;
use Chiaki\CustomerSizeQuestion\Model\ResourceModel\Question\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json as SerializerJson;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{

    protected $loadedData;
    protected $dataPersistor;

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var SerializerJson
     */
    protected $serializer;

    /**
     * @var ImageProcessor
     */
    protected $imageProcessor;

    /**
     * @var GalleryCollection
     */
    protected $galleryCollection;

    /**
     * DataProvider constructor.
     *
     * @param string                 $name
     * @param string                 $primaryFieldName
     * @param string                 $requestFieldName
     * @param CollectionFactory      $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param RequestInterface       $request
     * @param SerializerJson         $serializer
     * @param ImageProcessor         $imageProcessor
     * @param GalleryCollection      $galleryCollection
     * @param array                  $meta
     * @param array                  $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        RequestInterface $request,
        SerializerJson $serializer,
        ImageProcessor $imageProcessor,
        GalleryCollection $galleryCollection,
        array $meta = [],
        array $data = []
    ) {
        $this->collection        = $collectionFactory->create();
        $this->dataPersistor     = $dataPersistor;
        $this->request           = $request;
        $this->serializer        = $serializer;
        $this->imageProcessor    = $imageProcessor;
        $this->galleryCollection = $galleryCollection;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            if (is_string($model->getData('options'))) {
                $model->setData('options', $this->serializer->unserialize($model->getData('options')));
            }
            $galleryImages = $this->galleryCollection->getImagesByQuestion($model->getData('question_id'));
            if (!empty($galleryImages)) {
                $gallery_image = [];

                foreach ($galleryImages as $image) {
                    $imgName = $image->getData('image_name');
                    array_push(
                        $gallery_image,
                        [
                            'name' => $imgName,
                            'url'  => $this->imageProcessor->getImageUrl(
                                [ImageProcessor::CHIAKI_QUESTION_GALLERY_MEDIA_PATH, $model->getData('question_id'), $imgName]
                            )
                        ]
                    );
                }
                $model->setData('gallery_image', $gallery_image);
            }
            $this->loadedData[$model->getId()] = $model->getData();
        }
        $data = $this->dataPersistor->get('customer_size_question');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            if (is_string($model->getData('options'))) {
                $model->setData('options', $this->serializer->unserialize($model->getData('options')));
            }
            $galleryImages = $this->galleryCollection->getImagesByQuestion($model->getData('question_id'));
            if (!empty($galleryImages)) {
                $gallery_image = [];

                foreach ($galleryImages as $image) {
                    $imgName = $image->getData('image_name');
                    array_push(
                        $gallery_image,
                        [
                            'name' => $imgName,
                            'url'  => $this->imageProcessor->getImageUrl(
                                [ImageProcessor::CHIAKI_QUESTION_GALLERY_MEDIA_PATH, $model->getData('question_id'), $imgName]
                            )
                        ]
                    );
                }
                $model->setData('gallery_image', $gallery_image);
            }
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('customer_size_question');
        }

        return $this->loadedData;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getMeta()
    {
        $this->meta = parent::getMeta();

        $questionId = (int)$this->request->getParam('id');

        $this->meta['general']['children']['gallery']['arguments']['data']['config']['uploaderConfig']['url']
            = 'customersize/file/upload/type/gallery_image/id/' . $questionId;

        return $this->meta;
    }

}

