<?php

namespace Chiaki\CustomerSizeQuestion\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\File\Csv;
use Magento\Framework\Module\Dir\Reader;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * @var Reader
     */
    protected $_moduleReader;

    /**
     * @var Csv
     */
    protected $_csv;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param Reader  $moduleReader
     * @param Csv     $csv
     */
    public function __construct(
        Context $context,
        Reader $moduleReader,
        Csv $csv
    ) {
        parent::__construct($context);
        $this->_moduleReader = $moduleReader;
        $this->_csv          = $csv;
    }

    public function getSelectedQuestionSetId()
    {
        $selectedSet = $this->scopeConfig->getValue('customer_size_question/general/selected_question_set', ScopeInterface::SCOPE_STORE);
        return !empty($selectedSet) ? $selectedSet : 1;
    }

    public function getQuestionHeightId()
    {
        return $this->scopeConfig->getValue('customer_size_question/general/height_question', ScopeInterface::SCOPE_STORE);
    }

    public function getQuestionWeightId()
    {
        return $this->scopeConfig->getValue('customer_size_question/general/weight_question', ScopeInterface::SCOPE_STORE);
    }

    public function getQuestionCollarId()
    {
        return $this->scopeConfig->getValue('customer_size_question/general/collar_question', ScopeInterface::SCOPE_STORE);
    }

    public function getQuestionSleeveId()
    {
        return $this->scopeConfig->getValue('customer_size_question/general/sleeve_question', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSizeData()
    {
        $dir  = $this->_moduleReader->getModuleDir(
            "",
            "Chiaki_CustomerSizeQuestion"
        );
        $file = $dir . "/data/size_data.csv";
        return $this->_csv->getData($file);
    }
}
