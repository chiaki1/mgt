<?php

namespace Chiaki\Vnpay\Ui\Component\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

class RspCode extends Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                switch ($item[$this->getData('name')]) {
                    case "00":
                        $item['vnp_ResponseCode'] = "Giao dịch thành công.";
                        break;
                    case "01":
                        $item['vnp_ResponseCode'] = "Giao dịch đã tồn tại.";
                        break;
                    case "02":
                        $item['vnp_ResponseCode'] = "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)";
                        break;
                    case "03":
                        $item['vnp_ResponseCode'] = "Dữ liệu gửi sang không đúng định dạng";
                        break;
                    case "04":
                        $item['vnp_ResponseCode'] = "Khởi tạo GD không thành công do Website đang bị tạm khóa";
                        break;
                    case "05":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Quý khách nhập sai mật khẩu quá số lần quy định.
                Xin quý khách vui lòng thực hiện lại giao dịch";
                        break;
                    case "13":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP).
                Xin quý khách vui lòng thực hiện lại giao dịch.";
                        break;
                    case "07":
                        $item['vnp_ResponseCode'] = "Giao dịch bị nghi ngờ là giao dịch gian lận";
                        break;
                    case "09":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ
                InternetBanking tại ngân hàng.";
                        break;
                    case "10":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản
                không đúng quá 3 lần";
                        break;
                    case "11":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng
                thực hiện lại giao dịch.";
                        break;
                    case "12":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.";
                        break;
                    case "51":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư
                để thực hiện giao dịch.";
                        break;
                    case "65":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức
                giao dịch trong ngày.";
                        break;
                    case "08":
                        $item['vnp_ResponseCode'] = "Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì.
                Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này.";
                        break;
                    case "99":
                        $item['vnp_ResponseCode'] = "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)";
                        break;
                    default:
                        $item['vnp_ResponseCode'] = "Failured";
                }
            }
        }
        return $dataSource;
    }
}
