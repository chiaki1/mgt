<?php

namespace Chiaki\Vnpay\Cron;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;

class CancelOrder
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var FilterGroup
     */
    protected $filterGroup;

    /**
     * @var OrderManagementInterface
     */
    protected $orderManagement;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * CancelOrder constructor.
     *
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder    $searchCriteriaBuilder
     * @param FilterBuilder            $filterBuilder
     * @param FilterGroup              $filterGroup
     * @param OrderManagementInterface $orderManagement
     * @param ScopeConfigInterface     $scopeConfig
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroup $filterGroup,
        OrderManagementInterface $orderManagement,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->orderRepository       = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder         = $filterBuilder;
        $this->filterGroup           = $filterGroup;
        $this->orderManagement       = $orderManagement;
        $this->_scopeConfig          = $scopeConfig;
    }

    public function execute()
    {
        $filterGroupStatus = $this->filterGroup;
        $filterStatus      = $this->filterBuilder
            ->setField('status')
            ->setConditionType('eq')
            ->setValue('pending')
            ->create();
        $filterGroupStatus->setFilters([$filterStatus]);

        $searchCriteria = $this->searchCriteriaBuilder->setFilterGroups(
            [$filterGroupStatus]
        );
        $searchResults  = $this->orderRepository->getList($searchCriteria->create());

        /** @var Order $order */
        foreach ($searchResults->getItems() as $order) {
            if ($order->getPayment()->getMethod() == 'vnpay') {
                if ($this->isExpiry($order)) {
                    $this->orderManagement->cancel($order->getId());
                    $order->addStatusHistoryComment('Order Payment Expiry')
                          ->setIsCustomerNotified(true)->save();
                }
            }
        }
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isExpiry($order)
    {
        $today           = date("Y-m-d h:i:s");
        $expiry          = $this->_scopeConfig->getValue('payment/' . $order->getPayment()->getMethod() . '/expiry');
        $expiry          = $expiry ?: 1400;
        $to              = strtotime('-' . $expiry . ' min', strtotime($today));
        $order_create_at = strtotime($order->getCreatedAt());
        return $to > $order_create_at;
    }
}
