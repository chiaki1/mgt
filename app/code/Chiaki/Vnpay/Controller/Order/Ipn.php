<?php

namespace Chiaki\Vnpay\Controller\Order;

use Chiaki\Vnpay\Helper\Data;
use Chiaki\Vnpay\Logger\Logger;
use Chiaki\Vnpay\Model\TransactionFactory;
use Exception;
use Magento\Checkout\Model\SessionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;

class Ipn extends Action
{
    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @var  Order
     */
    protected $order;

    /**
     * @var  SessionFactory
     */
    protected $checkoutSession;

    /**
     * @var  ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var OrderManagementInterface
     */
    protected $_orderManagement;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * Ipn constructor.
     *
     * @param Context                  $context
     * @param JsonFactory              $resultJsonFactory
     * @param Order                    $order
     * @param SessionFactory           $checkoutSession
     * @param ScopeConfigInterface     $scopeConfig
     * @param Data                     $helper
     * @param Logger                   $logger
     * @param OrderManagementInterface $orderManagement
     * @param TransactionFactory       $transactionFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Order $order,
        SessionFactory $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        Data $helper,
        Logger $logger,
        OrderManagementInterface $orderManagement,
        TransactionFactory $transactionFactory
    ) {
        parent::__construct($context);
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->order              = $order;
        $this->checkoutSession    = $checkoutSession;
        $this->scopeConfig        = $scopeConfig;
        $this->_helper            = $helper;
        $this->_logger            = $logger;
        $this->_orderManagement   = $orderManagement;
        $this->transactionFactory = $transactionFactory;
    }

    public function execute()
    {
        /** @var Json $resultJson */
        $resultJson         = $this->_resultJsonFactory->create();
        $vnp_ResponseCode   = $this->getRequest()->getParam('vnp_ResponseCode', '');
        $vnp_SecureHashType = $this->getRequest()->getParam('vnp_SecureHashType', '');
        $vnp_SecureHash     = $this->getRequest()->getParam('vnp_SecureHash', '');
        $orderInfo          = $this->getRequest()->getParam('vnp_OrderInfo', '000000000');
        $vnp_TxnRef         = $this->getRequest()->getParam('vnp_TxnRef');
        $vnp_Amount         = $this->getRequest()->getParam('vnp_Amount');
        $vnp_HashSecret     = $this->_helper->getSecureHash(Info::PAYMENT_CODE);
        $responseParams     = $this->getRequest()->getParams();
        ksort($responseParams);
        $this->_logger->info('######## Response Data From Vnpay Via IPN Url For OrderId: ' . $orderInfo . ' ########');
        $this->_logger->info('######## List Response IPN Parameter: ########');
        $this->_helper->writeLog($responseParams);
        $this->_logger->info('##########################################');
        $hashData = "";
        $response = [];
        foreach ($responseParams as $key => $value) {
            if (
                $key != "vnp_SecureHashType" &&
                $key != "vnp_SecureHash" && strlen($value) > 0 &&
                (substr($key, 0, 4) == "vnp_")
            ) {
                $hashData .= $key . "=" . $value . "&";
            }
        }
        $hashData   = rtrim($hashData, "&");
        $secureHash = $this->getVnpSecureHash($vnp_HashSecret, $hashData, $vnp_SecureHashType);
        try {
            if ($secureHash == $vnp_SecureHash) {
                $orderInfo = explode(',', $orderInfo);
                foreach ($orderInfo as $incrementId) {
                    $order = $this->order->loadByIncrementId($incrementId);
                    if ($order->getId()) {
                        if ($order->getVnpayTnxRef() == $vnp_TxnRef) {
                            if ($order->getStatus() == 'pending') {
                                if ($vnp_ResponseCode == "00") {
                                    $this->_logger->info('Create invoice => started ...');
                                    // create processing status and invoice

                                    $this->_helper->invoiceOrder($order, $vnp_ResponseCode, $responseParams);

                                    $this->_logger->info(
                                        'Order status for: ' . $order->getIncrementId() . ' has been changed to processing'
                                    );
                                    $response['RspCode'] = '00';
                                    $response['Message'] = 'Confirm Success';
                                } elseif ($vnp_ResponseCode != "00") {
                                    $order = $this->order->loadByIncrementId($orderInfo);
                                    if ($order->getId()) {
                                        $this->cancelOrder($order, $vnp_ResponseCode);
                                        $response['RspCode'] = "00";
                                        $response['Message'] = "Confirm Success";
                                    }
                                }
                            } elseif ($order->getStatus() != 'pending') {
                                $response['RspCode'] = '02';
                                $response['Message'] = 'Order already confirmed';
                            }
                        } elseif ($order->getVnpayTnxRef() != $vnp_TxnRef) {
                            $response['RspCode'] = '01';
                            $response['Message'] = 'Vnpay transaction does not exist.';
                        }

                        if ($vnp_Amount != (int)$order->getGrandTotal() * 100) {
                            $response['RspCode'] = '04';
                            $response['Message'] = 'Invalid amount.';
                        }
                    } elseif (!$order->getId()) {
                        $response['RspCode'] = '01';
                        $response['Message'] = 'Order not found';
                    }
                }
            } elseif ($secureHash != $vnp_SecureHash) {
                $order = $this->order->loadByIncrementId($orderInfo);
                if ($order->getId()) {
                    $this->cancelOrder($order, $vnp_ResponseCode);
                }
                $response['RspCode'] = '97';
                $response['Message'] = 'Chu ky khong hop le';
            }
        } catch (Exception $e) {
            $response['RspCode'] = '99';
            $response['Message'] = 'Unknow error';
        }
        $this->saveTransaction($vnp_TxnRef, $responseParams, $response['Message']);

        $resultJson->setData($response);
        return $resultJson;
    }

    /**
     * @param $vnp_HashSecret
     * @param $hashdata
     * @param $vnp_SecureHashType
     *
     * @return string
     */
    protected function getVnpSecureHash($vnp_HashSecret, $hashdata, $vnp_SecureHashType)
    {
        if ($vnp_SecureHashType == "sha256" || $vnp_SecureHashType == "SHA256") {
            return hash('sha256', $vnp_HashSecret . $hashdata);
        }
        return md5($vnp_HashSecret . $hashdata);
        /** @codingStandardsIgnoreLine */
    }

    /**
     * @param Order $order
     * @param       $responseCode
     *
     * @throws Exception
     */
    public function cancelOrder($order, $responseCode)
    {
        $this->_logger->info('Cancel order for: ' . $order->getIncrementId() . ' Proceed...');
        $this->_orderManagement->cancel($order->getId());
        $order->addStatusHistoryComment($this->_helper->getResponseDescriptionPay($responseCode))
              ->setIsCustomerNotified(true)->save();
        $this->_logger->info('Cancel order for: ' . $order->getIncrementId() . ' Done');
    }

    /**
     * @param       $vnp_TxnRef
     * @param array $additionalData
     * @param       $message
     */
    public function saveTransaction($vnp_TxnRef, array $additionalData, $message)
    {
        try {
            $tranFactory = $this->transactionFactory->create();
            $tranFactory->addData(
                [
                    'vnpay_tnx_ref'          => $vnp_TxnRef,
                    'additional_information' => json_encode($additionalData),
                    'message'                => $message
                ]
            );
            $tranFactory->save();
        } catch (\Exception $e) {
            $this->_logger->info($e->getMessage());
        }
    }
}
