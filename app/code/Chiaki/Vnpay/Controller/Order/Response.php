<?php

namespace Chiaki\Vnpay\Controller\Order;

use Chiaki\Vnpay\Helper\Data;
use Chiaki\Vnpay\Logger\Logger;
use Magento\Checkout\Model\SessionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;

/**
 * Class Response
 *
 * @package Chiaki\Vnpay\Controller\Order
 */
class Response extends Action
{
    /**
     * @var  Order
     */
    protected $order;

    /**
     * @var  SessionFactory
     */
    protected $checkoutSession;

    /**
     * @var  ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var OrderManagementInterface
     */
    protected $_orderManagement;

    public function __construct(
        Context $context,
        Order $order,
        SessionFactory $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        Data $helper,
        Logger $logger,
        OrderManagementInterface $orderManagement
    ) {
        parent::__construct($context);
        $this->order              = $order;
        $this->checkoutSession    = $checkoutSession;
        $this->scopeConfig        = $scopeConfig;
        $this->_helper            = $helper;
        $this->_logger            = $logger;
        $this->_orderManagement   = $orderManagement;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $vnp_ResponseCode   = $this->getRequest()->getParam('vnp_ResponseCode', '');
        $vnp_SecureHashType = $this->getRequest()->getParam('vnp_SecureHashType', '');
        $vnp_SecureHash     = $this->getRequest()->getParam('vnp_SecureHash', '');
        $orderInfo          = $this->getRequest()->getParam('vnp_OrderInfo', '000000000');
        $vnp_Amount         = $this->getRequest()->getParam('vnp_Amount');
        $vnp_HashSecret     = $this->_helper->getSecureHash(Info::PAYMENT_CODE);
        $responseParams     = $this->getRequest()->getParams();
        ksort($responseParams);
        $this->_logger->info('######## Response Data From Vnpay For OrderId: ' . $orderInfo . ' ########');
        $this->_logger->info('######## List Response Parameter: ########');
        $this->_helper->writeLog($responseParams);
        $this->_logger->info('##########################################');
        $hashData = "";
        foreach ($responseParams as $key => $value) {
            if (
                $key != "vnp_SecureHashType" &&
                $key != "vnp_SecureHash" && strlen($value) > 0 &&
                (substr($key, 0, 4) == "vnp_")
            ) {
                $hashData .= $key . "=" . $value . "&";
            }
        }
        $hashData   = rtrim($hashData, "&");
        $secureHash = $this->getVnpSecureHash($vnp_HashSecret, $hashData, $vnp_SecureHashType);
        $order      = $this->order->loadByIncrementId($orderInfo);
        if ($order->getId()) {
            if ($secureHash == $vnp_SecureHash) {
                if ($vnp_ResponseCode == "00") {
                    if ($vnp_Amount != $order->getGrandTotal() * 100) {
                        $this->messageManager->addError(__("The return amount is not valid."));
                        return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
                    }
                    if ($this->_helper->getNotIpnMode(Info::PAYMENT_CODE) && !$this->_helper->isProduction(Info::PAYMENT_CODE)) {
                        $this->_helper->invoiceOrder($order, $vnp_ResponseCode, $responseParams);
                    }
                    $this->messageManager->addSuccess(__($this->_helper->getResponseDescriptionPay($vnp_ResponseCode)));
                    return $this->resultRedirectFactory->create()->setPath('checkout/onepage/success');
                } elseif ($vnp_ResponseCode != "00") {
                    $this->messageManager->addError(
                        $this->_helper->getResponseDescriptionPay($vnp_ResponseCode)
                    );
                    return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
                }
            } elseif ($secureHash != $vnp_SecureHash) {
                $this->messageManager->addError(__('Invalid return data'));
                return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
            }
        } elseif (!$order->getId()) {
            $this->messageManager->addError(__("Order does not exist."));
            return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
        }
    }

    /**
     * @param $vnp_HashSecret
     * @param $hashdata
     * @param $vnp_SecureHashType
     *
     * @return string
     */
    protected function getVnpSecureHash($vnp_HashSecret, $hashdata, $vnp_SecureHashType)
    {
        if ($vnp_SecureHashType == "sha256" || $vnp_SecureHashType == "SHA256") {
            return hash('sha256', $vnp_HashSecret . $hashdata);
        }
        return md5($vnp_HashSecret . $hashdata);
        /** @codingStandardsIgnoreLine */
    }
}
