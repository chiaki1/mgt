<?php

namespace Chiaki\Vnpay\Controller\Order;

use Chiaki\Vnpay\Helper\Data;
use Chiaki\Vnpay\Logger\Logger;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;

class Info extends Action
{
    const PAYMENT_CODE = 'vnpay';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Json
     */
    protected $jsonFac;

    /**
     * @var  Order
     */
    protected $order;

    /**
     * @var ScopeConfigInterface $scopeConfig
     */
    protected $scopeConfig;

    /**
     * @var  StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var  Session
     */
    protected $checkoutSession;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * Info constructor.
     *
     * @param Context               $context
     * @param PageFactory           $resultPageFactory
     * @param Json                  $json
     * @param Order                 $order
     * @param ScopeConfigInterface  $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param Session               $checkoutSession
     * @param Logger                $logger
     * @param Data                  $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Json $json,
        Order $order,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        Session $checkoutSession,
        Logger $logger,
        Data $helper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonFac           = $json;
        $this->order             = $order;
        $this->scopeConfig       = $scopeConfig;
        $this->storeManager      = $storeManager;
        $this->checkoutSession   = $checkoutSession;
        $this->_helper           = $helper;
        $this->logger            = $logger;
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $id    = $this->getRequest()->getParam('order_id');
        $id    = $id ? $id : $this->checkoutSession->getLastOrderId();
        $order = $this->order->load((int)$id);
        if ($order->getId()) {
            $incrementID = $order->getIncrementId();

            $returnUrl = $this->storeManager->getStore()->getBaseUrl();
            $returnUrl = rtrim($returnUrl, "/");
            $returnUrl .= "/vnpay/order/response";

            $inputData = [
                "vnp_Version"    => $this->_helper->getVnpVersion(),
                "vnp_TmnCode"    => $this->_helper->getMerchantId(self::PAYMENT_CODE),
                "vnp_Amount"     => round($order->getTotalDue() * 100, 0),
                "vnp_Command"    => "pay",
                "vnp_CreateDate" => date('YmdHis'),
                "vnp_CurrCode"   => $this->_helper->getCurrencyCode(),
                "vnp_IpAddr"     => $order->getRemoteIp(),
                "vnp_Locale"     => 'vn',
                "vnp_OrderInfo"  => $incrementID,
                "vnp_OrderType"  => 'other',
                "vnp_ReturnUrl"  => $returnUrl,
                "vnp_TxnRef"     => $this->_helper->getTxnRef(self::PAYMENT_CODE),
            ];
            ksort($inputData);
            $this->logger->info('######## Request Data To Vnpay For OrderId: ' . $incrementID . ' ########');
            $this->logger->info('######## List Request Parameter: ########');
            $this->_helper->writeLog($inputData);
            $this->logger->info('############################################');
            $url = $this->_helper->getCallUrl(self::PAYMENT_CODE, $inputData);
            $this->_helper->saveVnpayTnxRef($order, $inputData["vnp_TxnRef"]);
            $this->jsonFac->setData($url);
        }
        return $this->jsonFac;
    }
}
