<?php

namespace Chiaki\Vnpay\Model\Payment;

use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Quote\Api\Data\CartInterface;

class Vnpay extends AbstractMethod
{

    protected $_code      = "vnpay";
    protected $_isOffline = true;

    public function isAvailable(
        CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}
