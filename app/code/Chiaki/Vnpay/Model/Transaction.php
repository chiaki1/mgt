<?php

namespace Chiaki\Vnpay\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Transaction extends AbstractModel
{

    const CACHE_TAG = 'vnpay_transaction';

    protected $_cacheTag = 'vnpay_transaction';

    protected $_eventPrefix = 'vnpay_transaction';

    function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Chiaki\Vnpay\Model\ResourceModel\Transaction');
    }
}
