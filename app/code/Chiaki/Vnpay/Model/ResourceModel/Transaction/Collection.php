<?php

namespace Chiaki\Vnpay\Model\ResourceModel\Transaction;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Chiaki\Vnpay\Model\Transaction', 'Chiaki\Vnpay\Model\ResourceModel\Transaction');

    }
}
