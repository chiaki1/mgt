<?php

namespace Chiaki\Vnpay\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Transaction extends AbstractDb
{

    protected $_blockProductTable;

    public function __construct(
        Context $context,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
    }

    protected function _construct()
    {
        $this->_init('vnpay_transaction', 'id');
    }
}
