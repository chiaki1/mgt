<?php

namespace Chiaki\Vnpay\Helper;

use Chiaki\Vnpay\Logger\Logger;
use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    const SANDBOX_URL    = 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html';
    const PRODUCTION_URL = 'https://pay.vnpay.vn/vpcpay.html';
    const VNP_VERSION    = '2.0.0';

    /** @var ScopeConfigInterface */
    protected $_scopeConfig;
    /** @var StoreManagerInterface */
    protected $_storeManager;
    /** @var DateTime */
    protected $_date;
    /** @var Logger */
    protected $logger;
    /** @var InvoiceService */
    protected $_invoiceService;
    /** @var InvoiceSender */
    protected $_invoiceSender;
    /** @var BuilderInterface */
    protected $_builder;
    /** @var ObjectManagerInterface */
    protected $_objectManager;

    /**
     * Data constructor.
     *
     * @param Context                $context
     * @param ScopeConfigInterface   $scopeConfig
     * @param StoreManagerInterface  $storeManager
     * @param DateTime               $date
     * @param Logger                 $logger
     * @param InvoiceService         $invoiceService
     * @param InvoiceSender          $invoiceSender
     * @param BuilderInterface       $builder
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        DateTime $date,
        Logger $logger,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        BuilderInterface $builder,
        ObjectManagerInterface $objectManager
    ) {
        $this->_scopeConfig    = $scopeConfig;
        $this->_storeManager   = $storeManager;
        $this->_date           = $date;
        $this->logger          = $logger;
        $this->_invoiceService = $invoiceService;
        $this->_invoiceSender  = $invoiceSender;
        $this->_builder        = $builder;
        $this->_objectManager  = $objectManager;
        parent::__construct($context);
    }

    /**
     * Check is production mode
     *
     * @param $method
     *
     * @return mixed
     */
    public function isProduction($method)
    {
        return $this->_scopeConfig->getValue(
            'payment/' . $method . '/is_production',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get Merchant Id
     *
     * @param $method
     *
     * @return mixed
     */
    public function getMerchantId($method)
    {
        return $this->_scopeConfig->getValue(
            'payment/' . $method . '/merchant_id',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get Secure Hash
     *
     * @param $method
     *
     * @return mixed
     */
    public function getSecureHash($method)
    {
        return $this->_scopeConfig->getValue(
            'payment/' . $method . '/secure_hash',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get Secure Hash
     *
     * @param $method
     *
     * @return mixed
     */
    public function getNotIpnMode($method)
    {
        return $this->_scopeConfig->getValue(
            'payment/' . $method . '/not_ipn_mode',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * get Url Payment Domestic
     *
     * @param $method
     *
     * @return string
     */
    public function getPaymentUrl($method)
    {
        return $this->isProduction($method) ? self::PRODUCTION_URL : self::SANDBOX_URL;
    }

    /**
     * get vpc_version
     *
     * @return string
     */
    public function getVnpVersion()
    {
        return self::VNP_VERSION;
    }

    /**
     * get currency code
     */
    public function getCurrencyCode()
    {
        return "VND";
    }

    /**
     * @param $method
     *
     * @return string
     */
    public function getTxnRef($method)
    {
        $prefix = $this->_scopeConfig->getValue(
            'payment/' . $method . '/prefix_transaction_code',
            ScopeInterface::SCOPE_STORE
        );
        return $prefix . rand(111111111, 999999999) . $this->_date->timestamp();
    }

    /**
     * @param $method
     * @param $params
     *
     * @return string
     */
    public function getCallUrl($method, $params)
    {
        $vnp_HashSecret = $this->getSecureHash($method);
        $vnp_Url        = $this->getPaymentUrl($method) . '?';
        $hashdata       = "";

        foreach ($params as $key => $value) {
            $vnp_Url .= urlencode($key) . "=" . urlencode($value) . "&";
            if ((strlen($value) > 0) && ((substr($key, 0, 4) == "vnp_"))) {
                $hashdata .= $key . "=" . $value . "&";
            }
        }
        $hashdata = rtrim($hashdata, "&");

        if (isset($vnp_HashSecret)) {
            $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
            $vnp_Url       .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
        return $vnp_Url;
    }

    /**
     * @param $params
     */
    public function writeLog($params)
    {
        if (isset($params)) {
            foreach ($params as $key => $value) {
                $this->logger->info($key . ':' . $value);
            }
        }
    }

    /**
     * @param Order $order
     * @param       $vnp_ResponseCode
     *
     * @throws LocalizedException
     * @throws Exception
     */
    public function invoiceOrder($order, $vnp_ResponseCode, $responseData)
    {
        if ($order->canInvoice() && !$order->hasInvoices()) {
            $this->_logger->info('Create invoice => Proceed...');
            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->pay();
            $invoice->save();
            $invoice->getOrder()->setIsInProcess(true);
            $transactionSave = $this->_objectManager->create(
                'Magento\Framework\DB\Transaction'
            )->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $transactionSave->save();

            $this->_logger->info('Step transactionSave => done');

            $payment = $order->getPayment();
            $payment->setLastTransactionId($order->getIncrementId());
            $payment->setTransactionId($order->getIncrementId());
            $payment->setAdditionalInformation([]);
            $message     = __('Payment successfully.');
            $trans       = $this->_builder;
            $transaction = $trans->setPayment($payment)
                                 ->setOrder($order)
                                 ->setTransactionId($responseData['vnp_TransactionNo'])
                                 ->setAdditionalInformation([Transaction::RAW_DETAILS => $responseData])
                                 ->setFailSafe(true)
                                 ->build(Transaction::TYPE_CAPTURE);
            $payment->addTransactionCommentsToOrder($transaction, $message);
            $payment->save();
            $transaction->save();
            $order->setStatus('waiting_to_pickup');
            $order->save();
            $this->_logger->info('Step paymentSave => done');

            if (!$invoice->getEmailSent()) {
                $invoiceSender = $this->_invoiceSender;
                $invoiceSender->send($invoice);
                $order->addRelatedObject($invoice);
                $order->addStatusHistoryComment(__('Vnpay message: ' .
                                                   $this->getResponseDescriptionPay($vnp_ResponseCode) .
                                                   '. Your Invoice for Order ID #%1.', $order->getIncrementId()
                                                )
                )
                      ->setIsCustomerNotified(true);
                $this->_logger->info('Step email sent => done');
            }

            $this->_logger->info('Create invoice process => done');
        } elseif (!$order->canInvoice() || $order->hasInvoices()) {
            $this->_logger->info(
                'Invoice did not created: canInvoice status (' . $order->canInvoice() .
                ') - hasInvoices status (' . $order->hasInvoices() . ')'
            );
        }
    }

    /**
     * @param $responseCode
     *
     * @return string
     */
    public function getResponseDescriptionPay($responseCode)
    {
        switch ($responseCode) {
            case "00":
                $result = "Giao dịch thành công.";
                break;
            case "01":
                $result = "Giao dịch đã tồn tại.";
                break;
            case "02":
                $result = "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)";
                break;
            case "03":
                $result = "Dữ liệu gửi sang không đúng định dạng";
                break;
            case "04":
                $result = "Khởi tạo GD không thành công do Website đang bị tạm khóa";
                break;
            case "05":
                $result = "Giao dịch không thành công do: Quý khách nhập sai mật khẩu quá số lần quy định.
                Xin quý khách vui lòng thực hiện lại giao dịch";
                break;
            case "13":
                $result = "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP).
                Xin quý khách vui lòng thực hiện lại giao dịch.";
                break;
            case "07":
                $result = "Giao dịch bị nghi ngờ là giao dịch gian lận";
                break;
            case "09":
                $result = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ
                InternetBanking tại ngân hàng.";
                break;
            case "10":
                $result = "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản
                không đúng quá 3 lần";
                break;
            case "11":
                $result = "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng
                thực hiện lại giao dịch.";
                break;
            case "12":
                $result = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.";
                break;
            case "51":
                $result = "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư
                để thực hiện giao dịch.";
                break;
            case "65":
                $result = "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức
                giao dịch trong ngày.";
                break;
            case "08":
                $result = "Giao dịch không thành công do: Hệ thống Ngân hàng đang bảo trì.
                Xin quý khách tạm thời không thực hiện giao dịch bằng thẻ/tài khoản của Ngân hàng này.";
                break;
            case "99":
                $result = "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)";
                break;
            default:
                $result = "Failured";
        }
        return $result;
    }

    /**
     * @param $responseCode
     *
     * @return string
     */
    public function getResponseDescriptionLookUpTransactions($responseCode)
    {
        switch ($responseCode) {
            case "91":
                $result = "Không tìm thấy giao dịch yêu cầu.";
                break;
            case "02":
                $result = "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)";
                break;
            case "03":
                $result = "Dữ liệu gửi sang không đúng định dạng";
                break;
            case "08":
                $result = "Hệ thống đang bảo trì";
                break;
            case "97":
                $result = "Chữ ký không hợp lệ";
                break;
            case "99":
                $result = "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)";
                break;
            default:
                $result = "Failured";
        }
        return $result;
    }

    /**
     * @param $responseCode
     *
     * @return string
     */
    public function getResponseDescriptionRefund($responseCode)
    {
        switch ($responseCode) {
            case "91":
                $result = "Không tìm thấy giao dịch yêu cầu hoàn trả.";
                break;
            case "02":
                $result = "Merchant không hợp lệ (kiểm tra lại vnp_TmnCode)";
                break;
            case "03":
                $result = "Dữ liệu gửi sang không đúng định dạng";
                break;
            case "08":
                $result = "Hệ thống đang bảo trì";
                break;
            case "93":
                $result = "Số tiền hoàn trả không hợp lệ. Số tiền hoàn trả phải nhỏ hơn hoặc bằng
                số tiền thanh toán.";
                break;
            case "94":
                $result = "Giao dịch đã được gửi yêu cầu hoàn tiền trước đó. Yêu cầu này VNPAY đang xử lý";
                break;
            case "95":
                $result = "Giao dịch này không thành công bên VNPAY. VNPAY từ chối xử lý yêu cầu.";
                break;
            case "97":
                $result = "Chữ ký không hợp lệ";
                break;
            case "99":
                $result = "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)";
                break;
            default:
                $result = "Failured";
        }
        return $result;
    }

    /**
     * @param Order $order
     * @param       $tnxRef
     */
    public function saveVnpayTnxRef($order, $tnxRef)
    {
        $order->setVnpayTnxRef($tnxRef)->save();
    }
}
