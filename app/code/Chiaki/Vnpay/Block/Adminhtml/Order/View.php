<?php

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Vnpay\Block\Adminhtml\Order;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;
use Magento\Sales\Block\Adminhtml\Order\View as OrderView;
use Magento\Sales\Helper\Reorder;
use Magento\Sales\Model\ConfigInterface;
use Magento\Sales\Model\Order\Payment\TransactionFactory;

/**
 * Adminhtml sales order view.
 *
 * @api
 * @since 100.2.2
 */
class View extends OrderView
{
    /**
     * @var TransactionFactory
     */
    protected $_transactionFactory;

    /**
     * @param Context            $context
     * @param Registry           $registry
     * @param ConfigInterface    $salesConfig
     * @param Reorder            $reorderHelper
     * @param TransactionFactory $transactionFactory
     * @param array              $data
     */
    public function __construct(
        Context            $context,
        Registry           $registry,
        ConfigInterface    $salesConfig,
        Reorder            $reorderHelper,
        TransactionFactory $transactionFactory,
        array              $data = []
    ) {
        $this->_transactionFactory = $transactionFactory;
        parent::__construct($context, $registry, $salesConfig, $reorderHelper, $data);
    }

    public function getTransactionUrl()
    {
        if ($this->getOrder()->getPayment()->getMethod() == 'vnpay') {
            $transactionId = $this->getOrder()->getPayment()->getLastTransId();
            $transaction   = $this->_transactionFactory->create()->load($transactionId, 'txn_id');
            if ($transactionId) {
                return $this->getUrl('sales/transactions/view', ['txn_id' => $transaction->getId()]);
            }
        }
        return false;
    }
}
