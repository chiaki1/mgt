<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Chiaki\Sales\Controller\Adminhtml\Order;

use Exception;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\OrderFactory;

class PrepareStatus extends \Magento\Backend\App\Action
{
    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * Complete constructor.
     *
     * @param Action\Context  $context
     * @param OrderFactory    $orderFactory
     * @param RedirectFactory $resultRedirectFactory
     */
    public function __construct(
        Action\Context $context,
        OrderFactory $orderFactory,
        RedirectFactory $resultRedirectFactory
    ) {
        $this->orderFactory          = $orderFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $orderId        = $this->getRequest()->getParam('order_id');
        $status         = $this->getRequest()->getParam('status');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($orderId) {
            $order = $this->orderFactory->create()->load($orderId);
            if ($order->getId()) {
                try {
                    if ($status == 'complete') {
                        if ($order->getState() == "complete" && $order->getStatus() == "prepared") {
                            $order->setStatus($status)->save();
                            $this->messageManager->addSuccessMessage(__("Update Order Status Success."));
                        } else {
                            $this->messageManager->addNoticeMessage(__("Can not update order status now."));
                        }
                    } else {
                        $order->setStatus($status)->save();
                        $this->messageManager->addSuccessMessage(__("Update Order Status Success."));
                    }
                } catch (Exception $exception) {
                    $this->messageManager->addErrorMessage(__($exception->getMessage()));
                }
                return $resultRedirect->setPath('sales/order/view', ['order_id' => $order->getId()]);
            }
        } else {
            $this->messageManager->addErrorMessage(__("Order is not exist."));
        }
        return $resultRedirect->setPath('sales/*/');
    }
}
