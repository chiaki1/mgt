<?php

namespace Chiaki\Sales\Plugin;

use Magento\Ui\Component\ExportButton;

class AddXlsxType
{

    /**
     * @param ExportButton $subject
     */
    public function beforePrepare(ExportButton $subject)
    {
        $config = $subject->getData('config');
        if (isset($config['options'])) {
            $config['options']['xlsx'] = [
                'value' => 'xlsx',
                'label' => 'Xlsx',
                'url'   => 'mui/export/gridToXlsx'
            ];
        }
        $subject->setData('config', $config);
    }
}
