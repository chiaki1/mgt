<?php

namespace Chiaki\Sales\Plugin\Block\Adminhtml\Order;

use Magento\Framework\UrlInterface;

class View
{
    /**
     * Url Builder
     *
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * View constructor.
     *
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * @param \Magento\Sales\Block\Adminhtml\Order\View $subject
     */
    public function beforeGetOrderId(\Magento\Sales\Block\Adminhtml\Order\View $subject)
    {
        if (!$subject->getOrder()->hasShipments() && !$subject->getOrder()->hasInvoices() && $subject->getOrder()->getStatus() == 'pending') {
            $subject->addButton(
                'confirm',
                ['label' => __('Confirm'), 'onclick' => 'setLocation(\'' . $this->getCompleteOrderUrl($subject->getOrder()->getId(), 'waiting_to_pickup') . '\')', 'class' => 'confirm'],
                -1
            );
        }
        if (!$subject->getOrder()->hasShipments() && !$subject->getOrder()->hasInvoices() && $subject->getOrder()->getStatus() == 'waiting_to_pickup') {
            $subject->addButton(
                'pickup',
                ['label' => __('Pickup'), 'onclick' => 'setLocation(\'' . $this->getCompleteOrderUrl($subject->getOrder()->getId(), 'delivery') . '\')', 'class' => 'pickup'],
                -1
            );
        }
        if (!$subject->getOrder()->hasShipments() && $subject->getOrder()->hasInvoices() && $subject->getOrder()->getStatus() == 'waiting_to_pickup') {
            $subject->addButton(
                'pickup',
                ['label' => __('Pickup'), 'onclick' => 'setLocation(\'' . $this->getCompleteOrderUrl($subject->getOrder()->getId(), 'delivery') . '\')', 'class' => 'pickup'],
                -1
            );
        }
        if ($subject->getOrder()->getState() == "complete" && $subject->getOrder()->getStatus() == "prepared") {
            $subject->addButton(
                'complete',
                ['label' => __('Complete Order'), 'onclick' => 'setLocation(\'' . $this->getCompleteOrderUrl($subject->getOrder()->getId(), 'complete') . '\')', 'class' => 'complete'],
                -1
            );
        }
    }

    /**
     * @param $orderId
     *
     * @param $status
     *
     * @return string
     */
    public function getCompleteOrderUrl($orderId, $status)
    {
        return $this->getUrl('sales/order/prepareStatus', ['order_id' => $orderId, 'status' => $status]);
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array  $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }
}
