<?php

namespace Chiaki\Sales\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class OrderInfo extends Column
{
    const URL_PATH_DETAILS = 'sales/order/view';
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * OrderInfo constructor.
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param OrderFactory       $orderFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        OrderFactory $orderFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder   = $urlBuilder;
        $this->orderFactory = $orderFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['main_order']) && $orderId = $this->getOrderId($item['main_order'])) {
                    $item[$this->getData('name')] = [
                        'details' => [
                            'href'  => $this->urlBuilder->getUrl(
                                static::URL_PATH_DETAILS,
                                [
                                    'order_id' => $orderId
                                ]
                            ),
                            'label' => '#' . $item['main_order']
                        ],
                    ];
                }
            }
        }

        return $dataSource;
    }

    public function getOrderId($incrementId)
    {
        $order = $this->orderFactory->create()->loadByIncrementId($incrementId);
        return $order->getId();
    }
}
