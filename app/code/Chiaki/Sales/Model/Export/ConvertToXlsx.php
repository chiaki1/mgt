<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Chiaki\Sales\Model\Export;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Ui\Model\Export\MetadataProvider;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Class ConvertToXlsx
 *
 * @package Chiaki\Sales\Model\Export
 */
class ConvertToXlsx
{
    /**
     * @var DirectoryList
     */
    protected $directory;

    /**
     * @var MetadataProvider
     */
    protected $metadataProvider;

    /**
     * @var int|null
     */
    protected $pageSize = null;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @param Filesystem $filesystem
     * @param Filter $filter
     * @param MetadataProvider $metadataProvider
     * @param int $pageSize
     * @throws FileSystemException
     */
    public function __construct(
        Filesystem $filesystem,
        Filter $filter,
        MetadataProvider $metadataProvider,
        $pageSize = 200
    ) {
        $this->filter = $filter;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->metadataProvider = $metadataProvider;
        $this->pageSize = $pageSize;
    }

    /**
     * Returns CSV file
     *
     * @return array
     * @throws LocalizedException
     */
    public function getXlsxFile()
    {
        $component = $this->filter->getComponent();

        $this->filter->prepareComponent($component);
        $this->filter->applySelectionOnTargetProvider();
        $dataProvider = $component->getContext()->getDataProvider();
        $itemsData = [];
        $i = 1;
        $searchCriteria = $dataProvider->getSearchCriteria()
            ->setCurrentPage($i)
            ->setPageSize($this->pageSize);
        $totalCount = (int) $dataProvider->getSearchResult()->getTotalCount();
        while ($totalCount > 0) {
            $items = $dataProvider->getSearchResult()->getItems();
            foreach ($items as $item) {
                $this->metadataProvider->convertDate($item, $component->getName());
                $itemsData[] = $item->getData();
            }
            $searchCriteria->setCurrentPage(++$i);
            $totalCount = $totalCount - $this->pageSize;
        }

        $excelDataArray = [];
        $sheetTitle = 'export';
        $headersArray = $this->metadataProvider->getHeaders($component);
        $fieldsArray = $this->metadataProvider->getFields($component);
        if(($key = array_search('actions', $fieldsArray)) !== false) {
            unset($fieldsArray[$key]);
        }

        $hPos = 0;
        foreach($headersArray as $key =>$value){
            $excelDataArray[$hPos][$key] = $value;
        }

        $dPos = 1;
        if(count($itemsData) > 1){
            foreach($itemsData as $itemData){
                foreach($fieldsArray as $key=>$value){
                    $excelDataArray[$dPos][$key] = isset($itemData[$value]) ? $itemData[$value] : '';
                }
                $dPos++;
            }
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->fromArray($excelDataArray, null, 'A1' );

        $sheet->setTitle($sheetTitle);
        $spreadsheet->setActiveSheetIndex(0);

        $name = md5(microtime());
        $file = '/export/'. $component->getName() . $name . '.xlsx';
        $this->directory->create('export');
        $filepath = $this->directory->getAbsolutePath($file);
        $writer = new Xlsx($spreadsheet);
        $writer->save($filepath);

        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true  // can delete file after use
        ];
    }
}
