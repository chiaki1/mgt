<?php

namespace Chiaki\Sales\Model\ResourceModel\Order\Item\Grid;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;
use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;
use Magento\Framework\Event\ManagerInterface as EventManager;
use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;
use Magento\Sales\Model\Order\Item as ItemOrder;
use Magento\Sales\Model\ResourceModel\Order\Item;
use Magento\Ui\DataProvider\AddFilterToCollectionInterface;
use Psr\Log\LoggerInterface as Logger;
use Magento\Framework\Module\Manager;

class Collection extends SearchResult
{
    private $eavAttribute;

    protected $itemOrder;

    protected $moduleManager;

    /**
     * @var AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    protected $_map
        = [
            'fields' => [
                'main_order'        => 'so.increment_id',
                'order_date'        => 'so.created_at',
                'status_main_order' => 'so.status',
                'product_sku'       => 'cpe.sku',
                'product_name'      => 'cpev.value'
            ]
        ];

    public function __construct(
        Attribute $eavAttribute,
        ItemOrder $itemOrder,
        EntityFactory $entityFactory,
        Logger $logger,
        FetchStrategy $fetchStrategy,
        EventManager $eventManager,
        Manager $moduleManager,
        array $addFilterStrategies = [],
        $mainTable = 'sales_order_item',
        $resourceModel = Item::class,
        $identifierName = null,
        $connectionName = null
    ) {
        $this->addFilterStrategies = $addFilterStrategies;
        $this->itemOrder           = $itemOrder;
        $this->eavAttribute        = $eavAttribute;
        $this->moduleManager       = $moduleManager;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel, $identifierName, $connectionName);
    }

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->getSelect()
             ->joinLeft(
                 ['so' => $this->getTable('sales_order')],
                 'main_table.order_id = so.entity_id',
                 ['main_order' => 'so.increment_id', 'order_date' => 'so.created_at', 'status_main_order' => 'so.status']
             )->joinLeft(
                ['cpe' => $this->getTable('catalog_product_entity')],
                'main_table.product_id = cpe.entity_id',
                ['product_sku' => 'cpe.sku']
            )->joinLeft(
                ['cpev' => $this->getTable('catalog_product_entity_varchar')],
                "cpe.row_id = cpev.row_id AND cpev.attribute_id = {$this->getBarcodeAttributeId()}",
                ['barcode' => 'cpev.value']
            );
        if ($this->moduleManager->isOutputEnabled('BoostMyShop_AdvancedStock')) {
            $this->getSelect()->joinLeft(
                ['oiw' => $this->getTable('bms_advancedstock_extended_sales_flat_order_item')],
                'main_table.item_id = oiw.esfoi_order_item_id',
                ['warehouse_id' => 'oiw.esfoi_warehouse_id']
            );
        }

        $this->getSelect()->where('main_table.parent_item_id IS NULL')
             ->group('main_table.item_id');

        return $this;
    }

    private function getBarcodeAttributeId()
    {
        return $this->getProductAttribute('barcode');
    }

    private function getProductAttribute($code)
    {
        return $this->eavAttribute->getIdByCode(Product::ENTITY, $code);
    }
}
