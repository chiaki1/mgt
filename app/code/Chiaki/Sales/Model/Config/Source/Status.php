<?php

namespace Chiaki\Sales\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;

class Status implements ArrayInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
    }

    public function toOptionArray()
    {
        $statuses = [];

        foreach ($this->_collectionFactory->create() as $item) {
            $statuses[] = array('value' => $item->getStatus(), 'label' => $item->getLabel());
        }

        return $statuses;
    }
}
